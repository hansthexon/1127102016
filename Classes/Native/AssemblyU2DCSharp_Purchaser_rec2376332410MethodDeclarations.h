﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Purchaser/rec
struct rec_t2376332410;

#include "codegen/il2cpp-codegen.h"

// System.Void Purchaser/rec::.ctor()
extern "C"  void rec__ctor_m2854104741 (rec_t2376332410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
