﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PolitePanelScript1/<disabelle>c__Iterator3
struct U3CdisabelleU3Ec__Iterator3_t1202804707;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PolitePanelScript1/<disabelle>c__Iterator3::.ctor()
extern "C"  void U3CdisabelleU3Ec__Iterator3__ctor_m1617355108 (U3CdisabelleU3Ec__Iterator3_t1202804707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PolitePanelScript1/<disabelle>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdisabelleU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2096059720 (U3CdisabelleU3Ec__Iterator3_t1202804707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PolitePanelScript1/<disabelle>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdisabelleU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1240336960 (U3CdisabelleU3Ec__Iterator3_t1202804707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PolitePanelScript1/<disabelle>c__Iterator3::MoveNext()
extern "C"  bool U3CdisabelleU3Ec__Iterator3_MoveNext_m455461488 (U3CdisabelleU3Ec__Iterator3_t1202804707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1/<disabelle>c__Iterator3::Dispose()
extern "C"  void U3CdisabelleU3Ec__Iterator3_Dispose_m1667289657 (U3CdisabelleU3Ec__Iterator3_t1202804707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1/<disabelle>c__Iterator3::Reset()
extern "C"  void U3CdisabelleU3Ec__Iterator3_Reset_m1434748531 (U3CdisabelleU3Ec__Iterator3_t1202804707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
