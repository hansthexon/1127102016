﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// leftoright
struct leftoright_t233454036;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void leftoright::.ctor()
extern "C"  void leftoright__ctor_m2929981791 (leftoright_t233454036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void leftoright::Start()
extern "C"  void leftoright_Start_m4056731219 (leftoright_t233454036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void leftoright::Update()
extern "C"  void leftoright_Update_m4101436086 (leftoright_t233454036 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void leftoright::LeftorRight(UnityEngine.Vector2)
extern "C"  void leftoright_LeftorRight_m4100200553 (leftoright_t233454036 * __this, Vector2_t2243707579  ___vec0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
