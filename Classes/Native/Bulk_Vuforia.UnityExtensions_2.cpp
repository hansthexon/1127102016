﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Vuforia.WebCamTexAdaptor
struct WebCamTexAdaptor_t2106169489;
// Vuforia.WebCamTexAdaptorImpl
struct WebCamTexAdaptorImpl_t1817875757;
// UnityEngine.Texture
struct Texture_t2243626319;
// System.String
struct String_t;
// Vuforia.WordAbstractBehaviour
struct WordAbstractBehaviour_t2878458725;
// Vuforia.Word
struct Word_t3872119486;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// System.Object
struct Il2CppObject;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Vuforia.WordImpl
struct WordImpl_t1843145168;
// Vuforia.Image
struct Image_t1391689025;
// Vuforia.RectangleData[]
struct RectangleDataU5BU5D_t2944021518;
// Vuforia.ImageImpl
struct ImageImpl_t2564717533;
// Vuforia.WordList
struct WordList_t1278495262;
// Vuforia.WordListImpl
struct WordListImpl_t2150426444;
// Vuforia.WordManager
struct WordManager_t1585193471;
// Vuforia.WordManagerImpl
struct WordManagerImpl_t4282786523;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult>
struct IEnumerable_1_t2207634242;
// System.Collections.Generic.IEnumerable`1<Vuforia.Word>
struct IEnumerable_1_t4164246531;
// System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour>
struct IEnumerable_1_t3170585770;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t2321347278;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Collections.Generic.List`1<Vuforia.WordAbstractBehaviour>
struct List_1_t2247579857;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// Vuforia.VuforiaManagerImpl/WordData[]
struct WordDataU5BU5D_t1250596477;
// Vuforia.VuforiaManagerImpl/WordResultData[]
struct WordResultDataU5BU5D_t148237038;
// System.Collections.Generic.IEnumerable`1<Vuforia.VuforiaManagerImpl/WordData>
struct IEnumerable_1_t1566091129;
// System.Collections.Generic.IEnumerable`1<Vuforia.VuforiaManagerImpl/WordResultData>
struct IEnumerable_1_t4034363676;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t1440998580;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t2364004493;
// Vuforia.WordResult
struct WordResult_t1915507197;
// Vuforia.WordResultImpl
struct WordResultImpl_t911273601;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1724666488.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1724666488MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec829768013.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec829768013MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor2106169489.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor2106169489MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptorIm1817875757.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptorIm1817875757MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_WebCamTexture1079476942MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamTexture1079476942.h"
#include "UnityEngine_UnityEngine_Texture2243626319.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Int322071877448.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UserAuthorization3217794812.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279.h"
#include "UnityEngine_UnityEngine_AsyncOperation3814632279MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_PlayModeEditorUtil1237840826MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1779888572.h"
#include "Vuforia_UnityExtensions_Vuforia_WordTemplateMode1097144495.h"
#include "UnityEngine_UnityEngine_Vector32243707580MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh1356156583MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Bounds3033363703MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1779888572MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour955675639MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "Vuforia_UnityExtensions_Vuforia_WordFilterMode695600879.h"
#include "Vuforia_UnityExtensions_Vuforia_WordFilterMode695600879MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WordImpl1843145168.h"
#include "Vuforia_UnityExtensions_Vuforia_WordImpl1843145168MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableImpl3421455115MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_Image1391689025.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil3083157244MethodDeclarations.h"
#include "Vuforia.UnityExtensions_ArrayTypes.h"
#include "Vuforia_UnityExtensions_Vuforia_RectangleData934532407.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "mscorlib_System_Type1303803226MethodDeclarations.h"
#include "mscorlib_System_Runtime_InteropServices_Marshal785896760MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaWrapper3750170617MethodDeclarations.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableImpl3421455115.h"
#include "mscorlib_System_IntPtr2504060609MethodDeclarations.h"
#include "mscorlib_System_Int64909078037.h"
#include "mscorlib_System_Object2689449295.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageImpl2564717533MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3150040852.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageImpl2564717533.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_Image1391689025MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT3010530044.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte3683104436.h"
#include "Vuforia_UnityExtensions_Vuforia_WordList1278495262.h"
#include "Vuforia_UnityExtensions_Vuforia_WordList1278495262MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WordListImpl2150426444.h"
#include "Vuforia_UnityExtensions_Vuforia_WordListImpl2150426444MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Stora3897282321.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManager1585193471.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManager1585193471MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManagerImpl4282786523.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManagerImpl4282786523MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge923332832MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge923332832.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3921359971.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1284628329.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3241240618.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1886284360MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1886284360.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2247579857MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4162359119MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2865418962MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2247579857.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1553924587.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4162359119.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2865418962.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1553924587MethodDeclarations.h"
#include "System_Core_System_Linq_Enumerable2148412300MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2350889594.h"
#include "System_Core_System_Linq_Enumerable2148412300.h"
#include "Vuforia_UnityExtensions_Vuforia_WordPrefabCreation3171836134.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1782309531.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1782309531MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1273964084.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3742236631.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va589344203MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3572817124.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va589344203.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3572817124MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4057911311.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1284628329MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3241240618MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResultImpl911273601MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResultImpl911273601.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1440998580.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResult1915507197.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3406830603.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat975728254MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResult1915507197MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3319870759MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice3827827595MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Video3451594282.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "Vuforia_UnityExtensions_Vuforia_OrientedBoundingBo3172429123.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaAbstractBeh3319870759.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4106934884.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice3827827595.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1264148721.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3491121689.h"
#include "Vuforia_UnityExtensions_Vuforia_OrientedBoundingBo3172429123MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2775970292.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat819358003.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2775970292MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat819358003MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23938596878.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3206309062.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3206309062MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23938596878MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_BehaviourComponent3267823770MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WordPrefabCreation3171836134MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WordTemplateMode1097144495MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m2721246802_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m2721246802(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.MeshFilter>()
#define Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, method) ((  MeshFilter_t3026937449 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  ObjectU5BU5D_t3614634134* Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToArray_TisIl2CppObject_m3301715283(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t3614634134* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared)(__this /* static, unused */, p0, method)
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToArray_TisString_t_m1953054010(__this /* static, unused */, p0, method) ((  StringU5BU5D_t1642385972* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToArray_TisIl2CppObject_m3301715283_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t2058570427 * Enumerable_ToList_TisIl2CppObject_m3361462832_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisIl2CppObject_m3361462832(__this /* static, unused */, p0, method) ((  List_1_t2058570427 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3361462832_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<Vuforia.WordAbstractBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_ToList_TisWordAbstractBehaviour_t2878458725_m3684143909(__this /* static, unused */, p0, method) ((  List_1_t2247579857 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisIl2CppObject_m3361462832_gshared)(__this /* static, unused */, p0, method)
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Int32>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  List_1_t1440998580 * Enumerable_ToList_TisInt32_t2071877448_m2886835297_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_ToList_TisInt32_t2071877448_m2886835297(__this /* static, unused */, p0, method) ((  List_1_t1440998580 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_ToList_TisInt32_t2071877448_m2886835297_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::First<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C"  Il2CppObject * Enumerable_First_TisIl2CppObject_m2000881923_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* p0, const MethodInfo* method);
#define Enumerable_First_TisIl2CppObject_m2000881923(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisIl2CppObject_m2000881923_gshared)(__this /* static, unused */, p0, method)
// !!0 System.Linq.Enumerable::First<Vuforia.WordAbstractBehaviour>(System.Collections.Generic.IEnumerable`1<!!0>)
#define Enumerable_First_TisWordAbstractBehaviour_t2878458725_m1729057050(__this /* static, unused */, p0, method) ((  WordAbstractBehaviour_t2878458725 * (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Enumerable_First_TisIl2CppObject_m2000881923_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m447919519(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1756533147_m3664764861(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Vuforia.WordAbstractBehaviour>()
#define GameObject_GetComponent_TisWordAbstractBehaviour_t2878458725_m4244081605(__this, method) ((  WordAbstractBehaviour_t2878458725 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Vuforia.WebCamProfile/ProfileData
extern "C" void ProfileData_t1724666488_marshal_pinvoke(const ProfileData_t1724666488& unmarshaled, ProfileData_t1724666488_marshaled_pinvoke& marshaled)
{
	Vec2I_t829768013_marshal_pinvoke(unmarshaled.get_RequestedTextureSize_0(), marshaled.___RequestedTextureSize_0);
	Vec2I_t829768013_marshal_pinvoke(unmarshaled.get_ResampledTextureSize_1(), marshaled.___ResampledTextureSize_1);
	marshaled.___RequestedFPS_2 = unmarshaled.get_RequestedFPS_2();
}
extern "C" void ProfileData_t1724666488_marshal_pinvoke_back(const ProfileData_t1724666488_marshaled_pinvoke& marshaled, ProfileData_t1724666488& unmarshaled)
{
	Vec2I_t829768013  unmarshaled_RequestedTextureSize_temp_0;
	memset(&unmarshaled_RequestedTextureSize_temp_0, 0, sizeof(unmarshaled_RequestedTextureSize_temp_0));
	Vec2I_t829768013_marshal_pinvoke_back(marshaled.___RequestedTextureSize_0, unmarshaled_RequestedTextureSize_temp_0);
	unmarshaled.set_RequestedTextureSize_0(unmarshaled_RequestedTextureSize_temp_0);
	Vec2I_t829768013  unmarshaled_ResampledTextureSize_temp_1;
	memset(&unmarshaled_ResampledTextureSize_temp_1, 0, sizeof(unmarshaled_ResampledTextureSize_temp_1));
	Vec2I_t829768013_marshal_pinvoke_back(marshaled.___ResampledTextureSize_1, unmarshaled_ResampledTextureSize_temp_1);
	unmarshaled.set_ResampledTextureSize_1(unmarshaled_ResampledTextureSize_temp_1);
	int32_t unmarshaled_RequestedFPS_temp_2 = 0;
	unmarshaled_RequestedFPS_temp_2 = marshaled.___RequestedFPS_2;
	unmarshaled.set_RequestedFPS_2(unmarshaled_RequestedFPS_temp_2);
}
// Conversion method for clean up from marshalling of: Vuforia.WebCamProfile/ProfileData
extern "C" void ProfileData_t1724666488_marshal_pinvoke_cleanup(ProfileData_t1724666488_marshaled_pinvoke& marshaled)
{
	Vec2I_t829768013_marshal_pinvoke_cleanup(marshaled.___RequestedTextureSize_0);
	Vec2I_t829768013_marshal_pinvoke_cleanup(marshaled.___ResampledTextureSize_1);
}
// Conversion methods for marshalling of: Vuforia.WebCamProfile/ProfileData
extern "C" void ProfileData_t1724666488_marshal_com(const ProfileData_t1724666488& unmarshaled, ProfileData_t1724666488_marshaled_com& marshaled)
{
	Vec2I_t829768013_marshal_com(unmarshaled.get_RequestedTextureSize_0(), marshaled.___RequestedTextureSize_0);
	Vec2I_t829768013_marshal_com(unmarshaled.get_ResampledTextureSize_1(), marshaled.___ResampledTextureSize_1);
	marshaled.___RequestedFPS_2 = unmarshaled.get_RequestedFPS_2();
}
extern "C" void ProfileData_t1724666488_marshal_com_back(const ProfileData_t1724666488_marshaled_com& marshaled, ProfileData_t1724666488& unmarshaled)
{
	Vec2I_t829768013  unmarshaled_RequestedTextureSize_temp_0;
	memset(&unmarshaled_RequestedTextureSize_temp_0, 0, sizeof(unmarshaled_RequestedTextureSize_temp_0));
	Vec2I_t829768013_marshal_com_back(marshaled.___RequestedTextureSize_0, unmarshaled_RequestedTextureSize_temp_0);
	unmarshaled.set_RequestedTextureSize_0(unmarshaled_RequestedTextureSize_temp_0);
	Vec2I_t829768013  unmarshaled_ResampledTextureSize_temp_1;
	memset(&unmarshaled_ResampledTextureSize_temp_1, 0, sizeof(unmarshaled_ResampledTextureSize_temp_1));
	Vec2I_t829768013_marshal_com_back(marshaled.___ResampledTextureSize_1, unmarshaled_ResampledTextureSize_temp_1);
	unmarshaled.set_ResampledTextureSize_1(unmarshaled_ResampledTextureSize_temp_1);
	int32_t unmarshaled_RequestedFPS_temp_2 = 0;
	unmarshaled_RequestedFPS_temp_2 = marshaled.___RequestedFPS_2;
	unmarshaled.set_RequestedFPS_2(unmarshaled_RequestedFPS_temp_2);
}
// Conversion method for clean up from marshalling of: Vuforia.WebCamProfile/ProfileData
extern "C" void ProfileData_t1724666488_marshal_com_cleanup(ProfileData_t1724666488_marshaled_com& marshaled)
{
	Vec2I_t829768013_marshal_com_cleanup(marshaled.___RequestedTextureSize_0);
	Vec2I_t829768013_marshal_com_cleanup(marshaled.___ResampledTextureSize_1);
}
// System.Void Vuforia.WebCamTexAdaptor::.ctor()
extern "C"  void WebCamTexAdaptor__ctor_m3326895553 (WebCamTexAdaptor_t2106169489 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.WebCamTexAdaptorImpl::get_DidUpdateThisFrame()
extern "C"  bool WebCamTexAdaptorImpl_get_DidUpdateThisFrame_m2756135115 (WebCamTexAdaptorImpl_t1817875757 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_t1079476942 * L_0 = __this->get_mWebCamTexture_0();
		NullCheck(L_0);
		bool L_1 = WebCamTexture_get_didUpdateThisFrame_m4104709683(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Vuforia.WebCamTexAdaptorImpl::get_IsPlaying()
extern "C"  bool WebCamTexAdaptorImpl_get_IsPlaying_m1323517514 (WebCamTexAdaptorImpl_t1817875757 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_t1079476942 * L_0 = __this->get_mWebCamTexture_0();
		NullCheck(L_0);
		bool L_1 = WebCamTexture_get_isPlaying_m1392703560(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Texture Vuforia.WebCamTexAdaptorImpl::get_Texture()
extern "C"  Texture_t2243626319 * WebCamTexAdaptorImpl_get_Texture_m594176022 (WebCamTexAdaptorImpl_t1817875757 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_t1079476942 * L_0 = __this->get_mWebCamTexture_0();
		return L_0;
	}
}
// System.Void Vuforia.WebCamTexAdaptorImpl::.ctor(System.String,System.Int32,Vuforia.VuforiaRenderer/Vec2I)
extern Il2CppClass* WebCamTexture_t1079476942_il2cpp_TypeInfo_var;
extern const uint32_t WebCamTexAdaptorImpl__ctor_m2179149417_MetadataUsageId;
extern "C"  void WebCamTexAdaptorImpl__ctor_m2179149417 (WebCamTexAdaptorImpl_t1817875757 * __this, String_t* ___deviceName0, int32_t ___requestedFPS1, Vec2I_t829768013  ___requestedTextureSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebCamTexAdaptorImpl__ctor_m2179149417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		WebCamTexAdaptor__ctor_m3326895553(__this, /*hidden argument*/NULL);
		WebCamTexture_t1079476942 * L_0 = (WebCamTexture_t1079476942 *)il2cpp_codegen_object_new(WebCamTexture_t1079476942_il2cpp_TypeInfo_var);
		WebCamTexture__ctor_m1125343005(L_0, /*hidden argument*/NULL);
		__this->set_mWebCamTexture_0(L_0);
		WebCamTexture_t1079476942 * L_1 = __this->get_mWebCamTexture_0();
		String_t* L_2 = ___deviceName0;
		NullCheck(L_1);
		WebCamTexture_set_deviceName_m2284562713(L_1, L_2, /*hidden argument*/NULL);
		WebCamTexture_t1079476942 * L_3 = __this->get_mWebCamTexture_0();
		int32_t L_4 = ___requestedFPS1;
		NullCheck(L_3);
		WebCamTexture_set_requestedFPS_m608448968(L_3, (((float)((float)L_4))), /*hidden argument*/NULL);
		WebCamTexture_t1079476942 * L_5 = __this->get_mWebCamTexture_0();
		int32_t L_6 = (&___requestedTextureSize2)->get_x_0();
		NullCheck(L_5);
		WebCamTexture_set_requestedWidth_m3863095(L_5, L_6, /*hidden argument*/NULL);
		WebCamTexture_t1079476942 * L_7 = __this->get_mWebCamTexture_0();
		int32_t L_8 = (&___requestedTextureSize2)->get_y_1();
		NullCheck(L_7);
		WebCamTexture_set_requestedHeight_m299835388(L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WebCamTexAdaptorImpl::Play()
extern "C"  void WebCamTexAdaptorImpl_Play_m3880371477 (WebCamTexAdaptorImpl_t1817875757 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Application_HasUserAuthorization_m3858687304(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		AsyncOperation_t3814632279 * L_1 = Application_RequestUserAuthorization_m2712302050(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		__this->set_mCheckCameraPermissions_1(L_1);
		return;
	}

IL_0015:
	{
		WebCamTexture_t1079476942 * L_2 = __this->get_mWebCamTexture_0();
		NullCheck(L_2);
		WebCamTexture_Play_m1997372813(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WebCamTexAdaptorImpl::Stop()
extern "C"  void WebCamTexAdaptorImpl_Stop_m946549237 (WebCamTexAdaptorImpl_t1817875757 * __this, const MethodInfo* method)
{
	{
		WebCamTexture_t1079476942 * L_0 = __this->get_mWebCamTexture_0();
		NullCheck(L_0);
		WebCamTexture_Stop_m4045220381(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WebCamTexAdaptorImpl::CheckPermissions()
extern Il2CppClass* IPlayModeEditorUtility_t1551560165_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1820776075;
extern const uint32_t WebCamTexAdaptorImpl_CheckPermissions_m644464339_MetadataUsageId;
extern "C"  void WebCamTexAdaptorImpl_CheckPermissions_m644464339 (WebCamTexAdaptorImpl_t1817875757 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WebCamTexAdaptorImpl_CheckPermissions_m644464339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AsyncOperation_t3814632279 * L_0 = __this->get_mCheckCameraPermissions_1();
		if (!L_0)
		{
			goto IL_0040;
		}
	}
	{
		AsyncOperation_t3814632279 * L_1 = __this->get_mCheckCameraPermissions_1();
		NullCheck(L_1);
		bool L_2 = AsyncOperation_get_isDone_m1085614149(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0040;
		}
	}
	{
		bool L_3 = Application_HasUserAuthorization_m3858687304(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		WebCamTexture_t1079476942 * L_4 = __this->get_mWebCamTexture_0();
		NullCheck(L_4);
		WebCamTexture_Play_m1997372813(L_4, /*hidden argument*/NULL);
		goto IL_0039;
	}

IL_002a:
	{
		Il2CppObject * L_5 = PlayModeEditorUtility_get_Instance_m3503318396(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		InterfaceActionInvoker1< String_t* >::Invoke(3 /* System.Void Vuforia.IPlayModeEditorUtility::ShowErrorInMouseOverWindow(System.String) */, IPlayModeEditorUtility_t1551560165_il2cpp_TypeInfo_var, L_5, _stringLiteral1820776075);
	}

IL_0039:
	{
		__this->set_mCheckCameraPermissions_1((AsyncOperation_t3814632279 *)NULL);
	}

IL_0040:
	{
		return;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::InternalUnregisterTrackable()
extern "C"  void WordAbstractBehaviour_InternalUnregisterTrackable_m1616159713 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		V_0 = (Il2CppObject *)NULL;
		__this->set_mWord_12((Il2CppObject *)NULL);
		Il2CppObject * L_0 = V_0;
		((TrackableBehaviour_t1779888572 *)__this)->set_mTrackable_7(L_0);
		return;
	}
}
// Vuforia.Word Vuforia.WordAbstractBehaviour::get_Word()
extern "C"  Il2CppObject * WordAbstractBehaviour_get_Word_m2554944169 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_mWord_12();
		return L_0;
	}
}
// System.String Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_SpecificWord()
extern "C"  String_t* WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_SpecificWord_m3036158392 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mSpecificWord_11();
		return L_0;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetSpecificWord(System.String)
extern "C"  void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetSpecificWord_m3261411872 (WordAbstractBehaviour_t2878458725 * __this, String_t* ___word0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___word0;
		__this->set_mSpecificWord_11(L_0);
		return;
	}
}
// Vuforia.WordTemplateMode Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_Mode()
extern "C"  int32_t WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_Mode_m2878670072 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mMode_10();
		return L_0;
	}
}
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsTemplateMode()
extern "C"  bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsTemplateMode_m2245541380 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mMode_10();
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.get_IsSpecificWordMode()
extern "C"  bool WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_get_IsSpecificWordMode_m3280833542 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mMode_10();
		return (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.SetMode(Vuforia.WordTemplateMode)
extern "C"  void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_SetMode_m845570902 (WordAbstractBehaviour_t2878458725 * __this, int32_t ___mode0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode0;
		__this->set_mMode_10(L_0);
		return;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorWordBehaviour.InitializeWord(Vuforia.Word)
extern Il2CppClass* Word_t3872119486_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var;
extern const uint32_t WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m3111632870_MetadataUsageId;
extern "C"  void WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m3111632870 (WordAbstractBehaviour_t2878458725 * __this, Il2CppObject * ___word0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordAbstractBehaviour_Vuforia_IEditorWordBehaviour_InitializeWord_m3111632870_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t2243707579  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	MeshFilter_t3026937449 * V_2 = NULL;
	float V_3 = 0.0f;
	Il2CppObject * V_4 = NULL;
	Bounds_t3033363703  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Il2CppObject * L_0 = ___word0;
		Il2CppObject * L_1 = L_0;
		V_4 = L_1;
		__this->set_mWord_12(L_1);
		Il2CppObject * L_2 = V_4;
		((TrackableBehaviour_t1779888572 *)__this)->set_mTrackable_7(L_2);
		Il2CppObject * L_3 = ___word0;
		NullCheck(L_3);
		Vector2_t2243707579  L_4 = InterfaceFuncInvoker0< Vector2_t2243707579  >::Invoke(1 /* UnityEngine.Vector2 Vuforia.Word::get_Size() */, Word_t3872119486_il2cpp_TypeInfo_var, L_3);
		V_0 = L_4;
		Vector3_t2243707580  L_5 = Vector3_get_one_m627547232(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_5;
		MeshFilter_t3026937449 * L_6 = Component_GetComponent_TisMeshFilter_t3026937449_m1427366350(__this, /*hidden argument*/Component_GetComponent_TisMeshFilter_t3026937449_m1427366350_MethodInfo_var);
		V_2 = L_6;
		MeshFilter_t3026937449 * L_7 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_7, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0044;
		}
	}
	{
		MeshFilter_t3026937449 * L_9 = V_2;
		NullCheck(L_9);
		Mesh_t1356156583 * L_10 = MeshFilter_get_sharedMesh_m1310789932(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Bounds_t3033363703  L_11 = Mesh_get_bounds_m1752141467(L_10, /*hidden argument*/NULL);
		V_5 = L_11;
		Vector3_t2243707580  L_12 = Bounds_get_size_m1728027642((&V_5), /*hidden argument*/NULL);
		V_1 = L_12;
	}

IL_0044:
	{
		float L_13 = (&V_0)->get_y_2();
		float L_14 = (&V_1)->get_z_3();
		V_3 = ((float)((float)L_13/(float)L_14));
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_16 = V_3;
		float L_17 = V_3;
		float L_18 = V_3;
		Vector3_t2243707580  L_19;
		memset(&L_19, 0, sizeof(L_19));
		Vector3__ctor_m2638739322(&L_19, L_16, L_17, L_18, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localScale_m2325460848(L_15, L_19, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::.ctor()
extern "C"  void WordAbstractBehaviour__ctor_m1415816009 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method)
{
	{
		TrackableBehaviour__ctor_m3061730430(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C"  bool WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m1796602413 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m4079055610(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C"  void WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m776300256 (WordAbstractBehaviour_t2878458725 * __this, bool p0, const MethodInfo* method)
{
	{
		bool L_0 = p0;
		Behaviour_set_enabled_m1796096907(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C"  Transform_t3275118058 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m1279349318 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method)
{
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.GameObject Vuforia.WordAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C"  GameObject_t1756533147 * WordAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m2929090518 (WordAbstractBehaviour_t2878458725 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Vuforia.WordImpl::.ctor(System.Int32,System.String,UnityEngine.Vector2)
extern "C"  void WordImpl__ctor_m2135802911 (WordImpl_t1843145168 * __this, int32_t ___id0, String_t* ___text1, Vector2_t2243707579  ___size2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___text1;
		int32_t L_1 = ___id0;
		TrackableImpl__ctor_m1519495996(__this, L_0, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ___text1;
		__this->set_mText_2(L_2);
		Vector2_t2243707579  L_3 = ___size2;
		__this->set_mSize_3(L_3);
		return;
	}
}
// System.String Vuforia.WordImpl::get_StringValue()
extern "C"  String_t* WordImpl_get_StringValue_m131447204 (WordImpl_t1843145168 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mText_2();
		return L_0;
	}
}
// UnityEngine.Vector2 Vuforia.WordImpl::get_Size()
extern "C"  Vector2_t2243707579  WordImpl_get_Size_m2498138279 (WordImpl_t1843145168 * __this, const MethodInfo* method)
{
	{
		Vector2_t2243707579  L_0 = __this->get_mSize_3();
		return L_0;
	}
}
// Vuforia.Image Vuforia.WordImpl::GetLetterMask()
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern const uint32_t WordImpl_GetLetterMask_m1285376160_MetadataUsageId;
extern "C"  Image_t1391689025 * WordImpl_GetLetterMask_m1285376160 (WordImpl_t1843145168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordImpl_GetLetterMask_m1285376160_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m1774515559(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		return (Image_t1391689025 *)NULL;
	}

IL_0009:
	{
		Image_t1391689025 * L_1 = __this->get_mLetterMask_4();
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		WordImpl_CreateLetterMask_m1974445140(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		Image_t1391689025 * L_2 = __this->get_mLetterMask_4();
		return L_2;
	}
}
// Vuforia.RectangleData[] Vuforia.WordImpl::GetLetterBoundingBoxes()
extern const Il2CppType* RectangleData_t934532407_0_0_0_var;
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern Il2CppClass* RectangleDataU5BU5D_t2944021518_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern Il2CppClass* RectangleData_t934532407_il2cpp_TypeInfo_var;
extern const uint32_t WordImpl_GetLetterBoundingBoxes_m3281201375_MetadataUsageId;
extern "C"  RectangleDataU5BU5D_t2944021518* WordImpl_GetLetterBoundingBoxes_m3281201375 (WordImpl_t1843145168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordImpl_GetLetterBoundingBoxes_m3281201375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	IntPtr_t V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_0 = VuforiaRuntimeUtilities_IsVuforiaEnabled_m1774515559(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		return ((RectangleDataU5BU5D_t2944021518*)SZArrayNew(RectangleDataU5BU5D_t2944021518_il2cpp_TypeInfo_var, (uint32_t)0));
	}

IL_000e:
	{
		RectangleDataU5BU5D_t2944021518* L_1 = __this->get_mLetterBoundingBoxes_6();
		if (L_1)
		{
			goto IL_00bf;
		}
	}
	{
		String_t* L_2 = __this->get_mText_2();
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1606060069(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		__this->set_mLetterBoundingBoxes_6(((RectangleDataU5BU5D_t2944021518*)SZArrayNew(RectangleDataU5BU5D_t2944021518_il2cpp_TypeInfo_var, (uint32_t)L_4)));
		int32_t L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(RectangleData_t934532407_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		int32_t L_7 = Marshal_SizeOf_m2982427619(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IntPtr_t L_8 = Marshal_AllocHGlobal_m4258042074(NULL /*static, unused*/, ((int32_t)((int32_t)L_5*(int32_t)L_7)), /*hidden argument*/NULL);
		V_1 = L_8;
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_9 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_10 = TrackableImpl_get_ID_m4059169333(__this, /*hidden argument*/NULL);
		IntPtr_t L_11 = V_1;
		NullCheck(L_9);
		InterfaceFuncInvoker2< int32_t, int32_t, IntPtr_t >::Invoke(117 /* System.Int32 Vuforia.IVuforiaWrapper::WordGetLetterBoundingBoxes(System.Int32,System.IntPtr) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_9, L_10, L_11);
		int64_t L_12 = IntPtr_ToInt64_m39971741((&V_1), /*hidden argument*/NULL);
		IntPtr__ctor_m3803259710((&V_2), L_12, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_00b5;
	}

IL_006c:
	{
		RectangleDataU5BU5D_t2944021518* L_13 = __this->get_mLetterBoundingBoxes_6();
		int32_t L_14 = V_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		IntPtr_t L_15 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(RectangleData_t934532407_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		Il2CppObject * L_17 = Marshal_PtrToStructure_m673412918(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		(*(RectangleData_t934532407 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_14)))) = ((*(RectangleData_t934532407 *)((RectangleData_t934532407 *)UnBox (L_17, RectangleData_t934532407_il2cpp_TypeInfo_var))));
		int64_t L_18 = IntPtr_ToInt64_m39971741((&V_2), /*hidden argument*/NULL);
		Type_t * L_19 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(RectangleData_t934532407_0_0_0_var), /*hidden argument*/NULL);
		int32_t L_20 = Marshal_SizeOf_m2982427619(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		IntPtr__ctor_m3803259710((&V_2), ((int64_t)((int64_t)L_18+(int64_t)(((int64_t)((int64_t)L_20))))), /*hidden argument*/NULL);
		int32_t L_21 = V_3;
		V_3 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_00b5:
	{
		int32_t L_22 = V_3;
		int32_t L_23 = V_0;
		if ((((int32_t)L_22) < ((int32_t)L_23)))
		{
			goto IL_006c;
		}
	}
	{
		IntPtr_t L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m2238467479(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
	}

IL_00bf:
	{
		RectangleDataU5BU5D_t2944021518* L_25 = __this->get_mLetterBoundingBoxes_6();
		return L_25;
	}
}
// System.Void Vuforia.WordImpl::InitImageHeader()
extern Il2CppClass* ImageHeaderData_t3150040852_il2cpp_TypeInfo_var;
extern Il2CppClass* ImageImpl_t2564717533_il2cpp_TypeInfo_var;
extern const uint32_t WordImpl_InitImageHeader_m492255088_MetadataUsageId;
extern "C"  void WordImpl_InitImageHeader_m492255088 (WordImpl_t1843145168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordImpl_InitImageHeader_m492255088_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ImageHeaderData_t3150040852 * L_0 = __this->get_address_of_mLetterImageHeader_5();
		Initobj (ImageHeaderData_t3150040852_il2cpp_TypeInfo_var, L_0);
		ImageHeaderData_t3150040852 * L_1 = __this->get_address_of_mLetterImageHeader_5();
		ImageHeaderData_t3150040852 * L_2 = __this->get_address_of_mLetterImageHeader_5();
		Vector2_t2243707579  L_3 = WordImpl_get_Size_m2498138279(__this, /*hidden argument*/NULL);
		float L_4 = L_3.get_x_1();
		int32_t L_5 = (((int32_t)((int32_t)((float)((float)L_4+(float)(1.0f))))));
		V_0 = L_5;
		L_2->set_bufferWidth_4(L_5);
		int32_t L_6 = V_0;
		L_1->set_width_1(L_6);
		ImageHeaderData_t3150040852 * L_7 = __this->get_address_of_mLetterImageHeader_5();
		ImageHeaderData_t3150040852 * L_8 = __this->get_address_of_mLetterImageHeader_5();
		Vector2_t2243707579  L_9 = WordImpl_get_Size_m2498138279(__this, /*hidden argument*/NULL);
		float L_10 = L_9.get_y_2();
		int32_t L_11 = (((int32_t)((int32_t)((float)((float)L_10+(float)(1.0f))))));
		V_1 = L_11;
		L_8->set_bufferHeight_5(L_11);
		int32_t L_12 = V_1;
		L_7->set_height_2(L_12);
		ImageHeaderData_t3150040852 * L_13 = __this->get_address_of_mLetterImageHeader_5();
		L_13->set_format_6(4);
		ImageImpl_t2564717533 * L_14 = (ImageImpl_t2564717533 *)il2cpp_codegen_object_new(ImageImpl_t2564717533_il2cpp_TypeInfo_var);
		ImageImpl__ctor_m1562615945(L_14, /*hidden argument*/NULL);
		__this->set_mLetterMask_4(L_14);
		return;
	}
}
// System.Void Vuforia.WordImpl::CreateLetterMask()
extern const Il2CppType* ImageHeaderData_t3150040852_0_0_0_var;
extern Il2CppClass* ImageImpl_t2564717533_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern Il2CppClass* ImageHeaderData_t3150040852_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral955959069;
extern const uint32_t WordImpl_CreateLetterMask_m1974445140_MetadataUsageId;
extern "C"  void WordImpl_CreateLetterMask_m1974445140 (WordImpl_t1843145168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordImpl_CreateLetterMask_m1974445140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ImageImpl_t2564717533 * V_0 = NULL;
	IntPtr_t V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		WordImpl_InitImageHeader_m492255088(__this, /*hidden argument*/NULL);
		Image_t1391689025 * L_0 = __this->get_mLetterMask_4();
		V_0 = ((ImageImpl_t2564717533 *)CastclassClass(L_0, ImageImpl_t2564717533_il2cpp_TypeInfo_var));
		ImageHeaderData_t3150040852  L_1 = __this->get_mLetterImageHeader_5();
		ImageImpl_t2564717533 * L_2 = V_0;
		WordImpl_SetImageValues_m2453811954(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		ImageImpl_t2564717533 * L_3 = V_0;
		WordImpl_AllocateImage_m2347164983(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		ImageHeaderData_t3150040852 * L_4 = __this->get_address_of_mLetterImageHeader_5();
		ImageImpl_t2564717533 * L_5 = V_0;
		NullCheck(L_5);
		IntPtr_t L_6 = ImageImpl_get_UnmanagedData_m4100531499(L_5, /*hidden argument*/NULL);
		L_4->set_data_0(L_6);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ImageHeaderData_t3150040852_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		int32_t L_8 = Marshal_SizeOf_m2982427619(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		IntPtr_t L_9 = Marshal_AllocHGlobal_m4258042074(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		ImageHeaderData_t3150040852  L_10 = __this->get_mLetterImageHeader_5();
		ImageHeaderData_t3150040852  L_11 = L_10;
		Il2CppObject * L_12 = Box(ImageHeaderData_t3150040852_il2cpp_TypeInfo_var, &L_11);
		IntPtr_t L_13 = V_1;
		Marshal_StructureToPtr_m3205507777(NULL /*static, unused*/, L_12, L_13, (bool)0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_14 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_15 = TrackableImpl_get_ID_m4059169333(__this, /*hidden argument*/NULL);
		IntPtr_t L_16 = V_1;
		NullCheck(L_14);
		InterfaceFuncInvoker2< int32_t, int32_t, IntPtr_t >::Invoke(116 /* System.Int32 Vuforia.IVuforiaWrapper::WordGetLetterMask(System.Int32,System.IntPtr) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_14, L_15, L_16);
		IntPtr_t L_17 = V_1;
		Type_t * L_18 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(ImageHeaderData_t3150040852_0_0_0_var), /*hidden argument*/NULL);
		Il2CppObject * L_19 = Marshal_PtrToStructure_m673412918(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		__this->set_mLetterImageHeader_5(((*(ImageHeaderData_t3150040852 *)((ImageHeaderData_t3150040852 *)UnBox (L_19, ImageHeaderData_t3150040852_il2cpp_TypeInfo_var)))));
		ImageHeaderData_t3150040852 * L_20 = __this->get_address_of_mLetterImageHeader_5();
		int32_t L_21 = L_20->get_reallocate_7();
		if ((!(((uint32_t)L_21) == ((uint32_t)1))))
		{
			goto IL_00a2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m2503577968(NULL /*static, unused*/, _stringLiteral955959069, /*hidden argument*/NULL);
		return;
	}

IL_00a2:
	{
		ImageImpl_t2564717533 * L_22 = V_0;
		NullCheck(L_22);
		ImageImpl_CopyPixelsFromUnmanagedBuffer_m3959285315(L_22, /*hidden argument*/NULL);
		ImageImpl_t2564717533 * L_23 = V_0;
		__this->set_mLetterMask_4(L_23);
		IntPtr_t L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m2238467479(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordImpl::SetImageValues(Vuforia.VuforiaManagerImpl/ImageHeaderData,Vuforia.ImageImpl)
extern "C"  void WordImpl_SetImageValues_m2453811954 (Il2CppObject * __this /* static, unused */, ImageHeaderData_t3150040852  ___imageHeader0, ImageImpl_t2564717533 * ___image1, const MethodInfo* method)
{
	{
		ImageImpl_t2564717533 * L_0 = ___image1;
		int32_t L_1 = (&___imageHeader0)->get_width_1();
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(5 /* System.Void Vuforia.Image::set_Width(System.Int32) */, L_0, L_1);
		ImageImpl_t2564717533 * L_2 = ___image1;
		int32_t L_3 = (&___imageHeader0)->get_height_2();
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void Vuforia.Image::set_Height(System.Int32) */, L_2, L_3);
		ImageImpl_t2564717533 * L_4 = ___image1;
		int32_t L_5 = (&___imageHeader0)->get_stride_3();
		NullCheck(L_4);
		VirtActionInvoker1< int32_t >::Invoke(9 /* System.Void Vuforia.Image::set_Stride(System.Int32) */, L_4, L_5);
		ImageImpl_t2564717533 * L_6 = ___image1;
		int32_t L_7 = (&___imageHeader0)->get_bufferWidth_4();
		NullCheck(L_6);
		VirtActionInvoker1< int32_t >::Invoke(11 /* System.Void Vuforia.Image::set_BufferWidth(System.Int32) */, L_6, L_7);
		ImageImpl_t2564717533 * L_8 = ___image1;
		int32_t L_9 = (&___imageHeader0)->get_bufferHeight_5();
		NullCheck(L_8);
		VirtActionInvoker1< int32_t >::Invoke(13 /* System.Void Vuforia.Image::set_BufferHeight(System.Int32) */, L_8, L_9);
		ImageImpl_t2564717533 * L_10 = ___image1;
		int32_t L_11 = (&___imageHeader0)->get_format_6();
		NullCheck(L_10);
		VirtActionInvoker1< int32_t >::Invoke(15 /* System.Void Vuforia.Image::set_PixelFormat(Vuforia.Image/PIXEL_FORMAT) */, L_10, L_11);
		return;
	}
}
// System.Void Vuforia.WordImpl::AllocateImage(Vuforia.ImageImpl)
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern const uint32_t WordImpl_AllocateImage_m2347164983_MetadataUsageId;
extern "C"  void WordImpl_AllocateImage_m2347164983 (Il2CppObject * __this /* static, unused */, ImageImpl_t2564717533 * ___image0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordImpl_AllocateImage_m2347164983_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ImageImpl_t2564717533 * L_0 = ___image0;
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_1 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		ImageImpl_t2564717533 * L_2 = ___image0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 Vuforia.Image::get_BufferWidth() */, L_2);
		ImageImpl_t2564717533 * L_4 = ___image0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Int32 Vuforia.Image::get_BufferHeight() */, L_4);
		ImageImpl_t2564717533 * L_6 = ___image0;
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(14 /* Vuforia.Image/PIXEL_FORMAT Vuforia.Image::get_PixelFormat() */, L_6);
		NullCheck(L_1);
		int32_t L_8 = InterfaceFuncInvoker3< int32_t, int32_t, int32_t, int32_t >::Invoke(58 /* System.Int32 Vuforia.IVuforiaWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_1, L_3, L_5, L_7);
		NullCheck(L_0);
		VirtActionInvoker1< ByteU5BU5D_t3397334013* >::Invoke(17 /* System.Void Vuforia.Image::set_Pixels(System.Byte[]) */, L_0, ((ByteU5BU5D_t3397334013*)SZArrayNew(ByteU5BU5D_t3397334013_il2cpp_TypeInfo_var, (uint32_t)L_8)));
		ImageImpl_t2564717533 * L_9 = ___image0;
		NullCheck(L_9);
		IntPtr_t L_10 = ImageImpl_get_UnmanagedData_m4100531499(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		Marshal_FreeHGlobal_m2238467479(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		ImageImpl_t2564717533 * L_11 = ___image0;
		Il2CppObject * L_12 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		ImageImpl_t2564717533 * L_13 = ___image0;
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 Vuforia.Image::get_BufferWidth() */, L_13);
		ImageImpl_t2564717533 * L_15 = ___image0;
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(12 /* System.Int32 Vuforia.Image::get_BufferHeight() */, L_15);
		ImageImpl_t2564717533 * L_17 = ___image0;
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(14 /* Vuforia.Image/PIXEL_FORMAT Vuforia.Image::get_PixelFormat() */, L_17);
		NullCheck(L_12);
		int32_t L_19 = InterfaceFuncInvoker3< int32_t, int32_t, int32_t, int32_t >::Invoke(58 /* System.Int32 Vuforia.IVuforiaWrapper::QcarGetBufferSize(System.Int32,System.Int32,System.Int32) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_12, L_14, L_16, L_18);
		IntPtr_t L_20 = Marshal_AllocHGlobal_m4258042074(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_11);
		ImageImpl_set_UnmanagedData_m768794850(L_11, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordList::.ctor()
extern "C"  void WordList__ctor_m1845098908 (WordList_t1278495262 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Vuforia.WordListImpl::LoadWordListFile(System.String)
extern "C"  bool WordListImpl_LoadWordListFile_m2220640552 (WordListImpl_t2150426444 * __this, String_t* ___relativePath0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___relativePath0;
		bool L_1 = VirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(5 /* System.Boolean Vuforia.WordList::LoadWordListFile(System.String,Vuforia.VuforiaUnity/StorageType) */, __this, L_0, 1);
		return L_1;
	}
}
// System.Boolean Vuforia.WordListImpl::LoadWordListFile(System.String,Vuforia.VuforiaUnity/StorageType)
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2742314638;
extern const uint32_t WordListImpl_LoadWordListFile_m3178229187_MetadataUsageId;
extern "C"  bool WordListImpl_LoadWordListFile_m3178229187 (WordListImpl_t2150426444 * __this, String_t* ___path0, int32_t ___storageType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_LoadWordListFile_m3178229187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___storageType1;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_1 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_2 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2742314638, L_2, /*hidden argument*/NULL);
		___path0 = L_3;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = ___path0;
		int32_t L_6 = ___storageType1;
		NullCheck(L_4);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, String_t*, int32_t >::Invoke(102 /* System.Int32 Vuforia.IVuforiaWrapper::WordListLoadWordList(System.String,System.Int32) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_4, L_5, L_6);
		return (bool)((((int32_t)L_7) == ((int32_t)1))? 1 : 0);
	}
}
// System.Int32 Vuforia.WordListImpl::AddWordsFromFile(System.String)
extern "C"  int32_t WordListImpl_AddWordsFromFile_m934599620 (WordListImpl_t2150426444 * __this, String_t* ___relativePath0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___relativePath0;
		int32_t L_1 = VirtFuncInvoker2< int32_t, String_t*, int32_t >::Invoke(7 /* System.Int32 Vuforia.WordList::AddWordsFromFile(System.String,Vuforia.VuforiaUnity/StorageType) */, __this, L_0, 1);
		return L_1;
	}
}
// System.Int32 Vuforia.WordListImpl::AddWordsFromFile(System.String,Vuforia.VuforiaUnity/StorageType)
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2742314638;
extern const uint32_t WordListImpl_AddWordsFromFile_m4239366397_MetadataUsageId;
extern "C"  int32_t WordListImpl_AddWordsFromFile_m4239366397 (WordListImpl_t2150426444 * __this, String_t* ___path0, int32_t ___storageType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_AddWordsFromFile_m4239366397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___storageType1;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_1 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_2 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2742314638, L_2, /*hidden argument*/NULL);
		___path0 = L_3;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = ___path0;
		int32_t L_6 = ___storageType1;
		NullCheck(L_4);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, String_t*, int32_t >::Invoke(103 /* System.Int32 Vuforia.IVuforiaWrapper::WordListAddWordsFromFile(System.String,System.Int32) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_4, L_5, L_6);
		return L_7;
	}
}
// System.Boolean Vuforia.WordListImpl::AddWord(System.String)
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern const uint32_t WordListImpl_AddWord_m2914751619_MetadataUsageId;
extern "C"  bool WordListImpl_AddWord_m2914751619 (WordListImpl_t2150426444 * __this, String_t* ___word0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_AddWord_m2914751619_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		String_t* L_0 = ___word0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Marshal_StringToHGlobalUni_m919584414(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(104 /* System.Int32 Vuforia.IVuforiaWrapper::WordListAddWordU(System.IntPtr) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_2, L_3);
		V_1 = (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0);
		IntPtr_t L_5 = V_0;
		Marshal_FreeHGlobal_m2238467479(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean Vuforia.WordListImpl::RemoveWord(System.String)
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern const uint32_t WordListImpl_RemoveWord_m1457146834_MetadataUsageId;
extern "C"  bool WordListImpl_RemoveWord_m1457146834 (WordListImpl_t2150426444 * __this, String_t* ___word0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_RemoveWord_m1457146834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		String_t* L_0 = ___word0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Marshal_StringToHGlobalUni_m919584414(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(105 /* System.Int32 Vuforia.IVuforiaWrapper::WordListRemoveWordU(System.IntPtr) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_2, L_3);
		V_1 = (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0);
		IntPtr_t L_5 = V_0;
		Marshal_FreeHGlobal_m2238467479(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean Vuforia.WordListImpl::ContainsWord(System.String)
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern const uint32_t WordListImpl_ContainsWord_m784687157_MetadataUsageId;
extern "C"  bool WordListImpl_ContainsWord_m784687157 (WordListImpl_t2150426444 * __this, String_t* ___word0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_ContainsWord_m784687157_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		String_t* L_0 = ___word0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Marshal_StringToHGlobalUni_m919584414(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(106 /* System.Int32 Vuforia.IVuforiaWrapper::WordListContainsWordU(System.IntPtr) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_2, L_3);
		V_1 = (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0);
		IntPtr_t L_5 = V_0;
		Marshal_FreeHGlobal_m2238467479(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean Vuforia.WordListImpl::UnloadAllLists()
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern const uint32_t WordListImpl_UnloadAllLists_m672527667_MetadataUsageId;
extern "C"  bool WordListImpl_UnloadAllLists_m672527667 (WordListImpl_t2150426444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_UnloadAllLists_m672527667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(107 /* System.Int32 Vuforia.IVuforiaWrapper::WordListUnloadAllLists() */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_0);
		return (bool)((((int32_t)L_1) == ((int32_t)1))? 1 : 0);
	}
}
// Vuforia.WordFilterMode Vuforia.WordListImpl::GetFilterMode()
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern const uint32_t WordListImpl_GetFilterMode_m978650999_MetadataUsageId;
extern "C"  int32_t WordListImpl_GetFilterMode_m978650999 (WordListImpl_t2150426444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_GetFilterMode_m978650999_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(109 /* System.Int32 Vuforia.IVuforiaWrapper::WordListGetFilterMode() */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_0);
		return (int32_t)(L_1);
	}
}
// System.Boolean Vuforia.WordListImpl::SetFilterMode(Vuforia.WordFilterMode)
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern const uint32_t WordListImpl_SetFilterMode_m2627170536_MetadataUsageId;
extern "C"  bool WordListImpl_SetFilterMode_m2627170536 (WordListImpl_t2150426444 * __this, int32_t ___mode0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_SetFilterMode_m2627170536_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___mode0;
		NullCheck(L_0);
		int32_t L_2 = InterfaceFuncInvoker1< int32_t, int32_t >::Invoke(108 /* System.Int32 Vuforia.IVuforiaWrapper::WordListSetFilterMode(System.Int32) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_0, L_1);
		return (bool)((((int32_t)L_2) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Vuforia.WordListImpl::LoadFilterListFile(System.String)
extern "C"  bool WordListImpl_LoadFilterListFile_m1282317742 (WordListImpl_t2150426444 * __this, String_t* ___relativePath0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___relativePath0;
		bool L_1 = VirtFuncInvoker2< bool, String_t*, int32_t >::Invoke(15 /* System.Boolean Vuforia.WordList::LoadFilterListFile(System.String,Vuforia.VuforiaUnity/StorageType) */, __this, L_0, 1);
		return L_1;
	}
}
// System.Boolean Vuforia.WordListImpl::LoadFilterListFile(System.String,Vuforia.VuforiaUnity/StorageType)
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2742314638;
extern const uint32_t WordListImpl_LoadFilterListFile_m969843205_MetadataUsageId;
extern "C"  bool WordListImpl_LoadFilterListFile_m969843205 (WordListImpl_t2150426444 * __this, String_t* ___path0, int32_t ___storageType1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_LoadFilterListFile_m969843205_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___storageType1;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
		bool L_1 = VuforiaRuntimeUtilities_IsPlayMode_m2939358997(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_2 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral2742314638, L_2, /*hidden argument*/NULL);
		___path0 = L_3;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_4 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = ___path0;
		int32_t L_6 = ___storageType1;
		NullCheck(L_4);
		int32_t L_7 = InterfaceFuncInvoker2< int32_t, String_t*, int32_t >::Invoke(110 /* System.Int32 Vuforia.IVuforiaWrapper::WordListLoadFilterList(System.String,System.Int32) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_4, L_5, L_6);
		return (bool)((((int32_t)L_7) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean Vuforia.WordListImpl::AddWordToFilterList(System.String)
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern const uint32_t WordListImpl_AddWordToFilterList_m3484515656_MetadataUsageId;
extern "C"  bool WordListImpl_AddWordToFilterList_m3484515656 (WordListImpl_t2150426444 * __this, String_t* ___word0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_AddWordToFilterList_m3484515656_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		String_t* L_0 = ___word0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Marshal_StringToHGlobalUni_m919584414(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(111 /* System.Int32 Vuforia.IVuforiaWrapper::WordListAddWordToFilterListU(System.IntPtr) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_2, L_3);
		V_1 = (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0);
		IntPtr_t L_5 = V_0;
		Marshal_FreeHGlobal_m2238467479(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean Vuforia.WordListImpl::RemoveWordFromFilterList(System.String)
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern const uint32_t WordListImpl_RemoveWordFromFilterList_m2724088440_MetadataUsageId;
extern "C"  bool WordListImpl_RemoveWordFromFilterList_m2724088440 (WordListImpl_t2150426444 * __this, String_t* ___word0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_RemoveWordFromFilterList_m2724088440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		String_t* L_0 = ___word0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		IntPtr_t L_1 = Marshal_StringToHGlobalUni_m919584414(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_2 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		IntPtr_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = InterfaceFuncInvoker1< int32_t, IntPtr_t >::Invoke(112 /* System.Int32 Vuforia.IVuforiaWrapper::WordListRemoveWordFromFilterListU(System.IntPtr) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_2, L_3);
		V_1 = (bool)((((int32_t)L_4) == ((int32_t)1))? 1 : 0);
		IntPtr_t L_5 = V_0;
		Marshal_FreeHGlobal_m2238467479(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean Vuforia.WordListImpl::ClearFilterList()
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern const uint32_t WordListImpl_ClearFilterList_m2443475993_MetadataUsageId;
extern "C"  bool WordListImpl_ClearFilterList_m2443475993 (WordListImpl_t2150426444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_ClearFilterList_m2443475993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(113 /* System.Int32 Vuforia.IVuforiaWrapper::WordListClearFilterList() */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_0);
		return (bool)((((int32_t)L_1) == ((int32_t)1))? 1 : 0);
	}
}
// System.Int32 Vuforia.WordListImpl::GetFilterListWordCount()
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern const uint32_t WordListImpl_GetFilterListWordCount_m346006489_MetadataUsageId;
extern "C"  int32_t WordListImpl_GetFilterListWordCount_m346006489 (WordListImpl_t2150426444 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_GetFilterListWordCount_m346006489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(114 /* System.Int32 Vuforia.IVuforiaWrapper::WordListGetFilterListWordCount() */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_0);
		return L_1;
	}
}
// System.String Vuforia.WordListImpl::GetFilterListWord(System.Int32)
extern Il2CppClass* VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var;
extern Il2CppClass* IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern const uint32_t WordListImpl_GetFilterListWord_m2629737458_MetadataUsageId;
extern "C"  String_t* WordListImpl_GetFilterListWord_m2629737458 (WordListImpl_t2150426444 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordListImpl_GetFilterListWord_m2629737458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	String_t* V_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(VuforiaWrapper_t3750170617_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = VuforiaWrapper_get_Instance_m128711606(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = ___index0;
		NullCheck(L_0);
		IntPtr_t L_2 = InterfaceFuncInvoker1< IntPtr_t, int32_t >::Invoke(115 /* System.IntPtr Vuforia.IVuforiaWrapper::WordListGetFilterListWordU(System.Int32) */, IVuforiaWrapper_t2426275120_il2cpp_TypeInfo_var, L_0, L_1);
		V_0 = L_2;
		IntPtr_t L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
		String_t* L_4 = Marshal_PtrToStringUni_m4287319443(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		String_t* L_5 = V_1;
		return L_5;
	}
}
// System.Void Vuforia.WordListImpl::.ctor()
extern "C"  void WordListImpl__ctor_m2953304286 (WordListImpl_t2150426444 * __this, const MethodInfo* method)
{
	{
		WordList__ctor_m1845098908(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordManager::.ctor()
extern "C"  void WordManager__ctor_m501606791 (WordManager_t1585193471 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult> Vuforia.WordManagerImpl::GetActiveWordResults()
extern const MethodInfo* Dictionary_2_get_Values_m2747429105_MethodInfo_var;
extern const uint32_t WordManagerImpl_GetActiveWordResults_m1373666590_MetadataUsageId;
extern "C"  Il2CppObject* WordManagerImpl_GetActiveWordResults_m1373666590 (WordManagerImpl_t4282786523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_GetActiveWordResults_m1373666590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t923332832 * L_0 = __this->get_mTrackedWords_1();
		NullCheck(L_0);
		ValueCollection_t3921359971 * L_1 = Dictionary_2_get_Values_m2747429105(L_0, /*hidden argument*/Dictionary_2_get_Values_m2747429105_MethodInfo_var);
		return L_1;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.WordResult> Vuforia.WordManagerImpl::GetNewWords()
extern "C"  Il2CppObject* WordManagerImpl_GetNewWords_m3002296077 (WordManagerImpl_t4282786523 * __this, const MethodInfo* method)
{
	{
		List_1_t1284628329 * L_0 = __this->get_mNewWords_2();
		return L_0;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.Word> Vuforia.WordManagerImpl::GetLostWords()
extern "C"  Il2CppObject* WordManagerImpl_GetLostWords_m3146633038 (WordManagerImpl_t4282786523 * __this, const MethodInfo* method)
{
	{
		List_1_t3241240618 * L_0 = __this->get_mLostWords_3();
		return L_0;
	}
}
// System.Boolean Vuforia.WordManagerImpl::TryGetWordBehaviour(Vuforia.Word,Vuforia.WordAbstractBehaviour&)
extern Il2CppClass* Trackable_t432275407_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_TryGetValue_m3894482256_MethodInfo_var;
extern const uint32_t WordManagerImpl_TryGetWordBehaviour_m1106367364_MetadataUsageId;
extern "C"  bool WordManagerImpl_TryGetWordBehaviour_m1106367364 (WordManagerImpl_t4282786523 * __this, Il2CppObject * ___word0, WordAbstractBehaviour_t2878458725 ** ___behaviour1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_TryGetWordBehaviour_m1106367364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t1886284360 * L_0 = __this->get_mActiveWordBehaviours_4();
		Il2CppObject * L_1 = ___word0;
		NullCheck(L_1);
		int32_t L_2 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t432275407_il2cpp_TypeInfo_var, L_1);
		WordAbstractBehaviour_t2878458725 ** L_3 = ___behaviour1;
		NullCheck(L_0);
		bool L_4 = Dictionary_2_TryGetValue_m3894482256(L_0, L_2, L_3, /*hidden argument*/Dictionary_2_TryGetValue_m3894482256_MethodInfo_var);
		return L_4;
	}
}
// System.Collections.Generic.IEnumerable`1<Vuforia.WordAbstractBehaviour> Vuforia.WordManagerImpl::GetTrackableBehaviours()
extern Il2CppClass* List_1_t2247579857_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1035509995_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m1858939878_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4256312324_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m10001906_MethodInfo_var;
extern const MethodInfo* List_1_AddRange_m1547460111_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2691440873_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2063823636_MethodInfo_var;
extern const uint32_t WordManagerImpl_GetTrackableBehaviours_m896923779_MetadataUsageId;
extern "C"  Il2CppObject* WordManagerImpl_GetTrackableBehaviours_m896923779 (WordManagerImpl_t4282786523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_GetTrackableBehaviours_m896923779_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2247579857 * V_0 = NULL;
	List_1_t2247579857 * V_1 = NULL;
	Enumerator_t1553924587  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2247579857 * L_0 = (List_1_t2247579857 *)il2cpp_codegen_object_new(List_1_t2247579857_il2cpp_TypeInfo_var);
		List_1__ctor_m1035509995(L_0, /*hidden argument*/List_1__ctor_m1035509995_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t4162359119 * L_1 = __this->get_mWordBehaviours_7();
		NullCheck(L_1);
		ValueCollection_t2865418962 * L_2 = Dictionary_2_get_Values_m1858939878(L_1, /*hidden argument*/Dictionary_2_get_Values_m1858939878_MethodInfo_var);
		NullCheck(L_2);
		Enumerator_t1553924587  L_3 = ValueCollection_GetEnumerator_m4256312324(L_2, /*hidden argument*/ValueCollection_GetEnumerator_m4256312324_MethodInfo_var);
		V_2 = L_3;
	}

IL_0017:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0028;
		}

IL_0019:
		{
			List_1_t2247579857 * L_4 = Enumerator_get_Current_m10001906((&V_2), /*hidden argument*/Enumerator_get_Current_m10001906_MethodInfo_var);
			V_1 = L_4;
			List_1_t2247579857 * L_5 = V_0;
			List_1_t2247579857 * L_6 = V_1;
			NullCheck(L_5);
			List_1_AddRange_m1547460111(L_5, L_6, /*hidden argument*/List_1_AddRange_m1547460111_MethodInfo_var);
		}

IL_0028:
		{
			bool L_7 = Enumerator_MoveNext_m2691440873((&V_2), /*hidden argument*/Enumerator_MoveNext_m2691440873_MethodInfo_var);
			if (L_7)
			{
				goto IL_0019;
			}
		}

IL_0031:
		{
			IL2CPP_LEAVE(0x41, FINALLY_0033);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0033;
	}

FINALLY_0033:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2063823636((&V_2), /*hidden argument*/Enumerator_Dispose_m2063823636_MethodInfo_var);
		IL2CPP_END_FINALLY(51)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(51)
	{
		IL2CPP_JUMP_TBL(0x41, IL_0041)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0041:
	{
		List_1_t2247579857 * L_8 = V_0;
		return L_8;
	}
}
// System.Void Vuforia.WordManagerImpl::DestroyWordBehaviour(Vuforia.WordAbstractBehaviour,System.Boolean)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* IEditorTrackableBehaviour_t2323791534_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Keys_m1792287238_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m1953054010_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m1779921936_MethodInfo_var;
extern const MethodInfo* List_1_Contains_m14929093_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m3284721738_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3798745807_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m4027261045_MethodInfo_var;
extern const MethodInfo* List_1_Add_m342378119_MethodInfo_var;
extern const uint32_t WordManagerImpl_DestroyWordBehaviour_m2846089908_MetadataUsageId;
extern "C"  void WordManagerImpl_DestroyWordBehaviour_m2846089908 (WordManagerImpl_t4282786523 * __this, WordAbstractBehaviour_t2878458725 * ___behaviour0, bool ___destroyGameObject1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_DestroyWordBehaviour_m2846089908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t1642385972* V_0 = NULL;
	String_t* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	StringU5BU5D_t1642385972* V_3 = NULL;
	int32_t V_4 = 0;
	{
		Dictionary_2_t4162359119 * L_0 = __this->get_mWordBehaviours_7();
		NullCheck(L_0);
		KeyCollection_t2350889594 * L_1 = Dictionary_2_get_Keys_m1792287238(L_0, /*hidden argument*/Dictionary_2_get_Keys_m1792287238_MethodInfo_var);
		StringU5BU5D_t1642385972* L_2 = Enumerable_ToArray_TisString_t_m1953054010(NULL /*static, unused*/, L_1, /*hidden argument*/Enumerable_ToArray_TisString_t_m1953054010_MethodInfo_var);
		V_0 = L_2;
		StringU5BU5D_t1642385972* L_3 = V_0;
		V_3 = L_3;
		V_4 = 0;
		goto IL_008e;
	}

IL_0018:
	{
		StringU5BU5D_t1642385972* L_4 = V_3;
		int32_t L_5 = V_4;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		String_t* L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = L_7;
		Dictionary_2_t4162359119 * L_8 = __this->get_mWordBehaviours_7();
		String_t* L_9 = V_1;
		NullCheck(L_8);
		List_1_t2247579857 * L_10 = Dictionary_2_get_Item_m1779921936(L_8, L_9, /*hidden argument*/Dictionary_2_get_Item_m1779921936_MethodInfo_var);
		WordAbstractBehaviour_t2878458725 * L_11 = ___behaviour0;
		NullCheck(L_10);
		bool L_12 = List_1_Contains_m14929093(L_10, L_11, /*hidden argument*/List_1_Contains_m14929093_MethodInfo_var);
		if (!L_12)
		{
			goto IL_0088;
		}
	}
	{
		Dictionary_2_t4162359119 * L_13 = __this->get_mWordBehaviours_7();
		String_t* L_14 = V_1;
		NullCheck(L_13);
		List_1_t2247579857 * L_15 = Dictionary_2_get_Item_m1779921936(L_13, L_14, /*hidden argument*/Dictionary_2_get_Item_m1779921936_MethodInfo_var);
		WordAbstractBehaviour_t2878458725 * L_16 = ___behaviour0;
		NullCheck(L_15);
		List_1_Remove_m3284721738(L_15, L_16, /*hidden argument*/List_1_Remove_m3284721738_MethodInfo_var);
		Dictionary_2_t4162359119 * L_17 = __this->get_mWordBehaviours_7();
		String_t* L_18 = V_1;
		NullCheck(L_17);
		List_1_t2247579857 * L_19 = Dictionary_2_get_Item_m1779921936(L_17, L_18, /*hidden argument*/Dictionary_2_get_Item_m1779921936_MethodInfo_var);
		NullCheck(L_19);
		int32_t L_20 = List_1_get_Count_m3798745807(L_19, /*hidden argument*/List_1_get_Count_m3798745807_MethodInfo_var);
		if (L_20)
		{
			goto IL_0064;
		}
	}
	{
		Dictionary_2_t4162359119 * L_21 = __this->get_mWordBehaviours_7();
		String_t* L_22 = V_1;
		NullCheck(L_21);
		Dictionary_2_Remove_m4027261045(L_21, L_22, /*hidden argument*/Dictionary_2_Remove_m4027261045_MethodInfo_var);
	}

IL_0064:
	{
		bool L_23 = ___destroyGameObject1;
		if (!L_23)
		{
			goto IL_0080;
		}
	}
	{
		WordAbstractBehaviour_t2878458725 * L_24 = ___behaviour0;
		NullCheck(L_24);
		GameObject_t1756533147 * L_25 = Component_get_gameObject_m3105766835(L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		List_1_t2247579857 * L_26 = __this->get_mWordBehavioursMarkedForDeletion_5();
		WordAbstractBehaviour_t2878458725 * L_27 = ___behaviour0;
		NullCheck(L_26);
		List_1_Add_m342378119(L_26, L_27, /*hidden argument*/List_1_Add_m342378119_MethodInfo_var);
		goto IL_0088;
	}

IL_0080:
	{
		WordAbstractBehaviour_t2878458725 * L_28 = ___behaviour0;
		V_2 = L_28;
		Il2CppObject * L_29 = V_2;
		NullCheck(L_29);
		InterfaceActionInvoker0::Invoke(10 /* System.Void Vuforia.IEditorTrackableBehaviour::UnregisterTrackable() */, IEditorTrackableBehaviour_t2323791534_il2cpp_TypeInfo_var, L_29);
	}

IL_0088:
	{
		int32_t L_30 = V_4;
		V_4 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_008e:
	{
		int32_t L_31 = V_4;
		StringU5BU5D_t1642385972* L_32 = V_3;
		NullCheck(L_32);
		if ((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_32)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		return;
	}
}
// System.Void Vuforia.WordManagerImpl::InitializeWordBehaviourTemplates(Vuforia.WordPrefabCreationMode,System.Int32)
extern "C"  void WordManagerImpl_InitializeWordBehaviourTemplates_m3992314456 (WordManagerImpl_t4282786523 * __this, int32_t ___wordPrefabCreationMode0, int32_t ___maxInstances1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___wordPrefabCreationMode0;
		__this->set_mWordPrefabCreationMode_10(L_0);
		int32_t L_1 = ___maxInstances1;
		__this->set_mMaxInstances_9(L_1);
		WordManagerImpl_InitializeWordBehaviourTemplates_m4020699935(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordManagerImpl::InitializeWordBehaviourTemplates()
extern const Il2CppType* WordAbstractBehaviour_t2878458725_0_0_0_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* WordAbstractBehaviourU5BU5D_t3037784424_il2cpp_TypeInfo_var;
extern Il2CppClass* IEditorWordBehaviour_t525465685_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2247579857_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Enumerable_ToList_TisWordAbstractBehaviour_t2878458725_m3684143909_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m4147027175_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m1779921936_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2655044316_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1612811480_MethodInfo_var;
extern const MethodInfo* List_1_Add_m342378119_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1529797818_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3163303335_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m4027261045_MethodInfo_var;
extern const MethodInfo* List_1_Contains_m14929093_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1035509995_MethodInfo_var;
extern const MethodInfo* Dictionary_2_set_Item_m869107287_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m396537286_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m951565972_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1328721296;
extern const uint32_t WordManagerImpl_InitializeWordBehaviourTemplates_m4020699935_MetadataUsageId;
extern "C"  void WordManagerImpl_InitializeWordBehaviourTemplates_m4020699935 (WordManagerImpl_t4282786523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_InitializeWordBehaviourTemplates_m4020699935_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2247579857 * V_0 = NULL;
	WordAbstractBehaviour_t2878458725 * V_1 = NULL;
	WordAbstractBehaviourU5BU5D_t3037784424* V_2 = NULL;
	WordAbstractBehaviour_t2878458725 * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	String_t* V_5 = NULL;
	List_1_t2247579857 * V_6 = NULL;
	WordAbstractBehaviour_t2878458725 * V_7 = NULL;
	List_1_t2247579857 * V_8 = NULL;
	Enumerator_t1782309531  V_9;
	memset(&V_9, 0, sizeof(V_9));
	WordAbstractBehaviourU5BU5D_t3037784424* V_10 = NULL;
	int32_t V_11 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	String_t* G_B15_0 = NULL;
	{
		int32_t L_0 = __this->get_mWordPrefabCreationMode_10();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_016e;
		}
	}
	{
		List_1_t2247579857 * L_1 = __this->get_mWordBehavioursMarkedForDeletion_5();
		List_1_t2247579857 * L_2 = Enumerable_ToList_TisWordAbstractBehaviour_t2878458725_m3684143909(NULL /*static, unused*/, L_1, /*hidden argument*/Enumerable_ToList_TisWordAbstractBehaviour_t2878458725_m3684143909_MethodInfo_var);
		V_0 = L_2;
		bool L_3 = __this->get_mAutomaticTemplate_8();
		if (!L_3)
		{
			goto IL_008f;
		}
	}
	{
		Dictionary_2_t4162359119 * L_4 = __this->get_mWordBehaviours_7();
		NullCheck(L_4);
		bool L_5 = Dictionary_2_ContainsKey_m4147027175(L_4, _stringLiteral1328721296, /*hidden argument*/Dictionary_2_ContainsKey_m4147027175_MethodInfo_var);
		if (!L_5)
		{
			goto IL_008f;
		}
	}
	{
		Dictionary_2_t4162359119 * L_6 = __this->get_mWordBehaviours_7();
		NullCheck(L_6);
		List_1_t2247579857 * L_7 = Dictionary_2_get_Item_m1779921936(L_6, _stringLiteral1328721296, /*hidden argument*/Dictionary_2_get_Item_m1779921936_MethodInfo_var);
		NullCheck(L_7);
		Enumerator_t1782309531  L_8 = List_1_GetEnumerator_m2655044316(L_7, /*hidden argument*/List_1_GetEnumerator_m2655044316_MethodInfo_var);
		V_9 = L_8;
	}

IL_0049:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0065;
		}

IL_004b:
		{
			WordAbstractBehaviour_t2878458725 * L_9 = Enumerator_get_Current_m1612811480((&V_9), /*hidden argument*/Enumerator_get_Current_m1612811480_MethodInfo_var);
			V_1 = L_9;
			List_1_t2247579857 * L_10 = V_0;
			WordAbstractBehaviour_t2878458725 * L_11 = V_1;
			NullCheck(L_10);
			List_1_Add_m342378119(L_10, L_11, /*hidden argument*/List_1_Add_m342378119_MethodInfo_var);
			WordAbstractBehaviour_t2878458725 * L_12 = V_1;
			NullCheck(L_12);
			GameObject_t1756533147 * L_13 = Component_get_gameObject_m3105766835(L_12, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			Object_Destroy_m4145850038(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		}

IL_0065:
		{
			bool L_14 = Enumerator_MoveNext_m1529797818((&V_9), /*hidden argument*/Enumerator_MoveNext_m1529797818_MethodInfo_var);
			if (L_14)
			{
				goto IL_004b;
			}
		}

IL_006e:
		{
			IL2CPP_LEAVE(0x7E, FINALLY_0070);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0070;
	}

FINALLY_0070:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3163303335((&V_9), /*hidden argument*/Enumerator_Dispose_m3163303335_MethodInfo_var);
		IL2CPP_END_FINALLY(112)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(112)
	{
		IL2CPP_JUMP_TBL(0x7E, IL_007e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007e:
	{
		Dictionary_2_t4162359119 * L_15 = __this->get_mWordBehaviours_7();
		NullCheck(L_15);
		Dictionary_2_Remove_m4027261045(L_15, _stringLiteral1328721296, /*hidden argument*/Dictionary_2_Remove_m4027261045_MethodInfo_var);
	}

IL_008f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_16 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(WordAbstractBehaviour_t2878458725_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t4217747464* L_17 = Object_FindObjectsOfType_m2121813744(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		V_2 = ((WordAbstractBehaviourU5BU5D_t3037784424*)Castclass(L_17, WordAbstractBehaviourU5BU5D_t3037784424_il2cpp_TypeInfo_var));
		WordAbstractBehaviourU5BU5D_t3037784424* L_18 = V_2;
		V_10 = L_18;
		V_11 = 0;
		goto IL_0124;
	}

IL_00ac:
	{
		WordAbstractBehaviourU5BU5D_t3037784424* L_19 = V_10;
		int32_t L_20 = V_11;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		WordAbstractBehaviour_t2878458725 * L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		V_3 = L_22;
		List_1_t2247579857 * L_23 = V_0;
		WordAbstractBehaviour_t2878458725 * L_24 = V_3;
		NullCheck(L_23);
		bool L_25 = List_1_Contains_m14929093(L_23, L_24, /*hidden argument*/List_1_Contains_m14929093_MethodInfo_var);
		if (L_25)
		{
			goto IL_011e;
		}
	}
	{
		WordAbstractBehaviour_t2878458725 * L_26 = V_3;
		V_4 = L_26;
		Il2CppObject * L_27 = V_4;
		NullCheck(L_27);
		bool L_28 = InterfaceFuncInvoker0< bool >::Invoke(3 /* System.Boolean Vuforia.IEditorWordBehaviour::get_IsTemplateMode() */, IEditorWordBehaviour_t525465685_il2cpp_TypeInfo_var, L_27);
		if (L_28)
		{
			goto IL_00d5;
		}
	}
	{
		Il2CppObject * L_29 = V_4;
		NullCheck(L_29);
		String_t* L_30 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.IEditorWordBehaviour::get_SpecificWord() */, IEditorWordBehaviour_t525465685_il2cpp_TypeInfo_var, L_29);
		NullCheck(L_30);
		String_t* L_31 = String_ToLowerInvariant_m3152527487(L_30, /*hidden argument*/NULL);
		G_B15_0 = L_31;
		goto IL_00da;
	}

IL_00d5:
	{
		G_B15_0 = _stringLiteral1328721296;
	}

IL_00da:
	{
		V_5 = G_B15_0;
		Dictionary_2_t4162359119 * L_32 = __this->get_mWordBehaviours_7();
		String_t* L_33 = V_5;
		NullCheck(L_32);
		bool L_34 = Dictionary_2_ContainsKey_m4147027175(L_32, L_33, /*hidden argument*/Dictionary_2_ContainsKey_m4147027175_MethodInfo_var);
		if (L_34)
		{
			goto IL_011e;
		}
	}
	{
		Dictionary_2_t4162359119 * L_35 = __this->get_mWordBehaviours_7();
		String_t* L_36 = V_5;
		List_1_t2247579857 * L_37 = (List_1_t2247579857 *)il2cpp_codegen_object_new(List_1_t2247579857_il2cpp_TypeInfo_var);
		List_1__ctor_m1035509995(L_37, /*hidden argument*/List_1__ctor_m1035509995_MethodInfo_var);
		V_6 = L_37;
		List_1_t2247579857 * L_38 = V_6;
		WordAbstractBehaviour_t2878458725 * L_39 = V_3;
		NullCheck(L_38);
		List_1_Add_m342378119(L_38, L_39, /*hidden argument*/List_1_Add_m342378119_MethodInfo_var);
		List_1_t2247579857 * L_40 = V_6;
		NullCheck(L_35);
		Dictionary_2_set_Item_m869107287(L_35, L_36, L_40, /*hidden argument*/Dictionary_2_set_Item_m869107287_MethodInfo_var);
		String_t* L_41 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_42 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_41, _stringLiteral1328721296, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_011e;
		}
	}
	{
		__this->set_mAutomaticTemplate_8((bool)0);
	}

IL_011e:
	{
		int32_t L_43 = V_11;
		V_11 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_0124:
	{
		int32_t L_44 = V_11;
		WordAbstractBehaviourU5BU5D_t3037784424* L_45 = V_10;
		NullCheck(L_45);
		if ((((int32_t)L_44) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_45)->max_length)))))))
		{
			goto IL_00ac;
		}
	}
	{
		Dictionary_2_t4162359119 * L_46 = __this->get_mWordBehaviours_7();
		NullCheck(L_46);
		bool L_47 = Dictionary_2_ContainsKey_m4147027175(L_46, _stringLiteral1328721296, /*hidden argument*/Dictionary_2_ContainsKey_m4147027175_MethodInfo_var);
		if (L_47)
		{
			goto IL_016e;
		}
	}
	{
		WordAbstractBehaviour_t2878458725 * L_48 = WordManagerImpl_CreateWordBehaviour_m810645364(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_7 = L_48;
		Dictionary_2_t4162359119 * L_49 = __this->get_mWordBehaviours_7();
		List_1_t2247579857 * L_50 = (List_1_t2247579857 *)il2cpp_codegen_object_new(List_1_t2247579857_il2cpp_TypeInfo_var);
		List_1__ctor_m1035509995(L_50, /*hidden argument*/List_1__ctor_m1035509995_MethodInfo_var);
		V_8 = L_50;
		List_1_t2247579857 * L_51 = V_8;
		WordAbstractBehaviour_t2878458725 * L_52 = V_7;
		NullCheck(L_51);
		List_1_Add_m342378119(L_51, L_52, /*hidden argument*/List_1_Add_m342378119_MethodInfo_var);
		List_1_t2247579857 * L_53 = V_8;
		NullCheck(L_49);
		Dictionary_2_Add_m396537286(L_49, _stringLiteral1328721296, L_53, /*hidden argument*/Dictionary_2_Add_m396537286_MethodInfo_var);
		__this->set_mAutomaticTemplate_8((bool)1);
	}

IL_016e:
	{
		List_1_t2247579857 * L_54 = __this->get_mWordBehavioursMarkedForDeletion_5();
		NullCheck(L_54);
		List_1_Clear_m951565972(L_54, /*hidden argument*/List_1_Clear_m951565972_MethodInfo_var);
		return;
	}
}
// System.Void Vuforia.WordManagerImpl::RemoveDestroyedTrackables()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m1858939878_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4256312324_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m10001906_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3798745807_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m4089333484_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m2559791933_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2691440873_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2063823636_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Keys_m1792287238_MethodInfo_var;
extern const MethodInfo* Enumerable_ToArray_TisString_t_m1953054010_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m1779921936_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m4027261045_MethodInfo_var;
extern const uint32_t WordManagerImpl_RemoveDestroyedTrackables_m3725489426_MetadataUsageId;
extern "C"  void WordManagerImpl_RemoveDestroyedTrackables_m3725489426 (WordManagerImpl_t4282786523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_RemoveDestroyedTrackables_m3725489426_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2247579857 * V_0 = NULL;
	int32_t V_1 = 0;
	StringU5BU5D_t1642385972* V_2 = NULL;
	String_t* V_3 = NULL;
	Enumerator_t1553924587  V_4;
	memset(&V_4, 0, sizeof(V_4));
	StringU5BU5D_t1642385972* V_5 = NULL;
	int32_t V_6 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t4162359119 * L_0 = __this->get_mWordBehaviours_7();
		NullCheck(L_0);
		ValueCollection_t2865418962 * L_1 = Dictionary_2_get_Values_m1858939878(L_0, /*hidden argument*/Dictionary_2_get_Values_m1858939878_MethodInfo_var);
		NullCheck(L_1);
		Enumerator_t1553924587  L_2 = ValueCollection_GetEnumerator_m4256312324(L_1, /*hidden argument*/ValueCollection_GetEnumerator_m4256312324_MethodInfo_var);
		V_4 = L_2;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0045;
		}

IL_0014:
		{
			List_1_t2247579857 * L_3 = Enumerator_get_Current_m10001906((&V_4), /*hidden argument*/Enumerator_get_Current_m10001906_MethodInfo_var);
			V_0 = L_3;
			List_1_t2247579857 * L_4 = V_0;
			NullCheck(L_4);
			int32_t L_5 = List_1_get_Count_m3798745807(L_4, /*hidden argument*/List_1_get_Count_m3798745807_MethodInfo_var);
			V_1 = ((int32_t)((int32_t)L_5-(int32_t)1));
			goto IL_0041;
		}

IL_0027:
		{
			List_1_t2247579857 * L_6 = V_0;
			int32_t L_7 = V_1;
			NullCheck(L_6);
			WordAbstractBehaviour_t2878458725 * L_8 = List_1_get_Item_m4089333484(L_6, L_7, /*hidden argument*/List_1_get_Item_m4089333484_MethodInfo_var);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_9 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_8, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_9)
			{
				goto IL_003d;
			}
		}

IL_0036:
		{
			List_1_t2247579857 * L_10 = V_0;
			int32_t L_11 = V_1;
			NullCheck(L_10);
			List_1_RemoveAt_m2559791933(L_10, L_11, /*hidden argument*/List_1_RemoveAt_m2559791933_MethodInfo_var);
		}

IL_003d:
		{
			int32_t L_12 = V_1;
			V_1 = ((int32_t)((int32_t)L_12-(int32_t)1));
		}

IL_0041:
		{
			int32_t L_13 = V_1;
			if ((((int32_t)L_13) >= ((int32_t)0)))
			{
				goto IL_0027;
			}
		}

IL_0045:
		{
			bool L_14 = Enumerator_MoveNext_m2691440873((&V_4), /*hidden argument*/Enumerator_MoveNext_m2691440873_MethodInfo_var);
			if (L_14)
			{
				goto IL_0014;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x5E, FINALLY_0050);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0050;
	}

FINALLY_0050:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2063823636((&V_4), /*hidden argument*/Enumerator_Dispose_m2063823636_MethodInfo_var);
		IL2CPP_END_FINALLY(80)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(80)
	{
		IL2CPP_JUMP_TBL(0x5E, IL_005e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_005e:
	{
		Dictionary_2_t4162359119 * L_15 = __this->get_mWordBehaviours_7();
		NullCheck(L_15);
		KeyCollection_t2350889594 * L_16 = Dictionary_2_get_Keys_m1792287238(L_15, /*hidden argument*/Dictionary_2_get_Keys_m1792287238_MethodInfo_var);
		StringU5BU5D_t1642385972* L_17 = Enumerable_ToArray_TisString_t_m1953054010(NULL /*static, unused*/, L_16, /*hidden argument*/Enumerable_ToArray_TisString_t_m1953054010_MethodInfo_var);
		V_2 = L_17;
		StringU5BU5D_t1642385972* L_18 = V_2;
		V_5 = L_18;
		V_6 = 0;
		goto IL_00a3;
	}

IL_0077:
	{
		StringU5BU5D_t1642385972* L_19 = V_5;
		int32_t L_20 = V_6;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		String_t* L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		V_3 = L_22;
		Dictionary_2_t4162359119 * L_23 = __this->get_mWordBehaviours_7();
		String_t* L_24 = V_3;
		NullCheck(L_23);
		List_1_t2247579857 * L_25 = Dictionary_2_get_Item_m1779921936(L_23, L_24, /*hidden argument*/Dictionary_2_get_Item_m1779921936_MethodInfo_var);
		NullCheck(L_25);
		int32_t L_26 = List_1_get_Count_m3798745807(L_25, /*hidden argument*/List_1_get_Count_m3798745807_MethodInfo_var);
		if (L_26)
		{
			goto IL_009d;
		}
	}
	{
		Dictionary_2_t4162359119 * L_27 = __this->get_mWordBehaviours_7();
		String_t* L_28 = V_3;
		NullCheck(L_27);
		Dictionary_2_Remove_m4027261045(L_27, L_28, /*hidden argument*/Dictionary_2_Remove_m4027261045_MethodInfo_var);
	}

IL_009d:
	{
		int32_t L_29 = V_6;
		V_6 = ((int32_t)((int32_t)L_29+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_30 = V_6;
		StringU5BU5D_t1642385972* L_31 = V_5;
		NullCheck(L_31);
		if ((((int32_t)L_30) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_31)->max_length)))))))
		{
			goto IL_0077;
		}
	}
	{
		return;
	}
}
// System.Void Vuforia.WordManagerImpl::UpdateWords(UnityEngine.Transform,Vuforia.VuforiaManagerImpl/WordData[],Vuforia.VuforiaManagerImpl/WordResultData[])
extern "C"  void WordManagerImpl_UpdateWords_m506580541 (WordManagerImpl_t4282786523 * __this, Transform_t3275118058 * ___arCameraTransform0, WordDataU5BU5D_t1250596477* ___newWordData1, WordResultDataU5BU5D_t148237038* ___wordResults2, const MethodInfo* method)
{
	{
		WordDataU5BU5D_t1250596477* L_0 = ___newWordData1;
		WordResultDataU5BU5D_t148237038* L_1 = ___wordResults2;
		WordManagerImpl_UpdateWords_m2132655282(__this, (Il2CppObject*)(Il2CppObject*)L_0, (Il2CppObject*)(Il2CppObject*)L_1, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = ___arCameraTransform0;
		WordResultDataU5BU5D_t148237038* L_3 = ___wordResults2;
		WordManagerImpl_UpdateWordResultPoses_m1487928222(__this, L_2, (Il2CppObject*)(Il2CppObject*)L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordManagerImpl::SetWordBehavioursToNotFound()
extern const MethodInfo* Dictionary_2_get_Values_m4195412189_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m4101546403_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3342904645_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3611162142_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3089730385_MethodInfo_var;
extern const uint32_t WordManagerImpl_SetWordBehavioursToNotFound_m1520214889_MetadataUsageId;
extern "C"  void WordManagerImpl_SetWordBehavioursToNotFound_m1520214889 (WordManagerImpl_t4282786523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_SetWordBehavioursToNotFound_m1520214889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WordAbstractBehaviour_t2878458725 * V_0 = NULL;
	Enumerator_t3572817124  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t1886284360 * L_0 = __this->get_mActiveWordBehaviours_4();
		NullCheck(L_0);
		ValueCollection_t589344203 * L_1 = Dictionary_2_get_Values_m4195412189(L_0, /*hidden argument*/Dictionary_2_get_Values_m4195412189_MethodInfo_var);
		NullCheck(L_1);
		Enumerator_t3572817124  L_2 = ValueCollection_GetEnumerator_m4101546403(L_1, /*hidden argument*/ValueCollection_GetEnumerator_m4101546403_MethodInfo_var);
		V_1 = L_2;
	}

IL_0011:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0022;
		}

IL_0013:
		{
			WordAbstractBehaviour_t2878458725 * L_3 = Enumerator_get_Current_m3342904645((&V_1), /*hidden argument*/Enumerator_get_Current_m3342904645_MethodInfo_var);
			V_0 = L_3;
			WordAbstractBehaviour_t2878458725 * L_4 = V_0;
			NullCheck(L_4);
			VirtActionInvoker1< int32_t >::Invoke(21 /* System.Void Vuforia.TrackableBehaviour::OnTrackerUpdate(Vuforia.TrackableBehaviour/Status) */, L_4, (-1));
		}

IL_0022:
		{
			bool L_5 = Enumerator_MoveNext_m3611162142((&V_1), /*hidden argument*/Enumerator_MoveNext_m3611162142_MethodInfo_var);
			if (L_5)
			{
				goto IL_0013;
			}
		}

IL_002b:
		{
			IL2CPP_LEAVE(0x3B, FINALLY_002d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_002d;
	}

FINALLY_002d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3089730385((&V_1), /*hidden argument*/Enumerator_Dispose_m3089730385_MethodInfo_var);
		IL2CPP_END_FINALLY(45)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(45)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void Vuforia.WordManagerImpl::CleanupWordBehaviours()
extern const MethodInfo* List_1_Clear_m2388774782_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m1888356359_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Clear_m612749752_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Clear_m8975922_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Clear_m2383915171_MethodInfo_var;
extern const uint32_t WordManagerImpl_CleanupWordBehaviours_m3463819555_MetadataUsageId;
extern "C"  void WordManagerImpl_CleanupWordBehaviours_m3463819555 (WordManagerImpl_t4282786523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_CleanupWordBehaviours_m3463819555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1284628329 * L_0 = __this->get_mNewWords_2();
		NullCheck(L_0);
		List_1_Clear_m2388774782(L_0, /*hidden argument*/List_1_Clear_m2388774782_MethodInfo_var);
		List_1_t3241240618 * L_1 = __this->get_mLostWords_3();
		NullCheck(L_1);
		List_1_Clear_m1888356359(L_1, /*hidden argument*/List_1_Clear_m1888356359_MethodInfo_var);
		List_1_t3241240618 * L_2 = __this->get_mWaitingQueue_6();
		NullCheck(L_2);
		List_1_Clear_m1888356359(L_2, /*hidden argument*/List_1_Clear_m1888356359_MethodInfo_var);
		Dictionary_2_t923332832 * L_3 = __this->get_mTrackedWords_1();
		NullCheck(L_3);
		Dictionary_2_Clear_m612749752(L_3, /*hidden argument*/Dictionary_2_Clear_m612749752_MethodInfo_var);
		Dictionary_2_t1886284360 * L_4 = __this->get_mActiveWordBehaviours_4();
		NullCheck(L_4);
		Dictionary_2_Clear_m8975922(L_4, /*hidden argument*/Dictionary_2_Clear_m8975922_MethodInfo_var);
		Dictionary_2_t4162359119 * L_5 = __this->get_mWordBehaviours_7();
		NullCheck(L_5);
		Dictionary_2_Clear_m2383915171(L_5, /*hidden argument*/Dictionary_2_Clear_m2383915171_MethodInfo_var);
		return;
	}
}
// System.Void Vuforia.WordManagerImpl::UpdateWords(System.Collections.Generic.IEnumerable`1<Vuforia.VuforiaManagerImpl/WordData>,System.Collections.Generic.IEnumerable`1<Vuforia.VuforiaManagerImpl/WordResultData>)
extern Il2CppClass* IEnumerable_1_t1566091129_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t3044455207_il2cpp_TypeInfo_var;
extern Il2CppClass* Marshal_t785896760_il2cpp_TypeInfo_var;
extern Il2CppClass* WordImpl_t1843145168_il2cpp_TypeInfo_var;
extern Il2CppClass* WordResultImpl_t911273601_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1440998580_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t4034363676_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1217760458_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Clear_m2388774782_MethodInfo_var;
extern const MethodInfo* List_1_Clear_m1888356359_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m1381125446_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m1160437991_MethodInfo_var;
extern const MethodInfo* List_1_Add_m1260916951_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2876066959_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2828939739_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Keys_m3651855137_MethodInfo_var;
extern const MethodInfo* Enumerable_ToList_TisInt32_t2071877448_m2886835297_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m633037102_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2400786426_MethodInfo_var;
extern const MethodInfo* List_1_Contains_m1136004261_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m965870205_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3079565742_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m2087782974_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2584216872_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m1274756239_MethodInfo_var;
extern const uint32_t WordManagerImpl_UpdateWords_m2132655282_MetadataUsageId;
extern "C"  void WordManagerImpl_UpdateWords_m2132655282 (WordManagerImpl_t4282786523 * __this, Il2CppObject* ___newWordData0, Il2CppObject* ___wordResults1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_UpdateWords_m2132655282_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	WordData_t1273964084  V_0;
	memset(&V_0, 0, sizeof(V_0));
	WordImpl_t1843145168 * V_1 = NULL;
	WordResultImpl_t911273601 * V_2 = NULL;
	List_1_t1440998580 * V_3 = NULL;
	WordResultData_t3742236631  V_4;
	memset(&V_4, 0, sizeof(V_4));
	List_1_t1440998580 * V_5 = NULL;
	int32_t V_6 = 0;
	Il2CppObject* V_7 = NULL;
	Il2CppObject* V_8 = NULL;
	Enumerator_t975728254  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1284628329 * L_0 = __this->get_mNewWords_2();
		NullCheck(L_0);
		List_1_Clear_m2388774782(L_0, /*hidden argument*/List_1_Clear_m2388774782_MethodInfo_var);
		List_1_t3241240618 * L_1 = __this->get_mLostWords_3();
		NullCheck(L_1);
		List_1_Clear_m1888356359(L_1, /*hidden argument*/List_1_Clear_m1888356359_MethodInfo_var);
		Il2CppObject* L_2 = ___newWordData0;
		NullCheck(L_2);
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.VuforiaManagerImpl/WordData>::GetEnumerator() */, IEnumerable_1_t1566091129_il2cpp_TypeInfo_var, L_2);
		V_7 = L_3;
	}

IL_001e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0082;
		}

IL_0020:
		{
			Il2CppObject* L_4 = V_7;
			NullCheck(L_4);
			WordData_t1273964084  L_5 = InterfaceFuncInvoker0< WordData_t1273964084  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.VuforiaManagerImpl/WordData>::get_Current() */, IEnumerator_1_t3044455207_il2cpp_TypeInfo_var, L_4);
			V_0 = L_5;
			Dictionary_2_t923332832 * L_6 = __this->get_mTrackedWords_1();
			int32_t L_7 = (&V_0)->get_id_1();
			NullCheck(L_6);
			bool L_8 = Dictionary_2_ContainsKey_m1381125446(L_6, L_7, /*hidden argument*/Dictionary_2_ContainsKey_m1381125446_MethodInfo_var);
			if (L_8)
			{
				goto IL_0082;
			}
		}

IL_003c:
		{
			int32_t L_9 = (&V_0)->get_id_1();
			IntPtr_t L_10 = (&V_0)->get_stringValue_0();
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_t785896760_il2cpp_TypeInfo_var);
			String_t* L_11 = Marshal_PtrToStringUni_m4287319443(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			Vector2_t2243707579  L_12 = (&V_0)->get_size_2();
			WordImpl_t1843145168 * L_13 = (WordImpl_t1843145168 *)il2cpp_codegen_object_new(WordImpl_t1843145168_il2cpp_TypeInfo_var);
			WordImpl__ctor_m2135802911(L_13, L_9, L_11, L_12, /*hidden argument*/NULL);
			V_1 = L_13;
			WordImpl_t1843145168 * L_14 = V_1;
			WordResultImpl_t911273601 * L_15 = (WordResultImpl_t911273601 *)il2cpp_codegen_object_new(WordResultImpl_t911273601_il2cpp_TypeInfo_var);
			WordResultImpl__ctor_m401521253(L_15, L_14, /*hidden argument*/NULL);
			V_2 = L_15;
			Dictionary_2_t923332832 * L_16 = __this->get_mTrackedWords_1();
			int32_t L_17 = (&V_0)->get_id_1();
			WordResultImpl_t911273601 * L_18 = V_2;
			NullCheck(L_16);
			Dictionary_2_Add_m1160437991(L_16, L_17, L_18, /*hidden argument*/Dictionary_2_Add_m1160437991_MethodInfo_var);
			List_1_t1284628329 * L_19 = __this->get_mNewWords_2();
			WordResultImpl_t911273601 * L_20 = V_2;
			NullCheck(L_19);
			List_1_Add_m1260916951(L_19, L_20, /*hidden argument*/List_1_Add_m1260916951_MethodInfo_var);
		}

IL_0082:
		{
			Il2CppObject* L_21 = V_7;
			NullCheck(L_21);
			bool L_22 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_21);
			if (L_22)
			{
				goto IL_0020;
			}
		}

IL_008b:
		{
			IL2CPP_LEAVE(0x99, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_23 = V_7;
			if (!L_23)
			{
				goto IL_0098;
			}
		}

IL_0091:
		{
			Il2CppObject* L_24 = V_7;
			NullCheck(L_24);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_24);
		}

IL_0098:
		{
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0x99, IL_0099)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0099:
	{
		List_1_t1440998580 * L_25 = (List_1_t1440998580 *)il2cpp_codegen_object_new(List_1_t1440998580_il2cpp_TypeInfo_var);
		List_1__ctor_m2876066959(L_25, /*hidden argument*/List_1__ctor_m2876066959_MethodInfo_var);
		V_3 = L_25;
		Il2CppObject* L_26 = ___wordResults1;
		NullCheck(L_26);
		Il2CppObject* L_27 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.VuforiaManagerImpl/WordResultData>::GetEnumerator() */, IEnumerable_1_t4034363676_il2cpp_TypeInfo_var, L_26);
		V_8 = L_27;
	}

IL_00a7:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00bf;
		}

IL_00a9:
		{
			Il2CppObject* L_28 = V_8;
			NullCheck(L_28);
			WordResultData_t3742236631  L_29 = InterfaceFuncInvoker0< WordResultData_t3742236631  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::get_Current() */, IEnumerator_1_t1217760458_il2cpp_TypeInfo_var, L_28);
			V_4 = L_29;
			List_1_t1440998580 * L_30 = V_3;
			int32_t L_31 = (&V_4)->get_id_3();
			NullCheck(L_30);
			List_1_Add_m2828939739(L_30, L_31, /*hidden argument*/List_1_Add_m2828939739_MethodInfo_var);
		}

IL_00bf:
		{
			Il2CppObject* L_32 = V_8;
			NullCheck(L_32);
			bool L_33 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_32);
			if (L_33)
			{
				goto IL_00a9;
			}
		}

IL_00c8:
		{
			IL2CPP_LEAVE(0xD6, FINALLY_00ca);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00ca;
	}

FINALLY_00ca:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_34 = V_8;
			if (!L_34)
			{
				goto IL_00d5;
			}
		}

IL_00ce:
		{
			Il2CppObject* L_35 = V_8;
			NullCheck(L_35);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_35);
		}

IL_00d5:
		{
			IL2CPP_END_FINALLY(202)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(202)
	{
		IL2CPP_JUMP_TBL(0xD6, IL_00d6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00d6:
	{
		Dictionary_2_t923332832 * L_36 = __this->get_mTrackedWords_1();
		NullCheck(L_36);
		KeyCollection_t3406830603 * L_37 = Dictionary_2_get_Keys_m3651855137(L_36, /*hidden argument*/Dictionary_2_get_Keys_m3651855137_MethodInfo_var);
		List_1_t1440998580 * L_38 = Enumerable_ToList_TisInt32_t2071877448_m2886835297(NULL /*static, unused*/, L_37, /*hidden argument*/Enumerable_ToList_TisInt32_t2071877448_m2886835297_MethodInfo_var);
		V_5 = L_38;
		List_1_t1440998580 * L_39 = V_5;
		NullCheck(L_39);
		Enumerator_t975728254  L_40 = List_1_GetEnumerator_m633037102(L_39, /*hidden argument*/List_1_GetEnumerator_m633037102_MethodInfo_var);
		V_9 = L_40;
	}

IL_00f1:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0131;
		}

IL_00f3:
		{
			int32_t L_41 = Enumerator_get_Current_m2400786426((&V_9), /*hidden argument*/Enumerator_get_Current_m2400786426_MethodInfo_var);
			V_6 = L_41;
			List_1_t1440998580 * L_42 = V_3;
			int32_t L_43 = V_6;
			NullCheck(L_42);
			bool L_44 = List_1_Contains_m1136004261(L_42, L_43, /*hidden argument*/List_1_Contains_m1136004261_MethodInfo_var);
			if (L_44)
			{
				goto IL_0131;
			}
		}

IL_0106:
		{
			List_1_t3241240618 * L_45 = __this->get_mLostWords_3();
			Dictionary_2_t923332832 * L_46 = __this->get_mTrackedWords_1();
			int32_t L_47 = V_6;
			NullCheck(L_46);
			WordResult_t1915507197 * L_48 = Dictionary_2_get_Item_m965870205(L_46, L_47, /*hidden argument*/Dictionary_2_get_Item_m965870205_MethodInfo_var);
			NullCheck(L_48);
			Il2CppObject * L_49 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* Vuforia.Word Vuforia.WordResult::get_Word() */, L_48);
			NullCheck(L_45);
			List_1_Add_m3079565742(L_45, L_49, /*hidden argument*/List_1_Add_m3079565742_MethodInfo_var);
			Dictionary_2_t923332832 * L_50 = __this->get_mTrackedWords_1();
			int32_t L_51 = V_6;
			NullCheck(L_50);
			Dictionary_2_Remove_m2087782974(L_50, L_51, /*hidden argument*/Dictionary_2_Remove_m2087782974_MethodInfo_var);
		}

IL_0131:
		{
			bool L_52 = Enumerator_MoveNext_m2584216872((&V_9), /*hidden argument*/Enumerator_MoveNext_m2584216872_MethodInfo_var);
			if (L_52)
			{
				goto IL_00f3;
			}
		}

IL_013a:
		{
			IL2CPP_LEAVE(0x14A, FINALLY_013c);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_013c;
	}

FINALLY_013c:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m1274756239((&V_9), /*hidden argument*/Enumerator_Dispose_m1274756239_MethodInfo_var);
		IL2CPP_END_FINALLY(316)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(316)
	{
		IL2CPP_JUMP_TBL(0x14A, IL_014a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_014a:
	{
		int32_t L_53 = __this->get_mWordPrefabCreationMode_10();
		if ((!(((uint32_t)L_53) == ((uint32_t)1))))
		{
			goto IL_015f;
		}
	}
	{
		WordManagerImpl_UnregisterLostWords_m1604343620(__this, /*hidden argument*/NULL);
		WordManagerImpl_AssociateWordResultsWithBehaviours_m2500550211(__this, /*hidden argument*/NULL);
	}

IL_015f:
	{
		return;
	}
}
// System.Void Vuforia.WordManagerImpl::UpdateWordResultPoses(UnityEngine.Transform,System.Collections.Generic.IEnumerable`1<Vuforia.VuforiaManagerImpl/WordResultData>)
extern const Il2CppType* VuforiaAbstractBehaviour_t3319870759_0_0_0_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaAbstractBehaviour_t3319870759_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* CameraDevice_t3827827595_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerable_1_t4034363676_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_1_t1217760458_il2cpp_TypeInfo_var;
extern Il2CppClass* WordResultImpl_t911273601_il2cpp_TypeInfo_var;
extern Il2CppClass* VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m965870205_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral829717964;
extern const uint32_t WordManagerImpl_UpdateWordResultPoses_m1487928222_MetadataUsageId;
extern "C"  void WordManagerImpl_UpdateWordResultPoses_m1487928222 (WordManagerImpl_t4282786523 * __this, Transform_t3275118058 * ___arCameraTransform0, Il2CppObject* ___wordResults1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_UpdateWordResultPoses_m1487928222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t3681755626  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	VideoModeData_t3451594282  V_2;
	memset(&V_2, 0, sizeof(V_2));
	WordResultData_t3742236631  V_3;
	memset(&V_3, 0, sizeof(V_3));
	WordResultImpl_t911273601 * V_4 = NULL;
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Quaternion_t4030073918  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Quaternion_t4030073918  V_7;
	memset(&V_7, 0, sizeof(V_7));
	OrientedBoundingBox_t3172429123  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Il2CppObject* V_9 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		VuforiaAbstractBehaviour_t3319870759 * L_0 = __this->get_mVuforiaBehaviour_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(VuforiaAbstractBehaviour_t3319870759_0_0_0_var), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_t1021602117 * L_3 = Object_FindObjectOfType_m2330404063(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		__this->set_mVuforiaBehaviour_11(((VuforiaAbstractBehaviour_t3319870759 *)CastclassClass(L_3, VuforiaAbstractBehaviour_t3319870759_il2cpp_TypeInfo_var)));
		VuforiaAbstractBehaviour_t3319870759 * L_4 = __this->get_mVuforiaBehaviour_11();
		bool L_5 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_4, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0041;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral829717964, /*hidden argument*/NULL);
		return;
	}

IL_0041:
	{
		VuforiaAbstractBehaviour_t3319870759 * L_6 = __this->get_mVuforiaBehaviour_11();
		NullCheck(L_6);
		Rect_t3681755626  L_7 = VuforiaAbstractBehaviour_GetVideoBackgroundRectInViewPort_m980857609(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		VuforiaAbstractBehaviour_t3319870759 * L_8 = __this->get_mVuforiaBehaviour_11();
		NullCheck(L_8);
		int32_t L_9 = VuforiaAbstractBehaviour_get_VideoBackGroundMirrored_m2667958568(L_8, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_9) == ((int32_t)1))? 1 : 0);
		IL2CPP_RUNTIME_CLASS_INIT(CameraDevice_t3827827595_il2cpp_TypeInfo_var);
		CameraDevice_t3827827595 * L_10 = CameraDevice_get_Instance_m197511331(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_10);
		VideoModeData_t3451594282  L_11 = VirtFuncInvoker0< VideoModeData_t3451594282  >::Invoke(9 /* Vuforia.CameraDevice/VideoModeData Vuforia.CameraDevice::GetVideoMode() */, L_10);
		V_2 = L_11;
		Il2CppObject* L_12 = ___wordResults1;
		NullCheck(L_12);
		Il2CppObject* L_13 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<Vuforia.VuforiaManagerImpl/WordResultData>::GetEnumerator() */, IEnumerable_1_t4034363676_il2cpp_TypeInfo_var, L_12);
		V_9 = L_13;
	}

IL_006f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_012f;
		}

IL_0074:
		{
			Il2CppObject* L_14 = V_9;
			NullCheck(L_14);
			WordResultData_t3742236631  L_15 = InterfaceFuncInvoker0< WordResultData_t3742236631  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<Vuforia.VuforiaManagerImpl/WordResultData>::get_Current() */, IEnumerator_1_t1217760458_il2cpp_TypeInfo_var, L_14);
			V_3 = L_15;
			Dictionary_2_t923332832 * L_16 = __this->get_mTrackedWords_1();
			int32_t L_17 = (&V_3)->get_id_3();
			NullCheck(L_16);
			WordResult_t1915507197 * L_18 = Dictionary_2_get_Item_m965870205(L_16, L_17, /*hidden argument*/Dictionary_2_get_Item_m965870205_MethodInfo_var);
			V_4 = ((WordResultImpl_t911273601 *)CastclassClass(L_18, WordResultImpl_t911273601_il2cpp_TypeInfo_var));
			Transform_t3275118058 * L_19 = ___arCameraTransform0;
			PoseData_t1264148721  L_20 = (&V_3)->get_pose_0();
			Vector3_t2243707580  L_21 = L_20.get_position_0();
			NullCheck(L_19);
			Vector3_t2243707580  L_22 = Transform_TransformPoint_m3272254198(L_19, L_21, /*hidden argument*/NULL);
			V_5 = L_22;
			PoseData_t1264148721  L_23 = (&V_3)->get_pose_0();
			Quaternion_t4030073918  L_24 = L_23.get_orientation_1();
			V_6 = L_24;
			Transform_t3275118058 * L_25 = ___arCameraTransform0;
			NullCheck(L_25);
			Quaternion_t4030073918  L_26 = Transform_get_rotation_m1033555130(L_25, /*hidden argument*/NULL);
			Quaternion_t4030073918  L_27 = V_6;
			Quaternion_t4030073918  L_28 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
			Vector3_t2243707580  L_29 = Vector3_get_left_m2429378123(NULL /*static, unused*/, /*hidden argument*/NULL);
			Quaternion_t4030073918  L_30 = Quaternion_AngleAxis_m2806222563(NULL /*static, unused*/, (270.0f), L_29, /*hidden argument*/NULL);
			Quaternion_t4030073918  L_31 = Quaternion_op_Multiply_m2426727589(NULL /*static, unused*/, L_28, L_30, /*hidden argument*/NULL);
			V_7 = L_31;
			WordResultImpl_t911273601 * L_32 = V_4;
			Vector3_t2243707580  L_33 = V_5;
			Quaternion_t4030073918  L_34 = V_7;
			NullCheck(L_32);
			WordResultImpl_SetPose_m3664988988(L_32, L_33, L_34, /*hidden argument*/NULL);
			WordResultImpl_t911273601 * L_35 = V_4;
			int32_t L_36 = (&V_3)->get_status_2();
			NullCheck(L_35);
			WordResultImpl_SetStatus_m3989517476(L_35, L_36, /*hidden argument*/NULL);
			Obb2D_t3491121689  L_37 = (&V_3)->get_orientedBoundingBox_4();
			Vector2_t2243707579  L_38 = L_37.get_center_0();
			Obb2D_t3491121689  L_39 = (&V_3)->get_orientedBoundingBox_4();
			Vector2_t2243707579  L_40 = L_39.get_halfExtents_1();
			Obb2D_t3491121689  L_41 = (&V_3)->get_orientedBoundingBox_4();
			float L_42 = L_41.get_rotation_2();
			OrientedBoundingBox__ctor_m1614144038((&V_8), L_38, L_40, L_42, /*hidden argument*/NULL);
			WordResultImpl_t911273601 * L_43 = V_4;
			OrientedBoundingBox_t3172429123  L_44 = V_8;
			Rect_t3681755626  L_45 = V_0;
			bool L_46 = V_1;
			VideoModeData_t3451594282  L_47 = V_2;
			IL2CPP_RUNTIME_CLASS_INIT(VuforiaRuntimeUtilities_t3083157244_il2cpp_TypeInfo_var);
			OrientedBoundingBox_t3172429123  L_48 = VuforiaRuntimeUtilities_CameraFrameToScreenSpaceCoordinates_m3404428623(NULL /*static, unused*/, L_44, L_45, L_46, L_47, /*hidden argument*/NULL);
			NullCheck(L_43);
			WordResultImpl_SetObb_m82808223(L_43, L_48, /*hidden argument*/NULL);
		}

IL_012f:
		{
			Il2CppObject* L_49 = V_9;
			NullCheck(L_49);
			bool L_50 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_49);
			if (L_50)
			{
				goto IL_0074;
			}
		}

IL_013b:
		{
			IL2CPP_LEAVE(0x149, FINALLY_013d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_013d;
	}

FINALLY_013d:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_51 = V_9;
			if (!L_51)
			{
				goto IL_0148;
			}
		}

IL_0141:
		{
			Il2CppObject* L_52 = V_9;
			NullCheck(L_52);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_52);
		}

IL_0148:
		{
			IL2CPP_END_FINALLY(317)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(317)
	{
		IL2CPP_JUMP_TBL(0x149, IL_0149)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0149:
	{
		int32_t L_53 = __this->get_mWordPrefabCreationMode_10();
		if ((!(((uint32_t)L_53) == ((uint32_t)1))))
		{
			goto IL_0158;
		}
	}
	{
		WordManagerImpl_UpdateWordBehaviourPoses_m93294755(__this, /*hidden argument*/NULL);
	}

IL_0158:
	{
		return;
	}
}
// System.Void Vuforia.WordManagerImpl::AssociateWordResultsWithBehaviours()
extern Il2CppClass* List_1_t3241240618_il2cpp_TypeInfo_var;
extern Il2CppClass* Trackable_t432275407_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3023380462_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2809331959_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1259914695_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m1381125446_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m965870205_MethodInfo_var;
extern const MethodInfo* List_1_Remove_m1388207957_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m6674267_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2589868092_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1549224580_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m4132458184_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3079565742_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2648856890_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m4163284387_MethodInfo_var;
extern const uint32_t WordManagerImpl_AssociateWordResultsWithBehaviours_m2500550211_MetadataUsageId;
extern "C"  void WordManagerImpl_AssociateWordResultsWithBehaviours_m2500550211 (WordManagerImpl_t4282786523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_AssociateWordResultsWithBehaviours_m2500550211_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t3241240618 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	WordResult_t1915507197 * V_2 = NULL;
	WordResult_t1915507197 * V_3 = NULL;
	WordAbstractBehaviour_t2878458725 * V_4 = NULL;
	Enumerator_t2775970292  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Enumerator_t819358003  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3241240618 * L_0 = __this->get_mWaitingQueue_6();
		List_1_t3241240618 * L_1 = (List_1_t3241240618 *)il2cpp_codegen_object_new(List_1_t3241240618_il2cpp_TypeInfo_var);
		List_1__ctor_m3023380462(L_1, L_0, /*hidden argument*/List_1__ctor_m3023380462_MethodInfo_var);
		V_0 = L_1;
		List_1_t3241240618 * L_2 = V_0;
		NullCheck(L_2);
		Enumerator_t2775970292  L_3 = List_1_GetEnumerator_m2809331959(L_2, /*hidden argument*/List_1_GetEnumerator_m2809331959_MethodInfo_var);
		V_5 = L_3;
	}

IL_0014:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006e;
		}

IL_0016:
		{
			Il2CppObject * L_4 = Enumerator_get_Current_m1259914695((&V_5), /*hidden argument*/Enumerator_get_Current_m1259914695_MethodInfo_var);
			V_1 = L_4;
			Dictionary_2_t923332832 * L_5 = __this->get_mTrackedWords_1();
			Il2CppObject * L_6 = V_1;
			NullCheck(L_6);
			int32_t L_7 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t432275407_il2cpp_TypeInfo_var, L_6);
			NullCheck(L_5);
			bool L_8 = Dictionary_2_ContainsKey_m1381125446(L_5, L_7, /*hidden argument*/Dictionary_2_ContainsKey_m1381125446_MethodInfo_var);
			if (!L_8)
			{
				goto IL_0061;
			}
		}

IL_0031:
		{
			Dictionary_2_t923332832 * L_9 = __this->get_mTrackedWords_1();
			Il2CppObject * L_10 = V_1;
			NullCheck(L_10);
			int32_t L_11 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t432275407_il2cpp_TypeInfo_var, L_10);
			NullCheck(L_9);
			WordResult_t1915507197 * L_12 = Dictionary_2_get_Item_m965870205(L_9, L_11, /*hidden argument*/Dictionary_2_get_Item_m965870205_MethodInfo_var);
			V_2 = L_12;
			WordResult_t1915507197 * L_13 = V_2;
			WordAbstractBehaviour_t2878458725 * L_14 = WordManagerImpl_AssociateWordBehaviour_m1493851823(__this, L_13, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_15 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_14, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_15)
			{
				goto IL_006e;
			}
		}

IL_0052:
		{
			List_1_t3241240618 * L_16 = __this->get_mWaitingQueue_6();
			Il2CppObject * L_17 = V_1;
			NullCheck(L_16);
			List_1_Remove_m1388207957(L_16, L_17, /*hidden argument*/List_1_Remove_m1388207957_MethodInfo_var);
			goto IL_006e;
		}

IL_0061:
		{
			List_1_t3241240618 * L_18 = __this->get_mWaitingQueue_6();
			Il2CppObject * L_19 = V_1;
			NullCheck(L_18);
			List_1_Remove_m1388207957(L_18, L_19, /*hidden argument*/List_1_Remove_m1388207957_MethodInfo_var);
		}

IL_006e:
		{
			bool L_20 = Enumerator_MoveNext_m6674267((&V_5), /*hidden argument*/Enumerator_MoveNext_m6674267_MethodInfo_var);
			if (L_20)
			{
				goto IL_0016;
			}
		}

IL_0077:
		{
			IL2CPP_LEAVE(0x87, FINALLY_0079);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2589868092((&V_5), /*hidden argument*/Enumerator_Dispose_m2589868092_MethodInfo_var);
		IL2CPP_END_FINALLY(121)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_JUMP_TBL(0x87, IL_0087)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0087:
	{
		List_1_t1284628329 * L_21 = __this->get_mNewWords_2();
		NullCheck(L_21);
		Enumerator_t819358003  L_22 = List_1_GetEnumerator_m1549224580(L_21, /*hidden argument*/List_1_GetEnumerator_m1549224580_MethodInfo_var);
		V_6 = L_22;
	}

IL_0094:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00c2;
		}

IL_0096:
		{
			WordResult_t1915507197 * L_23 = Enumerator_get_Current_m4132458184((&V_6), /*hidden argument*/Enumerator_get_Current_m4132458184_MethodInfo_var);
			V_3 = L_23;
			WordResult_t1915507197 * L_24 = V_3;
			WordAbstractBehaviour_t2878458725 * L_25 = WordManagerImpl_AssociateWordBehaviour_m1493851823(__this, L_24, /*hidden argument*/NULL);
			V_4 = L_25;
			WordAbstractBehaviour_t2878458725 * L_26 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_27 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_26, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_27)
			{
				goto IL_00c2;
			}
		}

IL_00b1:
		{
			List_1_t3241240618 * L_28 = __this->get_mWaitingQueue_6();
			WordResult_t1915507197 * L_29 = V_3;
			NullCheck(L_29);
			Il2CppObject * L_30 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* Vuforia.Word Vuforia.WordResult::get_Word() */, L_29);
			NullCheck(L_28);
			List_1_Add_m3079565742(L_28, L_30, /*hidden argument*/List_1_Add_m3079565742_MethodInfo_var);
		}

IL_00c2:
		{
			bool L_31 = Enumerator_MoveNext_m2648856890((&V_6), /*hidden argument*/Enumerator_MoveNext_m2648856890_MethodInfo_var);
			if (L_31)
			{
				goto IL_0096;
			}
		}

IL_00cb:
		{
			IL2CPP_LEAVE(0xDB, FINALLY_00cd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00cd;
	}

FINALLY_00cd:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4163284387((&V_6), /*hidden argument*/Enumerator_Dispose_m4163284387_MethodInfo_var);
		IL2CPP_END_FINALLY(205)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(205)
	{
		IL2CPP_JUMP_TBL(0xDB, IL_00db)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00db:
	{
		return;
	}
}
// System.Void Vuforia.WordManagerImpl::UnregisterLostWords()
extern Il2CppClass* Trackable_t432275407_il2cpp_TypeInfo_var;
extern Il2CppClass* IEditorTrackableBehaviour_t2323791534_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2809331959_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1259914695_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m2090172236_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m1626704729_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Remove_m2717389388_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m6674267_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2589868092_MethodInfo_var;
extern const uint32_t WordManagerImpl_UnregisterLostWords_m1604343620_MetadataUsageId;
extern "C"  void WordManagerImpl_UnregisterLostWords_m1604343620 (WordManagerImpl_t4282786523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_UnregisterLostWords_m1604343620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	WordAbstractBehaviour_t2878458725 * V_1 = NULL;
	Enumerator_t2775970292  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3241240618 * L_0 = __this->get_mLostWords_3();
		NullCheck(L_0);
		Enumerator_t2775970292  L_1 = List_1_GetEnumerator_m2809331959(L_0, /*hidden argument*/List_1_GetEnumerator_m2809331959_MethodInfo_var);
		V_2 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005a;
		}

IL_000e:
		{
			Il2CppObject * L_2 = Enumerator_get_Current_m1259914695((&V_2), /*hidden argument*/Enumerator_get_Current_m1259914695_MethodInfo_var);
			V_0 = L_2;
			Dictionary_2_t1886284360 * L_3 = __this->get_mActiveWordBehaviours_4();
			Il2CppObject * L_4 = V_0;
			NullCheck(L_4);
			int32_t L_5 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t432275407_il2cpp_TypeInfo_var, L_4);
			NullCheck(L_3);
			bool L_6 = Dictionary_2_ContainsKey_m2090172236(L_3, L_5, /*hidden argument*/Dictionary_2_ContainsKey_m2090172236_MethodInfo_var);
			if (!L_6)
			{
				goto IL_005a;
			}
		}

IL_0029:
		{
			Dictionary_2_t1886284360 * L_7 = __this->get_mActiveWordBehaviours_4();
			Il2CppObject * L_8 = V_0;
			NullCheck(L_8);
			int32_t L_9 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t432275407_il2cpp_TypeInfo_var, L_8);
			NullCheck(L_7);
			WordAbstractBehaviour_t2878458725 * L_10 = Dictionary_2_get_Item_m1626704729(L_7, L_9, /*hidden argument*/Dictionary_2_get_Item_m1626704729_MethodInfo_var);
			V_1 = L_10;
			WordAbstractBehaviour_t2878458725 * L_11 = V_1;
			NullCheck(L_11);
			VirtActionInvoker1< int32_t >::Invoke(21 /* System.Void Vuforia.TrackableBehaviour::OnTrackerUpdate(Vuforia.TrackableBehaviour/Status) */, L_11, (-1));
			WordAbstractBehaviour_t2878458725 * L_12 = V_1;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(10 /* System.Void Vuforia.IEditorTrackableBehaviour::UnregisterTrackable() */, IEditorTrackableBehaviour_t2323791534_il2cpp_TypeInfo_var, L_12);
			Dictionary_2_t1886284360 * L_13 = __this->get_mActiveWordBehaviours_4();
			Il2CppObject * L_14 = V_0;
			NullCheck(L_14);
			int32_t L_15 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t432275407_il2cpp_TypeInfo_var, L_14);
			NullCheck(L_13);
			Dictionary_2_Remove_m2717389388(L_13, L_15, /*hidden argument*/Dictionary_2_Remove_m2717389388_MethodInfo_var);
		}

IL_005a:
		{
			bool L_16 = Enumerator_MoveNext_m6674267((&V_2), /*hidden argument*/Enumerator_MoveNext_m6674267_MethodInfo_var);
			if (L_16)
			{
				goto IL_000e;
			}
		}

IL_0063:
		{
			IL2CPP_LEAVE(0x73, FINALLY_0065);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0065;
	}

FINALLY_0065:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2589868092((&V_2), /*hidden argument*/Enumerator_Dispose_m2589868092_MethodInfo_var);
		IL2CPP_END_FINALLY(101)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(101)
	{
		IL2CPP_JUMP_TBL(0x73, IL_0073)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0073:
	{
		return;
	}
}
// System.Void Vuforia.WordManagerImpl::UpdateWordBehaviourPoses()
extern Il2CppClass* Word_t3872119486_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m772489251_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1973866435_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m384757977_MethodInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m1381125446_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m965870205_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m1675843494_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3640556268_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3961145619_MethodInfo_var;
extern const uint32_t WordManagerImpl_UpdateWordBehaviourPoses_m93294755_MetadataUsageId;
extern "C"  void WordManagerImpl_UpdateWordBehaviourPoses_m93294755 (WordManagerImpl_t4282786523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_UpdateWordBehaviourPoses_m93294755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	KeyValuePair_2_t3938596878  V_0;
	memset(&V_0, 0, sizeof(V_0));
	WordResult_t1915507197 * V_1 = NULL;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Quaternion_t4030073918  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2243707579  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t2243707580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Enumerator_t3206309062  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t1886284360 * L_0 = __this->get_mActiveWordBehaviours_4();
		NullCheck(L_0);
		Enumerator_t3206309062  L_1 = Dictionary_2_GetEnumerator_m772489251(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m772489251_MethodInfo_var);
		V_6 = L_1;
	}

IL_000d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00da;
		}

IL_0012:
		{
			KeyValuePair_2_t3938596878  L_2 = Enumerator_get_Current_m1973866435((&V_6), /*hidden argument*/Enumerator_get_Current_m1973866435_MethodInfo_var);
			V_0 = L_2;
			Dictionary_2_t923332832 * L_3 = __this->get_mTrackedWords_1();
			int32_t L_4 = KeyValuePair_2_get_Key_m384757977((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m384757977_MethodInfo_var);
			NullCheck(L_3);
			bool L_5 = Dictionary_2_ContainsKey_m1381125446(L_3, L_4, /*hidden argument*/Dictionary_2_ContainsKey_m1381125446_MethodInfo_var);
			if (!L_5)
			{
				goto IL_00da;
			}
		}

IL_0031:
		{
			Dictionary_2_t923332832 * L_6 = __this->get_mTrackedWords_1();
			int32_t L_7 = KeyValuePair_2_get_Key_m384757977((&V_0), /*hidden argument*/KeyValuePair_2_get_Key_m384757977_MethodInfo_var);
			NullCheck(L_6);
			WordResult_t1915507197 * L_8 = Dictionary_2_get_Item_m965870205(L_6, L_7, /*hidden argument*/Dictionary_2_get_Item_m965870205_MethodInfo_var);
			V_1 = L_8;
			WordResult_t1915507197 * L_9 = V_1;
			NullCheck(L_9);
			Vector3_t2243707580  L_10 = VirtFuncInvoker0< Vector3_t2243707580  >::Invoke(6 /* UnityEngine.Vector3 Vuforia.WordResult::get_Position() */, L_9);
			V_2 = L_10;
			WordResult_t1915507197 * L_11 = V_1;
			NullCheck(L_11);
			Quaternion_t4030073918  L_12 = VirtFuncInvoker0< Quaternion_t4030073918  >::Invoke(7 /* UnityEngine.Quaternion Vuforia.WordResult::get_Orientation() */, L_11);
			V_3 = L_12;
			WordResult_t1915507197 * L_13 = V_1;
			NullCheck(L_13);
			Il2CppObject * L_14 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* Vuforia.Word Vuforia.WordResult::get_Word() */, L_13);
			NullCheck(L_14);
			Vector2_t2243707579  L_15 = InterfaceFuncInvoker0< Vector2_t2243707579  >::Invoke(1 /* UnityEngine.Vector2 Vuforia.Word::get_Size() */, Word_t3872119486_il2cpp_TypeInfo_var, L_14);
			V_4 = L_15;
			WordAbstractBehaviour_t2878458725 * L_16 = KeyValuePair_2_get_Value_m1675843494((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1675843494_MethodInfo_var);
			NullCheck(L_16);
			Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(L_16, /*hidden argument*/NULL);
			Quaternion_t4030073918  L_18 = V_3;
			NullCheck(L_17);
			Transform_set_rotation_m3411284563(L_17, L_18, /*hidden argument*/NULL);
			WordAbstractBehaviour_t2878458725 * L_19 = KeyValuePair_2_get_Value_m1675843494((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1675843494_MethodInfo_var);
			NullCheck(L_19);
			Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(L_19, /*hidden argument*/NULL);
			NullCheck(L_20);
			Quaternion_t4030073918  L_21 = Transform_get_rotation_m1033555130(L_20, /*hidden argument*/NULL);
			float L_22 = (&V_4)->get_x_1();
			float L_23 = (&V_4)->get_y_2();
			Vector3_t2243707580  L_24;
			memset(&L_24, 0, sizeof(L_24));
			Vector3__ctor_m2638739322(&L_24, ((float)((float)((-L_22))*(float)(0.5f))), (0.0f), ((float)((float)((-L_23))*(float)(0.5f))), /*hidden argument*/NULL);
			Vector3_t2243707580  L_25 = Quaternion_op_Multiply_m1483423721(NULL /*static, unused*/, L_21, L_24, /*hidden argument*/NULL);
			V_5 = L_25;
			WordAbstractBehaviour_t2878458725 * L_26 = KeyValuePair_2_get_Value_m1675843494((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1675843494_MethodInfo_var);
			NullCheck(L_26);
			Transform_t3275118058 * L_27 = Component_get_transform_m2697483695(L_26, /*hidden argument*/NULL);
			Vector3_t2243707580  L_28 = V_2;
			Vector3_t2243707580  L_29 = V_5;
			Vector3_t2243707580  L_30 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
			NullCheck(L_27);
			Transform_set_position_m2469242620(L_27, L_30, /*hidden argument*/NULL);
			WordAbstractBehaviour_t2878458725 * L_31 = KeyValuePair_2_get_Value_m1675843494((&V_0), /*hidden argument*/KeyValuePair_2_get_Value_m1675843494_MethodInfo_var);
			WordResult_t1915507197 * L_32 = V_1;
			NullCheck(L_32);
			int32_t L_33 = VirtFuncInvoker0< int32_t >::Invoke(8 /* Vuforia.TrackableBehaviour/Status Vuforia.WordResult::get_CurrentStatus() */, L_32);
			NullCheck(L_31);
			VirtActionInvoker1< int32_t >::Invoke(21 /* System.Void Vuforia.TrackableBehaviour::OnTrackerUpdate(Vuforia.TrackableBehaviour/Status) */, L_31, L_33);
		}

IL_00da:
		{
			bool L_34 = Enumerator_MoveNext_m3640556268((&V_6), /*hidden argument*/Enumerator_MoveNext_m3640556268_MethodInfo_var);
			if (L_34)
			{
				goto IL_0012;
			}
		}

IL_00e6:
		{
			IL2CPP_LEAVE(0xF6, FINALLY_00e8);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00e8;
	}

FINALLY_00e8:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3961145619((&V_6), /*hidden argument*/Enumerator_Dispose_m3961145619_MethodInfo_var);
		IL2CPP_END_FINALLY(232)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(232)
	{
		IL2CPP_JUMP_TBL(0xF6, IL_00f6)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00f6:
	{
		return;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.WordManagerImpl::AssociateWordBehaviour(Vuforia.WordResult)
extern Il2CppClass* Word_t3872119486_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m4147027175_MethodInfo_var;
extern const MethodInfo* Dictionary_2_get_Item_m1779921936_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2655044316_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1612811480_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1529797818_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3163303335_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m3798745807_MethodInfo_var;
extern const MethodInfo* Enumerable_First_TisWordAbstractBehaviour_t2878458725_m1729057050_MethodInfo_var;
extern const MethodInfo* List_1_Add_m342378119_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1328721296;
extern Il2CppCodeGenString* _stringLiteral156991511;
extern const uint32_t WordManagerImpl_AssociateWordBehaviour_m1493851823_MetadataUsageId;
extern "C"  WordAbstractBehaviour_t2878458725 * WordManagerImpl_AssociateWordBehaviour_m1493851823 (WordManagerImpl_t4282786523 * __this, WordResult_t1915507197 * ___wordResult0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_AssociateWordBehaviour_m1493851823_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	List_1_t2247579857 * V_1 = NULL;
	WordAbstractBehaviour_t2878458725 * V_2 = NULL;
	WordAbstractBehaviour_t2878458725 * V_3 = NULL;
	WordAbstractBehaviour_t2878458725 * V_4 = NULL;
	Enumerator_t1782309531  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		WordResult_t1915507197 * L_0 = ___wordResult0;
		NullCheck(L_0);
		Il2CppObject * L_1 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* Vuforia.Word Vuforia.WordResult::get_Word() */, L_0);
		NullCheck(L_1);
		String_t* L_2 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.Word::get_StringValue() */, Word_t3872119486_il2cpp_TypeInfo_var, L_1);
		NullCheck(L_2);
		String_t* L_3 = String_ToLowerInvariant_m3152527487(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Dictionary_2_t4162359119 * L_4 = __this->get_mWordBehaviours_7();
		String_t* L_5 = V_0;
		NullCheck(L_4);
		bool L_6 = Dictionary_2_ContainsKey_m4147027175(L_4, L_5, /*hidden argument*/Dictionary_2_ContainsKey_m4147027175_MethodInfo_var);
		if (!L_6)
		{
			goto IL_002e;
		}
	}
	{
		Dictionary_2_t4162359119 * L_7 = __this->get_mWordBehaviours_7();
		String_t* L_8 = V_0;
		NullCheck(L_7);
		List_1_t2247579857 * L_9 = Dictionary_2_get_Item_m1779921936(L_7, L_8, /*hidden argument*/Dictionary_2_get_Item_m1779921936_MethodInfo_var);
		V_1 = L_9;
		goto IL_0065;
	}

IL_002e:
	{
		Dictionary_2_t4162359119 * L_10 = __this->get_mWordBehaviours_7();
		NullCheck(L_10);
		bool L_11 = Dictionary_2_ContainsKey_m4147027175(L_10, _stringLiteral1328721296, /*hidden argument*/Dictionary_2_ContainsKey_m4147027175_MethodInfo_var);
		if (!L_11)
		{
			goto IL_0053;
		}
	}
	{
		Dictionary_2_t4162359119 * L_12 = __this->get_mWordBehaviours_7();
		NullCheck(L_12);
		List_1_t2247579857 * L_13 = Dictionary_2_get_Item_m1779921936(L_12, _stringLiteral1328721296, /*hidden argument*/Dictionary_2_get_Item_m1779921936_MethodInfo_var);
		V_1 = L_13;
		goto IL_0065;
	}

IL_0053:
	{
		String_t* L_14 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral156991511, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		return (WordAbstractBehaviour_t2878458725 *)NULL;
	}

IL_0065:
	{
		List_1_t2247579857 * L_16 = V_1;
		NullCheck(L_16);
		Enumerator_t1782309531  L_17 = List_1_GetEnumerator_m2655044316(L_16, /*hidden argument*/List_1_GetEnumerator_m2655044316_MethodInfo_var);
		V_5 = L_17;
	}

IL_006d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_008b;
		}

IL_006f:
		{
			WordAbstractBehaviour_t2878458725 * L_18 = Enumerator_get_Current_m1612811480((&V_5), /*hidden argument*/Enumerator_get_Current_m1612811480_MethodInfo_var);
			V_2 = L_18;
			WordAbstractBehaviour_t2878458725 * L_19 = V_2;
			NullCheck(L_19);
			Il2CppObject * L_20 = VirtFuncInvoker0< Il2CppObject * >::Invoke(20 /* Vuforia.Trackable Vuforia.TrackableBehaviour::get_Trackable() */, L_19);
			if (L_20)
			{
				goto IL_008b;
			}
		}

IL_007f:
		{
			WordResult_t1915507197 * L_21 = ___wordResult0;
			WordAbstractBehaviour_t2878458725 * L_22 = V_2;
			WordAbstractBehaviour_t2878458725 * L_23 = WordManagerImpl_AssociateWordBehaviour_m3359416954(__this, L_21, L_22, /*hidden argument*/NULL);
			V_4 = L_23;
			IL2CPP_LEAVE(0xD0, FINALLY_0096);
		}

IL_008b:
		{
			bool L_24 = Enumerator_MoveNext_m1529797818((&V_5), /*hidden argument*/Enumerator_MoveNext_m1529797818_MethodInfo_var);
			if (L_24)
			{
				goto IL_006f;
			}
		}

IL_0094:
		{
			IL2CPP_LEAVE(0xA4, FINALLY_0096);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0096;
	}

FINALLY_0096:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3163303335((&V_5), /*hidden argument*/Enumerator_Dispose_m3163303335_MethodInfo_var);
		IL2CPP_END_FINALLY(150)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(150)
	{
		IL2CPP_JUMP_TBL(0xD0, IL_00d0)
		IL2CPP_JUMP_TBL(0xA4, IL_00a4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00a4:
	{
		List_1_t2247579857 * L_25 = V_1;
		NullCheck(L_25);
		int32_t L_26 = List_1_get_Count_m3798745807(L_25, /*hidden argument*/List_1_get_Count_m3798745807_MethodInfo_var);
		int32_t L_27 = __this->get_mMaxInstances_9();
		if ((((int32_t)L_26) >= ((int32_t)L_27)))
		{
			goto IL_00ce;
		}
	}
	{
		List_1_t2247579857 * L_28 = V_1;
		WordAbstractBehaviour_t2878458725 * L_29 = Enumerable_First_TisWordAbstractBehaviour_t2878458725_m1729057050(NULL /*static, unused*/, L_28, /*hidden argument*/Enumerable_First_TisWordAbstractBehaviour_t2878458725_m1729057050_MethodInfo_var);
		WordAbstractBehaviour_t2878458725 * L_30 = WordManagerImpl_InstantiateWordBehaviour_m2525602245(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		V_3 = L_30;
		List_1_t2247579857 * L_31 = V_1;
		WordAbstractBehaviour_t2878458725 * L_32 = V_3;
		NullCheck(L_31);
		List_1_Add_m342378119(L_31, L_32, /*hidden argument*/List_1_Add_m342378119_MethodInfo_var);
		WordResult_t1915507197 * L_33 = ___wordResult0;
		WordAbstractBehaviour_t2878458725 * L_34 = V_3;
		WordAbstractBehaviour_t2878458725 * L_35 = WordManagerImpl_AssociateWordBehaviour_m3359416954(__this, L_33, L_34, /*hidden argument*/NULL);
		return L_35;
	}

IL_00ce:
	{
		return (WordAbstractBehaviour_t2878458725 *)NULL;
	}

IL_00d0:
	{
		WordAbstractBehaviour_t2878458725 * L_36 = V_4;
		return L_36;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.WordManagerImpl::AssociateWordBehaviour(Vuforia.WordResult,Vuforia.WordAbstractBehaviour)
extern Il2CppClass* Word_t3872119486_il2cpp_TypeInfo_var;
extern Il2CppClass* IEditorTrackableBehaviour_t2323791534_il2cpp_TypeInfo_var;
extern Il2CppClass* IEditorWordBehaviour_t525465685_il2cpp_TypeInfo_var;
extern Il2CppClass* Trackable_t432275407_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Count_m721758843_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m938495191_MethodInfo_var;
extern const uint32_t WordManagerImpl_AssociateWordBehaviour_m3359416954_MetadataUsageId;
extern "C"  WordAbstractBehaviour_t2878458725 * WordManagerImpl_AssociateWordBehaviour_m3359416954 (WordManagerImpl_t4282786523 * __this, WordResult_t1915507197 * ___wordResult0, WordAbstractBehaviour_t2878458725 * ___wordBehaviourTemplate1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_AssociateWordBehaviour_m3359416954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	WordAbstractBehaviour_t2878458725 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		Dictionary_2_t1886284360 * L_0 = __this->get_mActiveWordBehaviours_4();
		NullCheck(L_0);
		int32_t L_1 = Dictionary_2_get_Count_m721758843(L_0, /*hidden argument*/Dictionary_2_get_Count_m721758843_MethodInfo_var);
		int32_t L_2 = __this->get_mMaxInstances_9();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0015;
		}
	}
	{
		return (WordAbstractBehaviour_t2878458725 *)NULL;
	}

IL_0015:
	{
		WordResult_t1915507197 * L_3 = ___wordResult0;
		NullCheck(L_3);
		Il2CppObject * L_4 = VirtFuncInvoker0< Il2CppObject * >::Invoke(4 /* Vuforia.Word Vuforia.WordResult::get_Word() */, L_3);
		V_0 = L_4;
		WordAbstractBehaviour_t2878458725 * L_5 = ___wordBehaviourTemplate1;
		V_1 = L_5;
		WordAbstractBehaviour_t2878458725 * L_6 = V_1;
		V_2 = L_6;
		Il2CppObject * L_7 = V_2;
		Il2CppObject * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Vuforia.Word::get_StringValue() */, Word_t3872119486_il2cpp_TypeInfo_var, L_8);
		NullCheck(L_7);
		InterfaceFuncInvoker1< bool, String_t* >::Invoke(2 /* System.Boolean Vuforia.IEditorTrackableBehaviour::SetNameForTrackable(System.String) */, IEditorTrackableBehaviour_t2323791534_il2cpp_TypeInfo_var, L_7, L_9);
		Il2CppObject * L_10 = V_2;
		Il2CppObject * L_11 = V_0;
		NullCheck(L_10);
		InterfaceActionInvoker1< Il2CppObject * >::Invoke(6 /* System.Void Vuforia.IEditorWordBehaviour::InitializeWord(Vuforia.Word) */, IEditorWordBehaviour_t525465685_il2cpp_TypeInfo_var, L_10, L_11);
		Dictionary_2_t1886284360 * L_12 = __this->get_mActiveWordBehaviours_4();
		Il2CppObject * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = InterfaceFuncInvoker0< int32_t >::Invoke(1 /* System.Int32 Vuforia.Trackable::get_ID() */, Trackable_t432275407_il2cpp_TypeInfo_var, L_13);
		WordAbstractBehaviour_t2878458725 * L_15 = V_1;
		NullCheck(L_12);
		Dictionary_2_Add_m938495191(L_12, L_14, L_15, /*hidden argument*/Dictionary_2_Add_m938495191_MethodInfo_var);
		WordAbstractBehaviour_t2878458725 * L_16 = V_1;
		return L_16;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.WordManagerImpl::InstantiateWordBehaviour(Vuforia.WordAbstractBehaviour)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisWordAbstractBehaviour_t2878458725_m4244081605_MethodInfo_var;
extern const uint32_t WordManagerImpl_InstantiateWordBehaviour_m2525602245_MetadataUsageId;
extern "C"  WordAbstractBehaviour_t2878458725 * WordManagerImpl_InstantiateWordBehaviour_m2525602245 (Il2CppObject * __this /* static, unused */, WordAbstractBehaviour_t2878458725 * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_InstantiateWordBehaviour_m2525602245_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	{
		WordAbstractBehaviour_t2878458725 * L_0 = ___input0;
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_2 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_1, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		V_0 = L_2;
		GameObject_t1756533147 * L_3 = V_0;
		NullCheck(L_3);
		WordAbstractBehaviour_t2878458725 * L_4 = GameObject_GetComponent_TisWordAbstractBehaviour_t2878458725_m4244081605(L_3, /*hidden argument*/GameObject_GetComponent_TisWordAbstractBehaviour_t2878458725_m4244081605_MethodInfo_var);
		return L_4;
	}
}
// Vuforia.WordAbstractBehaviour Vuforia.WordManagerImpl::CreateWordBehaviour()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* IBehaviourComponentFactory_t3575155525_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1374409224;
extern Il2CppCodeGenString* _stringLiteral2422965484;
extern const uint32_t WordManagerImpl_CreateWordBehaviour_m810645364_MetadataUsageId;
extern "C"  WordAbstractBehaviour_t2878458725 * WordManagerImpl_CreateWordBehaviour_m810645364 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl_CreateWordBehaviour_m810645364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t1756533147 * V_0 = NULL;
	WordAbstractBehaviour_t2878458725 * V_1 = NULL;
	{
		GameObject_t1756533147 * L_0 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_0, _stringLiteral1374409224, /*hidden argument*/NULL);
		V_0 = L_0;
		Il2CppObject * L_1 = BehaviourComponentFactory_get_Instance_m3607279548(NULL /*static, unused*/, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = V_0;
		NullCheck(L_1);
		WordAbstractBehaviour_t2878458725 * L_3 = InterfaceFuncInvoker1< WordAbstractBehaviour_t2878458725 *, GameObject_t1756533147 * >::Invoke(7 /* Vuforia.WordAbstractBehaviour Vuforia.IBehaviourComponentFactory::AddWordBehaviour(UnityEngine.GameObject) */, IBehaviourComponentFactory_t3575155525_il2cpp_TypeInfo_var, L_1, L_2);
		V_1 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral2422965484, /*hidden argument*/NULL);
		WordAbstractBehaviour_t2878458725 * L_4 = V_1;
		return L_4;
	}
}
// System.Void Vuforia.WordManagerImpl::.ctor()
extern Il2CppClass* Dictionary_2_t923332832_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1284628329_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3241240618_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1886284360_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2247579857_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t4162359119_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m4252959775_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m888184387_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3605484210_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1430915711_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m1035509995_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2656341038_MethodInfo_var;
extern const uint32_t WordManagerImpl__ctor_m1978406723_MetadataUsageId;
extern "C"  void WordManagerImpl__ctor_m1978406723 (WordManagerImpl_t4282786523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WordManagerImpl__ctor_m1978406723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t923332832 * L_0 = (Dictionary_2_t923332832 *)il2cpp_codegen_object_new(Dictionary_2_t923332832_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m4252959775(L_0, /*hidden argument*/Dictionary_2__ctor_m4252959775_MethodInfo_var);
		__this->set_mTrackedWords_1(L_0);
		List_1_t1284628329 * L_1 = (List_1_t1284628329 *)il2cpp_codegen_object_new(List_1_t1284628329_il2cpp_TypeInfo_var);
		List_1__ctor_m888184387(L_1, /*hidden argument*/List_1__ctor_m888184387_MethodInfo_var);
		__this->set_mNewWords_2(L_1);
		List_1_t3241240618 * L_2 = (List_1_t3241240618 *)il2cpp_codegen_object_new(List_1_t3241240618_il2cpp_TypeInfo_var);
		List_1__ctor_m3605484210(L_2, /*hidden argument*/List_1__ctor_m3605484210_MethodInfo_var);
		__this->set_mLostWords_3(L_2);
		Dictionary_2_t1886284360 * L_3 = (Dictionary_2_t1886284360 *)il2cpp_codegen_object_new(Dictionary_2_t1886284360_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1430915711(L_3, /*hidden argument*/Dictionary_2__ctor_m1430915711_MethodInfo_var);
		__this->set_mActiveWordBehaviours_4(L_3);
		List_1_t2247579857 * L_4 = (List_1_t2247579857 *)il2cpp_codegen_object_new(List_1_t2247579857_il2cpp_TypeInfo_var);
		List_1__ctor_m1035509995(L_4, /*hidden argument*/List_1__ctor_m1035509995_MethodInfo_var);
		__this->set_mWordBehavioursMarkedForDeletion_5(L_4);
		List_1_t3241240618 * L_5 = (List_1_t3241240618 *)il2cpp_codegen_object_new(List_1_t3241240618_il2cpp_TypeInfo_var);
		List_1__ctor_m3605484210(L_5, /*hidden argument*/List_1__ctor_m3605484210_MethodInfo_var);
		__this->set_mWaitingQueue_6(L_5);
		Dictionary_2_t4162359119 * L_6 = (Dictionary_2_t4162359119 *)il2cpp_codegen_object_new(Dictionary_2_t4162359119_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2656341038(L_6, /*hidden argument*/Dictionary_2__ctor_m2656341038_MethodInfo_var);
		__this->set_mWordBehaviours_7(L_6);
		__this->set_mMaxInstances_9(1);
		WordManager__ctor_m501606791(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordResult::.ctor()
extern "C"  void WordResult__ctor_m56695885 (WordResult_t1915507197 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Vuforia.WordResultImpl::.ctor(Vuforia.Word)
extern "C"  void WordResultImpl__ctor_m401521253 (WordResultImpl_t911273601 * __this, Il2CppObject * ___word0, const MethodInfo* method)
{
	{
		WordResult__ctor_m56695885(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___word0;
		__this->set_mWord_3(L_0);
		return;
	}
}
// Vuforia.Word Vuforia.WordResultImpl::get_Word()
extern "C"  Il2CppObject * WordResultImpl_get_Word_m3362229457 (WordResultImpl_t911273601 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_mWord_3();
		return L_0;
	}
}
// UnityEngine.Vector3 Vuforia.WordResultImpl::get_Position()
extern "C"  Vector3_t2243707580  WordResultImpl_get_Position_m3408670079 (WordResultImpl_t911273601 * __this, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = __this->get_mPosition_1();
		return L_0;
	}
}
// UnityEngine.Quaternion Vuforia.WordResultImpl::get_Orientation()
extern "C"  Quaternion_t4030073918  WordResultImpl_get_Orientation_m2760402236 (WordResultImpl_t911273601 * __this, const MethodInfo* method)
{
	{
		Quaternion_t4030073918  L_0 = __this->get_mOrientation_2();
		return L_0;
	}
}
// Vuforia.OrientedBoundingBox Vuforia.WordResultImpl::get_Obb()
extern "C"  OrientedBoundingBox_t3172429123  WordResultImpl_get_Obb_m2405245731 (WordResultImpl_t911273601 * __this, const MethodInfo* method)
{
	{
		OrientedBoundingBox_t3172429123  L_0 = __this->get_mObb_0();
		return L_0;
	}
}
// Vuforia.TrackableBehaviour/Status Vuforia.WordResultImpl::get_CurrentStatus()
extern "C"  int32_t WordResultImpl_get_CurrentStatus_m3077354831 (WordResultImpl_t911273601 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mStatus_4();
		return L_0;
	}
}
// System.Void Vuforia.WordResultImpl::SetPose(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void WordResultImpl_SetPose_m3664988988 (WordResultImpl_t911273601 * __this, Vector3_t2243707580  ___position0, Quaternion_t4030073918  ___orientation1, const MethodInfo* method)
{
	{
		Vector3_t2243707580  L_0 = ___position0;
		__this->set_mPosition_1(L_0);
		Quaternion_t4030073918  L_1 = ___orientation1;
		__this->set_mOrientation_2(L_1);
		return;
	}
}
// System.Void Vuforia.WordResultImpl::SetObb(Vuforia.OrientedBoundingBox)
extern "C"  void WordResultImpl_SetObb_m82808223 (WordResultImpl_t911273601 * __this, OrientedBoundingBox_t3172429123  ___obb0, const MethodInfo* method)
{
	{
		OrientedBoundingBox_t3172429123  L_0 = ___obb0;
		__this->set_mObb_0(L_0);
		return;
	}
}
// System.Void Vuforia.WordResultImpl::SetStatus(Vuforia.TrackableBehaviour/Status)
extern "C"  void WordResultImpl_SetStatus_m3989517476 (WordResultImpl_t911273601 * __this, int32_t ___status0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___status0;
		__this->set_mStatus_4(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
