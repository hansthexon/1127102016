﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2620100900MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m47961949(__this, ___l0, method) ((  void (*) (Enumerator_t647852738 *, List_1_t1113123064 *, const MethodInfo*))Enumerator__ctor_m3896932057_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4099922221(__this, method) ((  void (*) (Enumerator_t647852738 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4116876945_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1031436765(__this, method) ((  Il2CppObject * (*) (Enumerator_t647852738 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2417842817_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::Dispose()
#define Enumerator_Dispose_m3968877264(__this, method) ((  void (*) (Enumerator_t647852738 *, const MethodInfo*))Enumerator_Dispose_m643661990_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::VerifyState()
#define Enumerator_VerifyState_m3563897887(__this, method) ((  void (*) (Enumerator_t647852738 *, const MethodInfo*))Enumerator_VerifyState_m570381259_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::MoveNext()
#define Enumerator_MoveNext_m1856027968(__this, method) ((  bool (*) (Enumerator_t647852738 *, const MethodInfo*))Enumerator_MoveNext_m1558544689_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::get_Current()
#define Enumerator_get_Current_m1386428546(__this, method) ((  KeyValuePair_2_t1744001932  (*) (Enumerator_t647852738 *, const MethodInfo*))Enumerator_get_Current_m3902255900_gshared)(__this, method)
