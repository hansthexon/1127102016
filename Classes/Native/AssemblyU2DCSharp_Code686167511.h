﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Code
struct  Code_t686167511  : public Il2CppObject
{
public:
	// System.String Code::<CodeARimage>k__BackingField
	String_t* ___U3CCodeARimageU3Ek__BackingField_0;
	// System.String Code::<VideoFile>k__BackingField
	String_t* ___U3CVideoFileU3Ek__BackingField_1;
	// System.String Code::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_2;
	// System.String Code::<Type2>k__BackingField
	String_t* ___U3CType2U3Ek__BackingField_3;
	// System.String Code::<PolitePlanet>k__BackingField
	String_t* ___U3CPolitePlanetU3Ek__BackingField_4;
	// System.String Code::<SelfVideoFile>k__BackingField
	String_t* ___U3CSelfVideoFileU3Ek__BackingField_5;
	// System.String Code::<Country>k__BackingField
	String_t* ___U3CCountryU3Ek__BackingField_6;
	// System.String Code::<Purchased>k__BackingField
	String_t* ___U3CPurchasedU3Ek__BackingField_7;
	// System.String Code::<Content>k__BackingField
	String_t* ___U3CContentU3Ek__BackingField_8;
	// System.Int32 Code::<FirstScore>k__BackingField
	int32_t ___U3CFirstScoreU3Ek__BackingField_9;
	// System.Int32 Code::<RescanScore>k__BackingField
	int32_t ___U3CRescanScoreU3Ek__BackingField_10;
	// System.Int32 Code::<SelfFirstScore>k__BackingField
	int32_t ___U3CSelfFirstScoreU3Ek__BackingField_11;
	// System.Int32 Code::<SelfRescanScore>k__BackingField
	int32_t ___U3CSelfRescanScoreU3Ek__BackingField_12;
	// System.Int32 Code::<Version>k__BackingField
	int32_t ___U3CVersionU3Ek__BackingField_13;
	// System.String Code::<ForMonth>k__BackingField
	String_t* ___U3CForMonthU3Ek__BackingField_14;
	// System.String Code::<First>k__BackingField
	String_t* ___U3CFirstU3Ek__BackingField_15;
	// System.String Code::<SelfFirst>k__BackingField
	String_t* ___U3CSelfFirstU3Ek__BackingField_16;
	// System.String Code::<Location>k__BackingField
	String_t* ___U3CLocationU3Ek__BackingField_17;
	// System.String Code::<PoliteFirst>k__BackingField
	String_t* ___U3CPoliteFirstU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_U3CCodeARimageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CCodeARimageU3Ek__BackingField_0)); }
	inline String_t* get_U3CCodeARimageU3Ek__BackingField_0() const { return ___U3CCodeARimageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCodeARimageU3Ek__BackingField_0() { return &___U3CCodeARimageU3Ek__BackingField_0; }
	inline void set_U3CCodeARimageU3Ek__BackingField_0(String_t* value)
	{
		___U3CCodeARimageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCodeARimageU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CVideoFileU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CVideoFileU3Ek__BackingField_1)); }
	inline String_t* get_U3CVideoFileU3Ek__BackingField_1() const { return ___U3CVideoFileU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CVideoFileU3Ek__BackingField_1() { return &___U3CVideoFileU3Ek__BackingField_1; }
	inline void set_U3CVideoFileU3Ek__BackingField_1(String_t* value)
	{
		___U3CVideoFileU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CVideoFileU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CTypeU3Ek__BackingField_2)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTypeU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CType2U3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CType2U3Ek__BackingField_3)); }
	inline String_t* get_U3CType2U3Ek__BackingField_3() const { return ___U3CType2U3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CType2U3Ek__BackingField_3() { return &___U3CType2U3Ek__BackingField_3; }
	inline void set_U3CType2U3Ek__BackingField_3(String_t* value)
	{
		___U3CType2U3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CType2U3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CPolitePlanetU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CPolitePlanetU3Ek__BackingField_4)); }
	inline String_t* get_U3CPolitePlanetU3Ek__BackingField_4() const { return ___U3CPolitePlanetU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CPolitePlanetU3Ek__BackingField_4() { return &___U3CPolitePlanetU3Ek__BackingField_4; }
	inline void set_U3CPolitePlanetU3Ek__BackingField_4(String_t* value)
	{
		___U3CPolitePlanetU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPolitePlanetU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CSelfVideoFileU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CSelfVideoFileU3Ek__BackingField_5)); }
	inline String_t* get_U3CSelfVideoFileU3Ek__BackingField_5() const { return ___U3CSelfVideoFileU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CSelfVideoFileU3Ek__BackingField_5() { return &___U3CSelfVideoFileU3Ek__BackingField_5; }
	inline void set_U3CSelfVideoFileU3Ek__BackingField_5(String_t* value)
	{
		___U3CSelfVideoFileU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSelfVideoFileU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_U3CCountryU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CCountryU3Ek__BackingField_6)); }
	inline String_t* get_U3CCountryU3Ek__BackingField_6() const { return ___U3CCountryU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CCountryU3Ek__BackingField_6() { return &___U3CCountryU3Ek__BackingField_6; }
	inline void set_U3CCountryU3Ek__BackingField_6(String_t* value)
	{
		___U3CCountryU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CCountryU3Ek__BackingField_6, value);
	}

	inline static int32_t get_offset_of_U3CPurchasedU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CPurchasedU3Ek__BackingField_7)); }
	inline String_t* get_U3CPurchasedU3Ek__BackingField_7() const { return ___U3CPurchasedU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CPurchasedU3Ek__BackingField_7() { return &___U3CPurchasedU3Ek__BackingField_7; }
	inline void set_U3CPurchasedU3Ek__BackingField_7(String_t* value)
	{
		___U3CPurchasedU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPurchasedU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CContentU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CContentU3Ek__BackingField_8)); }
	inline String_t* get_U3CContentU3Ek__BackingField_8() const { return ___U3CContentU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CContentU3Ek__BackingField_8() { return &___U3CContentU3Ek__BackingField_8; }
	inline void set_U3CContentU3Ek__BackingField_8(String_t* value)
	{
		___U3CContentU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CContentU3Ek__BackingField_8, value);
	}

	inline static int32_t get_offset_of_U3CFirstScoreU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CFirstScoreU3Ek__BackingField_9)); }
	inline int32_t get_U3CFirstScoreU3Ek__BackingField_9() const { return ___U3CFirstScoreU3Ek__BackingField_9; }
	inline int32_t* get_address_of_U3CFirstScoreU3Ek__BackingField_9() { return &___U3CFirstScoreU3Ek__BackingField_9; }
	inline void set_U3CFirstScoreU3Ek__BackingField_9(int32_t value)
	{
		___U3CFirstScoreU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CRescanScoreU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CRescanScoreU3Ek__BackingField_10)); }
	inline int32_t get_U3CRescanScoreU3Ek__BackingField_10() const { return ___U3CRescanScoreU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CRescanScoreU3Ek__BackingField_10() { return &___U3CRescanScoreU3Ek__BackingField_10; }
	inline void set_U3CRescanScoreU3Ek__BackingField_10(int32_t value)
	{
		___U3CRescanScoreU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CSelfFirstScoreU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CSelfFirstScoreU3Ek__BackingField_11)); }
	inline int32_t get_U3CSelfFirstScoreU3Ek__BackingField_11() const { return ___U3CSelfFirstScoreU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CSelfFirstScoreU3Ek__BackingField_11() { return &___U3CSelfFirstScoreU3Ek__BackingField_11; }
	inline void set_U3CSelfFirstScoreU3Ek__BackingField_11(int32_t value)
	{
		___U3CSelfFirstScoreU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CSelfRescanScoreU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CSelfRescanScoreU3Ek__BackingField_12)); }
	inline int32_t get_U3CSelfRescanScoreU3Ek__BackingField_12() const { return ___U3CSelfRescanScoreU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CSelfRescanScoreU3Ek__BackingField_12() { return &___U3CSelfRescanScoreU3Ek__BackingField_12; }
	inline void set_U3CSelfRescanScoreU3Ek__BackingField_12(int32_t value)
	{
		___U3CSelfRescanScoreU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CVersionU3Ek__BackingField_13)); }
	inline int32_t get_U3CVersionU3Ek__BackingField_13() const { return ___U3CVersionU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CVersionU3Ek__BackingField_13() { return &___U3CVersionU3Ek__BackingField_13; }
	inline void set_U3CVersionU3Ek__BackingField_13(int32_t value)
	{
		___U3CVersionU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CForMonthU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CForMonthU3Ek__BackingField_14)); }
	inline String_t* get_U3CForMonthU3Ek__BackingField_14() const { return ___U3CForMonthU3Ek__BackingField_14; }
	inline String_t** get_address_of_U3CForMonthU3Ek__BackingField_14() { return &___U3CForMonthU3Ek__BackingField_14; }
	inline void set_U3CForMonthU3Ek__BackingField_14(String_t* value)
	{
		___U3CForMonthU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CForMonthU3Ek__BackingField_14, value);
	}

	inline static int32_t get_offset_of_U3CFirstU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CFirstU3Ek__BackingField_15)); }
	inline String_t* get_U3CFirstU3Ek__BackingField_15() const { return ___U3CFirstU3Ek__BackingField_15; }
	inline String_t** get_address_of_U3CFirstU3Ek__BackingField_15() { return &___U3CFirstU3Ek__BackingField_15; }
	inline void set_U3CFirstU3Ek__BackingField_15(String_t* value)
	{
		___U3CFirstU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CFirstU3Ek__BackingField_15, value);
	}

	inline static int32_t get_offset_of_U3CSelfFirstU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CSelfFirstU3Ek__BackingField_16)); }
	inline String_t* get_U3CSelfFirstU3Ek__BackingField_16() const { return ___U3CSelfFirstU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CSelfFirstU3Ek__BackingField_16() { return &___U3CSelfFirstU3Ek__BackingField_16; }
	inline void set_U3CSelfFirstU3Ek__BackingField_16(String_t* value)
	{
		___U3CSelfFirstU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CSelfFirstU3Ek__BackingField_16, value);
	}

	inline static int32_t get_offset_of_U3CLocationU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CLocationU3Ek__BackingField_17)); }
	inline String_t* get_U3CLocationU3Ek__BackingField_17() const { return ___U3CLocationU3Ek__BackingField_17; }
	inline String_t** get_address_of_U3CLocationU3Ek__BackingField_17() { return &___U3CLocationU3Ek__BackingField_17; }
	inline void set_U3CLocationU3Ek__BackingField_17(String_t* value)
	{
		___U3CLocationU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CLocationU3Ek__BackingField_17, value);
	}

	inline static int32_t get_offset_of_U3CPoliteFirstU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Code_t686167511, ___U3CPoliteFirstU3Ek__BackingField_18)); }
	inline String_t* get_U3CPoliteFirstU3Ek__BackingField_18() const { return ___U3CPoliteFirstU3Ek__BackingField_18; }
	inline String_t** get_address_of_U3CPoliteFirstU3Ek__BackingField_18() { return &___U3CPoliteFirstU3Ek__BackingField_18; }
	inline void set_U3CPoliteFirstU3Ek__BackingField_18(String_t* value)
	{
		___U3CPoliteFirstU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPoliteFirstU3Ek__BackingField_18, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
