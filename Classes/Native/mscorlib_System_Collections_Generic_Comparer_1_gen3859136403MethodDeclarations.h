﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct Comparer_1_t3859136403;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void System.Collections.Generic.Comparer`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor()
extern "C"  void Comparer_1__ctor_m534919979_gshared (Comparer_1_t3859136403 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m534919979(__this, method) ((  void (*) (Comparer_1_t3859136403 *, const MethodInfo*))Comparer_1__ctor_m534919979_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.cctor()
extern "C"  void Comparer_1__cctor_m1579751478_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m1579751478(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m1579751478_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m407422248_gshared (Comparer_1_t3859136403 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m407422248(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t3859136403 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m407422248_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Default()
extern "C"  Comparer_1_t3859136403 * Comparer_1_get_Default_m2968249755_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m2968249755(__this /* static, unused */, method) ((  Comparer_1_t3859136403 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m2968249755_gshared)(__this /* static, unused */, method)
