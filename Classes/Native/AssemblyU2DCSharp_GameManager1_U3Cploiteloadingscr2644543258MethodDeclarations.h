﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager1/<ploiteloadingscreen>c__IteratorE
struct U3CploiteloadingscreenU3Ec__IteratorE_t2644543258;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager1/<ploiteloadingscreen>c__IteratorE::.ctor()
extern "C"  void U3CploiteloadingscreenU3Ec__IteratorE__ctor_m3362641699 (U3CploiteloadingscreenU3Ec__IteratorE_t2644543258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager1/<ploiteloadingscreen>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CploiteloadingscreenU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2010477673 (U3CploiteloadingscreenU3Ec__IteratorE_t2644543258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager1/<ploiteloadingscreen>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CploiteloadingscreenU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m1989770849 (U3CploiteloadingscreenU3Ec__IteratorE_t2644543258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager1/<ploiteloadingscreen>c__IteratorE::MoveNext()
extern "C"  bool U3CploiteloadingscreenU3Ec__IteratorE_MoveNext_m3507160189 (U3CploiteloadingscreenU3Ec__IteratorE_t2644543258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1/<ploiteloadingscreen>c__IteratorE::Dispose()
extern "C"  void U3CploiteloadingscreenU3Ec__IteratorE_Dispose_m1663848656 (U3CploiteloadingscreenU3Ec__IteratorE_t2644543258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1/<ploiteloadingscreen>c__IteratorE::Reset()
extern "C"  void U3CploiteloadingscreenU3Ec__IteratorE_Reset_m1404607978 (U3CploiteloadingscreenU3Ec__IteratorE_t2644543258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
