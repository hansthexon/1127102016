﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.NotNullAttribute
struct NotNullAttribute_t3892473118;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.NotNullAttribute::.ctor()
extern "C"  void NotNullAttribute__ctor_m2368164217 (NotNullAttribute_t3892473118 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
