﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata>
struct IDictionary_2_t3607688819;

#include "mscorlib_System_ValueType3507792607.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.ObjectMetadata
struct  ObjectMetadata_t3995922398 
{
public:
	// System.Type LitJson.ObjectMetadata::elemType
	Type_t * ___elemType_0;
	// System.Collections.Generic.IDictionary`2<System.String,LitJson.PropertyMetadata> LitJson.ObjectMetadata::<Properties>k__BackingField
	Il2CppObject* ___U3CPropertiesU3Ek__BackingField_1;
	// System.Boolean LitJson.ObjectMetadata::<IsDictionary>k__BackingField
	bool ___U3CIsDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_elemType_0() { return static_cast<int32_t>(offsetof(ObjectMetadata_t3995922398, ___elemType_0)); }
	inline Type_t * get_elemType_0() const { return ___elemType_0; }
	inline Type_t ** get_address_of_elemType_0() { return &___elemType_0; }
	inline void set_elemType_0(Type_t * value)
	{
		___elemType_0 = value;
		Il2CppCodeGenWriteBarrier(&___elemType_0, value);
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ObjectMetadata_t3995922398, ___U3CPropertiesU3Ek__BackingField_1)); }
	inline Il2CppObject* get_U3CPropertiesU3Ek__BackingField_1() const { return ___U3CPropertiesU3Ek__BackingField_1; }
	inline Il2CppObject** get_address_of_U3CPropertiesU3Ek__BackingField_1() { return &___U3CPropertiesU3Ek__BackingField_1; }
	inline void set_U3CPropertiesU3Ek__BackingField_1(Il2CppObject* value)
	{
		___U3CPropertiesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPropertiesU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CIsDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ObjectMetadata_t3995922398, ___U3CIsDictionaryU3Ek__BackingField_2)); }
	inline bool get_U3CIsDictionaryU3Ek__BackingField_2() const { return ___U3CIsDictionaryU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsDictionaryU3Ek__BackingField_2() { return &___U3CIsDictionaryU3Ek__BackingField_2; }
	inline void set_U3CIsDictionaryU3Ek__BackingField_2(bool value)
	{
		___U3CIsDictionaryU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: LitJson.ObjectMetadata
struct ObjectMetadata_t3995922398_marshaled_pinvoke
{
	Type_t * ___elemType_0;
	Il2CppObject* ___U3CPropertiesU3Ek__BackingField_1;
	int32_t ___U3CIsDictionaryU3Ek__BackingField_2;
};
// Native definition for marshalling of: LitJson.ObjectMetadata
struct ObjectMetadata_t3995922398_marshaled_com
{
	Type_t * ___elemType_0;
	Il2CppObject* ___U3CPropertiesU3Ek__BackingField_1;
	int32_t ___U3CIsDictionaryU3Ek__BackingField_2;
};
