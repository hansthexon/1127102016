﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct DefaultComparer_t2469964926;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I848034765.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m3932508535_gshared (DefaultComparer_t2469964926 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3932508535(__this, method) ((  void (*) (DefaultComparer_t2469964926 *, const MethodInfo*))DefaultComparer__ctor_m3932508535_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3185308414_gshared (DefaultComparer_t2469964926 * __this, IndexInfo_t848034765  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3185308414(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2469964926 *, IndexInfo_t848034765 , const MethodInfo*))DefaultComparer_GetHashCode_m3185308414_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<SQLite4Unity3d.SQLiteConnection/IndexInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m51403158_gshared (DefaultComparer_t2469964926 * __this, IndexInfo_t848034765  ___x0, IndexInfo_t848034765  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m51403158(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2469964926 *, IndexInfo_t848034765 , IndexInfo_t848034765 , const MethodInfo*))DefaultComparer_Equals_m51403158_gshared)(__this, ___x0, ___y1, method)
