﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey15
struct U3CInsertAllU3Ec__AnonStorey15_t1887274984;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey15::.ctor()
extern "C"  void U3CInsertAllU3Ec__AnonStorey15__ctor_m2746112459 (U3CInsertAllU3Ec__AnonStorey15_t1887274984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey15::<>m__6()
extern "C"  void U3CInsertAllU3Ec__AnonStorey15_U3CU3Em__6_m1558484080 (U3CInsertAllU3Ec__AnonStorey15_t1887274984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
