﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>
struct Transform_1_t2082831055;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23007666127.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m2308281846_gshared (Transform_1_t2082831055 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m2308281846(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2082831055 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m2308281846_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t3007666127  Transform_1_Invoke_m1148894326_gshared (Transform_1_t2082831055 * __this, int32_t ___key0, TrackableResultData_t1947527974  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m1148894326(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t3007666127  (*) (Transform_1_t2082831055 *, int32_t, TrackableResultData_t1947527974 , const MethodInfo*))Transform_1_Invoke_m1148894326_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m1291268127_gshared (Transform_1_t2082831055 * __this, int32_t ___key0, TrackableResultData_t1947527974  ___value1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m1291268127(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2082831055 *, int32_t, TrackableResultData_t1947527974 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m1291268127_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData,System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t3007666127  Transform_1_EndInvoke_m2361950796_gshared (Transform_1_t2082831055 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m2361950796(__this, ___result0, method) ((  KeyValuePair_2_t3007666127  (*) (Transform_1_t2082831055 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2361950796_gshared)(__this, ___result0, method)
