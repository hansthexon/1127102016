﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.TableAttribute
struct TableAttribute_t2732753422;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SQLite4Unity3d.TableAttribute::.ctor(System.String)
extern "C"  void TableAttribute__ctor_m3900936443 (TableAttribute_t2732753422 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.TableAttribute::get_Name()
extern "C"  String_t* TableAttribute_get_Name_m3962059498 (TableAttribute_t2732753422 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableAttribute::set_Name(System.String)
extern "C"  void TableAttribute_set_Name_m1111522441 (TableAttribute_t2732753422 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
