﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CheckforUpdates
struct CheckforUpdates_t3659555713;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void CheckforUpdates::.ctor()
extern "C"  void CheckforUpdates__ctor_m1625443306 (CheckforUpdates_t3659555713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CheckforUpdates::Start()
extern "C"  void CheckforUpdates_Start_m781668658 (CheckforUpdates_t3659555713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CheckforUpdates::Update()
extern "C"  void CheckforUpdates_Update_m3249671309 (CheckforUpdates_t3659555713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CheckforUpdates::setdatapath()
extern "C"  void CheckforUpdates_setdatapath_m1998858079 (CheckforUpdates_t3659555713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CheckforUpdates::Download()
extern "C"  Il2CppObject * CheckforUpdates_Download_m418013760 (CheckforUpdates_t3659555713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CheckforUpdates::dowloadVideo(System.String)
extern "C"  Il2CppObject * CheckforUpdates_dowloadVideo_m1642469395 (CheckforUpdates_t3659555713 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CheckforUpdates::UpdateDb(System.String)
extern "C"  void CheckforUpdates_UpdateDb_m2072962201 (CheckforUpdates_t3659555713 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CheckforUpdates::Updatemytest(System.String)
extern "C"  void CheckforUpdates_Updatemytest_m132385825 (CheckforUpdates_t3659555713 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CheckforUpdates::nextscene()
extern "C"  void CheckforUpdates_nextscene_m3545160913 (CheckforUpdates_t3659555713 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
