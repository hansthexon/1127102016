﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen683021649MethodDeclarations.h"

// System.Void System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m217044339(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t3005740783 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m3205530687_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::Invoke(T,T)
#define Comparison_1_Invoke_m4181646073(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t3005740783 *, KeyValuePair_2_t1744001932 , KeyValuePair_2_t1744001932 , const MethodInfo*))Comparison_1_Invoke_m1435415269_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m1983039468(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t3005740783 *, KeyValuePair_2_t1744001932 , KeyValuePair_2_t1744001932 , AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m2126254898_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m704298927(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t3005740783 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m3110716299_gshared)(__this, ___result0, method)
