﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RoomObject
struct RoomObject_t2982870316;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void RoomObject::.ctor()
extern "C"  void RoomObject__ctor_m64603709 (RoomObject_t2982870316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RoomObject::get_ObjId()
extern "C"  String_t* RoomObject_get_ObjId_m4106434223 (RoomObject_t2982870316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomObject::set_ObjId(System.String)
extern "C"  void RoomObject_set_ObjId_m3073853130 (RoomObject_t2982870316 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RoomObject::get_ObjPNG()
extern "C"  String_t* RoomObject_get_ObjPNG_m2960871703 (RoomObject_t2982870316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomObject::set_ObjPNG(System.String)
extern "C"  void RoomObject_set_ObjPNG_m320097584 (RoomObject_t2982870316 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RoomObject::get_Room()
extern "C"  String_t* RoomObject_get_Room_m3906439940 (RoomObject_t2982870316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomObject::set_Room(System.String)
extern "C"  void RoomObject_set_Room_m4078115449 (RoomObject_t2982870316 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RoomObject::get_ReachScore()
extern "C"  String_t* RoomObject_get_ReachScore_m860680260 (RoomObject_t2982870316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomObject::set_ReachScore(System.String)
extern "C"  void RoomObject_set_ReachScore_m519746191 (RoomObject_t2982870316 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RoomObject::get_Earned()
extern "C"  String_t* RoomObject_get_Earned_m155128088 (RoomObject_t2982870316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomObject::set_Earned(System.String)
extern "C"  void RoomObject_set_Earned_m1640489187 (RoomObject_t2982870316 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String RoomObject::get_PlayerInRoomPosXY()
extern "C"  String_t* RoomObject_get_PlayerInRoomPosXY_m1094885311 (RoomObject_t2982870316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RoomObject::set_PlayerInRoomPosXY(System.String)
extern "C"  void RoomObject_set_PlayerInRoomPosXY_m2999327436 (RoomObject_t2982870316 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
