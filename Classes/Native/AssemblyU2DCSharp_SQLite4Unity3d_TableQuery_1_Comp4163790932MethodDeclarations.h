﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>
struct CompileResult_t4163790932;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>::.ctor()
extern "C"  void CompileResult__ctor_m3789497005_gshared (CompileResult_t4163790932 * __this, const MethodInfo* method);
#define CompileResult__ctor_m3789497005(__this, method) ((  void (*) (CompileResult_t4163790932 *, const MethodInfo*))CompileResult__ctor_m3789497005_gshared)(__this, method)
// System.String SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>::get_CommandText()
extern "C"  String_t* CompileResult_get_CommandText_m2764066139_gshared (CompileResult_t4163790932 * __this, const MethodInfo* method);
#define CompileResult_get_CommandText_m2764066139(__this, method) ((  String_t* (*) (CompileResult_t4163790932 *, const MethodInfo*))CompileResult_get_CommandText_m2764066139_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>::set_CommandText(System.String)
extern "C"  void CompileResult_set_CommandText_m1496493232_gshared (CompileResult_t4163790932 * __this, String_t* ___value0, const MethodInfo* method);
#define CompileResult_set_CommandText_m1496493232(__this, ___value0, method) ((  void (*) (CompileResult_t4163790932 *, String_t*, const MethodInfo*))CompileResult_set_CommandText_m1496493232_gshared)(__this, ___value0, method)
// System.Object SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>::get_Value()
extern "C"  Il2CppObject * CompileResult_get_Value_m2706335512_gshared (CompileResult_t4163790932 * __this, const MethodInfo* method);
#define CompileResult_get_Value_m2706335512(__this, method) ((  Il2CppObject * (*) (CompileResult_t4163790932 *, const MethodInfo*))CompileResult_get_Value_m2706335512_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>::set_Value(System.Object)
extern "C"  void CompileResult_set_Value_m420182477_gshared (CompileResult_t4163790932 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define CompileResult_set_Value_m420182477(__this, ___value0, method) ((  void (*) (CompileResult_t4163790932 *, Il2CppObject *, const MethodInfo*))CompileResult_set_Value_m420182477_gshared)(__this, ___value0, method)
