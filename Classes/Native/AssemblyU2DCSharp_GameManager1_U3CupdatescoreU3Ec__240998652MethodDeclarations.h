﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager1/<updatescore>c__IteratorC
struct U3CupdatescoreU3Ec__IteratorC_t240998652;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager1/<updatescore>c__IteratorC::.ctor()
extern "C"  void U3CupdatescoreU3Ec__IteratorC__ctor_m831746765 (U3CupdatescoreU3Ec__IteratorC_t240998652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager1/<updatescore>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CupdatescoreU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2326979327 (U3CupdatescoreU3Ec__IteratorC_t240998652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager1/<updatescore>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CupdatescoreU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m2983963911 (U3CupdatescoreU3Ec__IteratorC_t240998652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager1/<updatescore>c__IteratorC::MoveNext()
extern "C"  bool U3CupdatescoreU3Ec__IteratorC_MoveNext_m2173762679 (U3CupdatescoreU3Ec__IteratorC_t240998652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1/<updatescore>c__IteratorC::Dispose()
extern "C"  void U3CupdatescoreU3Ec__IteratorC_Dispose_m3826298934 (U3CupdatescoreU3Ec__IteratorC_t240998652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1/<updatescore>c__IteratorC::Reset()
extern "C"  void U3CupdatescoreU3Ec__IteratorC_Reset_m4054669100 (U3CupdatescoreU3Ec__IteratorC_t240998652 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
