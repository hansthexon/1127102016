﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<System.Object>
struct U3CRegisterExporterU3Ec__AnonStorey1B_1_t3955493240;
// System.Object
struct Il2CppObject;
// LitJson.JsonWriter
struct JsonWriter_t1927598499;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1927598499.h"

// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<System.Object>::.ctor()
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1__ctor_m650532481_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3955493240 * __this, const MethodInfo* method);
#define U3CRegisterExporterU3Ec__AnonStorey1B_1__ctor_m650532481(__this, method) ((  void (*) (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3955493240 *, const MethodInfo*))U3CRegisterExporterU3Ec__AnonStorey1B_1__ctor_m650532481_gshared)(__this, method)
// System.Void LitJson.JsonMapper/<RegisterExporter>c__AnonStorey1B`1<System.Object>::<>m__32(System.Object,LitJson.JsonWriter)
extern "C"  void U3CRegisterExporterU3Ec__AnonStorey1B_1_U3CU3Em__32_m900643521_gshared (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3955493240 * __this, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method);
#define U3CRegisterExporterU3Ec__AnonStorey1B_1_U3CU3Em__32_m900643521(__this, ___obj0, ___writer1, method) ((  void (*) (U3CRegisterExporterU3Ec__AnonStorey1B_1_t3955493240 *, Il2CppObject *, JsonWriter_t1927598499 *, const MethodInfo*))U3CRegisterExporterU3Ec__AnonStorey1B_1_U3CU3Em__32_m900643521_gshared)(__this, ___obj0, ___writer1, method)
