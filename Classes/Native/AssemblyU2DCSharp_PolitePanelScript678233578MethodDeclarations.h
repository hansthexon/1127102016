﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PolitePanelScript
struct PolitePanelScript_t678233578;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PolitePanelScript::.ctor()
extern "C"  void PolitePanelScript__ctor_m1064671649 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::Start()
extern "C"  void PolitePanelScript_Start_m2711379889 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::fetchPolCodeFromDb(System.String)
extern "C"  void PolitePanelScript_fetchPolCodeFromDb_m2996742489 (PolitePanelScript_t678233578 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::OpenPoliteVideo(System.String)
extern "C"  void PolitePanelScript_OpenPoliteVideo_m1643505189 (PolitePanelScript_t678233578 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::PlayVideo()
extern "C"  void PolitePanelScript_PlayVideo_m3854543392 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::showPopscreen()
extern "C"  void PolitePanelScript_showPopscreen_m421367549 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::showreplyscreen()
extern "C"  void PolitePanelScript_showreplyscreen_m3046167474 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::Replay()
extern "C"  void PolitePanelScript_Replay_m1353112538 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::Backreplay()
extern "C"  void PolitePanelScript_Backreplay_m3546489317 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::BackPopUp()
extern "C"  void PolitePanelScript_BackPopUp_m101622362 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::ScorePanelBack()
extern "C"  void PolitePanelScript_ScorePanelBack_m2825189542 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::OpenScorePanel()
extern "C"  void PolitePanelScript_OpenScorePanel_m1638669435 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::ClosePopup()
extern "C"  void PolitePanelScript_ClosePopup_m1831239709 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::ClosePolitePanel()
extern "C"  void PolitePanelScript_ClosePolitePanel_m2588470080 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript::OpenPolitePanel()
extern "C"  void PolitePanelScript_OpenPolitePanel_m1524011516 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PolitePanelScript::disabelle()
extern "C"  Il2CppObject * PolitePanelScript_disabelle_m775476116 (PolitePanelScript_t678233578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
