﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_SQLite4Unity3d_IndexedAttribute2684901191.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.UniqueAttribute
struct  UniqueAttribute_t3806252083  : public IndexedAttribute_t2684901191
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
