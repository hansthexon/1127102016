﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2391178261(__this, ___l0, method) ((  void (*) (Enumerator_t2175549555 *, List_1_t2640819881 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3289078973(__this, method) ((  void (*) (Enumerator_t2175549555 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m493587973(__this, method) ((  Il2CppObject * (*) (Enumerator_t2175549555 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt>::Dispose()
#define Enumerator_Dispose_m198944264(__this, method) ((  void (*) (Enumerator_t2175549555 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt>::VerifyState()
#define Enumerator_VerifyState_m3921585563(__this, method) ((  void (*) (Enumerator_t2175549555 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt>::MoveNext()
#define Enumerator_MoveNext_m856251637(__this, method) ((  bool (*) (Enumerator_t2175549555 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt>::get_Current()
#define Enumerator_get_Current_m3669405322(__this, method) ((  AppleInAppPurchaseReceipt_t3271698749 * (*) (Enumerator_t2175549555 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
