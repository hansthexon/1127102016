﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.RenderBufferHelper
struct RenderBufferHelper_t1943293662;
struct RenderBufferHelper_t1943293662_marshaled_pinvoke;
struct RenderBufferHelper_t1943293662_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr2504060609.h"

// System.IntPtr UnityEngine.RenderBufferHelper::GetNativeRenderBufferPtr(System.IntPtr)
extern "C"  IntPtr_t RenderBufferHelper_GetNativeRenderBufferPtr_m3984955661 (Il2CppObject * __this /* static, unused */, IntPtr_t ___rb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderBufferHelper::INTERNAL_CALL_GetNativeRenderBufferPtr(System.IntPtr,System.IntPtr&)
extern "C"  void RenderBufferHelper_INTERNAL_CALL_GetNativeRenderBufferPtr_m3796353729 (Il2CppObject * __this /* static, unused */, IntPtr_t ___rb0, IntPtr_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct RenderBufferHelper_t1943293662;
struct RenderBufferHelper_t1943293662_marshaled_pinvoke;

extern "C" void RenderBufferHelper_t1943293662_marshal_pinvoke(const RenderBufferHelper_t1943293662& unmarshaled, RenderBufferHelper_t1943293662_marshaled_pinvoke& marshaled);
extern "C" void RenderBufferHelper_t1943293662_marshal_pinvoke_back(const RenderBufferHelper_t1943293662_marshaled_pinvoke& marshaled, RenderBufferHelper_t1943293662& unmarshaled);
extern "C" void RenderBufferHelper_t1943293662_marshal_pinvoke_cleanup(RenderBufferHelper_t1943293662_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct RenderBufferHelper_t1943293662;
struct RenderBufferHelper_t1943293662_marshaled_com;

extern "C" void RenderBufferHelper_t1943293662_marshal_com(const RenderBufferHelper_t1943293662& unmarshaled, RenderBufferHelper_t1943293662_marshaled_com& marshaled);
extern "C" void RenderBufferHelper_t1943293662_marshal_com_back(const RenderBufferHelper_t1943293662_marshaled_com& marshaled, RenderBufferHelper_t1943293662& unmarshaled);
extern "C" void RenderBufferHelper_t1943293662_marshal_com_cleanup(RenderBufferHelper_t1943293662_marshaled_com& marshaled);
