﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Ball_anim
struct  Ball_anim_t472390881  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Ball_anim::ballButton
	GameObject_t1756533147 * ___ballButton_2;
	// UnityEngine.Animator Ball_anim::ballAnim
	Animator_t69676727 * ___ballAnim_3;

public:
	inline static int32_t get_offset_of_ballButton_2() { return static_cast<int32_t>(offsetof(Ball_anim_t472390881, ___ballButton_2)); }
	inline GameObject_t1756533147 * get_ballButton_2() const { return ___ballButton_2; }
	inline GameObject_t1756533147 ** get_address_of_ballButton_2() { return &___ballButton_2; }
	inline void set_ballButton_2(GameObject_t1756533147 * value)
	{
		___ballButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___ballButton_2, value);
	}

	inline static int32_t get_offset_of_ballAnim_3() { return static_cast<int32_t>(offsetof(Ball_anim_t472390881, ___ballAnim_3)); }
	inline Animator_t69676727 * get_ballAnim_3() const { return ___ballAnim_3; }
	inline Animator_t69676727 ** get_address_of_ballAnim_3() { return &___ballAnim_3; }
	inline void set_ballAnim_3(Animator_t69676727 * value)
	{
		___ballAnim_3 = value;
		Il2CppCodeGenWriteBarrier(&___ballAnim_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
