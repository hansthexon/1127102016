﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Doll_anim
struct  Doll_anim_t2243519265  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Doll_anim::dollButton
	GameObject_t1756533147 * ___dollButton_2;
	// UnityEngine.Animator Doll_anim::dollAnim
	Animator_t69676727 * ___dollAnim_3;

public:
	inline static int32_t get_offset_of_dollButton_2() { return static_cast<int32_t>(offsetof(Doll_anim_t2243519265, ___dollButton_2)); }
	inline GameObject_t1756533147 * get_dollButton_2() const { return ___dollButton_2; }
	inline GameObject_t1756533147 ** get_address_of_dollButton_2() { return &___dollButton_2; }
	inline void set_dollButton_2(GameObject_t1756533147 * value)
	{
		___dollButton_2 = value;
		Il2CppCodeGenWriteBarrier(&___dollButton_2, value);
	}

	inline static int32_t get_offset_of_dollAnim_3() { return static_cast<int32_t>(offsetof(Doll_anim_t2243519265, ___dollAnim_3)); }
	inline Animator_t69676727 * get_dollAnim_3() const { return ___dollAnim_3; }
	inline Animator_t69676727 ** get_address_of_dollAnim_3() { return &___dollAnim_3; }
	inline void set_dollAnim_3(Animator_t69676727 * value)
	{
		___dollAnim_3 = value;
		Il2CppCodeGenWriteBarrier(&___dollAnim_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
