﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3030399641;
// LitJson.Lexer/StateHandler[]
struct StateHandlerU5BU5D_t1593819722;
// LitJson.FsmContext
struct FsmContext_t1296252303;
// System.IO.TextReader
struct TextReader_t1561828458;
// System.Text.StringBuilder
struct StringBuilder_t1221177846;
// System.String
struct String_t;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.Lexer
struct  Lexer_t186508296  : public Il2CppObject
{
public:
	// System.Int32 LitJson.Lexer::inputBuffer
	int32_t ___inputBuffer_2;
	// System.Int32 LitJson.Lexer::inputChar
	int32_t ___inputChar_3;
	// System.Int32 LitJson.Lexer::state
	int32_t ___state_4;
	// System.Int32 LitJson.Lexer::unichar
	int32_t ___unichar_5;
	// LitJson.FsmContext LitJson.Lexer::context
	FsmContext_t1296252303 * ___context_6;
	// System.IO.TextReader LitJson.Lexer::reader
	TextReader_t1561828458 * ___reader_7;
	// System.Text.StringBuilder LitJson.Lexer::stringBuffer
	StringBuilder_t1221177846 * ___stringBuffer_8;
	// System.Boolean LitJson.Lexer::<AllowComments>k__BackingField
	bool ___U3CAllowCommentsU3Ek__BackingField_9;
	// System.Boolean LitJson.Lexer::<AllowSingleQuotedStrings>k__BackingField
	bool ___U3CAllowSingleQuotedStringsU3Ek__BackingField_10;
	// System.Boolean LitJson.Lexer::<EndOfInput>k__BackingField
	bool ___U3CEndOfInputU3Ek__BackingField_11;
	// System.Int32 LitJson.Lexer::<Token>k__BackingField
	int32_t ___U3CTokenU3Ek__BackingField_12;
	// System.String LitJson.Lexer::<StringValue>k__BackingField
	String_t* ___U3CStringValueU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_inputBuffer_2() { return static_cast<int32_t>(offsetof(Lexer_t186508296, ___inputBuffer_2)); }
	inline int32_t get_inputBuffer_2() const { return ___inputBuffer_2; }
	inline int32_t* get_address_of_inputBuffer_2() { return &___inputBuffer_2; }
	inline void set_inputBuffer_2(int32_t value)
	{
		___inputBuffer_2 = value;
	}

	inline static int32_t get_offset_of_inputChar_3() { return static_cast<int32_t>(offsetof(Lexer_t186508296, ___inputChar_3)); }
	inline int32_t get_inputChar_3() const { return ___inputChar_3; }
	inline int32_t* get_address_of_inputChar_3() { return &___inputChar_3; }
	inline void set_inputChar_3(int32_t value)
	{
		___inputChar_3 = value;
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(Lexer_t186508296, ___state_4)); }
	inline int32_t get_state_4() const { return ___state_4; }
	inline int32_t* get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(int32_t value)
	{
		___state_4 = value;
	}

	inline static int32_t get_offset_of_unichar_5() { return static_cast<int32_t>(offsetof(Lexer_t186508296, ___unichar_5)); }
	inline int32_t get_unichar_5() const { return ___unichar_5; }
	inline int32_t* get_address_of_unichar_5() { return &___unichar_5; }
	inline void set_unichar_5(int32_t value)
	{
		___unichar_5 = value;
	}

	inline static int32_t get_offset_of_context_6() { return static_cast<int32_t>(offsetof(Lexer_t186508296, ___context_6)); }
	inline FsmContext_t1296252303 * get_context_6() const { return ___context_6; }
	inline FsmContext_t1296252303 ** get_address_of_context_6() { return &___context_6; }
	inline void set_context_6(FsmContext_t1296252303 * value)
	{
		___context_6 = value;
		Il2CppCodeGenWriteBarrier(&___context_6, value);
	}

	inline static int32_t get_offset_of_reader_7() { return static_cast<int32_t>(offsetof(Lexer_t186508296, ___reader_7)); }
	inline TextReader_t1561828458 * get_reader_7() const { return ___reader_7; }
	inline TextReader_t1561828458 ** get_address_of_reader_7() { return &___reader_7; }
	inline void set_reader_7(TextReader_t1561828458 * value)
	{
		___reader_7 = value;
		Il2CppCodeGenWriteBarrier(&___reader_7, value);
	}

	inline static int32_t get_offset_of_stringBuffer_8() { return static_cast<int32_t>(offsetof(Lexer_t186508296, ___stringBuffer_8)); }
	inline StringBuilder_t1221177846 * get_stringBuffer_8() const { return ___stringBuffer_8; }
	inline StringBuilder_t1221177846 ** get_address_of_stringBuffer_8() { return &___stringBuffer_8; }
	inline void set_stringBuffer_8(StringBuilder_t1221177846 * value)
	{
		___stringBuffer_8 = value;
		Il2CppCodeGenWriteBarrier(&___stringBuffer_8, value);
	}

	inline static int32_t get_offset_of_U3CAllowCommentsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Lexer_t186508296, ___U3CAllowCommentsU3Ek__BackingField_9)); }
	inline bool get_U3CAllowCommentsU3Ek__BackingField_9() const { return ___U3CAllowCommentsU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CAllowCommentsU3Ek__BackingField_9() { return &___U3CAllowCommentsU3Ek__BackingField_9; }
	inline void set_U3CAllowCommentsU3Ek__BackingField_9(bool value)
	{
		___U3CAllowCommentsU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CAllowSingleQuotedStringsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Lexer_t186508296, ___U3CAllowSingleQuotedStringsU3Ek__BackingField_10)); }
	inline bool get_U3CAllowSingleQuotedStringsU3Ek__BackingField_10() const { return ___U3CAllowSingleQuotedStringsU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CAllowSingleQuotedStringsU3Ek__BackingField_10() { return &___U3CAllowSingleQuotedStringsU3Ek__BackingField_10; }
	inline void set_U3CAllowSingleQuotedStringsU3Ek__BackingField_10(bool value)
	{
		___U3CAllowSingleQuotedStringsU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CEndOfInputU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(Lexer_t186508296, ___U3CEndOfInputU3Ek__BackingField_11)); }
	inline bool get_U3CEndOfInputU3Ek__BackingField_11() const { return ___U3CEndOfInputU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CEndOfInputU3Ek__BackingField_11() { return &___U3CEndOfInputU3Ek__BackingField_11; }
	inline void set_U3CEndOfInputU3Ek__BackingField_11(bool value)
	{
		___U3CEndOfInputU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CTokenU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Lexer_t186508296, ___U3CTokenU3Ek__BackingField_12)); }
	inline int32_t get_U3CTokenU3Ek__BackingField_12() const { return ___U3CTokenU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CTokenU3Ek__BackingField_12() { return &___U3CTokenU3Ek__BackingField_12; }
	inline void set_U3CTokenU3Ek__BackingField_12(int32_t value)
	{
		___U3CTokenU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CStringValueU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Lexer_t186508296, ___U3CStringValueU3Ek__BackingField_13)); }
	inline String_t* get_U3CStringValueU3Ek__BackingField_13() const { return ___U3CStringValueU3Ek__BackingField_13; }
	inline String_t** get_address_of_U3CStringValueU3Ek__BackingField_13() { return &___U3CStringValueU3Ek__BackingField_13; }
	inline void set_U3CStringValueU3Ek__BackingField_13(String_t* value)
	{
		___U3CStringValueU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CStringValueU3Ek__BackingField_13, value);
	}
};

struct Lexer_t186508296_StaticFields
{
public:
	// System.Int32[] LitJson.Lexer::returnTable
	Int32U5BU5D_t3030399641* ___returnTable_0;
	// LitJson.Lexer/StateHandler[] LitJson.Lexer::handlerTable
	StateHandlerU5BU5D_t1593819722* ___handlerTable_1;

public:
	inline static int32_t get_offset_of_returnTable_0() { return static_cast<int32_t>(offsetof(Lexer_t186508296_StaticFields, ___returnTable_0)); }
	inline Int32U5BU5D_t3030399641* get_returnTable_0() const { return ___returnTable_0; }
	inline Int32U5BU5D_t3030399641** get_address_of_returnTable_0() { return &___returnTable_0; }
	inline void set_returnTable_0(Int32U5BU5D_t3030399641* value)
	{
		___returnTable_0 = value;
		Il2CppCodeGenWriteBarrier(&___returnTable_0, value);
	}

	inline static int32_t get_offset_of_handlerTable_1() { return static_cast<int32_t>(offsetof(Lexer_t186508296_StaticFields, ___handlerTable_1)); }
	inline StateHandlerU5BU5D_t1593819722* get_handlerTable_1() const { return ___handlerTable_1; }
	inline StateHandlerU5BU5D_t1593819722** get_address_of_handlerTable_1() { return &___handlerTable_1; }
	inline void set_handlerTable_1(StateHandlerU5BU5D_t1593819722* value)
	{
		___handlerTable_1 = value;
		Il2CppCodeGenWriteBarrier(&___handlerTable_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
