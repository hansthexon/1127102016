﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.FactoryFunc
struct FactoryFunc_t2436388556;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void LitJson.FactoryFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void FactoryFunc__ctor_m2782465188 (FactoryFunc_t2436388556 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.FactoryFunc::Invoke()
extern "C"  Il2CppObject * FactoryFunc_Invoke_m928291171 (FactoryFunc_t2436388556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult LitJson.FactoryFunc::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * FactoryFunc_BeginInvoke_m2597184889 (FactoryFunc_t2436388556 * __this, AsyncCallback_t163412349 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.FactoryFunc::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * FactoryFunc_EndInvoke_m3346033357 (FactoryFunc_t2436388556 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
