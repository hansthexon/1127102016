﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager1/<Objectstoserver>c__IteratorD
struct U3CObjectstoserverU3Ec__IteratorD_t666160444;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager1/<Objectstoserver>c__IteratorD::.ctor()
extern "C"  void U3CObjectstoserverU3Ec__IteratorD__ctor_m386742135 (U3CObjectstoserverU3Ec__IteratorD_t666160444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager1/<Objectstoserver>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CObjectstoserverU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3279010553 (U3CObjectstoserverU3Ec__IteratorD_t666160444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager1/<Objectstoserver>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CObjectstoserverU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m594427585 (U3CObjectstoserverU3Ec__IteratorD_t666160444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager1/<Objectstoserver>c__IteratorD::MoveNext()
extern "C"  bool U3CObjectstoserverU3Ec__IteratorD_MoveNext_m3573574237 (U3CObjectstoserverU3Ec__IteratorD_t666160444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1/<Objectstoserver>c__IteratorD::Dispose()
extern "C"  void U3CObjectstoserverU3Ec__IteratorD_Dispose_m2313627546 (U3CObjectstoserverU3Ec__IteratorD_t666160444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager1/<Objectstoserver>c__IteratorD::Reset()
extern "C"  void U3CObjectstoserverU3Ec__IteratorD_Reset_m3605061968 (U3CObjectstoserverU3Ec__IteratorD_t666160444 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
