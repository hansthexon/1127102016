﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonData
struct JsonData_t269267574;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Array
struct Il2CppArray;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// LitJson.JsonWriter
struct JsonWriter_t1927598499;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t2981295538;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData>
struct IDictionary_2_t183130257;
// System.Collections.Generic.IList`1<LitJson.JsonData>
struct IList_1_t810208175;
// System.Collections.IDictionary
struct IDictionary_t596158605;
// System.Collections.IList
struct IList_t3321498491;
// LitJson.IJsonWrapper
struct IJsonWrapper_t3045908096;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Decimal724701077.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1927598499.h"
#include "AssemblyU2DCSharp_LitJson_JsonData269267574.h"
#include "AssemblyU2DCSharp_LitJson_JsonType3145703806.h"

// System.Void LitJson.JsonData::.ctor(System.Boolean)
extern "C"  void JsonData__ctor_m2569786487 (JsonData_t269267574 * __this, bool ___boolean0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.Double)
extern "C"  void JsonData__ctor_m1730543662 (JsonData_t269267574 * __this, double ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.Int64)
extern "C"  void JsonData__ctor_m1455597840 (JsonData_t269267574 * __this, int64_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.String)
extern "C"  void JsonData__ctor_m3845109798 (JsonData_t269267574 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.Object)
extern "C"  void JsonData__ctor_m3622798684 (JsonData_t269267574 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor()
extern "C"  void JsonData__ctor_m3069984372 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.SByte)
extern "C"  void JsonData__ctor_m2276399448 (JsonData_t269267574 * __this, int8_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.Byte)
extern "C"  void JsonData__ctor_m1969282949 (JsonData_t269267574 * __this, uint8_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.Int16)
extern "C"  void JsonData__ctor_m2618397547 (JsonData_t269267574 * __this, int16_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.UInt16)
extern "C"  void JsonData__ctor_m2481951208 (JsonData_t269267574 * __this, uint16_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.Int32)
extern "C"  void JsonData__ctor_m292798653 (JsonData_t269267574 * __this, int32_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.UInt32)
extern "C"  void JsonData__ctor_m2764276214 (JsonData_t269267574 * __this, uint32_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.UInt64)
extern "C"  void JsonData__ctor_m1783045247 (JsonData_t269267574 * __this, uint64_t ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.Single)
extern "C"  void JsonData__ctor_m2868464133 (JsonData_t269267574 * __this, float ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::.ctor(System.Decimal)
extern "C"  void JsonData__ctor_m2089576128 (JsonData_t269267574 * __this, Decimal_t724701077  ___number0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonData::System.Collections.ICollection.get_Count()
extern "C"  int32_t JsonData_System_Collections_ICollection_get_Count_m571890873 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool JsonData_System_Collections_ICollection_get_IsSynchronized_m4151048270 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonData::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * JsonData_System_Collections_ICollection_get_SyncRoot_m1812955940 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool JsonData_System_Collections_IDictionary_get_IsFixedSize_m2326861627 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool JsonData_System_Collections_IDictionary_get_IsReadOnly_m325669830 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection LitJson.JsonData::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_get_Keys_m2061945284 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection LitJson.JsonData::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_get_Values_m2863610242 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::System.Collections.IList.get_IsFixedSize()
extern "C"  bool JsonData_System_Collections_IList_get_IsFixedSize_m1071291553 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::System.Collections.IList.get_IsReadOnly()
extern "C"  bool JsonData_System_Collections_IList_get_IsReadOnly_m595009218 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonData::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_get_Item_m472563764 (JsonData_t269267574 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void JsonData_System_Collections_IDictionary_set_Item_m2999951899 (JsonData_t269267574 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonData::System.Collections.Specialized.IOrderedDictionary.get_Item(System.Int32)
extern "C"  Il2CppObject * JsonData_System_Collections_Specialized_IOrderedDictionary_get_Item_m2858511653 (JsonData_t269267574 * __this, int32_t ___idx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.Specialized.IOrderedDictionary.set_Item(System.Int32,System.Object)
extern "C"  void JsonData_System_Collections_Specialized_IOrderedDictionary_set_Item_m1183431218 (JsonData_t269267574 * __this, int32_t ___idx0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LitJson.JsonData::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * JsonData_System_Collections_IList_get_Item_m711004753 (JsonData_t269267574 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void JsonData_System_Collections_IList_set_Item_m1748366562 (JsonData_t269267574 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void JsonData_System_Collections_ICollection_CopyTo_m3783564164 (JsonData_t269267574 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void JsonData_System_Collections_IDictionary_Add_m2356202054 (JsonData_t269267574 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.IDictionary.Clear()
extern "C"  void JsonData_System_Collections_IDictionary_Clear_m3290395890 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool JsonData_System_Collections_IDictionary_Contains_m4187115580 (JsonData_t269267574 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator LitJson.JsonData::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonData_System_Collections_IDictionary_GetEnumerator_m626147563 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void JsonData_System_Collections_IDictionary_Remove_m2266606159 (JsonData_t269267574 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LitJson.JsonData::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JsonData_System_Collections_IEnumerable_GetEnumerator_m4204737205 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::LitJson.IJsonWrapper.ToJson(LitJson.JsonWriter)
extern "C"  void JsonData_LitJson_IJsonWrapper_ToJson_m1801374742 (JsonData_t269267574 * __this, JsonWriter_t1927598499 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonData::System.Collections.IList.Add(System.Object)
extern "C"  int32_t JsonData_System_Collections_IList_Add_m1165043110 (JsonData_t269267574 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.IList.Clear()
extern "C"  void JsonData_System_Collections_IList_Clear_m2914629598 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::System.Collections.IList.Contains(System.Object)
extern "C"  bool JsonData_System_Collections_IList_Contains_m3521432384 (JsonData_t269267574 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonData::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t JsonData_System_Collections_IList_IndexOf_m1015669628 (JsonData_t269267574 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void JsonData_System_Collections_IList_Insert_m1309040309 (JsonData_t269267574 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.IList.Remove(System.Object)
extern "C"  void JsonData_System_Collections_IList_Remove_m1432476133 (JsonData_t269267574 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void JsonData_System_Collections_IList_RemoveAt_m3478545335 (JsonData_t269267574 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionaryEnumerator LitJson.JsonData::System.Collections.Specialized.IOrderedDictionary.GetEnumerator()
extern "C"  Il2CppObject * JsonData_System_Collections_Specialized_IOrderedDictionary_GetEnumerator_m3837487801 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.Specialized.IOrderedDictionary.Insert(System.Int32,System.Object,System.Object)
extern "C"  void JsonData_System_Collections_Specialized_IOrderedDictionary_Insert_m177735079 (JsonData_t269267574 * __this, int32_t ___idx0, Il2CppObject * ___key1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::System.Collections.Specialized.IOrderedDictionary.RemoveAt(System.Int32)
extern "C"  void JsonData_System_Collections_Specialized_IOrderedDictionary_RemoveAt_m3694504307 (JsonData_t269267574 * __this, int32_t ___idx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonData::get_Count()
extern "C"  int32_t JsonData_get_Count_m1584279558 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::get_IsArray()
extern "C"  bool JsonData_get_IsArray_m2633928780 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::get_IsBoolean()
extern "C"  bool JsonData_get_IsBoolean_m3787095175 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::get_IsReal()
extern "C"  bool JsonData_get_IsReal_m3564576953 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::get_IsNatural()
extern "C"  bool JsonData_get_IsNatural_m2732090332 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::get_IsObject()
extern "C"  bool JsonData_get_IsObject_m4069969874 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::get_IsString()
extern "C"  bool JsonData_get_IsString_m2224549720 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.String> LitJson.JsonData::get_Keys()
extern "C"  Il2CppObject* JsonData_get_Keys_m2138133699 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonData::get_Item(System.String)
extern "C"  JsonData_t269267574 * JsonData_get_Item_m831619934 (JsonData_t269267574 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::set_Item(System.String,LitJson.JsonData)
extern "C"  void JsonData_set_Item_m24780213 (JsonData_t269267574 * __this, String_t* ___name0, JsonData_t269267574 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonData::get_Item(System.Int32)
extern "C"  JsonData_t269267574 * JsonData_get_Item_m2655211997 (JsonData_t269267574 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::set_Item(System.Int32,LitJson.JsonData)
extern "C"  void JsonData_set_Item_m2193853438 (JsonData_t269267574 * __this, int32_t ___index0, JsonData_t269267574 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::GetBoolean()
extern "C"  bool JsonData_GetBoolean_m3600467092 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double LitJson.JsonData::GetReal()
extern "C"  double JsonData_GetReal_m2472435581 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 LitJson.JsonData::GetNatural()
extern "C"  int64_t JsonData_GetNatural_m1862393076 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LitJson.JsonData::GetString()
extern "C"  String_t* JsonData_GetString_m670893246 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,LitJson.JsonData> LitJson.JsonData::GetObject()
extern "C"  Il2CppObject* JsonData_GetObject_m4036801391 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<LitJson.JsonData> LitJson.JsonData::GetArray()
extern "C"  Il2CppObject* JsonData_GetArray_m2639648870 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::SetBoolean(System.Boolean)
extern "C"  void JsonData_SetBoolean_m2467432249 (JsonData_t269267574 * __this, bool ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::SetReal(System.Double)
extern "C"  void JsonData_SetReal_m1099570466 (JsonData_t269267574 * __this, double ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::SetNatural(System.Int64)
extern "C"  void JsonData_SetNatural_m3842559241 (JsonData_t269267574 * __this, int64_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::SetString(System.String)
extern "C"  void JsonData_SetString_m2972387761 (JsonData_t269267574 * __this, String_t* ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ICollection LitJson.JsonData::EnsureCollection()
extern "C"  Il2CppObject * JsonData_EnsureCollection_m1194492042 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IDictionary LitJson.JsonData::EnsureDictionary()
extern "C"  Il2CppObject * JsonData_EnsureDictionary_m2290433526 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IList LitJson.JsonData::EnsureList()
extern "C"  Il2CppObject * JsonData_EnsureList_m409706738 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonData::ToJsonData(System.Object)
extern "C"  JsonData_t269267574 * JsonData_ToJsonData_m1794926921 (JsonData_t269267574 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::WriteJson(LitJson.IJsonWrapper,LitJson.JsonWriter)
extern "C"  void JsonData_WriteJson_m2296070584 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, JsonWriter_t1927598499 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonData::Add(System.Object)
extern "C"  int32_t JsonData_Add_m3431622181 (JsonData_t269267574 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::Clear()
extern "C"  void JsonData_Clear_m3881293829 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::Equals(LitJson.JsonData)
extern "C"  bool JsonData_Equals_m2439336510 (JsonData_t269267574 * __this, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::Equals(System.Object)
extern "C"  bool JsonData_Equals_m2531943757 (JsonData_t269267574 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonData::GetHashCode()
extern "C"  int32_t JsonData_GetHashCode_m3417434851 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonType LitJson.JsonData::GetJsonType()
extern "C"  int32_t JsonData_GetJsonType_m3245864530 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::SetJsonType(LitJson.JsonType)
extern "C"  void JsonData_SetJsonType_m3008447885 (JsonData_t269267574 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LitJson.JsonData::ToJson()
extern "C"  String_t* JsonData_ToJson_m3686079716 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonData::ToJson(LitJson.JsonWriter)
extern "C"  void JsonData_ToJson_m183887703 (JsonData_t269267574 * __this, JsonWriter_t1927598499 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LitJson.JsonData::ToString()
extern "C"  String_t* JsonData_ToString_m3226006459 (JsonData_t269267574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonData::op_Implicit(System.Boolean)
extern "C"  JsonData_t269267574 * JsonData_op_Implicit_m1914861502 (Il2CppObject * __this /* static, unused */, bool ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonData::op_Implicit(System.Double)
extern "C"  JsonData_t269267574 * JsonData_op_Implicit_m2319857245 (Il2CppObject * __this /* static, unused */, double ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonData::op_Implicit(System.Int64)
extern "C"  JsonData_t269267574 * JsonData_op_Implicit_m3158813381 (Il2CppObject * __this /* static, unused */, int64_t ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LitJson.JsonData LitJson.JsonData::op_Implicit(System.String)
extern "C"  JsonData_t269267574 * JsonData_op_Implicit_m1003883913 (Il2CppObject * __this /* static, unused */, String_t* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  bool JsonData_op_Explicit_m3829788929 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  float JsonData_op_Explicit_m1111770211 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  double JsonData_op_Explicit_m2755897616 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  Decimal_t724701077  JsonData_op_Explicit_m525957458 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  int8_t JsonData_op_Explicit_m3714451498 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  uint8_t JsonData_op_Explicit_m3028707527 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  int16_t JsonData_op_Explicit_m599569141 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  uint16_t JsonData_op_Explicit_m3371249446 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  int32_t JsonData_op_Explicit_m1734525919 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  uint32_t JsonData_op_Explicit_m670288500 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  int64_t JsonData_op_Explicit_m1482685294 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  uint64_t JsonData_op_Explicit_m145214945 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LitJson.JsonData::op_Explicit(LitJson.JsonData)
extern "C"  String_t* JsonData_op_Explicit_m544061060 (Il2CppObject * __this /* static, unused */, JsonData_t269267574 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
