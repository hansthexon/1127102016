﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct Dictionary_2_t955353609;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1284510226;
// System.Collections.Generic.IDictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct IDictionary_2_t3249404326;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t228987430;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t3023952753;
// System.Collections.Generic.ICollection`1<Vuforia.VuforiaManagerImpl/TrackableResultData>
struct ICollection_1_t2899603279;
// System.Collections.ICollection
struct ICollection_t91669223;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>[]
struct KeyValuePair_2U5BU5D_t2718506006;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>>
struct IEnumerator_1_t483189954;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t259680273;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct KeyCollection_t3438851380;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>
struct ValueCollection_t3953380748;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serialization228987430.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon1417235061.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23007666127.h"
#include "mscorlib_System_Array3829468939.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2275378311.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor()
extern "C"  void Dictionary_2__ctor_m158897782_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m158897782(__this, method) ((  void (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2__ctor_m158897782_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m132253999_gshared (Dictionary_2_t955353609 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m132253999(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t955353609 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m132253999_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3847157416_gshared (Dictionary_2_t955353609 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3847157416(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t955353609 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3847157416_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3298220713_gshared (Dictionary_2_t955353609 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3298220713(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t955353609 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3298220713_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3617465753_gshared (Dictionary_2_t955353609 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3617465753(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t955353609 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3617465753_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1142500402_gshared (Dictionary_2_t955353609 * __this, int32_t ___capacity0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1142500402(__this, ___capacity0, ___comparer1, method) ((  void (*) (Dictionary_2_t955353609 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1142500402_gshared)(__this, ___capacity0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1768416279_gshared (Dictionary_2_t955353609 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1768416279(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t955353609 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2__ctor_m1768416279_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3386941512_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3386941512(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3386941512_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1785792832_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1785792832(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1785792832_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1408409264_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1408409264(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1408409264_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m88265350_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m88265350(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m88265350_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1076805763_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1076805763(__this, method) ((  bool (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1076805763_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2170729778_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2170729778(__this, method) ((  bool (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2170729778_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2810897254_gshared (Dictionary_2_t955353609 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2810897254(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t955353609 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2810897254_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1851182879_gshared (Dictionary_2_t955353609 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1851182879(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t955353609 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1851182879_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m763283792_gshared (Dictionary_2_t955353609 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m763283792(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t955353609 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m763283792_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m945340112_gshared (Dictionary_2_t955353609 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m945340112(__this, ___key0, method) ((  bool (*) (Dictionary_2_t955353609 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m945340112_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m959383643_gshared (Dictionary_2_t955353609 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m959383643(__this, ___key0, method) ((  void (*) (Dictionary_2_t955353609 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m959383643_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3622682294_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3622682294(__this, method) ((  bool (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3622682294_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m441270246_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m441270246(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m441270246_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1089958920_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1089958920(__this, method) ((  bool (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1089958920_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2246957287_gshared (Dictionary_2_t955353609 * __this, KeyValuePair_2_t3007666127  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2246957287(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t955353609 *, KeyValuePair_2_t3007666127 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2246957287_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m851380277_gshared (Dictionary_2_t955353609 * __this, KeyValuePair_2_t3007666127  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m851380277(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t955353609 *, KeyValuePair_2_t3007666127 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m851380277_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3166319691_gshared (Dictionary_2_t955353609 * __this, KeyValuePair_2U5BU5D_t2718506006* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3166319691(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t955353609 *, KeyValuePair_2U5BU5D_t2718506006*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3166319691_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1322279506_gshared (Dictionary_2_t955353609 * __this, KeyValuePair_2_t3007666127  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1322279506(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t955353609 *, KeyValuePair_2_t3007666127 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1322279506_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m464813670_gshared (Dictionary_2_t955353609 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m464813670(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t955353609 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m464813670_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m673170929_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m673170929(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m673170929_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m979038432_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m979038432(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m979038432_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4217558643_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4217558643(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m4217558643_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3320284758_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3320284758(__this, method) ((  int32_t (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_get_Count_m3320284758_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Item(TKey)
extern "C"  TrackableResultData_t1947527974  Dictionary_2_get_Item_m3178830267_gshared (Dictionary_2_t955353609 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3178830267(__this, ___key0, method) ((  TrackableResultData_t1947527974  (*) (Dictionary_2_t955353609 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3178830267_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1476377684_gshared (Dictionary_2_t955353609 * __this, int32_t ___key0, TrackableResultData_t1947527974  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1476377684(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t955353609 *, int32_t, TrackableResultData_t1947527974 , const MethodInfo*))Dictionary_2_set_Item_m1476377684_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m1590616146_gshared (Dictionary_2_t955353609 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m1590616146(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t955353609 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m1590616146_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m4155350001_gshared (Dictionary_2_t955353609 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m4155350001(__this, ___size0, method) ((  void (*) (Dictionary_2_t955353609 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m4155350001_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2670764163_gshared (Dictionary_2_t955353609 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2670764163(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t955353609 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2670764163_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3007666127  Dictionary_2_make_pair_m2245584521_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, TrackableResultData_t1947527974  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2245584521(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3007666127  (*) (Il2CppObject * /* static, unused */, int32_t, TrackableResultData_t1947527974 , const MethodInfo*))Dictionary_2_make_pair_m2245584521_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m3680604305_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, TrackableResultData_t1947527974  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3680604305(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, TrackableResultData_t1947527974 , const MethodInfo*))Dictionary_2_pick_key_m3680604305_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::pick_value(TKey,TValue)
extern "C"  TrackableResultData_t1947527974  Dictionary_2_pick_value_m2064847393_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, TrackableResultData_t1947527974  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2064847393(__this /* static, unused */, ___key0, ___value1, method) ((  TrackableResultData_t1947527974  (*) (Il2CppObject * /* static, unused */, int32_t, TrackableResultData_t1947527974 , const MethodInfo*))Dictionary_2_pick_value_m2064847393_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2819124408_gshared (Dictionary_2_t955353609 * __this, KeyValuePair_2U5BU5D_t2718506006* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2819124408(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t955353609 *, KeyValuePair_2U5BU5D_t2718506006*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2819124408_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Resize()
extern "C"  void Dictionary_2_Resize_m1842544392_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1842544392(__this, method) ((  void (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_Resize_m1842544392_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m293148158_gshared (Dictionary_2_t955353609 * __this, int32_t ___key0, TrackableResultData_t1947527974  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m293148158(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t955353609 *, int32_t, TrackableResultData_t1947527974 , const MethodInfo*))Dictionary_2_Add_m293148158_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Clear()
extern "C"  void Dictionary_2_Clear_m3278315681_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3278315681(__this, method) ((  void (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_Clear_m3278315681_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m4033826259_gshared (Dictionary_2_t955353609 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m4033826259(__this, ___key0, method) ((  bool (*) (Dictionary_2_t955353609 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m4033826259_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m410637539_gshared (Dictionary_2_t955353609 * __this, TrackableResultData_t1947527974  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m410637539(__this, ___value0, method) ((  bool (*) (Dictionary_2_t955353609 *, TrackableResultData_t1947527974 , const MethodInfo*))Dictionary_2_ContainsValue_m410637539_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m636405210_gshared (Dictionary_2_t955353609 * __this, SerializationInfo_t228987430 * ___info0, StreamingContext_t1417235061  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m636405210(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t955353609 *, SerializationInfo_t228987430 *, StreamingContext_t1417235061 , const MethodInfo*))Dictionary_2_GetObjectData_m636405210_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2695221940_gshared (Dictionary_2_t955353609 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2695221940(__this, ___sender0, method) ((  void (*) (Dictionary_2_t955353609 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2695221940_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3788208125_gshared (Dictionary_2_t955353609 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3788208125(__this, ___key0, method) ((  bool (*) (Dictionary_2_t955353609 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m3788208125_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3445754617_gshared (Dictionary_2_t955353609 * __this, int32_t ___key0, TrackableResultData_t1947527974 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3445754617(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t955353609 *, int32_t, TrackableResultData_t1947527974 *, const MethodInfo*))Dictionary_2_TryGetValue_m3445754617_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Keys()
extern "C"  KeyCollection_t3438851380 * Dictionary_2_get_Keys_m2028703215_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2028703215(__this, method) ((  KeyCollection_t3438851380 * (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_get_Keys_m2028703215_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::get_Values()
extern "C"  ValueCollection_t3953380748 * Dictionary_2_get_Values_m294776743_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m294776743(__this, method) ((  ValueCollection_t3953380748 * (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_get_Values_m294776743_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m3947901666_gshared (Dictionary_2_t955353609 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3947901666(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t955353609 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3947901666_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ToTValue(System.Object)
extern "C"  TrackableResultData_t1947527974  Dictionary_2_ToTValue_m354356154_gshared (Dictionary_2_t955353609 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m354356154(__this, ___value0, method) ((  TrackableResultData_t1947527974  (*) (Dictionary_2_t955353609 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m354356154_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m576331344_gshared (Dictionary_2_t955353609 * __this, KeyValuePair_2_t3007666127  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m576331344(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t955353609 *, KeyValuePair_2_t3007666127 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m576331344_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::GetEnumerator()
extern "C"  Enumerator_t2275378311  Dictionary_2_GetEnumerator_m1515531893_gshared (Dictionary_2_t955353609 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1515531893(__this, method) ((  Enumerator_t2275378311  (*) (Dictionary_2_t955353609 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1515531893_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,Vuforia.VuforiaManagerImpl/TrackableResultData>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t3048875398  Dictionary_2_U3CCopyToU3Em__0_m131191010_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, TrackableResultData_t1947527974  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m131191010(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t3048875398  (*) (Il2CppObject * /* static, unused */, int32_t, TrackableResultData_t1947527974 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m131191010_gshared)(__this /* static, unused */, ___key0, ___value1, method)
