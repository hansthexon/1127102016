﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.QuickSort`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct QuickSort_1_t4250470945;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IEnumerable_1_t966287033;
// System.Linq.SortContext`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct SortContext_1_t4078456443;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.QuickSort`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  void QuickSort_1__ctor_m1122223214_gshared (QuickSort_1_t4250470945 * __this, Il2CppObject* ___source0, SortContext_1_t4078456443 * ___context1, const MethodInfo* method);
#define QuickSort_1__ctor_m1122223214(__this, ___source0, ___context1, method) ((  void (*) (QuickSort_1_t4250470945 *, Il2CppObject*, SortContext_1_t4078456443 *, const MethodInfo*))QuickSort_1__ctor_m1122223214_gshared)(__this, ___source0, ___context1, method)
// System.Int32[] System.Linq.QuickSort`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CreateIndexes(System.Int32)
extern "C"  Int32U5BU5D_t3030399641* QuickSort_1_CreateIndexes_m1214331315_gshared (Il2CppObject * __this /* static, unused */, int32_t ___length0, const MethodInfo* method);
#define QuickSort_1_CreateIndexes_m1214331315(__this /* static, unused */, ___length0, method) ((  Int32U5BU5D_t3030399641* (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))QuickSort_1_CreateIndexes_m1214331315_gshared)(__this /* static, unused */, ___length0, method)
// System.Void System.Linq.QuickSort`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::PerformSort()
extern "C"  void QuickSort_1_PerformSort_m1872798941_gshared (QuickSort_1_t4250470945 * __this, const MethodInfo* method);
#define QuickSort_1_PerformSort_m1872798941(__this, method) ((  void (*) (QuickSort_1_t4250470945 *, const MethodInfo*))QuickSort_1_PerformSort_m1872798941_gshared)(__this, method)
// System.Int32 System.Linq.QuickSort`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CompareItems(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_CompareItems_m2251442577_gshared (QuickSort_1_t4250470945 * __this, int32_t ___first_index0, int32_t ___second_index1, const MethodInfo* method);
#define QuickSort_1_CompareItems_m2251442577(__this, ___first_index0, ___second_index1, method) ((  int32_t (*) (QuickSort_1_t4250470945 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_CompareItems_m2251442577_gshared)(__this, ___first_index0, ___second_index1, method)
// System.Int32 System.Linq.QuickSort`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::MedianOfThree(System.Int32,System.Int32)
extern "C"  int32_t QuickSort_1_MedianOfThree_m3935169177_gshared (QuickSort_1_t4250470945 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define QuickSort_1_MedianOfThree_m3935169177(__this, ___left0, ___right1, method) ((  int32_t (*) (QuickSort_1_t4250470945 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_MedianOfThree_m3935169177_gshared)(__this, ___left0, ___right1, method)
// System.Void System.Linq.QuickSort`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Sort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Sort_m2415536172_gshared (QuickSort_1_t4250470945 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define QuickSort_1_Sort_m2415536172(__this, ___left0, ___right1, method) ((  void (*) (QuickSort_1_t4250470945 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_Sort_m2415536172_gshared)(__this, ___left0, ___right1, method)
// System.Void System.Linq.QuickSort`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::InsertionSort(System.Int32,System.Int32)
extern "C"  void QuickSort_1_InsertionSort_m2110931455_gshared (QuickSort_1_t4250470945 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define QuickSort_1_InsertionSort_m2110931455(__this, ___left0, ___right1, method) ((  void (*) (QuickSort_1_t4250470945 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_InsertionSort_m2110931455_gshared)(__this, ___left0, ___right1, method)
// System.Void System.Linq.QuickSort`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Swap(System.Int32,System.Int32)
extern "C"  void QuickSort_1_Swap_m402190387_gshared (QuickSort_1_t4250470945 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
#define QuickSort_1_Swap_m402190387(__this, ___left0, ___right1, method) ((  void (*) (QuickSort_1_t4250470945 *, int32_t, int32_t, const MethodInfo*))QuickSort_1_Swap_m402190387_gshared)(__this, ___left0, ___right1, method)
// System.Collections.Generic.IEnumerable`1<TElement> System.Linq.QuickSort`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Sort(System.Collections.Generic.IEnumerable`1<TElement>,System.Linq.SortContext`1<TElement>)
extern "C"  Il2CppObject* QuickSort_1_Sort_m4194874618_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___source0, SortContext_1_t4078456443 * ___context1, const MethodInfo* method);
#define QuickSort_1_Sort_m4194874618(__this /* static, unused */, ___source0, ___context1, method) ((  Il2CppObject* (*) (Il2CppObject * /* static, unused */, Il2CppObject*, SortContext_1_t4078456443 *, const MethodInfo*))QuickSort_1_Sort_m4194874618_gshared)(__this /* static, unused */, ___source0, ___context1, method)
