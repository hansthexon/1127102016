﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct List_1_t3085371226;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerable_1_t4008377139;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerator_1_t1191773921;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct ICollection_1_t373358103;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct ReadOnlyCollection_1_t3902035786;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2270685851;
// System.Predicate`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct Predicate_1_t2159220209;
// System.Comparison`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct Comparison_1_t683021649;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2620100900.h"

// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor()
extern "C"  void List_1__ctor_m704232838_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1__ctor_m704232838(__this, method) ((  void (*) (List_1_t3085371226 *, const MethodInfo*))List_1__ctor_m704232838_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m220447847_gshared (List_1_t3085371226 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m220447847(__this, ___collection0, method) ((  void (*) (List_1_t3085371226 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m220447847_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1561386017_gshared (List_1_t3085371226 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1561386017(__this, ___capacity0, method) ((  void (*) (List_1_t3085371226 *, int32_t, const MethodInfo*))List_1__ctor_m1561386017_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.cctor()
extern "C"  void List_1__cctor_m4078307751_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m4078307751(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m4078307751_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3270447776_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3270447776(__this, method) ((  Il2CppObject* (*) (List_1_t3085371226 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3270447776_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3433625622_gshared (List_1_t3085371226 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3433625622(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3085371226 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3433625622_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m611758657_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m611758657(__this, method) ((  Il2CppObject * (*) (List_1_t3085371226 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m611758657_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2346559902_gshared (List_1_t3085371226 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2346559902(__this, ___item0, method) ((  int32_t (*) (List_1_t3085371226 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2346559902_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3644383536_gshared (List_1_t3085371226 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3644383536(__this, ___item0, method) ((  bool (*) (List_1_t3085371226 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3644383536_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2867419128_gshared (List_1_t3085371226 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2867419128(__this, ___item0, method) ((  int32_t (*) (List_1_t3085371226 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2867419128_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m915296601_gshared (List_1_t3085371226 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m915296601(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3085371226 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m915296601_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m642524937_gshared (List_1_t3085371226 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m642524937(__this, ___item0, method) ((  void (*) (List_1_t3085371226 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m642524937_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3401981549_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3401981549(__this, method) ((  bool (*) (List_1_t3085371226 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3401981549_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3440603726_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3440603726(__this, method) ((  bool (*) (List_1_t3085371226 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3440603726_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3664798950_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3664798950(__this, method) ((  Il2CppObject * (*) (List_1_t3085371226 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3664798950_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m631538197_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m631538197(__this, method) ((  bool (*) (List_1_t3085371226 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m631538197_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2466936834_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2466936834(__this, method) ((  bool (*) (List_1_t3085371226 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2466936834_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m60815181_gshared (List_1_t3085371226 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m60815181(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3085371226 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m60815181_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2504533684_gshared (List_1_t3085371226 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2504533684(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3085371226 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2504533684_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Add(T)
extern "C"  void List_1_Add_m2461406037_gshared (List_1_t3085371226 * __this, KeyValuePair_2_t3716250094  ___item0, const MethodInfo* method);
#define List_1_Add_m2461406037(__this, ___item0, method) ((  void (*) (List_1_t3085371226 *, KeyValuePair_2_t3716250094 , const MethodInfo*))List_1_Add_m2461406037_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3838986012_gshared (List_1_t3085371226 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3838986012(__this, ___newCount0, method) ((  void (*) (List_1_t3085371226 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3838986012_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2317287652_gshared (List_1_t3085371226 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2317287652(__this, ___collection0, method) ((  void (*) (List_1_t3085371226 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2317287652_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2984361172_gshared (List_1_t3085371226 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2984361172(__this, ___enumerable0, method) ((  void (*) (List_1_t3085371226 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2984361172_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2323276241_gshared (List_1_t3085371226 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2323276241(__this, ___collection0, method) ((  void (*) (List_1_t3085371226 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2323276241_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3902035786 * List_1_AsReadOnly_m924687210_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m924687210(__this, method) ((  ReadOnlyCollection_1_t3902035786 * (*) (List_1_t3085371226 *, const MethodInfo*))List_1_AsReadOnly_m924687210_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Clear()
extern "C"  void List_1_Clear_m2654815121_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_Clear_m2654815121(__this, method) ((  void (*) (List_1_t3085371226 *, const MethodInfo*))List_1_Clear_m2654815121_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Contains(T)
extern "C"  bool List_1_Contains_m569140735_gshared (List_1_t3085371226 * __this, KeyValuePair_2_t3716250094  ___item0, const MethodInfo* method);
#define List_1_Contains_m569140735(__this, ___item0, method) ((  bool (*) (List_1_t3085371226 *, KeyValuePair_2_t3716250094 , const MethodInfo*))List_1_Contains_m569140735_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m1006220766_gshared (List_1_t3085371226 * __this, KeyValuePair_2U5BU5D_t2270685851* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m1006220766(__this, ___array0, method) ((  void (*) (List_1_t3085371226 *, KeyValuePair_2U5BU5D_t2270685851*, const MethodInfo*))List_1_CopyTo_m1006220766_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2930972869_gshared (List_1_t3085371226 * __this, KeyValuePair_2U5BU5D_t2270685851* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2930972869(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3085371226 *, KeyValuePair_2U5BU5D_t2270685851*, int32_t, const MethodInfo*))List_1_CopyTo_m2930972869_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Find(System.Predicate`1<T>)
extern "C"  KeyValuePair_2_t3716250094  List_1_Find_m3054087411_gshared (List_1_t3085371226 * __this, Predicate_1_t2159220209 * ___match0, const MethodInfo* method);
#define List_1_Find_m3054087411(__this, ___match0, method) ((  KeyValuePair_2_t3716250094  (*) (List_1_t3085371226 *, Predicate_1_t2159220209 *, const MethodInfo*))List_1_Find_m3054087411_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m4059704790_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2159220209 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m4059704790(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2159220209 *, const MethodInfo*))List_1_CheckMatch_m4059704790_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1322314247_gshared (List_1_t3085371226 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2159220209 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1322314247(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3085371226 *, int32_t, int32_t, Predicate_1_t2159220209 *, const MethodInfo*))List_1_GetIndex_m1322314247_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::GetEnumerator()
extern "C"  Enumerator_t2620100900  List_1_GetEnumerator_m2128811726_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2128811726(__this, method) ((  Enumerator_t2620100900  (*) (List_1_t3085371226 *, const MethodInfo*))List_1_GetEnumerator_m2128811726_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m50341753_gshared (List_1_t3085371226 * __this, KeyValuePair_2_t3716250094  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m50341753(__this, ___item0, method) ((  int32_t (*) (List_1_t3085371226 *, KeyValuePair_2_t3716250094 , const MethodInfo*))List_1_IndexOf_m50341753_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3732522466_gshared (List_1_t3085371226 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3732522466(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3085371226 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3732522466_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3378332833_gshared (List_1_t3085371226 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3378332833(__this, ___index0, method) ((  void (*) (List_1_t3085371226 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3378332833_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m435761756_gshared (List_1_t3085371226 * __this, int32_t ___index0, KeyValuePair_2_t3716250094  ___item1, const MethodInfo* method);
#define List_1_Insert_m435761756(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3085371226 *, int32_t, KeyValuePair_2_t3716250094 , const MethodInfo*))List_1_Insert_m435761756_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m3800557211_gshared (List_1_t3085371226 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m3800557211(__this, ___collection0, method) ((  void (*) (List_1_t3085371226 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m3800557211_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Remove(T)
extern "C"  bool List_1_Remove_m1514906138_gshared (List_1_t3085371226 * __this, KeyValuePair_2_t3716250094  ___item0, const MethodInfo* method);
#define List_1_Remove_m1514906138(__this, ___item0, method) ((  bool (*) (List_1_t3085371226 *, KeyValuePair_2_t3716250094 , const MethodInfo*))List_1_Remove_m1514906138_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2881303366_gshared (List_1_t3085371226 * __this, Predicate_1_t2159220209 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2881303366(__this, ___match0, method) ((  int32_t (*) (List_1_t3085371226 *, Predicate_1_t2159220209 *, const MethodInfo*))List_1_RemoveAll_m2881303366_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m4270780760_gshared (List_1_t3085371226 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m4270780760(__this, ___index0, method) ((  void (*) (List_1_t3085371226 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4270780760_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Reverse()
extern "C"  void List_1_Reverse_m2910709376_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_Reverse_m2910709376(__this, method) ((  void (*) (List_1_t3085371226 *, const MethodInfo*))List_1_Reverse_m2910709376_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Sort()
extern "C"  void List_1_Sort_m1057539704_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_Sort_m1057539704(__this, method) ((  void (*) (List_1_t3085371226 *, const MethodInfo*))List_1_Sort_m1057539704_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m191928831_gshared (List_1_t3085371226 * __this, Comparison_1_t683021649 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m191928831(__this, ___comparison0, method) ((  void (*) (List_1_t3085371226 *, Comparison_1_t683021649 *, const MethodInfo*))List_1_Sort_m191928831_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::ToArray()
extern "C"  KeyValuePair_2U5BU5D_t2270685851* List_1_ToArray_m2215107535_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_ToArray_m2215107535(__this, method) ((  KeyValuePair_2U5BU5D_t2270685851* (*) (List_1_t3085371226 *, const MethodInfo*))List_1_ToArray_m2215107535_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2314388245_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2314388245(__this, method) ((  void (*) (List_1_t3085371226 *, const MethodInfo*))List_1_TrimExcess_m2314388245_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3644957943_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3644957943(__this, method) ((  int32_t (*) (List_1_t3085371226 *, const MethodInfo*))List_1_get_Capacity_m3644957943_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3813531268_gshared (List_1_t3085371226 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3813531268(__this, ___value0, method) ((  void (*) (List_1_t3085371226 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3813531268_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Count()
extern "C"  int32_t List_1_get_Count_m4118589774_gshared (List_1_t3085371226 * __this, const MethodInfo* method);
#define List_1_get_Count_m4118589774(__this, method) ((  int32_t (*) (List_1_t3085371226 *, const MethodInfo*))List_1_get_Count_m4118589774_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Item(System.Int32)
extern "C"  KeyValuePair_2_t3716250094  List_1_get_Item_m2444640342_gshared (List_1_t3085371226 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2444640342(__this, ___index0, method) ((  KeyValuePair_2_t3716250094  (*) (List_1_t3085371226 *, int32_t, const MethodInfo*))List_1_get_Item_m2444640342_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2932783109_gshared (List_1_t3085371226 * __this, int32_t ___index0, KeyValuePair_2_t3716250094  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2932783109(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3085371226 *, int32_t, KeyValuePair_2_t3716250094 , const MethodInfo*))List_1_set_Item_m2932783109_gshared)(__this, ___index0, ___value1, method)
