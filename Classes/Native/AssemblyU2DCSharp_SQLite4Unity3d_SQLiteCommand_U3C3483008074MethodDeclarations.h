﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>
struct U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::.ctor()
extern "C"  void U3CExecuteDeferredQueryU3Ec__IteratorF_1__ctor_m1814300075_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorF_1__ctor_m1814300075(__this, method) ((  void (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorF_1__ctor_m1814300075_gshared)(__this, method)
// T SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m127510910_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m127510910(__this, method) ((  Il2CppObject * (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m127510910_gshared)(__this, method)
// System.Object SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m2116757585_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m2116757585(__this, method) ((  Il2CppObject * (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_IEnumerator_get_Current_m2116757585_gshared)(__this, method)
// System.Collections.IEnumerator SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m1832336732_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m1832336732(__this, method) ((  Il2CppObject * (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_IEnumerable_GetEnumerator_m1832336732_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m964834083_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m964834083(__this, method) ((  Il2CppObject* (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorF_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m964834083_gshared)(__this, method)
// System.Boolean SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::MoveNext()
extern "C"  bool U3CExecuteDeferredQueryU3Ec__IteratorF_1_MoveNext_m1707194421_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorF_1_MoveNext_m1707194421(__this, method) ((  bool (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorF_1_MoveNext_m1707194421_gshared)(__this, method)
// System.Void SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::Dispose()
extern "C"  void U3CExecuteDeferredQueryU3Ec__IteratorF_1_Dispose_m497419912_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorF_1_Dispose_m497419912(__this, method) ((  void (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorF_1_Dispose_m497419912_gshared)(__this, method)
// System.Void SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::Reset()
extern "C"  void U3CExecuteDeferredQueryU3Ec__IteratorF_1_Reset_m3495349858_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorF_1_Reset_m3495349858(__this, method) ((  void (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorF_1_Reset_m3495349858_gshared)(__this, method)
// System.Void SQLite4Unity3d.SQLiteCommand/<ExecuteDeferredQuery>c__IteratorF`1<System.Object>::<>__Finally0()
extern "C"  void U3CExecuteDeferredQueryU3Ec__IteratorF_1_U3CU3E__Finally0_m3335794584_gshared (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 * __this, const MethodInfo* method);
#define U3CExecuteDeferredQueryU3Ec__IteratorF_1_U3CU3E__Finally0_m3335794584(__this, method) ((  void (*) (U3CExecuteDeferredQueryU3Ec__IteratorF_1_t3483008074 *, const MethodInfo*))U3CExecuteDeferredQueryU3Ec__IteratorF_1_U3CU3E__Finally0_m3335794584_gshared)(__this, method)
