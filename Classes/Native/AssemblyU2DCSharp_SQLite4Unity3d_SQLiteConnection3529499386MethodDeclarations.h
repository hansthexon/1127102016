﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection
struct SQLiteConnection_t3529499386;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.TableMapping>
struct IEnumerable_1_t4190837857;
// SQLite4Unity3d.TableMapping
struct TableMapping_t3898710812;
// System.Type
struct Type_t;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/ColumnInfo>
struct List_1_t3578493861;
// SQLite4Unity3d.SQLiteCommand
struct SQLiteCommand_t2935685145;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2981576340;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3226471752;
// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// SQLite4Unity3d.TableMapping/Column
struct Column_t441055761;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteOpenFlags2021628609.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_CreateFlags3110178347.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping3898710812.h"
#include "mscorlib_System_Object2689449295.h"
#include "System_Core_System_Action3226471752.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_Colum441055761.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"

// System.Void SQLite4Unity3d.SQLiteConnection::.ctor(System.String,System.Boolean)
extern "C"  void SQLiteConnection__ctor_m1910883150 (SQLiteConnection_t3529499386 * __this, String_t* ___databasePath0, bool ___storeDateTimeAsTicks1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::.ctor(System.String,SQLite4Unity3d.SQLiteOpenFlags,System.Boolean)
extern "C"  void SQLiteConnection__ctor_m2670200907 (SQLiteConnection_t3529499386 * __this, String_t* ___databasePath0, int32_t ___openFlags1, bool ___storeDateTimeAsTicks2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::.cctor()
extern "C"  void SQLiteConnection__cctor_m2905553706 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SQLite4Unity3d.SQLiteConnection::get_Handle()
extern "C"  IntPtr_t SQLiteConnection_get_Handle_m182941329 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::set_Handle(System.IntPtr)
extern "C"  void SQLiteConnection_set_Handle_m1309800158 (SQLiteConnection_t3529499386 * __this, IntPtr_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLiteConnection::get_DatabasePath()
extern "C"  String_t* SQLiteConnection_get_DatabasePath_m2317710899 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::set_DatabasePath(System.String)
extern "C"  void SQLiteConnection_set_DatabasePath_m3995959826 (SQLiteConnection_t3529499386 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.SQLiteConnection::get_TimeExecution()
extern "C"  bool SQLiteConnection_get_TimeExecution_m181253117 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::set_TimeExecution(System.Boolean)
extern "C"  void SQLiteConnection_set_TimeExecution_m933945912 (SQLiteConnection_t3529499386 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.SQLiteConnection::get_Trace()
extern "C"  bool SQLiteConnection_get_Trace_m699095851 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::set_Trace(System.Boolean)
extern "C"  void SQLiteConnection_set_Trace_m584727760 (SQLiteConnection_t3529499386 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.SQLiteConnection::get_StoreDateTimeAsTicks()
extern "C"  bool SQLiteConnection_get_StoreDateTimeAsTicks_m3713338666 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::set_StoreDateTimeAsTicks(System.Boolean)
extern "C"  void SQLiteConnection_set_StoreDateTimeAsTicks_m1172062671 (SQLiteConnection_t3529499386 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::EnableLoadExtension(System.Int32)
extern "C"  void SQLiteConnection_EnableLoadExtension_m1717944602 (SQLiteConnection_t3529499386 * __this, int32_t ___onoff0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] SQLite4Unity3d.SQLiteConnection::GetNullTerminatedUtf8(System.String)
extern "C"  ByteU5BU5D_t3397334013* SQLiteConnection_GetNullTerminatedUtf8_m431334438 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan SQLite4Unity3d.SQLiteConnection::get_BusyTimeout()
extern "C"  TimeSpan_t3430258949  SQLiteConnection_get_BusyTimeout_m2168493765 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::set_BusyTimeout(System.TimeSpan)
extern "C"  void SQLiteConnection_set_BusyTimeout_m3190195150 (SQLiteConnection_t3529499386 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.TableMapping> SQLite4Unity3d.SQLiteConnection::get_TableMappings()
extern "C"  Il2CppObject* SQLiteConnection_get_TableMappings_m3476655873 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.TableMapping SQLite4Unity3d.SQLiteConnection::GetMapping(System.Type,SQLite4Unity3d.CreateFlags)
extern "C"  TableMapping_t3898710812 * SQLiteConnection_GetMapping_m822344580 (SQLiteConnection_t3529499386 * __this, Type_t * ___type0, int32_t ___createFlags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::CreateTable(System.Type,SQLite4Unity3d.CreateFlags)
extern "C"  int32_t SQLiteConnection_CreateTable_m1024189691 (SQLiteConnection_t3529499386 * __this, Type_t * ___ty0, int32_t ___createFlags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::CreateIndex(System.String,System.String,System.String[],System.Boolean)
extern "C"  int32_t SQLiteConnection_CreateIndex_m1137663724 (SQLiteConnection_t3529499386 * __this, String_t* ___indexName0, String_t* ___tableName1, StringU5BU5D_t1642385972* ___columnNames2, bool ___unique3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::CreateIndex(System.String,System.String,System.String,System.Boolean)
extern "C"  int32_t SQLiteConnection_CreateIndex_m762318300 (SQLiteConnection_t3529499386 * __this, String_t* ___indexName0, String_t* ___tableName1, String_t* ___columnName2, bool ___unique3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::CreateIndex(System.String,System.String,System.Boolean)
extern "C"  int32_t SQLiteConnection_CreateIndex_m1140087120 (SQLiteConnection_t3529499386 * __this, String_t* ___tableName0, String_t* ___columnName1, bool ___unique2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::CreateIndex(System.String,System.String[],System.Boolean)
extern "C"  int32_t SQLiteConnection_CreateIndex_m2713425016 (SQLiteConnection_t3529499386 * __this, String_t* ___tableName0, StringU5BU5D_t1642385972* ___columnNames1, bool ___unique2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/ColumnInfo> SQLite4Unity3d.SQLiteConnection::GetTableInfo(System.String)
extern "C"  List_1_t3578493861 * SQLiteConnection_GetTableInfo_m852595561 (SQLiteConnection_t3529499386 * __this, String_t* ___tableName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::MigrateTable(SQLite4Unity3d.TableMapping)
extern "C"  void SQLiteConnection_MigrateTable_m1937168940 (SQLiteConnection_t3529499386 * __this, TableMapping_t3898710812 * ___map0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLiteCommand SQLite4Unity3d.SQLiteConnection::NewCommand()
extern "C"  SQLiteCommand_t2935685145 * SQLiteConnection_NewCommand_m2566254588 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.SQLiteCommand SQLite4Unity3d.SQLiteConnection::CreateCommand(System.String,System.Object[])
extern "C"  SQLiteCommand_t2935685145 * SQLiteConnection_CreateCommand_m393862614 (SQLiteConnection_t3529499386 * __this, String_t* ___cmdText0, ObjectU5BU5D_t3614634134* ___ps1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::Execute(System.String,System.Object[])
extern "C"  int32_t SQLiteConnection_Execute_m2824262474 (SQLiteConnection_t3529499386 * __this, String_t* ___query0, ObjectU5BU5D_t3614634134* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> SQLite4Unity3d.SQLiteConnection::Query(SQLite4Unity3d.TableMapping,System.String,System.Object[])
extern "C"  List_1_t2058570427 * SQLiteConnection_Query_m1625077248 (SQLiteConnection_t3529499386 * __this, TableMapping_t3898710812 * ___map0, String_t* ___query1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Object> SQLite4Unity3d.SQLiteConnection::DeferredQuery(SQLite4Unity3d.TableMapping,System.String,System.Object[])
extern "C"  Il2CppObject* SQLiteConnection_DeferredQuery_m3944770244 (SQLiteConnection_t3529499386 * __this, TableMapping_t3898710812 * ___map0, String_t* ___query1, ObjectU5BU5D_t3614634134* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SQLite4Unity3d.SQLiteConnection::Find(System.Object,SQLite4Unity3d.TableMapping)
extern "C"  Il2CppObject * SQLiteConnection_Find_m1103213251 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___pk0, TableMapping_t3898710812 * ___map1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.SQLiteConnection::get_IsInTransaction()
extern "C"  bool SQLiteConnection_get_IsInTransaction_m3238553403 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::BeginTransaction()
extern "C"  void SQLiteConnection_BeginTransaction_m3402245652 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLiteConnection::SaveTransactionPoint()
extern "C"  String_t* SQLiteConnection_SaveTransactionPoint_m4219295419 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::Rollback()
extern "C"  void SQLiteConnection_Rollback_m240504589 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::RollbackTo(System.String)
extern "C"  void SQLiteConnection_RollbackTo_m2828473796 (SQLiteConnection_t3529499386 * __this, String_t* ___savepoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::RollbackTo(System.String,System.Boolean)
extern "C"  void SQLiteConnection_RollbackTo_m532817043 (SQLiteConnection_t3529499386 * __this, String_t* ___savepoint0, bool ___noThrow1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::Release(System.String)
extern "C"  void SQLiteConnection_Release_m1354934100 (SQLiteConnection_t3529499386 * __this, String_t* ___savepoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::DoSavePointExecute(System.String,System.String)
extern "C"  void SQLiteConnection_DoSavePointExecute_m1519323584 (SQLiteConnection_t3529499386 * __this, String_t* ___savepoint0, String_t* ___cmd1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::Commit()
extern "C"  void SQLiteConnection_Commit_m3870129278 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::RunInTransaction(System.Action)
extern "C"  void SQLiteConnection_RunInTransaction_m889924484 (SQLiteConnection_t3529499386 * __this, Action_t3226471752 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::InsertAll(System.Collections.IEnumerable)
extern "C"  int32_t SQLiteConnection_InsertAll_m3840718986 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___objects0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::InsertAll(System.Collections.IEnumerable,System.String)
extern "C"  int32_t SQLiteConnection_InsertAll_m4244001426 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___objects0, String_t* ___extra1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::InsertAll(System.Collections.IEnumerable,System.Type)
extern "C"  int32_t SQLiteConnection_InsertAll_m220812623 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___objects0, Type_t * ___objType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::Insert(System.Object)
extern "C"  int32_t SQLiteConnection_Insert_m4193435916 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::InsertOrReplace(System.Object)
extern "C"  int32_t SQLiteConnection_InsertOrReplace_m2274199837 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::Insert(System.Object,System.Type)
extern "C"  int32_t SQLiteConnection_Insert_m535413867 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___obj0, Type_t * ___objType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::InsertOrReplace(System.Object,System.Type)
extern "C"  int32_t SQLiteConnection_InsertOrReplace_m76803436 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___obj0, Type_t * ___objType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::Insert(System.Object,System.String)
extern "C"  int32_t SQLiteConnection_Insert_m1588874208 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___obj0, String_t* ___extra1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::Insert(System.Object,System.String,System.Type)
extern "C"  int32_t SQLiteConnection_Insert_m3582905365 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___obj0, String_t* ___extra1, Type_t * ___objType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::Update(System.Object)
extern "C"  int32_t SQLiteConnection_Update_m2451439940 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::Update(System.Object,System.Type)
extern "C"  int32_t SQLiteConnection_Update_m347987037 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___obj0, Type_t * ___objType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::UpdateAll(System.Collections.IEnumerable)
extern "C"  int32_t SQLiteConnection_UpdateAll_m177488806 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___objects0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::Delete(System.Object)
extern "C"  int32_t SQLiteConnection_Delete_m3959954518 (SQLiteConnection_t3529499386 * __this, Il2CppObject * ___objectToDelete0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::Finalize()
extern "C"  void SQLiteConnection_Finalize_m3681529965 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::Dispose()
extern "C"  void SQLiteConnection_Dispose_m4071367718 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::Dispose(System.Boolean)
extern "C"  void SQLiteConnection_Dispose_m2388533791 (SQLiteConnection_t3529499386 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection::Close()
extern "C"  void SQLiteConnection_Close_m1343565277 (SQLiteConnection_t3529499386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLiteConnection::<CreateTable>m__2(SQLite4Unity3d.TableMapping/Column)
extern "C"  String_t* SQLiteConnection_U3CCreateTableU3Em__2_m3741686366 (SQLiteConnection_t3529499386 * __this, Column_t441055761 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteConnection::<CreateTable>m__3(SQLite4Unity3d.SQLiteConnection/IndexedColumn)
extern "C"  int32_t SQLiteConnection_U3CCreateTableU3Em__3_m2930803525 (Il2CppObject * __this /* static, unused */, IndexedColumn_t674159988  ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLiteConnection::<CreateTable>m__4(SQLite4Unity3d.SQLiteConnection/IndexedColumn)
extern "C"  String_t* SQLiteConnection_U3CCreateTableU3Em__4_m2921869825 (Il2CppObject * __this /* static, unused */, IndexedColumn_t674159988  ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLiteConnection::<Update>m__A(SQLite4Unity3d.TableMapping/Column)
extern "C"  String_t* SQLiteConnection_U3CUpdateU3Em__A_m2849700646 (Il2CppObject * __this /* static, unused */, Column_t441055761 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
