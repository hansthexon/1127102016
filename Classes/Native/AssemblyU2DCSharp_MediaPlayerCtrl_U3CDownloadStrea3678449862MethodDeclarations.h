﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0
struct U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0::.ctor()
extern "C"  void U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0__ctor_m534598961 (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1809869623 (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3497659343 (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0::MoveNext()
extern "C"  bool U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_MoveNext_m2406647707 (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0::Dispose()
extern "C"  void U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_Dispose_m3885342912 (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/<DownloadStreamingVideoAndLoad>c__Iterator0::Reset()
extern "C"  void U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_Reset_m337414286 (U3CDownloadStreamingVideoAndLoadU3Ec__Iterator0_t3678449862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
