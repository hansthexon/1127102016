﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t2376585677;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu1199491699.h"

// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m2750375776_gshared (Enumerator_t1199491699 * __this, LinkedList_1_t2376585677 * ___parent0, const MethodInfo* method);
#define Enumerator__ctor_m2750375776(__this, ___parent0, method) ((  void (*) (Enumerator_t1199491699 *, LinkedList_1_t2376585677 *, const MethodInfo*))Enumerator__ctor_m2750375776_gshared)(__this, ___parent0, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m31448777_gshared (Enumerator_t1199491699 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m31448777(__this, method) ((  Il2CppObject * (*) (Enumerator_t1199491699 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m31448777_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2493813781_gshared (Enumerator_t1199491699 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2493813781(__this, method) ((  void (*) (Enumerator_t1199491699 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2493813781_gshared)(__this, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2893808644_gshared (Enumerator_t1199491699 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2893808644(__this, method) ((  int32_t (*) (Enumerator_t1199491699 *, const MethodInfo*))Enumerator_get_Current_m2893808644_gshared)(__this, method)
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2295891353_gshared (Enumerator_t1199491699 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2295891353(__this, method) ((  bool (*) (Enumerator_t1199491699 *, const MethodInfo*))Enumerator_MoveNext_m2295891353_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m4265648796_gshared (Enumerator_t1199491699 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m4265648796(__this, method) ((  void (*) (Enumerator_t1199491699 *, const MethodInfo*))Enumerator_Dispose_m4265648796_gshared)(__this, method)
