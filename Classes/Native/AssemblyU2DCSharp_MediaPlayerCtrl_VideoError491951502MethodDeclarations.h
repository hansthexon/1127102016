﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerCtrl/VideoError
struct VideoError_t491951502;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIAPLAYER_ERRO3510122879.h"
#include "mscorlib_System_AsyncCallback163412349.h"

// System.Void MediaPlayerCtrl/VideoError::.ctor(System.Object,System.IntPtr)
extern "C"  void VideoError__ctor_m2345105267 (VideoError_t491951502 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/VideoError::Invoke(MediaPlayerCtrl/MEDIAPLAYER_ERROR,MediaPlayerCtrl/MEDIAPLAYER_ERROR)
extern "C"  void VideoError_Invoke_m2038619727 (VideoError_t491951502 * __this, int32_t ___errorCode0, int32_t ___errorCodeExtra1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult MediaPlayerCtrl/VideoError::BeginInvoke(MediaPlayerCtrl/MEDIAPLAYER_ERROR,MediaPlayerCtrl/MEDIAPLAYER_ERROR,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * VideoError_BeginInvoke_m1385297216 (VideoError_t491951502 * __this, int32_t ___errorCode0, int32_t ___errorCodeExtra1, AsyncCallback_t163412349 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl/VideoError::EndInvoke(System.IAsyncResult)
extern "C"  void VideoError_EndInvoke_m1390346241 (VideoError_t491951502 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
