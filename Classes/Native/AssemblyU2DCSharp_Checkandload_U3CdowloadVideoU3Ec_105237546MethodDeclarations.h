﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Checkandload/<dowloadVideo>c__Iterator6
struct U3CdowloadVideoU3Ec__Iterator6_t105237546;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Checkandload/<dowloadVideo>c__Iterator6::.ctor()
extern "C"  void U3CdowloadVideoU3Ec__Iterator6__ctor_m3637225385 (U3CdowloadVideoU3Ec__Iterator6_t105237546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Checkandload/<dowloadVideo>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CdowloadVideoU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2665676251 (U3CdowloadVideoU3Ec__Iterator6_t105237546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Checkandload/<dowloadVideo>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CdowloadVideoU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m2787221491 (U3CdowloadVideoU3Ec__Iterator6_t105237546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Checkandload/<dowloadVideo>c__Iterator6::MoveNext()
extern "C"  bool U3CdowloadVideoU3Ec__Iterator6_MoveNext_m2770696967 (U3CdowloadVideoU3Ec__Iterator6_t105237546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload/<dowloadVideo>c__Iterator6::Dispose()
extern "C"  void U3CdowloadVideoU3Ec__Iterator6_Dispose_m591616268 (U3CdowloadVideoU3Ec__Iterator6_t105237546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload/<dowloadVideo>c__Iterator6::Reset()
extern "C"  void U3CdowloadVideoU3Ec__Iterator6_Reset_m1619462414 (U3CdowloadVideoU3Ec__Iterator6_t105237546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
