﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey4
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey4::.ctor()
extern "C"  void U3CGetGetMethodByReflectionU3Ec__AnonStorey4__ctor_m555912566 (U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey4::<>m__8(System.Object)
extern "C"  Il2CppObject * U3CGetGetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__8_m2501469858 (U3CGetGetMethodByReflectionU3Ec__AnonStorey4_t3264409434 * __this, Il2CppObject * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
