﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct List_1_t43281120;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3872978090.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3053486497_gshared (Enumerator_t3872978090 * __this, List_1_t43281120 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3053486497(__this, ___l0, method) ((  void (*) (Enumerator_t3872978090 *, List_1_t43281120 *, const MethodInfo*))Enumerator__ctor_m3053486497_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2658021897_gshared (Enumerator_t3872978090 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2658021897(__this, method) ((  void (*) (Enumerator_t3872978090 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2658021897_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1210011841_gshared (Enumerator_t3872978090 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1210011841(__this, method) ((  Il2CppObject * (*) (Enumerator_t3872978090 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1210011841_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Dispose()
extern "C"  void Enumerator_Dispose_m3164348436_gshared (Enumerator_t3872978090 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3164348436(__this, method) ((  void (*) (Enumerator_t3872978090 *, const MethodInfo*))Enumerator_Dispose_m3164348436_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::VerifyState()
extern "C"  void Enumerator_VerifyState_m731607703_gshared (Enumerator_t3872978090 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m731607703(__this, method) ((  void (*) (Enumerator_t3872978090 *, const MethodInfo*))Enumerator_VerifyState_m731607703_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2843108425_gshared (Enumerator_t3872978090 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2843108425(__this, method) ((  bool (*) (Enumerator_t3872978090 *, const MethodInfo*))Enumerator_MoveNext_m2843108425_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Current()
extern "C"  IndexedColumn_t674159988  Enumerator_get_Current_m2946794598_gshared (Enumerator_t3872978090 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2946794598(__this, method) ((  IndexedColumn_t674159988  (*) (Enumerator_t3872978090 *, const MethodInfo*))Enumerator_get_Current_m2946794598_gshared)(__this, method)
