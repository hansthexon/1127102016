﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.IFormatProvider
struct IFormatProvider_t2849799027;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc>
struct IDictionary_2_t345319375;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>>
struct IDictionary_2_t2850733530;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.FactoryFunc>
struct IDictionary_2_t2372829874;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata>
struct IDictionary_2_t1945275780;
// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>>
struct IDictionary_2_t3203428973;
// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata>
struct IDictionary_2_t3932363716;
// LitJson.WrapperFactory
struct WrapperFactory_t2219329745;
// LitJson.ExporterFunc
struct ExporterFunc_t408878057;
// LitJson.ImporterFunc
struct ImporterFunc_t2977850894;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.JsonMapper
struct  JsonMapper_t800426905  : public Il2CppObject
{
public:

public:
};

struct JsonMapper_t800426905_StaticFields
{
public:
	// System.Int32 LitJson.JsonMapper::maxNestingDepth
	int32_t ___maxNestingDepth_0;
	// System.IFormatProvider LitJson.JsonMapper::datetimeFormat
	Il2CppObject * ___datetimeFormat_1;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc> LitJson.JsonMapper::baseExportTable
	Il2CppObject* ___baseExportTable_2;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ExporterFunc> LitJson.JsonMapper::customExportTable
	Il2CppObject* ___customExportTable_3;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>> LitJson.JsonMapper::baseImportTable
	Il2CppObject* ___baseImportTable_4;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,LitJson.ImporterFunc>> LitJson.JsonMapper::customImportTable
	Il2CppObject* ___customImportTable_5;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.FactoryFunc> LitJson.JsonMapper::customFactoryTable
	Il2CppObject* ___customFactoryTable_6;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ArrayMetadata> LitJson.JsonMapper::arrayMetadata
	Il2CppObject* ___arrayMetadata_7;
	// System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.Type,System.Reflection.MethodInfo>> LitJson.JsonMapper::convOps
	Il2CppObject* ___convOps_8;
	// System.Collections.Generic.IDictionary`2<System.Type,LitJson.ObjectMetadata> LitJson.JsonMapper::objectMetadata
	Il2CppObject* ___objectMetadata_9;
	// LitJson.WrapperFactory LitJson.JsonMapper::<>f__am$cacheA
	WrapperFactory_t2219329745 * ___U3CU3Ef__amU24cacheA_10;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cacheB
	ExporterFunc_t408878057 * ___U3CU3Ef__amU24cacheB_11;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cacheC
	ExporterFunc_t408878057 * ___U3CU3Ef__amU24cacheC_12;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cacheD
	ExporterFunc_t408878057 * ___U3CU3Ef__amU24cacheD_13;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cacheE
	ExporterFunc_t408878057 * ___U3CU3Ef__amU24cacheE_14;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cacheF
	ExporterFunc_t408878057 * ___U3CU3Ef__amU24cacheF_15;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache10
	ExporterFunc_t408878057 * ___U3CU3Ef__amU24cache10_16;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache11
	ExporterFunc_t408878057 * ___U3CU3Ef__amU24cache11_17;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache12
	ExporterFunc_t408878057 * ___U3CU3Ef__amU24cache12_18;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache13
	ExporterFunc_t408878057 * ___U3CU3Ef__amU24cache13_19;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache14
	ExporterFunc_t408878057 * ___U3CU3Ef__amU24cache14_20;
	// LitJson.ExporterFunc LitJson.JsonMapper::<>f__am$cache15
	ExporterFunc_t408878057 * ___U3CU3Ef__amU24cache15_21;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache16
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache16_22;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache17
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache17_23;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache18
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache18_24;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache19
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache19_25;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache1A
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache1A_26;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache1B
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache1B_27;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache1C
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache1C_28;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache1D
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache1D_29;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache1E
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache1E_30;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache1F
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache1F_31;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache20
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache20_32;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache21
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache21_33;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache22
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache22_34;
	// LitJson.ImporterFunc LitJson.JsonMapper::<>f__am$cache23
	ImporterFunc_t2977850894 * ___U3CU3Ef__amU24cache23_35;
	// LitJson.WrapperFactory LitJson.JsonMapper::<>f__am$cache24
	WrapperFactory_t2219329745 * ___U3CU3Ef__amU24cache24_36;
	// LitJson.WrapperFactory LitJson.JsonMapper::<>f__am$cache25
	WrapperFactory_t2219329745 * ___U3CU3Ef__amU24cache25_37;
	// LitJson.WrapperFactory LitJson.JsonMapper::<>f__am$cache26
	WrapperFactory_t2219329745 * ___U3CU3Ef__amU24cache26_38;

public:
	inline static int32_t get_offset_of_maxNestingDepth_0() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___maxNestingDepth_0)); }
	inline int32_t get_maxNestingDepth_0() const { return ___maxNestingDepth_0; }
	inline int32_t* get_address_of_maxNestingDepth_0() { return &___maxNestingDepth_0; }
	inline void set_maxNestingDepth_0(int32_t value)
	{
		___maxNestingDepth_0 = value;
	}

	inline static int32_t get_offset_of_datetimeFormat_1() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___datetimeFormat_1)); }
	inline Il2CppObject * get_datetimeFormat_1() const { return ___datetimeFormat_1; }
	inline Il2CppObject ** get_address_of_datetimeFormat_1() { return &___datetimeFormat_1; }
	inline void set_datetimeFormat_1(Il2CppObject * value)
	{
		___datetimeFormat_1 = value;
		Il2CppCodeGenWriteBarrier(&___datetimeFormat_1, value);
	}

	inline static int32_t get_offset_of_baseExportTable_2() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___baseExportTable_2)); }
	inline Il2CppObject* get_baseExportTable_2() const { return ___baseExportTable_2; }
	inline Il2CppObject** get_address_of_baseExportTable_2() { return &___baseExportTable_2; }
	inline void set_baseExportTable_2(Il2CppObject* value)
	{
		___baseExportTable_2 = value;
		Il2CppCodeGenWriteBarrier(&___baseExportTable_2, value);
	}

	inline static int32_t get_offset_of_customExportTable_3() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___customExportTable_3)); }
	inline Il2CppObject* get_customExportTable_3() const { return ___customExportTable_3; }
	inline Il2CppObject** get_address_of_customExportTable_3() { return &___customExportTable_3; }
	inline void set_customExportTable_3(Il2CppObject* value)
	{
		___customExportTable_3 = value;
		Il2CppCodeGenWriteBarrier(&___customExportTable_3, value);
	}

	inline static int32_t get_offset_of_baseImportTable_4() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___baseImportTable_4)); }
	inline Il2CppObject* get_baseImportTable_4() const { return ___baseImportTable_4; }
	inline Il2CppObject** get_address_of_baseImportTable_4() { return &___baseImportTable_4; }
	inline void set_baseImportTable_4(Il2CppObject* value)
	{
		___baseImportTable_4 = value;
		Il2CppCodeGenWriteBarrier(&___baseImportTable_4, value);
	}

	inline static int32_t get_offset_of_customImportTable_5() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___customImportTable_5)); }
	inline Il2CppObject* get_customImportTable_5() const { return ___customImportTable_5; }
	inline Il2CppObject** get_address_of_customImportTable_5() { return &___customImportTable_5; }
	inline void set_customImportTable_5(Il2CppObject* value)
	{
		___customImportTable_5 = value;
		Il2CppCodeGenWriteBarrier(&___customImportTable_5, value);
	}

	inline static int32_t get_offset_of_customFactoryTable_6() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___customFactoryTable_6)); }
	inline Il2CppObject* get_customFactoryTable_6() const { return ___customFactoryTable_6; }
	inline Il2CppObject** get_address_of_customFactoryTable_6() { return &___customFactoryTable_6; }
	inline void set_customFactoryTable_6(Il2CppObject* value)
	{
		___customFactoryTable_6 = value;
		Il2CppCodeGenWriteBarrier(&___customFactoryTable_6, value);
	}

	inline static int32_t get_offset_of_arrayMetadata_7() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___arrayMetadata_7)); }
	inline Il2CppObject* get_arrayMetadata_7() const { return ___arrayMetadata_7; }
	inline Il2CppObject** get_address_of_arrayMetadata_7() { return &___arrayMetadata_7; }
	inline void set_arrayMetadata_7(Il2CppObject* value)
	{
		___arrayMetadata_7 = value;
		Il2CppCodeGenWriteBarrier(&___arrayMetadata_7, value);
	}

	inline static int32_t get_offset_of_convOps_8() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___convOps_8)); }
	inline Il2CppObject* get_convOps_8() const { return ___convOps_8; }
	inline Il2CppObject** get_address_of_convOps_8() { return &___convOps_8; }
	inline void set_convOps_8(Il2CppObject* value)
	{
		___convOps_8 = value;
		Il2CppCodeGenWriteBarrier(&___convOps_8, value);
	}

	inline static int32_t get_offset_of_objectMetadata_9() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___objectMetadata_9)); }
	inline Il2CppObject* get_objectMetadata_9() const { return ___objectMetadata_9; }
	inline Il2CppObject** get_address_of_objectMetadata_9() { return &___objectMetadata_9; }
	inline void set_objectMetadata_9(Il2CppObject* value)
	{
		___objectMetadata_9 = value;
		Il2CppCodeGenWriteBarrier(&___objectMetadata_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_10() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cacheA_10)); }
	inline WrapperFactory_t2219329745 * get_U3CU3Ef__amU24cacheA_10() const { return ___U3CU3Ef__amU24cacheA_10; }
	inline WrapperFactory_t2219329745 ** get_address_of_U3CU3Ef__amU24cacheA_10() { return &___U3CU3Ef__amU24cacheA_10; }
	inline void set_U3CU3Ef__amU24cacheA_10(WrapperFactory_t2219329745 * value)
	{
		___U3CU3Ef__amU24cacheA_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheA_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheB_11() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cacheB_11)); }
	inline ExporterFunc_t408878057 * get_U3CU3Ef__amU24cacheB_11() const { return ___U3CU3Ef__amU24cacheB_11; }
	inline ExporterFunc_t408878057 ** get_address_of_U3CU3Ef__amU24cacheB_11() { return &___U3CU3Ef__amU24cacheB_11; }
	inline void set_U3CU3Ef__amU24cacheB_11(ExporterFunc_t408878057 * value)
	{
		___U3CU3Ef__amU24cacheB_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheB_11, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheC_12() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cacheC_12)); }
	inline ExporterFunc_t408878057 * get_U3CU3Ef__amU24cacheC_12() const { return ___U3CU3Ef__amU24cacheC_12; }
	inline ExporterFunc_t408878057 ** get_address_of_U3CU3Ef__amU24cacheC_12() { return &___U3CU3Ef__amU24cacheC_12; }
	inline void set_U3CU3Ef__amU24cacheC_12(ExporterFunc_t408878057 * value)
	{
		___U3CU3Ef__amU24cacheC_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheC_12, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheD_13() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cacheD_13)); }
	inline ExporterFunc_t408878057 * get_U3CU3Ef__amU24cacheD_13() const { return ___U3CU3Ef__amU24cacheD_13; }
	inline ExporterFunc_t408878057 ** get_address_of_U3CU3Ef__amU24cacheD_13() { return &___U3CU3Ef__amU24cacheD_13; }
	inline void set_U3CU3Ef__amU24cacheD_13(ExporterFunc_t408878057 * value)
	{
		___U3CU3Ef__amU24cacheD_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheD_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_14() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cacheE_14)); }
	inline ExporterFunc_t408878057 * get_U3CU3Ef__amU24cacheE_14() const { return ___U3CU3Ef__amU24cacheE_14; }
	inline ExporterFunc_t408878057 ** get_address_of_U3CU3Ef__amU24cacheE_14() { return &___U3CU3Ef__amU24cacheE_14; }
	inline void set_U3CU3Ef__amU24cacheE_14(ExporterFunc_t408878057 * value)
	{
		___U3CU3Ef__amU24cacheE_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_14, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_15() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cacheF_15)); }
	inline ExporterFunc_t408878057 * get_U3CU3Ef__amU24cacheF_15() const { return ___U3CU3Ef__amU24cacheF_15; }
	inline ExporterFunc_t408878057 ** get_address_of_U3CU3Ef__amU24cacheF_15() { return &___U3CU3Ef__amU24cacheF_15; }
	inline void set_U3CU3Ef__amU24cacheF_15(ExporterFunc_t408878057 * value)
	{
		___U3CU3Ef__amU24cacheF_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_16() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache10_16)); }
	inline ExporterFunc_t408878057 * get_U3CU3Ef__amU24cache10_16() const { return ___U3CU3Ef__amU24cache10_16; }
	inline ExporterFunc_t408878057 ** get_address_of_U3CU3Ef__amU24cache10_16() { return &___U3CU3Ef__amU24cache10_16; }
	inline void set_U3CU3Ef__amU24cache10_16(ExporterFunc_t408878057 * value)
	{
		___U3CU3Ef__amU24cache10_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_17() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache11_17)); }
	inline ExporterFunc_t408878057 * get_U3CU3Ef__amU24cache11_17() const { return ___U3CU3Ef__amU24cache11_17; }
	inline ExporterFunc_t408878057 ** get_address_of_U3CU3Ef__amU24cache11_17() { return &___U3CU3Ef__amU24cache11_17; }
	inline void set_U3CU3Ef__amU24cache11_17(ExporterFunc_t408878057 * value)
	{
		___U3CU3Ef__amU24cache11_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_17, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache12_18() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache12_18)); }
	inline ExporterFunc_t408878057 * get_U3CU3Ef__amU24cache12_18() const { return ___U3CU3Ef__amU24cache12_18; }
	inline ExporterFunc_t408878057 ** get_address_of_U3CU3Ef__amU24cache12_18() { return &___U3CU3Ef__amU24cache12_18; }
	inline void set_U3CU3Ef__amU24cache12_18(ExporterFunc_t408878057 * value)
	{
		___U3CU3Ef__amU24cache12_18 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache12_18, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache13_19() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache13_19)); }
	inline ExporterFunc_t408878057 * get_U3CU3Ef__amU24cache13_19() const { return ___U3CU3Ef__amU24cache13_19; }
	inline ExporterFunc_t408878057 ** get_address_of_U3CU3Ef__amU24cache13_19() { return &___U3CU3Ef__amU24cache13_19; }
	inline void set_U3CU3Ef__amU24cache13_19(ExporterFunc_t408878057 * value)
	{
		___U3CU3Ef__amU24cache13_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache13_19, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache14_20() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache14_20)); }
	inline ExporterFunc_t408878057 * get_U3CU3Ef__amU24cache14_20() const { return ___U3CU3Ef__amU24cache14_20; }
	inline ExporterFunc_t408878057 ** get_address_of_U3CU3Ef__amU24cache14_20() { return &___U3CU3Ef__amU24cache14_20; }
	inline void set_U3CU3Ef__amU24cache14_20(ExporterFunc_t408878057 * value)
	{
		___U3CU3Ef__amU24cache14_20 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache14_20, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache15_21() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache15_21)); }
	inline ExporterFunc_t408878057 * get_U3CU3Ef__amU24cache15_21() const { return ___U3CU3Ef__amU24cache15_21; }
	inline ExporterFunc_t408878057 ** get_address_of_U3CU3Ef__amU24cache15_21() { return &___U3CU3Ef__amU24cache15_21; }
	inline void set_U3CU3Ef__amU24cache15_21(ExporterFunc_t408878057 * value)
	{
		___U3CU3Ef__amU24cache15_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache15_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache16_22() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache16_22)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache16_22() const { return ___U3CU3Ef__amU24cache16_22; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache16_22() { return &___U3CU3Ef__amU24cache16_22; }
	inline void set_U3CU3Ef__amU24cache16_22(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache16_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache16_22, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache17_23() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache17_23)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache17_23() const { return ___U3CU3Ef__amU24cache17_23; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache17_23() { return &___U3CU3Ef__amU24cache17_23; }
	inline void set_U3CU3Ef__amU24cache17_23(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache17_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache17_23, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache18_24() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache18_24)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache18_24() const { return ___U3CU3Ef__amU24cache18_24; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache18_24() { return &___U3CU3Ef__amU24cache18_24; }
	inline void set_U3CU3Ef__amU24cache18_24(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache18_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache18_24, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache19_25() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache19_25)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache19_25() const { return ___U3CU3Ef__amU24cache19_25; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache19_25() { return &___U3CU3Ef__amU24cache19_25; }
	inline void set_U3CU3Ef__amU24cache19_25(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache19_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache19_25, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1A_26() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache1A_26)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache1A_26() const { return ___U3CU3Ef__amU24cache1A_26; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache1A_26() { return &___U3CU3Ef__amU24cache1A_26; }
	inline void set_U3CU3Ef__amU24cache1A_26(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache1A_26 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1A_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1B_27() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache1B_27)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache1B_27() const { return ___U3CU3Ef__amU24cache1B_27; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache1B_27() { return &___U3CU3Ef__amU24cache1B_27; }
	inline void set_U3CU3Ef__amU24cache1B_27(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache1B_27 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1B_27, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1C_28() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache1C_28)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache1C_28() const { return ___U3CU3Ef__amU24cache1C_28; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache1C_28() { return &___U3CU3Ef__amU24cache1C_28; }
	inline void set_U3CU3Ef__amU24cache1C_28(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache1C_28 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1C_28, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1D_29() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache1D_29)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache1D_29() const { return ___U3CU3Ef__amU24cache1D_29; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache1D_29() { return &___U3CU3Ef__amU24cache1D_29; }
	inline void set_U3CU3Ef__amU24cache1D_29(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache1D_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1D_29, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1E_30() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache1E_30)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache1E_30() const { return ___U3CU3Ef__amU24cache1E_30; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache1E_30() { return &___U3CU3Ef__amU24cache1E_30; }
	inline void set_U3CU3Ef__amU24cache1E_30(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache1E_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1E_30, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1F_31() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache1F_31)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache1F_31() const { return ___U3CU3Ef__amU24cache1F_31; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache1F_31() { return &___U3CU3Ef__amU24cache1F_31; }
	inline void set_U3CU3Ef__amU24cache1F_31(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache1F_31 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1F_31, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache20_32() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache20_32)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache20_32() const { return ___U3CU3Ef__amU24cache20_32; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache20_32() { return &___U3CU3Ef__amU24cache20_32; }
	inline void set_U3CU3Ef__amU24cache20_32(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache20_32 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache20_32, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache21_33() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache21_33)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache21_33() const { return ___U3CU3Ef__amU24cache21_33; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache21_33() { return &___U3CU3Ef__amU24cache21_33; }
	inline void set_U3CU3Ef__amU24cache21_33(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache21_33 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache21_33, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache22_34() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache22_34)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache22_34() const { return ___U3CU3Ef__amU24cache22_34; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache22_34() { return &___U3CU3Ef__amU24cache22_34; }
	inline void set_U3CU3Ef__amU24cache22_34(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache22_34 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache22_34, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache23_35() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache23_35)); }
	inline ImporterFunc_t2977850894 * get_U3CU3Ef__amU24cache23_35() const { return ___U3CU3Ef__amU24cache23_35; }
	inline ImporterFunc_t2977850894 ** get_address_of_U3CU3Ef__amU24cache23_35() { return &___U3CU3Ef__amU24cache23_35; }
	inline void set_U3CU3Ef__amU24cache23_35(ImporterFunc_t2977850894 * value)
	{
		___U3CU3Ef__amU24cache23_35 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache23_35, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache24_36() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache24_36)); }
	inline WrapperFactory_t2219329745 * get_U3CU3Ef__amU24cache24_36() const { return ___U3CU3Ef__amU24cache24_36; }
	inline WrapperFactory_t2219329745 ** get_address_of_U3CU3Ef__amU24cache24_36() { return &___U3CU3Ef__amU24cache24_36; }
	inline void set_U3CU3Ef__amU24cache24_36(WrapperFactory_t2219329745 * value)
	{
		___U3CU3Ef__amU24cache24_36 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache24_36, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache25_37() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache25_37)); }
	inline WrapperFactory_t2219329745 * get_U3CU3Ef__amU24cache25_37() const { return ___U3CU3Ef__amU24cache25_37; }
	inline WrapperFactory_t2219329745 ** get_address_of_U3CU3Ef__amU24cache25_37() { return &___U3CU3Ef__amU24cache25_37; }
	inline void set_U3CU3Ef__amU24cache25_37(WrapperFactory_t2219329745 * value)
	{
		___U3CU3Ef__amU24cache25_37 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache25_37, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache26_38() { return static_cast<int32_t>(offsetof(JsonMapper_t800426905_StaticFields, ___U3CU3Ef__amU24cache26_38)); }
	inline WrapperFactory_t2219329745 * get_U3CU3Ef__amU24cache26_38() const { return ___U3CU3Ef__amU24cache26_38; }
	inline WrapperFactory_t2219329745 ** get_address_of_U3CU3Ef__amU24cache26_38() { return &___U3CU3Ef__amU24cache26_38; }
	inline void set_U3CU3Ef__amU24cache26_38(WrapperFactory_t2219329745 * value)
	{
		___U3CU3Ef__amU24cache26_38 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache26_38, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
