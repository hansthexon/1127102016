﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct Collection_1_t215904742;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// SQLite4Unity3d.SQLiteConnection/IndexedColumn[]
struct IndexedColumnU5BU5D_t4219656765;
// System.Collections.Generic.IEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IEnumerator_1_t2444651111;
// System.Collections.Generic.IList`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IList_1_t1215100589;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"

// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor()
extern "C"  void Collection_1__ctor_m3467178115_gshared (Collection_1_t215904742 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3467178115(__this, method) ((  void (*) (Collection_1_t215904742 *, const MethodInfo*))Collection_1__ctor_m3467178115_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2515201064_gshared (Collection_1_t215904742 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2515201064(__this, method) ((  bool (*) (Collection_1_t215904742 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2515201064_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3208200215_gshared (Collection_1_t215904742 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3208200215(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t215904742 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3208200215_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m4082431884_gshared (Collection_1_t215904742 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m4082431884(__this, method) ((  Il2CppObject * (*) (Collection_1_t215904742 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m4082431884_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m712982575_gshared (Collection_1_t215904742 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m712982575(__this, ___value0, method) ((  int32_t (*) (Collection_1_t215904742 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m712982575_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2560496687_gshared (Collection_1_t215904742 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2560496687(__this, ___value0, method) ((  bool (*) (Collection_1_t215904742 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2560496687_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1301239477_gshared (Collection_1_t215904742 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1301239477(__this, ___value0, method) ((  int32_t (*) (Collection_1_t215904742 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1301239477_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3948362014_gshared (Collection_1_t215904742 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3948362014(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t215904742 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3948362014_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2103750952_gshared (Collection_1_t215904742 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2103750952(__this, ___value0, method) ((  void (*) (Collection_1_t215904742 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2103750952_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m4170295271_gshared (Collection_1_t215904742 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m4170295271(__this, method) ((  bool (*) (Collection_1_t215904742 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m4170295271_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2376486807_gshared (Collection_1_t215904742 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2376486807(__this, method) ((  Il2CppObject * (*) (Collection_1_t215904742 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2376486807_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1933073138_gshared (Collection_1_t215904742 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1933073138(__this, method) ((  bool (*) (Collection_1_t215904742 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1933073138_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m517217843_gshared (Collection_1_t215904742 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m517217843(__this, method) ((  bool (*) (Collection_1_t215904742 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m517217843_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m2848417646_gshared (Collection_1_t215904742 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m2848417646(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t215904742 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2848417646_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1488930333_gshared (Collection_1_t215904742 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1488930333(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t215904742 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1488930333_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Add(T)
extern "C"  void Collection_1_Add_m1975826930_gshared (Collection_1_t215904742 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1975826930(__this, ___item0, method) ((  void (*) (Collection_1_t215904742 *, IndexedColumn_t674159988 , const MethodInfo*))Collection_1_Add_m1975826930_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Clear()
extern "C"  void Collection_1_Clear_m959294158_gshared (Collection_1_t215904742 * __this, const MethodInfo* method);
#define Collection_1_Clear_m959294158(__this, method) ((  void (*) (Collection_1_t215904742 *, const MethodInfo*))Collection_1_Clear_m959294158_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::ClearItems()
extern "C"  void Collection_1_ClearItems_m3257423524_gshared (Collection_1_t215904742 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m3257423524(__this, method) ((  void (*) (Collection_1_t215904742 *, const MethodInfo*))Collection_1_ClearItems_m3257423524_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Contains(T)
extern "C"  bool Collection_1_Contains_m3274386256_gshared (Collection_1_t215904742 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3274386256(__this, ___item0, method) ((  bool (*) (Collection_1_t215904742 *, IndexedColumn_t674159988 , const MethodInfo*))Collection_1_Contains_m3274386256_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m29786294_gshared (Collection_1_t215904742 * __this, IndexedColumnU5BU5D_t4219656765* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m29786294(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t215904742 *, IndexedColumnU5BU5D_t4219656765*, int32_t, const MethodInfo*))Collection_1_CopyTo_m29786294_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m1243937871_gshared (Collection_1_t215904742 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m1243937871(__this, method) ((  Il2CppObject* (*) (Collection_1_t215904742 *, const MethodInfo*))Collection_1_GetEnumerator_m1243937871_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1187764936_gshared (Collection_1_t215904742 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1187764936(__this, ___item0, method) ((  int32_t (*) (Collection_1_t215904742 *, IndexedColumn_t674159988 , const MethodInfo*))Collection_1_IndexOf_m1187764936_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m4108468137_gshared (Collection_1_t215904742 * __this, int32_t ___index0, IndexedColumn_t674159988  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m4108468137(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t215904742 *, int32_t, IndexedColumn_t674159988 , const MethodInfo*))Collection_1_Insert_m4108468137_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m108186700_gshared (Collection_1_t215904742 * __this, int32_t ___index0, IndexedColumn_t674159988  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m108186700(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t215904742 *, int32_t, IndexedColumn_t674159988 , const MethodInfo*))Collection_1_InsertItem_m108186700_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Remove(T)
extern "C"  bool Collection_1_Remove_m2592370601_gshared (Collection_1_t215904742 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2592370601(__this, ___item0, method) ((  bool (*) (Collection_1_t215904742 *, IndexedColumn_t674159988 , const MethodInfo*))Collection_1_Remove_m2592370601_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m4170663349_gshared (Collection_1_t215904742 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m4170663349(__this, ___index0, method) ((  void (*) (Collection_1_t215904742 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m4170663349_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3958224121_gshared (Collection_1_t215904742 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3958224121(__this, ___index0, method) ((  void (*) (Collection_1_t215904742 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3958224121_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m673348175_gshared (Collection_1_t215904742 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m673348175(__this, method) ((  int32_t (*) (Collection_1_t215904742 *, const MethodInfo*))Collection_1_get_Count_m673348175_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Item(System.Int32)
extern "C"  IndexedColumn_t674159988  Collection_1_get_Item_m3683697449_gshared (Collection_1_t215904742 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3683697449(__this, ___index0, method) ((  IndexedColumn_t674159988  (*) (Collection_1_t215904742 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3683697449_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m208942612_gshared (Collection_1_t215904742 * __this, int32_t ___index0, IndexedColumn_t674159988  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m208942612(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t215904742 *, int32_t, IndexedColumn_t674159988 , const MethodInfo*))Collection_1_set_Item_m208942612_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2619073549_gshared (Collection_1_t215904742 * __this, int32_t ___index0, IndexedColumn_t674159988  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2619073549(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t215904742 *, int32_t, IndexedColumn_t674159988 , const MethodInfo*))Collection_1_SetItem_m2619073549_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m1073059694_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m1073059694(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1073059694_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::ConvertItem(System.Object)
extern "C"  IndexedColumn_t674159988  Collection_1_ConvertItem_m163838772_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m163838772(__this /* static, unused */, ___item0, method) ((  IndexedColumn_t674159988  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m163838772_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1007606242_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1007606242(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1007606242_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1162058164_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1162058164(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1162058164_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m1729179785_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m1729179785(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m1729179785_gshared)(__this /* static, unused */, ___list0, method)
