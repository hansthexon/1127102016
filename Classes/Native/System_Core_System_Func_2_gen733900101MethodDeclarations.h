﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen2825504181MethodDeclarations.h"

// System.Void System.Func`2<SQLite4Unity3d.TableQuery`1/CompileResult<RoomObject>,System.String>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m3624249360(__this, ___object0, ___method1, method) ((  void (*) (Func_2_t733900101 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Func_2__ctor_m1684831714_gshared)(__this, ___object0, ___method1, method)
// TResult System.Func`2<SQLite4Unity3d.TableQuery`1/CompileResult<RoomObject>,System.String>::Invoke(T)
#define Func_2_Invoke_m1194663878(__this, ___arg10, method) ((  String_t* (*) (Func_2_t733900101 *, CompileResult_t162244657 *, const MethodInfo*))Func_2_Invoke_m3288232740_gshared)(__this, ___arg10, method)
// System.IAsyncResult System.Func`2<SQLite4Unity3d.TableQuery`1/CompileResult<RoomObject>,System.String>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m1207836929(__this, ___arg10, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Func_2_t733900101 *, CompileResult_t162244657 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Func_2_BeginInvoke_m4034295761_gshared)(__this, ___arg10, ___callback1, ___object2, method)
// TResult System.Func`2<SQLite4Unity3d.TableQuery`1/CompileResult<RoomObject>,System.String>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m3585576552(__this, ___result0, method) ((  String_t* (*) (Func_2_t733900101 *, Il2CppObject *, const MethodInfo*))Func_2_EndInvoke_m1674435418_gshared)(__this, ___result0, method)
