﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PolitePanelScript1
struct PolitePanelScript1_t3432214389;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void PolitePanelScript1::.ctor()
extern "C"  void PolitePanelScript1__ctor_m1791496512 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::Start()
extern "C"  void PolitePanelScript1_Start_m57500000 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::fetchPolCodeFromDb(System.String)
extern "C"  void PolitePanelScript1_fetchPolCodeFromDb_m4124388888 (PolitePanelScript1_t3432214389 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::OpenPoliteVideo(System.String)
extern "C"  void PolitePanelScript1_OpenPoliteVideo_m888219862 (PolitePanelScript1_t3432214389 * __this, String_t* ___filename0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::PlayVideo()
extern "C"  void PolitePanelScript1_PlayVideo_m2322048581 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::showPopscreen()
extern "C"  void PolitePanelScript1_showPopscreen_m812262924 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::showreplyscreen()
extern "C"  void PolitePanelScript1_showreplyscreen_m3279744077 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::Replay()
extern "C"  void PolitePanelScript1_Replay_m2040831191 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::Backreplay()
extern "C"  void PolitePanelScript1_Backreplay_m1303547286 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::BackPopUp()
extern "C"  void PolitePanelScript1_BackPopUp_m463620229 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::ScorePanelBack()
extern "C"  void PolitePanelScript1_ScorePanelBack_m3188947265 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::OpenScorePanel()
extern "C"  void PolitePanelScript1_OpenScorePanel_m1649321426 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::ClosePopup()
extern "C"  void PolitePanelScript1_ClosePopup_m47678428 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::ClosePolitePanel()
extern "C"  void PolitePanelScript1_ClosePolitePanel_m2725834355 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PolitePanelScript1::OpenPolitePanel()
extern "C"  void PolitePanelScript1_OpenPolitePanel_m3055395633 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PolitePanelScript1::disabelle()
extern "C"  Il2CppObject * PolitePanelScript1_disabelle_m1904275811 (PolitePanelScript1_t3432214389 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
