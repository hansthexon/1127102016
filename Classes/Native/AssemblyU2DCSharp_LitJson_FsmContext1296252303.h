﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LitJson.Lexer
struct Lexer_t186508296;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.FsmContext
struct  FsmContext_t1296252303  : public Il2CppObject
{
public:
	// LitJson.Lexer LitJson.FsmContext::L
	Lexer_t186508296 * ___L_0;
	// System.Boolean LitJson.FsmContext::Return
	bool ___Return_1;
	// System.Int32 LitJson.FsmContext::NextState
	int32_t ___NextState_2;
	// System.Int32 LitJson.FsmContext::StateStack
	int32_t ___StateStack_3;

public:
	inline static int32_t get_offset_of_L_0() { return static_cast<int32_t>(offsetof(FsmContext_t1296252303, ___L_0)); }
	inline Lexer_t186508296 * get_L_0() const { return ___L_0; }
	inline Lexer_t186508296 ** get_address_of_L_0() { return &___L_0; }
	inline void set_L_0(Lexer_t186508296 * value)
	{
		___L_0 = value;
		Il2CppCodeGenWriteBarrier(&___L_0, value);
	}

	inline static int32_t get_offset_of_Return_1() { return static_cast<int32_t>(offsetof(FsmContext_t1296252303, ___Return_1)); }
	inline bool get_Return_1() const { return ___Return_1; }
	inline bool* get_address_of_Return_1() { return &___Return_1; }
	inline void set_Return_1(bool value)
	{
		___Return_1 = value;
	}

	inline static int32_t get_offset_of_NextState_2() { return static_cast<int32_t>(offsetof(FsmContext_t1296252303, ___NextState_2)); }
	inline int32_t get_NextState_2() const { return ___NextState_2; }
	inline int32_t* get_address_of_NextState_2() { return &___NextState_2; }
	inline void set_NextState_2(int32_t value)
	{
		___NextState_2 = value;
	}

	inline static int32_t get_offset_of_StateStack_3() { return static_cast<int32_t>(offsetof(FsmContext_t1296252303, ___StateStack_3)); }
	inline int32_t get_StateStack_3() const { return ___StateStack_3; }
	inline int32_t* get_address_of_StateStack_3() { return &___StateStack_3; }
	inline void set_StateStack_3(int32_t value)
	{
		___StateStack_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
