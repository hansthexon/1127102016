﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct Dictionary_2_t440094893;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2126627657.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I848034765.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m90569632_gshared (Enumerator_t2126627657 * __this, Dictionary_2_t440094893 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m90569632(__this, ___host0, method) ((  void (*) (Enumerator_t2126627657 *, Dictionary_2_t440094893 *, const MethodInfo*))Enumerator__ctor_m90569632_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3664199829_gshared (Enumerator_t2126627657 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3664199829(__this, method) ((  Il2CppObject * (*) (Enumerator_t2126627657 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3664199829_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4005611117_gshared (Enumerator_t2126627657 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4005611117(__this, method) ((  void (*) (Enumerator_t2126627657 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4005611117_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m1086873952_gshared (Enumerator_t2126627657 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1086873952(__this, method) ((  void (*) (Enumerator_t2126627657 *, const MethodInfo*))Enumerator_Dispose_m1086873952_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2067976277_gshared (Enumerator_t2126627657 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2067976277(__this, method) ((  bool (*) (Enumerator_t2126627657 *, const MethodInfo*))Enumerator_MoveNext_m2067976277_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Current()
extern "C"  IndexInfo_t848034765  Enumerator_get_Current_m1841612375_gshared (Enumerator_t2126627657 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1841612375(__this, method) ((  IndexInfo_t848034765  (*) (Enumerator_t2126627657 *, const MethodInfo*))Enumerator_get_Current_m1841612375_gshared)(__this, method)
