﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.IEnumerable
struct IEnumerable_t2911409499;
// System.String
struct String_t;
// SQLite4Unity3d.SQLiteConnection
struct SQLiteConnection_t3529499386;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey15
struct  U3CInsertAllU3Ec__AnonStorey15_t1887274984  : public Il2CppObject
{
public:
	// System.Collections.IEnumerable SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey15::objects
	Il2CppObject * ___objects_0;
	// System.String SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey15::extra
	String_t* ___extra_1;
	// System.Int32 SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey15::c
	int32_t ___c_2;
	// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey15::<>f__this
	SQLiteConnection_t3529499386 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_objects_0() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey15_t1887274984, ___objects_0)); }
	inline Il2CppObject * get_objects_0() const { return ___objects_0; }
	inline Il2CppObject ** get_address_of_objects_0() { return &___objects_0; }
	inline void set_objects_0(Il2CppObject * value)
	{
		___objects_0 = value;
		Il2CppCodeGenWriteBarrier(&___objects_0, value);
	}

	inline static int32_t get_offset_of_extra_1() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey15_t1887274984, ___extra_1)); }
	inline String_t* get_extra_1() const { return ___extra_1; }
	inline String_t** get_address_of_extra_1() { return &___extra_1; }
	inline void set_extra_1(String_t* value)
	{
		___extra_1 = value;
		Il2CppCodeGenWriteBarrier(&___extra_1, value);
	}

	inline static int32_t get_offset_of_c_2() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey15_t1887274984, ___c_2)); }
	inline int32_t get_c_2() const { return ___c_2; }
	inline int32_t* get_address_of_c_2() { return &___c_2; }
	inline void set_c_2(int32_t value)
	{
		___c_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CInsertAllU3Ec__AnonStorey15_t1887274984, ___U3CU3Ef__this_3)); }
	inline SQLiteConnection_t3529499386 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline SQLiteConnection_t3529499386 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(SQLiteConnection_t3529499386 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
