﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VuforiaManagerImpl/SurfaceData
struct SurfaceData_t2968608989;
struct SurfaceData_t2968608989_marshaled_pinvoke;
struct SurfaceData_t2968608989_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct SurfaceData_t2968608989;
struct SurfaceData_t2968608989_marshaled_pinvoke;

extern "C" void SurfaceData_t2968608989_marshal_pinvoke(const SurfaceData_t2968608989& unmarshaled, SurfaceData_t2968608989_marshaled_pinvoke& marshaled);
extern "C" void SurfaceData_t2968608989_marshal_pinvoke_back(const SurfaceData_t2968608989_marshaled_pinvoke& marshaled, SurfaceData_t2968608989& unmarshaled);
extern "C" void SurfaceData_t2968608989_marshal_pinvoke_cleanup(SurfaceData_t2968608989_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SurfaceData_t2968608989;
struct SurfaceData_t2968608989_marshaled_com;

extern "C" void SurfaceData_t2968608989_marshal_com(const SurfaceData_t2968608989& unmarshaled, SurfaceData_t2968608989_marshaled_com& marshaled);
extern "C" void SurfaceData_t2968608989_marshal_com_back(const SurfaceData_t2968608989_marshaled_com& marshaled, SurfaceData_t2968608989& unmarshaled);
extern "C" void SurfaceData_t2968608989_marshal_com_cleanup(SurfaceData_t2968608989_marshaled_com& marshaled);
