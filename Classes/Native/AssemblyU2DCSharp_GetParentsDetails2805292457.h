﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GetParentsDetails
struct  GetParentsDetails_t2805292457  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text GetParentsDetails::day
	Text_t356221433 * ___day_2;
	// UnityEngine.UI.Text GetParentsDetails::dayhol
	Text_t356221433 * ___dayhol_3;
	// UnityEngine.UI.Text GetParentsDetails::month
	Text_t356221433 * ___month_4;
	// UnityEngine.UI.Text GetParentsDetails::monthhol
	Text_t356221433 * ___monthhol_5;
	// UnityEngine.UI.Text GetParentsDetails::year
	Text_t356221433 * ___year_6;
	// UnityEngine.UI.Text GetParentsDetails::yeahol
	Text_t356221433 * ___yeahol_7;
	// UnityEngine.UI.Text GetParentsDetails::child
	Text_t356221433 * ___child_8;
	// UnityEngine.UI.Text GetParentsDetails::childhol
	Text_t356221433 * ___childhol_9;
	// UnityEngine.UI.Text GetParentsDetails::pname
	Text_t356221433 * ___pname_10;
	// UnityEngine.UI.Text GetParentsDetails::pnamehol
	Text_t356221433 * ___pnamehol_11;
	// UnityEngine.UI.Text GetParentsDetails::addr
	Text_t356221433 * ___addr_12;
	// UnityEngine.UI.Text GetParentsDetails::addrhol
	Text_t356221433 * ___addrhol_13;
	// UnityEngine.UI.Text GetParentsDetails::emailaddress
	Text_t356221433 * ___emailaddress_14;
	// UnityEngine.UI.Text GetParentsDetails::emailhol
	Text_t356221433 * ___emailhol_15;

public:
	inline static int32_t get_offset_of_day_2() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___day_2)); }
	inline Text_t356221433 * get_day_2() const { return ___day_2; }
	inline Text_t356221433 ** get_address_of_day_2() { return &___day_2; }
	inline void set_day_2(Text_t356221433 * value)
	{
		___day_2 = value;
		Il2CppCodeGenWriteBarrier(&___day_2, value);
	}

	inline static int32_t get_offset_of_dayhol_3() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___dayhol_3)); }
	inline Text_t356221433 * get_dayhol_3() const { return ___dayhol_3; }
	inline Text_t356221433 ** get_address_of_dayhol_3() { return &___dayhol_3; }
	inline void set_dayhol_3(Text_t356221433 * value)
	{
		___dayhol_3 = value;
		Il2CppCodeGenWriteBarrier(&___dayhol_3, value);
	}

	inline static int32_t get_offset_of_month_4() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___month_4)); }
	inline Text_t356221433 * get_month_4() const { return ___month_4; }
	inline Text_t356221433 ** get_address_of_month_4() { return &___month_4; }
	inline void set_month_4(Text_t356221433 * value)
	{
		___month_4 = value;
		Il2CppCodeGenWriteBarrier(&___month_4, value);
	}

	inline static int32_t get_offset_of_monthhol_5() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___monthhol_5)); }
	inline Text_t356221433 * get_monthhol_5() const { return ___monthhol_5; }
	inline Text_t356221433 ** get_address_of_monthhol_5() { return &___monthhol_5; }
	inline void set_monthhol_5(Text_t356221433 * value)
	{
		___monthhol_5 = value;
		Il2CppCodeGenWriteBarrier(&___monthhol_5, value);
	}

	inline static int32_t get_offset_of_year_6() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___year_6)); }
	inline Text_t356221433 * get_year_6() const { return ___year_6; }
	inline Text_t356221433 ** get_address_of_year_6() { return &___year_6; }
	inline void set_year_6(Text_t356221433 * value)
	{
		___year_6 = value;
		Il2CppCodeGenWriteBarrier(&___year_6, value);
	}

	inline static int32_t get_offset_of_yeahol_7() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___yeahol_7)); }
	inline Text_t356221433 * get_yeahol_7() const { return ___yeahol_7; }
	inline Text_t356221433 ** get_address_of_yeahol_7() { return &___yeahol_7; }
	inline void set_yeahol_7(Text_t356221433 * value)
	{
		___yeahol_7 = value;
		Il2CppCodeGenWriteBarrier(&___yeahol_7, value);
	}

	inline static int32_t get_offset_of_child_8() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___child_8)); }
	inline Text_t356221433 * get_child_8() const { return ___child_8; }
	inline Text_t356221433 ** get_address_of_child_8() { return &___child_8; }
	inline void set_child_8(Text_t356221433 * value)
	{
		___child_8 = value;
		Il2CppCodeGenWriteBarrier(&___child_8, value);
	}

	inline static int32_t get_offset_of_childhol_9() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___childhol_9)); }
	inline Text_t356221433 * get_childhol_9() const { return ___childhol_9; }
	inline Text_t356221433 ** get_address_of_childhol_9() { return &___childhol_9; }
	inline void set_childhol_9(Text_t356221433 * value)
	{
		___childhol_9 = value;
		Il2CppCodeGenWriteBarrier(&___childhol_9, value);
	}

	inline static int32_t get_offset_of_pname_10() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___pname_10)); }
	inline Text_t356221433 * get_pname_10() const { return ___pname_10; }
	inline Text_t356221433 ** get_address_of_pname_10() { return &___pname_10; }
	inline void set_pname_10(Text_t356221433 * value)
	{
		___pname_10 = value;
		Il2CppCodeGenWriteBarrier(&___pname_10, value);
	}

	inline static int32_t get_offset_of_pnamehol_11() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___pnamehol_11)); }
	inline Text_t356221433 * get_pnamehol_11() const { return ___pnamehol_11; }
	inline Text_t356221433 ** get_address_of_pnamehol_11() { return &___pnamehol_11; }
	inline void set_pnamehol_11(Text_t356221433 * value)
	{
		___pnamehol_11 = value;
		Il2CppCodeGenWriteBarrier(&___pnamehol_11, value);
	}

	inline static int32_t get_offset_of_addr_12() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___addr_12)); }
	inline Text_t356221433 * get_addr_12() const { return ___addr_12; }
	inline Text_t356221433 ** get_address_of_addr_12() { return &___addr_12; }
	inline void set_addr_12(Text_t356221433 * value)
	{
		___addr_12 = value;
		Il2CppCodeGenWriteBarrier(&___addr_12, value);
	}

	inline static int32_t get_offset_of_addrhol_13() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___addrhol_13)); }
	inline Text_t356221433 * get_addrhol_13() const { return ___addrhol_13; }
	inline Text_t356221433 ** get_address_of_addrhol_13() { return &___addrhol_13; }
	inline void set_addrhol_13(Text_t356221433 * value)
	{
		___addrhol_13 = value;
		Il2CppCodeGenWriteBarrier(&___addrhol_13, value);
	}

	inline static int32_t get_offset_of_emailaddress_14() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___emailaddress_14)); }
	inline Text_t356221433 * get_emailaddress_14() const { return ___emailaddress_14; }
	inline Text_t356221433 ** get_address_of_emailaddress_14() { return &___emailaddress_14; }
	inline void set_emailaddress_14(Text_t356221433 * value)
	{
		___emailaddress_14 = value;
		Il2CppCodeGenWriteBarrier(&___emailaddress_14, value);
	}

	inline static int32_t get_offset_of_emailhol_15() { return static_cast<int32_t>(offsetof(GetParentsDetails_t2805292457, ___emailhol_15)); }
	inline Text_t356221433 * get_emailhol_15() const { return ___emailhol_15; }
	inline Text_t356221433 ** get_address_of_emailhol_15() { return &___emailhol_15; }
	inline void set_emailhol_15(Text_t356221433 * value)
	{
		___emailhol_15 = value;
		Il2CppCodeGenWriteBarrier(&___emailhol_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
