﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t3950226929;
// System.Object
struct Il2CppObject;
// Purchaser
struct Purchaser_t1507804203;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Purchaser/<Paytoserver>c__Iterator4
struct  U3CPaytoserverU3Ec__Iterator4_t2045108845  : public Il2CppObject
{
public:
	// System.String Purchaser/<Paytoserver>c__Iterator4::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// UnityEngine.WWWForm Purchaser/<Paytoserver>c__Iterator4::<form>__1
	WWWForm_t3950226929 * ___U3CformU3E__1_1;
	// System.String Purchaser/<Paytoserver>c__Iterator4::pay
	String_t* ___pay_2;
	// System.String Purchaser/<Paytoserver>c__Iterator4::expriydate
	String_t* ___expriydate_3;
	// System.Int32 Purchaser/<Paytoserver>c__Iterator4::$PC
	int32_t ___U24PC_4;
	// System.Object Purchaser/<Paytoserver>c__Iterator4::$current
	Il2CppObject * ___U24current_5;
	// System.String Purchaser/<Paytoserver>c__Iterator4::<$>pay
	String_t* ___U3CU24U3Epay_6;
	// System.String Purchaser/<Paytoserver>c__Iterator4::<$>expriydate
	String_t* ___U3CU24U3Eexpriydate_7;
	// Purchaser Purchaser/<Paytoserver>c__Iterator4::<>f__this
	Purchaser_t1507804203 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CPaytoserverU3Ec__Iterator4_t2045108845, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CformU3E__1_1() { return static_cast<int32_t>(offsetof(U3CPaytoserverU3Ec__Iterator4_t2045108845, ___U3CformU3E__1_1)); }
	inline WWWForm_t3950226929 * get_U3CformU3E__1_1() const { return ___U3CformU3E__1_1; }
	inline WWWForm_t3950226929 ** get_address_of_U3CformU3E__1_1() { return &___U3CformU3E__1_1; }
	inline void set_U3CformU3E__1_1(WWWForm_t3950226929 * value)
	{
		___U3CformU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CformU3E__1_1, value);
	}

	inline static int32_t get_offset_of_pay_2() { return static_cast<int32_t>(offsetof(U3CPaytoserverU3Ec__Iterator4_t2045108845, ___pay_2)); }
	inline String_t* get_pay_2() const { return ___pay_2; }
	inline String_t** get_address_of_pay_2() { return &___pay_2; }
	inline void set_pay_2(String_t* value)
	{
		___pay_2 = value;
		Il2CppCodeGenWriteBarrier(&___pay_2, value);
	}

	inline static int32_t get_offset_of_expriydate_3() { return static_cast<int32_t>(offsetof(U3CPaytoserverU3Ec__Iterator4_t2045108845, ___expriydate_3)); }
	inline String_t* get_expriydate_3() const { return ___expriydate_3; }
	inline String_t** get_address_of_expriydate_3() { return &___expriydate_3; }
	inline void set_expriydate_3(String_t* value)
	{
		___expriydate_3 = value;
		Il2CppCodeGenWriteBarrier(&___expriydate_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CPaytoserverU3Ec__Iterator4_t2045108845, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CPaytoserverU3Ec__Iterator4_t2045108845, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Epay_6() { return static_cast<int32_t>(offsetof(U3CPaytoserverU3Ec__Iterator4_t2045108845, ___U3CU24U3Epay_6)); }
	inline String_t* get_U3CU24U3Epay_6() const { return ___U3CU24U3Epay_6; }
	inline String_t** get_address_of_U3CU24U3Epay_6() { return &___U3CU24U3Epay_6; }
	inline void set_U3CU24U3Epay_6(String_t* value)
	{
		___U3CU24U3Epay_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Epay_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eexpriydate_7() { return static_cast<int32_t>(offsetof(U3CPaytoserverU3Ec__Iterator4_t2045108845, ___U3CU24U3Eexpriydate_7)); }
	inline String_t* get_U3CU24U3Eexpriydate_7() const { return ___U3CU24U3Eexpriydate_7; }
	inline String_t** get_address_of_U3CU24U3Eexpriydate_7() { return &___U3CU24U3Eexpriydate_7; }
	inline void set_U3CU24U3Eexpriydate_7(String_t* value)
	{
		___U3CU24U3Eexpriydate_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eexpriydate_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CPaytoserverU3Ec__Iterator4_t2045108845, ___U3CU3Ef__this_8)); }
	inline Purchaser_t1507804203 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline Purchaser_t1507804203 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(Purchaser_t1507804203 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
