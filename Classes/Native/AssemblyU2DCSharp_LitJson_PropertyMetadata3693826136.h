﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.String
struct String_t;

#include "mscorlib_System_ValueType3507792607.h"
#include "AssemblyU2DCSharp_LitJson_JsonIgnoreWhen1352154806.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LitJson.PropertyMetadata
struct  PropertyMetadata_t3693826136 
{
public:
	// System.Type LitJson.PropertyMetadata::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_0;
	// System.Reflection.MemberInfo LitJson.PropertyMetadata::<Info>k__BackingField
	MemberInfo_t * ___U3CInfoU3Ek__BackingField_1;
	// LitJson.JsonIgnoreWhen LitJson.PropertyMetadata::<Ignore>k__BackingField
	int32_t ___U3CIgnoreU3Ek__BackingField_2;
	// System.String LitJson.PropertyMetadata::<Alias>k__BackingField
	String_t* ___U3CAliasU3Ek__BackingField_3;
	// System.Boolean LitJson.PropertyMetadata::<IsField>k__BackingField
	bool ___U3CIsFieldU3Ek__BackingField_4;
	// System.Boolean LitJson.PropertyMetadata::<Include>k__BackingField
	bool ___U3CIncludeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PropertyMetadata_t3693826136, ___U3CTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_0() const { return ___U3CTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_0() { return &___U3CTypeU3Ek__BackingField_0; }
	inline void set_U3CTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTypeU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PropertyMetadata_t3693826136, ___U3CInfoU3Ek__BackingField_1)); }
	inline MemberInfo_t * get_U3CInfoU3Ek__BackingField_1() const { return ___U3CInfoU3Ek__BackingField_1; }
	inline MemberInfo_t ** get_address_of_U3CInfoU3Ek__BackingField_1() { return &___U3CInfoU3Ek__BackingField_1; }
	inline void set_U3CInfoU3Ek__BackingField_1(MemberInfo_t * value)
	{
		___U3CInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CInfoU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CIgnoreU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PropertyMetadata_t3693826136, ___U3CIgnoreU3Ek__BackingField_2)); }
	inline int32_t get_U3CIgnoreU3Ek__BackingField_2() const { return ___U3CIgnoreU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CIgnoreU3Ek__BackingField_2() { return &___U3CIgnoreU3Ek__BackingField_2; }
	inline void set_U3CIgnoreU3Ek__BackingField_2(int32_t value)
	{
		___U3CIgnoreU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CAliasU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PropertyMetadata_t3693826136, ___U3CAliasU3Ek__BackingField_3)); }
	inline String_t* get_U3CAliasU3Ek__BackingField_3() const { return ___U3CAliasU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CAliasU3Ek__BackingField_3() { return &___U3CAliasU3Ek__BackingField_3; }
	inline void set_U3CAliasU3Ek__BackingField_3(String_t* value)
	{
		___U3CAliasU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAliasU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CIsFieldU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PropertyMetadata_t3693826136, ___U3CIsFieldU3Ek__BackingField_4)); }
	inline bool get_U3CIsFieldU3Ek__BackingField_4() const { return ___U3CIsFieldU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsFieldU3Ek__BackingField_4() { return &___U3CIsFieldU3Ek__BackingField_4; }
	inline void set_U3CIsFieldU3Ek__BackingField_4(bool value)
	{
		___U3CIsFieldU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CIncludeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PropertyMetadata_t3693826136, ___U3CIncludeU3Ek__BackingField_5)); }
	inline bool get_U3CIncludeU3Ek__BackingField_5() const { return ___U3CIncludeU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CIncludeU3Ek__BackingField_5() { return &___U3CIncludeU3Ek__BackingField_5; }
	inline void set_U3CIncludeU3Ek__BackingField_5(bool value)
	{
		___U3CIncludeU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: LitJson.PropertyMetadata
struct PropertyMetadata_t3693826136_marshaled_pinvoke
{
	Type_t * ___U3CTypeU3Ek__BackingField_0;
	MemberInfo_t * ___U3CInfoU3Ek__BackingField_1;
	int32_t ___U3CIgnoreU3Ek__BackingField_2;
	char* ___U3CAliasU3Ek__BackingField_3;
	int32_t ___U3CIsFieldU3Ek__BackingField_4;
	int32_t ___U3CIncludeU3Ek__BackingField_5;
};
// Native definition for marshalling of: LitJson.PropertyMetadata
struct PropertyMetadata_t3693826136_marshaled_com
{
	Type_t * ___U3CTypeU3Ek__BackingField_0;
	MemberInfo_t * ___U3CInfoU3Ek__BackingField_1;
	int32_t ___U3CIgnoreU3Ek__BackingField_2;
	Il2CppChar* ___U3CAliasU3Ek__BackingField_3;
	int32_t ___U3CIsFieldU3Ek__BackingField_4;
	int32_t ___U3CIncludeU3Ek__BackingField_5;
};
