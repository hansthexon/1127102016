﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct DefaultComparer_t1043212959;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23716250094.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor()
extern "C"  void DefaultComparer__ctor_m963705432_gshared (DefaultComparer_t1043212959 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m963705432(__this, method) ((  void (*) (DefaultComparer_t1043212959 *, const MethodInfo*))DefaultComparer__ctor_m963705432_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m995859237_gshared (DefaultComparer_t1043212959 * __this, KeyValuePair_2_t3716250094  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m995859237(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t1043212959 *, KeyValuePair_2_t3716250094 , const MethodInfo*))DefaultComparer_GetHashCode_m995859237_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m300724589_gshared (DefaultComparer_t1043212959 * __this, KeyValuePair_2_t3716250094  ___x0, KeyValuePair_2_t3716250094  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m300724589(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t1043212959 *, KeyValuePair_2_t3716250094 , KeyValuePair_2_t3716250094 , const MethodInfo*))DefaultComparer_Equals_m300724589_gshared)(__this, ___x0, ___y1, method)
