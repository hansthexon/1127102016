﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VideoBackgroundManagerAbstractBehaviour
struct VideoBackgroundManagerAbstractBehaviour_t3765780423;
// UnityEngine.Shader
struct Shader_t2430389951;

#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_HideExcessAreaAbst2100449024.h"
#include "UnityEngine_UnityEngine_Shader2430389951.h"

// System.Boolean Vuforia.VideoBackgroundManagerAbstractBehaviour::get_VideoBackgroundEnabled()
extern "C"  bool VideoBackgroundManagerAbstractBehaviour_get_VideoBackgroundEnabled_m3134612786 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE Vuforia.VideoBackgroundManagerAbstractBehaviour::get_ClippingMode()
extern "C"  int32_t VideoBackgroundManagerAbstractBehaviour_get_ClippingMode_m3958355796 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader Vuforia.VideoBackgroundManagerAbstractBehaviour::get_MatteShader()
extern "C"  Shader_t2430389951 * VideoBackgroundManagerAbstractBehaviour_get_MatteShader_m916288375 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundManagerAbstractBehaviour::SetVideoBackgroundEnabled(System.Boolean)
extern "C"  void VideoBackgroundManagerAbstractBehaviour_SetVideoBackgroundEnabled_m3734803682 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundManagerAbstractBehaviour::SetClippingMode(Vuforia.HideExcessAreaAbstractBehaviour/CLIPPING_MODE)
extern "C"  void VideoBackgroundManagerAbstractBehaviour_SetClippingMode_m188632720 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundManagerAbstractBehaviour::SetMatteShader(UnityEngine.Shader)
extern "C"  void VideoBackgroundManagerAbstractBehaviour_SetMatteShader_m2677126183 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, Shader_t2430389951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundManagerAbstractBehaviour::Initialize()
extern "C"  void VideoBackgroundManagerAbstractBehaviour_Initialize_m3462108883 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundManagerAbstractBehaviour::Start()
extern "C"  void VideoBackgroundManagerAbstractBehaviour_Start_m2776212651 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundManagerAbstractBehaviour::Update()
extern "C"  void VideoBackgroundManagerAbstractBehaviour_Update_m806977586 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundManagerAbstractBehaviour::OnDestroy()
extern "C"  void VideoBackgroundManagerAbstractBehaviour_OnDestroy_m3947277754 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundManagerAbstractBehaviour::OnVuforiaInitialized()
extern "C"  void VideoBackgroundManagerAbstractBehaviour_OnVuforiaInitialized_m3030144174 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundManagerAbstractBehaviour::UpdateVideoBackgroundEnabled()
extern "C"  void VideoBackgroundManagerAbstractBehaviour_UpdateVideoBackgroundEnabled_m4151398262 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundManagerAbstractBehaviour::OnVideoBackgroundConfigChanged()
extern "C"  void VideoBackgroundManagerAbstractBehaviour_OnVideoBackgroundConfigChanged_m1581451517 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.VideoBackgroundManagerAbstractBehaviour::.ctor()
extern "C"  void VideoBackgroundManagerAbstractBehaviour__ctor_m3638037263 (VideoBackgroundManagerAbstractBehaviour_t3765780423 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
