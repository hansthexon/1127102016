﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3438122032MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3138526911(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t1465873870 *, Dictionary_2_t2762814027 *, const MethodInfo*))ValueCollection__ctor_m2980779627_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3287886189(__this, ___item0, method) ((  void (*) (ValueCollection_t1465873870 *, IndexInfo_t848034765 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3467596809_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m134253488(__this, method) ((  void (*) (ValueCollection_t1465873870 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1073638090_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2606067227(__this, ___item0, method) ((  bool (*) (ValueCollection_t1465873870 *, IndexInfo_t848034765 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1440625111_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m606466706(__this, ___item0, method) ((  bool (*) (ValueCollection_t1465873870 *, IndexInfo_t848034765 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m100409112_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1686206468(__this, method) ((  Il2CppObject* (*) (ValueCollection_t1465873870 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m359754090_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m4018933492(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1465873870 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1375316058_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1888781609(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1465873870 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1800558157_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3871075302(__this, method) ((  bool (*) (ValueCollection_t1465873870 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3008407260_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1773407068(__this, method) ((  bool (*) (ValueCollection_t1465873870 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2828968226_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m263446660(__this, method) ((  Il2CppObject * (*) (ValueCollection_t1465873870 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m969375434_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1871027224(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t1465873870 *, IndexInfoU5BU5D_t625615456*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m55448994_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::GetEnumerator()
#define ValueCollection_GetEnumerator_m751351603(__this, method) ((  Enumerator_t154379495  (*) (ValueCollection_t1465873870 *, const MethodInfo*))ValueCollection_GetEnumerator_m2504057031_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Count()
#define ValueCollection_get_Count_m1028486652(__this, method) ((  int32_t (*) (ValueCollection_t1465873870 *, const MethodInfo*))ValueCollection_get_Count_m1235472514_gshared)(__this, method)
