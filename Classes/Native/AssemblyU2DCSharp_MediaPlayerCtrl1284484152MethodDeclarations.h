﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MediaPlayerCtrl
struct MediaPlayerCtrl_t1284484152;
// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIAPLAYER_ERRO3510122879.h"
#include "AssemblyU2DCSharp_MediaPlayerCtrl_MEDIAPLAYER_STAT2051437640.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void MediaPlayerCtrl::.ctor()
extern "C"  void MediaPlayerCtrl__ctor_m3988361949 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Awake()
extern "C"  void MediaPlayerCtrl_Awake_m2054862404 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Start()
extern "C"  void MediaPlayerCtrl_Start_m3180603197 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::OnApplicationQuit()
extern "C"  void MediaPlayerCtrl_OnApplicationQuit_m1096541347 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::OnDisable()
extern "C"  void MediaPlayerCtrl_OnDisable_m3170445452 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::OnEnable()
extern "C"  void MediaPlayerCtrl_OnEnable_m918022557 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Update()
extern "C"  void MediaPlayerCtrl_Update_m525310434 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Resize()
extern "C"  void MediaPlayerCtrl_Resize_m368931995 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::OnError(MediaPlayerCtrl/MEDIAPLAYER_ERROR,MediaPlayerCtrl/MEDIAPLAYER_ERROR)
extern "C"  void MediaPlayerCtrl_OnError_m4163002676 (MediaPlayerCtrl_t1284484152 * __this, int32_t ___iCode0, int32_t ___iCodeExtra1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::OnDestroy()
extern "C"  void MediaPlayerCtrl_OnDestroy_m1503112038 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::OnApplicationPause(System.Boolean)
extern "C"  void MediaPlayerCtrl_OnApplicationPause_m803645687 (MediaPlayerCtrl_t1284484152 * __this, bool ___bPause0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MediaPlayerCtrl/MEDIAPLAYER_STATE MediaPlayerCtrl::GetCurrentState()
extern "C"  int32_t MediaPlayerCtrl_GetCurrentState_m478483488 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D MediaPlayerCtrl::GetVideoTexture()
extern "C"  Texture2D_t3542995729 * MediaPlayerCtrl_GetVideoTexture_m928719100 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Play()
extern "C"  void MediaPlayerCtrl_Play_m3463996317 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Stop()
extern "C"  void MediaPlayerCtrl_Stop_m767571029 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Pause()
extern "C"  void MediaPlayerCtrl_Pause_m859757683 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Load(System.String)
extern "C"  void MediaPlayerCtrl_Load_m1234147701 (MediaPlayerCtrl_t1284484152 * __this, String_t* ___strFileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::SetVolume(System.Single)
extern "C"  void MediaPlayerCtrl_SetVolume_m3958862998 (MediaPlayerCtrl_t1284484152 * __this, float ___fVolume0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::GetSeekPosition()
extern "C"  int32_t MediaPlayerCtrl_GetSeekPosition_m412350594 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::SeekTo(System.Int32)
extern "C"  void MediaPlayerCtrl_SeekTo_m1225246587 (MediaPlayerCtrl_t1284484152 * __this, int32_t ___iSeek0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::GetDuration()
extern "C"  int32_t MediaPlayerCtrl_GetDuration_m1878841027 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::GetCurrentSeekPercent()
extern "C"  int32_t MediaPlayerCtrl_GetCurrentSeekPercent_m3579890011 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::GetVideoWidth()
extern "C"  int32_t MediaPlayerCtrl_GetVideoWidth_m2529203304 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::GetVideoHeight()
extern "C"  int32_t MediaPlayerCtrl_GetVideoHeight_m437606807 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::UnLoad()
extern "C"  void MediaPlayerCtrl_UnLoad_m1479978368 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::VideoPlayerPluginCreateInstance()
extern "C"  int32_t MediaPlayerCtrl_VideoPlayerPluginCreateInstance_m1260776317 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::VideoPlayerPluginDestroyInstance(System.Int32)
extern "C"  void MediaPlayerCtrl_VideoPlayerPluginDestroyInstance_m158541008 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MediaPlayerCtrl::VideoPlayerPluginCanOutputToTexture(System.String)
extern "C"  bool MediaPlayerCtrl_VideoPlayerPluginCanOutputToTexture_m637308739 (Il2CppObject * __this /* static, unused */, String_t* ___videoURL0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::VideoPlayerPluginSetLoop(System.Int32,System.Boolean)
extern "C"  void MediaPlayerCtrl_VideoPlayerPluginSetLoop_m1826555874 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, bool ___bLoop1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::VideoPlayerPluginSetVolume(System.Int32,System.Single)
extern "C"  void MediaPlayerCtrl_VideoPlayerPluginSetVolume_m2162131076 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, float ___fVolume1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MediaPlayerCtrl::VideoPlayerPluginPlayerReady(System.Int32)
extern "C"  bool MediaPlayerCtrl_VideoPlayerPluginPlayerReady_m2506194831 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MediaPlayerCtrl::VideoPlayerPluginDurationSeconds(System.Int32)
extern "C"  float MediaPlayerCtrl_VideoPlayerPluginDurationSeconds_m564640622 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::VideoPlayerPluginExtents(System.Int32,System.Int32&,System.Int32&)
extern "C"  void MediaPlayerCtrl_VideoPlayerPluginExtents_m1494968880 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, int32_t* ___width1, int32_t* ___height2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr MediaPlayerCtrl::VideoPlayerPluginCurFrameTexture(System.Int32)
extern "C"  IntPtr_t MediaPlayerCtrl_VideoPlayerPluginCurFrameTexture_m3887220932 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::VideoPlayerPluginLoadVideo(System.Int32,System.String)
extern "C"  void MediaPlayerCtrl_VideoPlayerPluginLoadVideo_m158281770 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, String_t* ___videoURL1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::VideoPlayerPluginPlayVideo(System.Int32)
extern "C"  void MediaPlayerCtrl_VideoPlayerPluginPlayVideo_m615482944 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::VideoPlayerPluginPauseVideo(System.Int32)
extern "C"  void MediaPlayerCtrl_VideoPlayerPluginPauseVideo_m3347955598 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::VideoPlayerPluginResumeVideo(System.Int32)
extern "C"  void MediaPlayerCtrl_VideoPlayerPluginResumeVideo_m4073884427 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::VideoPlayerPluginRewindVideo(System.Int32)
extern "C"  void MediaPlayerCtrl_VideoPlayerPluginRewindVideo_m94545805 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::VideoPlayerPluginSeekToVideo(System.Int32,System.Single)
extern "C"  void MediaPlayerCtrl_VideoPlayerPluginSeekToVideo_m3483025598 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MediaPlayerCtrl::VideoPlayerPluginCurTimeSeconds(System.Int32)
extern "C"  float MediaPlayerCtrl_VideoPlayerPluginCurTimeSeconds_m3879662055 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MediaPlayerCtrl::VideoPlayerPluginIsPlaying(System.Int32)
extern "C"  bool MediaPlayerCtrl_VideoPlayerPluginIsPlaying_m73108029 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::VideoPlayerPluginStopVideo(System.Int32)
extern "C"  void MediaPlayerCtrl_VideoPlayerPluginStopVideo_m3221760422 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MediaPlayerCtrl::VideoPlayerPluginFinish(System.Int32)
extern "C"  bool MediaPlayerCtrl_VideoPlayerPluginFinish_m3542847214 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::VideoPlayerPluginSetTexture(System.Int32,System.Int32)
extern "C"  void MediaPlayerCtrl_VideoPlayerPluginSetTexture_m2352176051 (Il2CppObject * __this /* static, unused */, int32_t ___iID0, int32_t ___iTextureID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MediaPlayerCtrl::get_ready()
extern "C"  bool MediaPlayerCtrl_get_ready_m3179820607 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MediaPlayerCtrl::get_duration()
extern "C"  float MediaPlayerCtrl_get_duration_m2182156532 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MediaPlayerCtrl::get_currentTime()
extern "C"  float MediaPlayerCtrl_get_currentTime_m4002269078 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MediaPlayerCtrl::get_isPlaying()
extern "C"  bool MediaPlayerCtrl_get_isPlaying_m776910936 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 MediaPlayerCtrl::get_videoSize()
extern "C"  Vector2_t2243707579  MediaPlayerCtrl_get_videoSize_m3885539695 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D MediaPlayerCtrl::get_videoTexture()
extern "C"  Texture2D_t3542995729 * MediaPlayerCtrl_get_videoTexture_m1024434559 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_Destroy()
extern "C"  void MediaPlayerCtrl_Call_Destroy_m3470843426 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_UnLoad()
extern "C"  void MediaPlayerCtrl_Call_UnLoad_m3029584045 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MediaPlayerCtrl::Call_Load(System.String,System.Int32)
extern "C"  bool MediaPlayerCtrl_Call_Load_m1018293293 (MediaPlayerCtrl_t1284484152 * __this, String_t* ___strFileName0, int32_t ___iSeek1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_UpdateVideoTexture()
extern "C"  void MediaPlayerCtrl_Call_UpdateVideoTexture_m612567955 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_SetVolume(System.Single)
extern "C"  void MediaPlayerCtrl_Call_SetVolume_m2160605539 (MediaPlayerCtrl_t1284484152 * __this, float ___fVolume0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_SetSeekPosition(System.Int32)
extern "C"  void MediaPlayerCtrl_Call_SetSeekPosition_m3965555602 (MediaPlayerCtrl_t1284484152 * __this, int32_t ___iSeek0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::Call_GetSeekPosition()
extern "C"  int32_t MediaPlayerCtrl_Call_GetSeekPosition_m3544964085 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_Play(System.Int32)
extern "C"  void MediaPlayerCtrl_Call_Play_m1017492841 (MediaPlayerCtrl_t1284484152 * __this, int32_t ___iSeek0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_Reset()
extern "C"  void MediaPlayerCtrl_Call_Reset_m3014036165 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_Stop()
extern "C"  void MediaPlayerCtrl_Call_Stop_m581862902 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_RePlay()
extern "C"  void MediaPlayerCtrl_Call_RePlay_m1899695271 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_Pause()
extern "C"  void MediaPlayerCtrl_Call_Pause_m148629930 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::Call_GetVideoWidth()
extern "C"  int32_t MediaPlayerCtrl_Call_GetVideoWidth_m4179706159 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::Call_GetVideoHeight()
extern "C"  int32_t MediaPlayerCtrl_Call_GetVideoHeight_m3686821108 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_SetUnityTexture(System.Int32)
extern "C"  void MediaPlayerCtrl_Call_SetUnityTexture_m1145018419 (MediaPlayerCtrl_t1284484152 * __this, int32_t ___iTextureID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_SetWindowSize()
extern "C"  void MediaPlayerCtrl_Call_SetWindowSize_m2281146867 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_SetLooping(System.Boolean)
extern "C"  void MediaPlayerCtrl_Call_SetLooping_m2780454657 (MediaPlayerCtrl_t1284484152 * __this, bool ___bLoop0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MediaPlayerCtrl::Call_SetUnityActivity()
extern "C"  void MediaPlayerCtrl_Call_SetUnityActivity_m1896432994 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::Call_GetError()
extern "C"  int32_t MediaPlayerCtrl_Call_GetError_m1453821702 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::Call_GetErrorExtra()
extern "C"  int32_t MediaPlayerCtrl_Call_GetErrorExtra_m1798728010 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::Call_GetDuration()
extern "C"  int32_t MediaPlayerCtrl_Call_GetDuration_m3116134198 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 MediaPlayerCtrl::Call_GetCurrentSeekPercent()
extern "C"  int32_t MediaPlayerCtrl_Call_GetCurrentSeekPercent_m1333391608 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// MediaPlayerCtrl/MEDIAPLAYER_STATE MediaPlayerCtrl::Call_GetStatus()
extern "C"  int32_t MediaPlayerCtrl_Call_GetStatus_m2806016939 (MediaPlayerCtrl_t1284484152 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MediaPlayerCtrl::DownloadStreamingVideoAndLoad(System.String)
extern "C"  Il2CppObject * MediaPlayerCtrl_DownloadStreamingVideoAndLoad_m1762522881 (MediaPlayerCtrl_t1284484152 * __this, String_t* ___strURL0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MediaPlayerCtrl::CopyStreamingAssetVideoAndLoad(System.String)
extern "C"  Il2CppObject * MediaPlayerCtrl_CopyStreamingAssetVideoAndLoad_m4077076508 (MediaPlayerCtrl_t1284484152 * __this, String_t* ___strURL0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
