﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.AppleValidator
struct AppleValidator_t3837389912;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// UnityEngine.Purchasing.Security.AppleReceipt
struct AppleReceipt_t3991411794;
// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t1770761751;
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt
struct AppleInAppPurchaseReceipt_t3271698749;

#include "codegen/il2cpp-codegen.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"
#include "mscorlib_System_DateTime693205669.h"

// System.Void UnityEngine.Purchasing.Security.AppleValidator::.ctor(System.Byte[])
extern "C"  void AppleValidator__ctor_m1455195731 (AppleValidator_t3837389912 * __this, ByteU5BU5D_t3397334013* ___appleRootCertificate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.Security.AppleValidator::Validate(System.Byte[])
extern "C"  AppleReceipt_t3991411794 * AppleValidator_Validate_m6433925 (AppleValidator_t3837389912 * __this, ByteU5BU5D_t3397334013* ___receiptData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.AppleReceipt UnityEngine.Purchasing.Security.AppleValidator::ParseReceipt(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  AppleReceipt_t3991411794 * AppleValidator_ParseReceipt_m3945723525 (AppleValidator_t3837389912 * __this, Asn1Node_t1770761751 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt UnityEngine.Purchasing.Security.AppleValidator::ParseInAppReceipt(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  AppleInAppPurchaseReceipt_t3271698749 * AppleValidator_ParseInAppReceipt_m3138849002 (AppleValidator_t3837389912 * __this, Asn1Node_t1770761751 * ___inApp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.Purchasing.Security.AppleValidator::TryParseDateTimeNode(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  DateTime_t693205669  AppleValidator_TryParseDateTimeNode_m190299298 (Il2CppObject * __this /* static, unused */, Asn1Node_t1770761751 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
