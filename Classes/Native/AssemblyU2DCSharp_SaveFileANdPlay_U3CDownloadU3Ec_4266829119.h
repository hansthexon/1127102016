﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t2919945039;
// System.Object
struct Il2CppObject;
// SaveFileANdPlay
struct SaveFileANdPlay_t1144534430;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SaveFileANdPlay/<Download>c__Iterator10
struct  U3CDownloadU3Ec__Iterator10_t4266829119  : public Il2CppObject
{
public:
	// UnityEngine.WWW SaveFileANdPlay/<Download>c__Iterator10::<www>__0
	WWW_t2919945039 * ___U3CwwwU3E__0_0;
	// System.Int32 SaveFileANdPlay/<Download>c__Iterator10::$PC
	int32_t ___U24PC_1;
	// System.Object SaveFileANdPlay/<Download>c__Iterator10::$current
	Il2CppObject * ___U24current_2;
	// SaveFileANdPlay SaveFileANdPlay/<Download>c__Iterator10::<>f__this
	SaveFileANdPlay_t1144534430 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator10_t4266829119, ___U3CwwwU3E__0_0)); }
	inline WWW_t2919945039 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t2919945039 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t2919945039 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator10_t4266829119, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator10_t4266829119, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CDownloadU3Ec__Iterator10_t4266829119, ___U3CU3Ef__this_3)); }
	inline SaveFileANdPlay_t1144534430 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline SaveFileANdPlay_t1144534430 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(SaveFileANdPlay_t1144534430 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
