﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct Dictionary_2_t440094893;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3129598331.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4292427602_gshared (Enumerator_t3129598331 * __this, Dictionary_2_t440094893 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4292427602(__this, ___host0, method) ((  void (*) (Enumerator_t3129598331 *, Dictionary_2_t440094893 *, const MethodInfo*))Enumerator__ctor_m4292427602_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1496472837_gshared (Enumerator_t3129598331 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1496472837(__this, method) ((  Il2CppObject * (*) (Enumerator_t3129598331 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1496472837_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m387949317_gshared (Enumerator_t3129598331 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m387949317(__this, method) ((  void (*) (Enumerator_t3129598331 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m387949317_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m2018226074_gshared (Enumerator_t3129598331 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2018226074(__this, method) ((  void (*) (Enumerator_t3129598331 *, const MethodInfo*))Enumerator_Dispose_m2018226074_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2380055421_gshared (Enumerator_t3129598331 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2380055421(__this, method) ((  bool (*) (Enumerator_t3129598331 *, const MethodInfo*))Enumerator_MoveNext_m2380055421_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m4000415415_gshared (Enumerator_t3129598331 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4000415415(__this, method) ((  Il2CppObject * (*) (Enumerator_t3129598331 *, const MethodInfo*))Enumerator_get_Current_m4000415415_gshared)(__this, method)
