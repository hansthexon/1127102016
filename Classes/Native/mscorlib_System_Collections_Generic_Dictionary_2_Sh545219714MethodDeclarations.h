﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct ShimEnumerator_t545219714;
// System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct Dictionary_2_t440094893;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1141344761_gshared (ShimEnumerator_t545219714 * __this, Dictionary_2_t440094893 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1141344761(__this, ___host0, method) ((  void (*) (ShimEnumerator_t545219714 *, Dictionary_2_t440094893 *, const MethodInfo*))ShimEnumerator__ctor_m1141344761_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m683594824_gshared (ShimEnumerator_t545219714 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m683594824(__this, method) ((  bool (*) (ShimEnumerator_t545219714 *, const MethodInfo*))ShimEnumerator_MoveNext_m683594824_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Entry()
extern "C"  DictionaryEntry_t3048875398  ShimEnumerator_get_Entry_m2651451228_gshared (ShimEnumerator_t545219714 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2651451228(__this, method) ((  DictionaryEntry_t3048875398  (*) (ShimEnumerator_t545219714 *, const MethodInfo*))ShimEnumerator_get_Entry_m2651451228_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3027249205_gshared (ShimEnumerator_t545219714 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3027249205(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t545219714 *, const MethodInfo*))ShimEnumerator_get_Key_m3027249205_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2958879149_gshared (ShimEnumerator_t545219714 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2958879149(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t545219714 *, const MethodInfo*))ShimEnumerator_get_Value_m2958879149_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3634860963_gshared (ShimEnumerator_t545219714 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3634860963(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t545219714 *, const MethodInfo*))ShimEnumerator_get_Current_m3634860963_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Reset()
extern "C"  void ShimEnumerator_Reset_m915444379_gshared (ShimEnumerator_t545219714 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m915444379(__this, method) ((  void (*) (ShimEnumerator_t545219714 *, const MethodInfo*))ShimEnumerator_Reset_m915444379_gshared)(__this, method)
