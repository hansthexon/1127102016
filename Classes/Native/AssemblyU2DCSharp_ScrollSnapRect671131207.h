﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.Sprite
struct Sprite_t309593783;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t1199013257;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1612828711;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t1411648341;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScrollSnapRect
struct  ScrollSnapRect_t671131207  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Animator ScrollSnapRect::Dog
	Animator_t69676727 * ___Dog_2;
	// System.Int32 ScrollSnapRect::startingPage
	int32_t ___startingPage_3;
	// System.Single ScrollSnapRect::fastSwipeThresholdTime
	float ___fastSwipeThresholdTime_4;
	// System.Int32 ScrollSnapRect::fastSwipeThresholdDistance
	int32_t ___fastSwipeThresholdDistance_5;
	// System.Single ScrollSnapRect::decelerationRate
	float ___decelerationRate_6;
	// UnityEngine.GameObject ScrollSnapRect::prevButton
	GameObject_t1756533147 * ___prevButton_7;
	// UnityEngine.GameObject ScrollSnapRect::nextButton
	GameObject_t1756533147 * ___nextButton_8;
	// UnityEngine.Sprite ScrollSnapRect::unselectedPage
	Sprite_t309593783 * ___unselectedPage_9;
	// UnityEngine.Sprite ScrollSnapRect::selectedPage
	Sprite_t309593783 * ___selectedPage_10;
	// UnityEngine.Transform ScrollSnapRect::pageSelectionIcons
	Transform_t3275118058 * ___pageSelectionIcons_11;
	// System.Int32 ScrollSnapRect::_fastSwipeThresholdMaxLimit
	int32_t ____fastSwipeThresholdMaxLimit_12;
	// UnityEngine.UI.ScrollRect ScrollSnapRect::_scrollRectComponent
	ScrollRect_t1199013257 * ____scrollRectComponent_13;
	// UnityEngine.RectTransform ScrollSnapRect::_scrollRectRect
	RectTransform_t3349966182 * ____scrollRectRect_14;
	// UnityEngine.RectTransform ScrollSnapRect::_container
	RectTransform_t3349966182 * ____container_15;
	// System.Boolean ScrollSnapRect::_horizontal
	bool ____horizontal_16;
	// System.Int32 ScrollSnapRect::_pageCount
	int32_t ____pageCount_17;
	// System.Int32 ScrollSnapRect::_currentPage
	int32_t ____currentPage_18;
	// System.Boolean ScrollSnapRect::_lerp
	bool ____lerp_19;
	// UnityEngine.Vector2 ScrollSnapRect::_lerpTo
	Vector2_t2243707579  ____lerpTo_20;
	// System.Collections.Generic.List`1<UnityEngine.Vector2> ScrollSnapRect::_pagePositions
	List_1_t1612828711 * ____pagePositions_21;
	// System.Boolean ScrollSnapRect::_dragging
	bool ____dragging_22;
	// System.Single ScrollSnapRect::_timeStamp
	float ____timeStamp_23;
	// UnityEngine.Vector2 ScrollSnapRect::_startPosition
	Vector2_t2243707579  ____startPosition_24;
	// System.Boolean ScrollSnapRect::_showPageSelection
	bool ____showPageSelection_25;
	// System.Int32 ScrollSnapRect::_previousPageSelectionIndex
	int32_t ____previousPageSelectionIndex_26;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> ScrollSnapRect::_pageSelectionImages
	List_1_t1411648341 * ____pageSelectionImages_27;

public:
	inline static int32_t get_offset_of_Dog_2() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ___Dog_2)); }
	inline Animator_t69676727 * get_Dog_2() const { return ___Dog_2; }
	inline Animator_t69676727 ** get_address_of_Dog_2() { return &___Dog_2; }
	inline void set_Dog_2(Animator_t69676727 * value)
	{
		___Dog_2 = value;
		Il2CppCodeGenWriteBarrier(&___Dog_2, value);
	}

	inline static int32_t get_offset_of_startingPage_3() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ___startingPage_3)); }
	inline int32_t get_startingPage_3() const { return ___startingPage_3; }
	inline int32_t* get_address_of_startingPage_3() { return &___startingPage_3; }
	inline void set_startingPage_3(int32_t value)
	{
		___startingPage_3 = value;
	}

	inline static int32_t get_offset_of_fastSwipeThresholdTime_4() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ___fastSwipeThresholdTime_4)); }
	inline float get_fastSwipeThresholdTime_4() const { return ___fastSwipeThresholdTime_4; }
	inline float* get_address_of_fastSwipeThresholdTime_4() { return &___fastSwipeThresholdTime_4; }
	inline void set_fastSwipeThresholdTime_4(float value)
	{
		___fastSwipeThresholdTime_4 = value;
	}

	inline static int32_t get_offset_of_fastSwipeThresholdDistance_5() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ___fastSwipeThresholdDistance_5)); }
	inline int32_t get_fastSwipeThresholdDistance_5() const { return ___fastSwipeThresholdDistance_5; }
	inline int32_t* get_address_of_fastSwipeThresholdDistance_5() { return &___fastSwipeThresholdDistance_5; }
	inline void set_fastSwipeThresholdDistance_5(int32_t value)
	{
		___fastSwipeThresholdDistance_5 = value;
	}

	inline static int32_t get_offset_of_decelerationRate_6() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ___decelerationRate_6)); }
	inline float get_decelerationRate_6() const { return ___decelerationRate_6; }
	inline float* get_address_of_decelerationRate_6() { return &___decelerationRate_6; }
	inline void set_decelerationRate_6(float value)
	{
		___decelerationRate_6 = value;
	}

	inline static int32_t get_offset_of_prevButton_7() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ___prevButton_7)); }
	inline GameObject_t1756533147 * get_prevButton_7() const { return ___prevButton_7; }
	inline GameObject_t1756533147 ** get_address_of_prevButton_7() { return &___prevButton_7; }
	inline void set_prevButton_7(GameObject_t1756533147 * value)
	{
		___prevButton_7 = value;
		Il2CppCodeGenWriteBarrier(&___prevButton_7, value);
	}

	inline static int32_t get_offset_of_nextButton_8() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ___nextButton_8)); }
	inline GameObject_t1756533147 * get_nextButton_8() const { return ___nextButton_8; }
	inline GameObject_t1756533147 ** get_address_of_nextButton_8() { return &___nextButton_8; }
	inline void set_nextButton_8(GameObject_t1756533147 * value)
	{
		___nextButton_8 = value;
		Il2CppCodeGenWriteBarrier(&___nextButton_8, value);
	}

	inline static int32_t get_offset_of_unselectedPage_9() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ___unselectedPage_9)); }
	inline Sprite_t309593783 * get_unselectedPage_9() const { return ___unselectedPage_9; }
	inline Sprite_t309593783 ** get_address_of_unselectedPage_9() { return &___unselectedPage_9; }
	inline void set_unselectedPage_9(Sprite_t309593783 * value)
	{
		___unselectedPage_9 = value;
		Il2CppCodeGenWriteBarrier(&___unselectedPage_9, value);
	}

	inline static int32_t get_offset_of_selectedPage_10() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ___selectedPage_10)); }
	inline Sprite_t309593783 * get_selectedPage_10() const { return ___selectedPage_10; }
	inline Sprite_t309593783 ** get_address_of_selectedPage_10() { return &___selectedPage_10; }
	inline void set_selectedPage_10(Sprite_t309593783 * value)
	{
		___selectedPage_10 = value;
		Il2CppCodeGenWriteBarrier(&___selectedPage_10, value);
	}

	inline static int32_t get_offset_of_pageSelectionIcons_11() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ___pageSelectionIcons_11)); }
	inline Transform_t3275118058 * get_pageSelectionIcons_11() const { return ___pageSelectionIcons_11; }
	inline Transform_t3275118058 ** get_address_of_pageSelectionIcons_11() { return &___pageSelectionIcons_11; }
	inline void set_pageSelectionIcons_11(Transform_t3275118058 * value)
	{
		___pageSelectionIcons_11 = value;
		Il2CppCodeGenWriteBarrier(&___pageSelectionIcons_11, value);
	}

	inline static int32_t get_offset_of__fastSwipeThresholdMaxLimit_12() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____fastSwipeThresholdMaxLimit_12)); }
	inline int32_t get__fastSwipeThresholdMaxLimit_12() const { return ____fastSwipeThresholdMaxLimit_12; }
	inline int32_t* get_address_of__fastSwipeThresholdMaxLimit_12() { return &____fastSwipeThresholdMaxLimit_12; }
	inline void set__fastSwipeThresholdMaxLimit_12(int32_t value)
	{
		____fastSwipeThresholdMaxLimit_12 = value;
	}

	inline static int32_t get_offset_of__scrollRectComponent_13() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____scrollRectComponent_13)); }
	inline ScrollRect_t1199013257 * get__scrollRectComponent_13() const { return ____scrollRectComponent_13; }
	inline ScrollRect_t1199013257 ** get_address_of__scrollRectComponent_13() { return &____scrollRectComponent_13; }
	inline void set__scrollRectComponent_13(ScrollRect_t1199013257 * value)
	{
		____scrollRectComponent_13 = value;
		Il2CppCodeGenWriteBarrier(&____scrollRectComponent_13, value);
	}

	inline static int32_t get_offset_of__scrollRectRect_14() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____scrollRectRect_14)); }
	inline RectTransform_t3349966182 * get__scrollRectRect_14() const { return ____scrollRectRect_14; }
	inline RectTransform_t3349966182 ** get_address_of__scrollRectRect_14() { return &____scrollRectRect_14; }
	inline void set__scrollRectRect_14(RectTransform_t3349966182 * value)
	{
		____scrollRectRect_14 = value;
		Il2CppCodeGenWriteBarrier(&____scrollRectRect_14, value);
	}

	inline static int32_t get_offset_of__container_15() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____container_15)); }
	inline RectTransform_t3349966182 * get__container_15() const { return ____container_15; }
	inline RectTransform_t3349966182 ** get_address_of__container_15() { return &____container_15; }
	inline void set__container_15(RectTransform_t3349966182 * value)
	{
		____container_15 = value;
		Il2CppCodeGenWriteBarrier(&____container_15, value);
	}

	inline static int32_t get_offset_of__horizontal_16() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____horizontal_16)); }
	inline bool get__horizontal_16() const { return ____horizontal_16; }
	inline bool* get_address_of__horizontal_16() { return &____horizontal_16; }
	inline void set__horizontal_16(bool value)
	{
		____horizontal_16 = value;
	}

	inline static int32_t get_offset_of__pageCount_17() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____pageCount_17)); }
	inline int32_t get__pageCount_17() const { return ____pageCount_17; }
	inline int32_t* get_address_of__pageCount_17() { return &____pageCount_17; }
	inline void set__pageCount_17(int32_t value)
	{
		____pageCount_17 = value;
	}

	inline static int32_t get_offset_of__currentPage_18() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____currentPage_18)); }
	inline int32_t get__currentPage_18() const { return ____currentPage_18; }
	inline int32_t* get_address_of__currentPage_18() { return &____currentPage_18; }
	inline void set__currentPage_18(int32_t value)
	{
		____currentPage_18 = value;
	}

	inline static int32_t get_offset_of__lerp_19() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____lerp_19)); }
	inline bool get__lerp_19() const { return ____lerp_19; }
	inline bool* get_address_of__lerp_19() { return &____lerp_19; }
	inline void set__lerp_19(bool value)
	{
		____lerp_19 = value;
	}

	inline static int32_t get_offset_of__lerpTo_20() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____lerpTo_20)); }
	inline Vector2_t2243707579  get__lerpTo_20() const { return ____lerpTo_20; }
	inline Vector2_t2243707579 * get_address_of__lerpTo_20() { return &____lerpTo_20; }
	inline void set__lerpTo_20(Vector2_t2243707579  value)
	{
		____lerpTo_20 = value;
	}

	inline static int32_t get_offset_of__pagePositions_21() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____pagePositions_21)); }
	inline List_1_t1612828711 * get__pagePositions_21() const { return ____pagePositions_21; }
	inline List_1_t1612828711 ** get_address_of__pagePositions_21() { return &____pagePositions_21; }
	inline void set__pagePositions_21(List_1_t1612828711 * value)
	{
		____pagePositions_21 = value;
		Il2CppCodeGenWriteBarrier(&____pagePositions_21, value);
	}

	inline static int32_t get_offset_of__dragging_22() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____dragging_22)); }
	inline bool get__dragging_22() const { return ____dragging_22; }
	inline bool* get_address_of__dragging_22() { return &____dragging_22; }
	inline void set__dragging_22(bool value)
	{
		____dragging_22 = value;
	}

	inline static int32_t get_offset_of__timeStamp_23() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____timeStamp_23)); }
	inline float get__timeStamp_23() const { return ____timeStamp_23; }
	inline float* get_address_of__timeStamp_23() { return &____timeStamp_23; }
	inline void set__timeStamp_23(float value)
	{
		____timeStamp_23 = value;
	}

	inline static int32_t get_offset_of__startPosition_24() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____startPosition_24)); }
	inline Vector2_t2243707579  get__startPosition_24() const { return ____startPosition_24; }
	inline Vector2_t2243707579 * get_address_of__startPosition_24() { return &____startPosition_24; }
	inline void set__startPosition_24(Vector2_t2243707579  value)
	{
		____startPosition_24 = value;
	}

	inline static int32_t get_offset_of__showPageSelection_25() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____showPageSelection_25)); }
	inline bool get__showPageSelection_25() const { return ____showPageSelection_25; }
	inline bool* get_address_of__showPageSelection_25() { return &____showPageSelection_25; }
	inline void set__showPageSelection_25(bool value)
	{
		____showPageSelection_25 = value;
	}

	inline static int32_t get_offset_of__previousPageSelectionIndex_26() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____previousPageSelectionIndex_26)); }
	inline int32_t get__previousPageSelectionIndex_26() const { return ____previousPageSelectionIndex_26; }
	inline int32_t* get_address_of__previousPageSelectionIndex_26() { return &____previousPageSelectionIndex_26; }
	inline void set__previousPageSelectionIndex_26(int32_t value)
	{
		____previousPageSelectionIndex_26 = value;
	}

	inline static int32_t get_offset_of__pageSelectionImages_27() { return static_cast<int32_t>(offsetof(ScrollSnapRect_t671131207, ____pageSelectionImages_27)); }
	inline List_1_t1411648341 * get__pageSelectionImages_27() const { return ____pageSelectionImages_27; }
	inline List_1_t1411648341 ** get_address_of__pageSelectionImages_27() { return &____pageSelectionImages_27; }
	inline void set__pageSelectionImages_27(List_1_t1411648341 * value)
	{
		____pageSelectionImages_27 = value;
		Il2CppCodeGenWriteBarrier(&____pageSelectionImages_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
