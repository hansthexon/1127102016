﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteCommand/Binding
struct Binding_t1831577297;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void SQLite4Unity3d.SQLiteCommand/Binding::.ctor()
extern "C"  void Binding__ctor_m2075819758 (Binding_t1831577297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLiteCommand/Binding::get_Name()
extern "C"  String_t* Binding_get_Name_m2360598743 (Binding_t1831577297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteCommand/Binding::set_Name(System.String)
extern "C"  void Binding_set_Name_m1321725948 (Binding_t1831577297 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SQLite4Unity3d.SQLiteCommand/Binding::get_Value()
extern "C"  Il2CppObject * Binding_get_Value_m1116695899 (Binding_t1831577297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteCommand/Binding::set_Value(System.Object)
extern "C"  void Binding_set_Value_m3072985124 (Binding_t1831577297 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteCommand/Binding::get_Index()
extern "C"  int32_t Binding_get_Index_m173143705 (Binding_t1831577297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteCommand/Binding::set_Index(System.Int32)
extern "C"  void Binding_set_Index_m3567104296 (Binding_t1831577297 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
