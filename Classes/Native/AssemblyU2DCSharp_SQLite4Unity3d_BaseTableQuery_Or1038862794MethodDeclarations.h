﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.BaseTableQuery/Ordering
struct Ordering_t1038862794;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SQLite4Unity3d.BaseTableQuery/Ordering::.ctor()
extern "C"  void Ordering__ctor_m254169355 (Ordering_t1038862794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.BaseTableQuery/Ordering::get_ColumnName()
extern "C"  String_t* Ordering_get_ColumnName_m1415693972 (Ordering_t1038862794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.BaseTableQuery/Ordering::set_ColumnName(System.String)
extern "C"  void Ordering_set_ColumnName_m1939430265 (Ordering_t1038862794 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.BaseTableQuery/Ordering::get_Ascending()
extern "C"  bool Ordering_get_Ascending_m2384915282 (Ordering_t1038862794 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.BaseTableQuery/Ordering::set_Ascending(System.Boolean)
extern "C"  void Ordering_set_Ascending_m2021754123 (Ordering_t1038862794 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
