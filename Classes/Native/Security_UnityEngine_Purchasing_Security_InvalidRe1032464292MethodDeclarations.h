﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.InvalidReceiptDataException
struct InvalidReceiptDataException_t1032464292;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.InvalidReceiptDataException::.ctor()
extern "C"  void InvalidReceiptDataException__ctor_m2773429078 (InvalidReceiptDataException_t1032464292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
