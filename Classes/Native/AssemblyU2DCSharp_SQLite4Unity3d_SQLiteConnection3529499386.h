﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,SQLite4Unity3d.TableMapping>
struct Dictionary_2_t1518522778;
// System.Diagnostics.Stopwatch
struct Stopwatch_t1380178105;
// System.Random
struct Random_t1044426839;
// System.String
struct String_t;
// System.Func`2<SQLite4Unity3d.SQLiteConnection/IndexedColumn,System.Int32>
struct Func_2_t2812954965;
// System.Func`2<SQLite4Unity3d.SQLiteConnection/IndexedColumn,System.String>
struct Func_2_t2770297750;
// System.Func`2<SQLite4Unity3d.TableMapping/Column,System.String>
struct Func_2_t129906661;

#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_IntPtr2504060609.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SQLite4Unity3d.SQLiteConnection
struct  SQLiteConnection_t3529499386  : public Il2CppObject
{
public:
	// System.Boolean SQLite4Unity3d.SQLiteConnection::_open
	bool ____open_0;
	// System.TimeSpan SQLite4Unity3d.SQLiteConnection::_busyTimeout
	TimeSpan_t3430258949  ____busyTimeout_1;
	// System.Collections.Generic.Dictionary`2<System.String,SQLite4Unity3d.TableMapping> SQLite4Unity3d.SQLiteConnection::_mappings
	Dictionary_2_t1518522778 * ____mappings_2;
	// System.Collections.Generic.Dictionary`2<System.String,SQLite4Unity3d.TableMapping> SQLite4Unity3d.SQLiteConnection::_tables
	Dictionary_2_t1518522778 * ____tables_3;
	// System.Diagnostics.Stopwatch SQLite4Unity3d.SQLiteConnection::_sw
	Stopwatch_t1380178105 * ____sw_4;
	// System.Int64 SQLite4Unity3d.SQLiteConnection::_elapsedMilliseconds
	int64_t ____elapsedMilliseconds_5;
	// System.Int32 SQLite4Unity3d.SQLiteConnection::_transactionDepth
	int32_t ____transactionDepth_6;
	// System.Random SQLite4Unity3d.SQLiteConnection::_rand
	Random_t1044426839 * ____rand_7;
	// System.IntPtr SQLite4Unity3d.SQLiteConnection::<Handle>k__BackingField
	IntPtr_t ___U3CHandleU3Ek__BackingField_10;
	// System.String SQLite4Unity3d.SQLiteConnection::<DatabasePath>k__BackingField
	String_t* ___U3CDatabasePathU3Ek__BackingField_11;
	// System.Boolean SQLite4Unity3d.SQLiteConnection::<TimeExecution>k__BackingField
	bool ___U3CTimeExecutionU3Ek__BackingField_12;
	// System.Boolean SQLite4Unity3d.SQLiteConnection::<Trace>k__BackingField
	bool ___U3CTraceU3Ek__BackingField_13;
	// System.Boolean SQLite4Unity3d.SQLiteConnection::<StoreDateTimeAsTicks>k__BackingField
	bool ___U3CStoreDateTimeAsTicksU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of__open_0() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ____open_0)); }
	inline bool get__open_0() const { return ____open_0; }
	inline bool* get_address_of__open_0() { return &____open_0; }
	inline void set__open_0(bool value)
	{
		____open_0 = value;
	}

	inline static int32_t get_offset_of__busyTimeout_1() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ____busyTimeout_1)); }
	inline TimeSpan_t3430258949  get__busyTimeout_1() const { return ____busyTimeout_1; }
	inline TimeSpan_t3430258949 * get_address_of__busyTimeout_1() { return &____busyTimeout_1; }
	inline void set__busyTimeout_1(TimeSpan_t3430258949  value)
	{
		____busyTimeout_1 = value;
	}

	inline static int32_t get_offset_of__mappings_2() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ____mappings_2)); }
	inline Dictionary_2_t1518522778 * get__mappings_2() const { return ____mappings_2; }
	inline Dictionary_2_t1518522778 ** get_address_of__mappings_2() { return &____mappings_2; }
	inline void set__mappings_2(Dictionary_2_t1518522778 * value)
	{
		____mappings_2 = value;
		Il2CppCodeGenWriteBarrier(&____mappings_2, value);
	}

	inline static int32_t get_offset_of__tables_3() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ____tables_3)); }
	inline Dictionary_2_t1518522778 * get__tables_3() const { return ____tables_3; }
	inline Dictionary_2_t1518522778 ** get_address_of__tables_3() { return &____tables_3; }
	inline void set__tables_3(Dictionary_2_t1518522778 * value)
	{
		____tables_3 = value;
		Il2CppCodeGenWriteBarrier(&____tables_3, value);
	}

	inline static int32_t get_offset_of__sw_4() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ____sw_4)); }
	inline Stopwatch_t1380178105 * get__sw_4() const { return ____sw_4; }
	inline Stopwatch_t1380178105 ** get_address_of__sw_4() { return &____sw_4; }
	inline void set__sw_4(Stopwatch_t1380178105 * value)
	{
		____sw_4 = value;
		Il2CppCodeGenWriteBarrier(&____sw_4, value);
	}

	inline static int32_t get_offset_of__elapsedMilliseconds_5() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ____elapsedMilliseconds_5)); }
	inline int64_t get__elapsedMilliseconds_5() const { return ____elapsedMilliseconds_5; }
	inline int64_t* get_address_of__elapsedMilliseconds_5() { return &____elapsedMilliseconds_5; }
	inline void set__elapsedMilliseconds_5(int64_t value)
	{
		____elapsedMilliseconds_5 = value;
	}

	inline static int32_t get_offset_of__transactionDepth_6() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ____transactionDepth_6)); }
	inline int32_t get__transactionDepth_6() const { return ____transactionDepth_6; }
	inline int32_t* get_address_of__transactionDepth_6() { return &____transactionDepth_6; }
	inline void set__transactionDepth_6(int32_t value)
	{
		____transactionDepth_6 = value;
	}

	inline static int32_t get_offset_of__rand_7() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ____rand_7)); }
	inline Random_t1044426839 * get__rand_7() const { return ____rand_7; }
	inline Random_t1044426839 ** get_address_of__rand_7() { return &____rand_7; }
	inline void set__rand_7(Random_t1044426839 * value)
	{
		____rand_7 = value;
		Il2CppCodeGenWriteBarrier(&____rand_7, value);
	}

	inline static int32_t get_offset_of_U3CHandleU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ___U3CHandleU3Ek__BackingField_10)); }
	inline IntPtr_t get_U3CHandleU3Ek__BackingField_10() const { return ___U3CHandleU3Ek__BackingField_10; }
	inline IntPtr_t* get_address_of_U3CHandleU3Ek__BackingField_10() { return &___U3CHandleU3Ek__BackingField_10; }
	inline void set_U3CHandleU3Ek__BackingField_10(IntPtr_t value)
	{
		___U3CHandleU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CDatabasePathU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ___U3CDatabasePathU3Ek__BackingField_11)); }
	inline String_t* get_U3CDatabasePathU3Ek__BackingField_11() const { return ___U3CDatabasePathU3Ek__BackingField_11; }
	inline String_t** get_address_of_U3CDatabasePathU3Ek__BackingField_11() { return &___U3CDatabasePathU3Ek__BackingField_11; }
	inline void set_U3CDatabasePathU3Ek__BackingField_11(String_t* value)
	{
		___U3CDatabasePathU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDatabasePathU3Ek__BackingField_11, value);
	}

	inline static int32_t get_offset_of_U3CTimeExecutionU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ___U3CTimeExecutionU3Ek__BackingField_12)); }
	inline bool get_U3CTimeExecutionU3Ek__BackingField_12() const { return ___U3CTimeExecutionU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CTimeExecutionU3Ek__BackingField_12() { return &___U3CTimeExecutionU3Ek__BackingField_12; }
	inline void set_U3CTimeExecutionU3Ek__BackingField_12(bool value)
	{
		___U3CTimeExecutionU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CTraceU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ___U3CTraceU3Ek__BackingField_13)); }
	inline bool get_U3CTraceU3Ek__BackingField_13() const { return ___U3CTraceU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CTraceU3Ek__BackingField_13() { return &___U3CTraceU3Ek__BackingField_13; }
	inline void set_U3CTraceU3Ek__BackingField_13(bool value)
	{
		___U3CTraceU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CStoreDateTimeAsTicksU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386, ___U3CStoreDateTimeAsTicksU3Ek__BackingField_14)); }
	inline bool get_U3CStoreDateTimeAsTicksU3Ek__BackingField_14() const { return ___U3CStoreDateTimeAsTicksU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CStoreDateTimeAsTicksU3Ek__BackingField_14() { return &___U3CStoreDateTimeAsTicksU3Ek__BackingField_14; }
	inline void set_U3CStoreDateTimeAsTicksU3Ek__BackingField_14(bool value)
	{
		___U3CStoreDateTimeAsTicksU3Ek__BackingField_14 = value;
	}
};

struct SQLiteConnection_t3529499386_StaticFields
{
public:
	// System.IntPtr SQLite4Unity3d.SQLiteConnection::NullHandle
	IntPtr_t ___NullHandle_8;
	// System.Boolean SQLite4Unity3d.SQLiteConnection::_preserveDuringLinkMagic
	bool ____preserveDuringLinkMagic_9;
	// System.Func`2<SQLite4Unity3d.SQLiteConnection/IndexedColumn,System.Int32> SQLite4Unity3d.SQLiteConnection::<>f__am$cacheF
	Func_2_t2812954965 * ___U3CU3Ef__amU24cacheF_15;
	// System.Func`2<SQLite4Unity3d.SQLiteConnection/IndexedColumn,System.String> SQLite4Unity3d.SQLiteConnection::<>f__am$cache10
	Func_2_t2770297750 * ___U3CU3Ef__amU24cache10_16;
	// System.Func`2<SQLite4Unity3d.TableMapping/Column,System.String> SQLite4Unity3d.SQLiteConnection::<>f__am$cache11
	Func_2_t129906661 * ___U3CU3Ef__amU24cache11_17;

public:
	inline static int32_t get_offset_of_NullHandle_8() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386_StaticFields, ___NullHandle_8)); }
	inline IntPtr_t get_NullHandle_8() const { return ___NullHandle_8; }
	inline IntPtr_t* get_address_of_NullHandle_8() { return &___NullHandle_8; }
	inline void set_NullHandle_8(IntPtr_t value)
	{
		___NullHandle_8 = value;
	}

	inline static int32_t get_offset_of__preserveDuringLinkMagic_9() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386_StaticFields, ____preserveDuringLinkMagic_9)); }
	inline bool get__preserveDuringLinkMagic_9() const { return ____preserveDuringLinkMagic_9; }
	inline bool* get_address_of__preserveDuringLinkMagic_9() { return &____preserveDuringLinkMagic_9; }
	inline void set__preserveDuringLinkMagic_9(bool value)
	{
		____preserveDuringLinkMagic_9 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_15() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386_StaticFields, ___U3CU3Ef__amU24cacheF_15)); }
	inline Func_2_t2812954965 * get_U3CU3Ef__amU24cacheF_15() const { return ___U3CU3Ef__amU24cacheF_15; }
	inline Func_2_t2812954965 ** get_address_of_U3CU3Ef__amU24cacheF_15() { return &___U3CU3Ef__amU24cacheF_15; }
	inline void set_U3CU3Ef__amU24cacheF_15(Func_2_t2812954965 * value)
	{
		___U3CU3Ef__amU24cacheF_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_15, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache10_16() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386_StaticFields, ___U3CU3Ef__amU24cache10_16)); }
	inline Func_2_t2770297750 * get_U3CU3Ef__amU24cache10_16() const { return ___U3CU3Ef__amU24cache10_16; }
	inline Func_2_t2770297750 ** get_address_of_U3CU3Ef__amU24cache10_16() { return &___U3CU3Ef__amU24cache10_16; }
	inline void set_U3CU3Ef__amU24cache10_16(Func_2_t2770297750 * value)
	{
		___U3CU3Ef__amU24cache10_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache10_16, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache11_17() { return static_cast<int32_t>(offsetof(SQLiteConnection_t3529499386_StaticFields, ___U3CU3Ef__amU24cache11_17)); }
	inline Func_2_t129906661 * get_U3CU3Ef__amU24cache11_17() const { return ___U3CU3Ef__amU24cache11_17; }
	inline Func_2_t129906661 ** get_address_of_U3CU3Ef__amU24cache11_17() { return &___U3CU3Ef__amU24cache11_17; }
	inline void set_U3CU3Ef__amU24cache11_17(Func_2_t129906661 * value)
	{
		___U3CU3Ef__amU24cache11_17 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache11_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
