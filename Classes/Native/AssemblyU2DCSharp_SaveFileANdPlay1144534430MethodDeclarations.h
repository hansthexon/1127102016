﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SaveFileANdPlay
struct SaveFileANdPlay_t1144534430;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void SaveFileANdPlay::.ctor()
extern "C"  void SaveFileANdPlay__ctor_m210315947 (SaveFileANdPlay_t1144534430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveFileANdPlay::Start()
extern "C"  void SaveFileANdPlay_Start_m2641608143 (SaveFileANdPlay_t1144534430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveFileANdPlay::Update()
extern "C"  void SaveFileANdPlay_Update_m3483032392 (SaveFileANdPlay_t1144534430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SaveFileANdPlay::nextscene()
extern "C"  void SaveFileANdPlay_nextscene_m1544047450 (SaveFileANdPlay_t1144534430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator SaveFileANdPlay::Download()
extern "C"  Il2CppObject * SaveFileANdPlay_Download_m2016550209 (SaveFileANdPlay_t1144534430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
