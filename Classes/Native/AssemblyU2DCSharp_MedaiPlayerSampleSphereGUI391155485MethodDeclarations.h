﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MedaiPlayerSampleSphereGUI
struct MedaiPlayerSampleSphereGUI_t391155485;

#include "codegen/il2cpp-codegen.h"

// System.Void MedaiPlayerSampleSphereGUI::.ctor()
extern "C"  void MedaiPlayerSampleSphereGUI__ctor_m1849212196 (MedaiPlayerSampleSphereGUI_t391155485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MedaiPlayerSampleSphereGUI::Start()
extern "C"  void MedaiPlayerSampleSphereGUI_Start_m2680068572 (MedaiPlayerSampleSphereGUI_t391155485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MedaiPlayerSampleSphereGUI::Update()
extern "C"  void MedaiPlayerSampleSphereGUI_Update_m3036919045 (MedaiPlayerSampleSphereGUI_t391155485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MedaiPlayerSampleSphereGUI::OnGUI()
extern "C"  void MedaiPlayerSampleSphereGUI_OnGUI_m24873648 (MedaiPlayerSampleSphereGUI_t391155485 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
