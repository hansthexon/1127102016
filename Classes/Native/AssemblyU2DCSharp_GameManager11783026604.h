﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Animator
struct Animator_t69676727;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t590162004;
// RoomObject
struct RoomObject_t2982870316;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// HandController
struct HandController_t317030225;
// Vuforia.VuforiaBehaviour
struct VuforiaBehaviour_t359035403;
// Code
struct Code_t686167511;
// MediaPlayerCtrl
struct MediaPlayerCtrl_t1284484152;
// DataService
struct DataService_t2602786891;
// UnityEngine.WWW
struct WWW_t2919945039;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager1
struct  GameManager1_t1783026604  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean GameManager1::buyfreebool
	bool ___buyfreebool_2;
	// UnityEngine.UI.Toggle GameManager1::freecode_toggle
	Toggle_t3976754468 * ___freecode_toggle_3;
	// UnityEngine.UI.Toggle GameManager1::buythree_toggle
	Toggle_t3976754468 * ___buythree_toggle_4;
	// UnityEngine.GameObject GameManager1::notfound
	GameObject_t1756533147 * ___notfound_5;
	// UnityEngine.GameObject GameManager1::videoPrefab
	GameObject_t1756533147 * ___videoPrefab_6;
	// UnityEngine.GameObject[] GameManager1::DisableMenu
	GameObjectU5BU5D_t3057952154* ___DisableMenu_7;
	// UnityEngine.Animator GameManager1::Dog
	Animator_t69676727 * ___Dog_8;
	// UnityEngine.UI.Image GameManager1::unlockedImage
	Image_t2042527209 * ___unlockedImage_9;
	// UnityEngine.UI.Image[] GameManager1::roomobjects
	ImageU5BU5D_t590162004* ___roomobjects_10;
	// UnityEngine.GameObject GameManager1::CongratsPanel
	GameObject_t1756533147 * ___CongratsPanel_11;
	// UnityEngine.UI.Image GameManager1::starbar
	Image_t2042527209 * ___starbar_12;
	// RoomObject GameManager1::robjects
	RoomObject_t2982870316 * ___robjects_13;
	// UnityEngine.GameObject GameManager1::ArPanel
	GameObject_t1756533147 * ___ArPanel_14;
	// UnityEngine.GameObject GameManager1::StarBracket
	GameObject_t1756533147 * ___StarBracket_15;
	// UnityEngine.GameObject GameManager1::PlanetBracket
	GameObject_t1756533147 * ___PlanetBracket_16;
	// UnityEngine.GameObject GameManager1::ReplayPanel
	GameObject_t1756533147 * ___ReplayPanel_17;
	// System.Int32 GameManager1::currentscore
	int32_t ___currentscore_18;
	// UnityEngine.GameObject GameManager1::PlanetEarnoints
	GameObject_t1756533147 * ___PlanetEarnoints_19;
	// UnityEngine.GameObject GameManager1::StarEarnPoints
	GameObject_t1756533147 * ___StarEarnPoints_20;
	// UnityEngine.GameObject GameManager1::PlanetInScorePanel
	GameObject_t1756533147 * ___PlanetInScorePanel_21;
	// UnityEngine.GameObject GameManager1::CoininScorePanel
	GameObject_t1756533147 * ___CoininScorePanel_22;
	// UnityEngine.GameObject GameManager1::FreeCodeScreen
	GameObject_t1756533147 * ___FreeCodeScreen_23;
	// UnityEngine.GameObject GameManager1::ThreeCodeScreen
	GameObject_t1756533147 * ___ThreeCodeScreen_24;
	// UnityEngine.GameObject GameManager1::ParentCorner
	GameObject_t1756533147 * ___ParentCorner_25;
	// UnityEngine.GameObject GameManager1::SlideE
	GameObject_t1756533147 * ___SlideE_26;
	// UnityEngine.GameObject GameManager1::SlideC
	GameObject_t1756533147 * ___SlideC_27;
	// UnityEngine.UI.Text GameManager1::CurrentScoretext
	Text_t356221433 * ___CurrentScoretext_28;
	// UnityEngine.UI.Text GameManager1::StarScoreText
	Text_t356221433 * ___StarScoreText_29;
	// UnityEngine.UI.Text GameManager1::PlanerScoreText
	Text_t356221433 * ___PlanerScoreText_30;
	// System.String GameManager1::StarScore
	String_t* ___StarScore_31;
	// System.String GameManager1::PlanetScore
	String_t* ___PlanetScore_32;
	// System.Boolean GameManager1::cantrack
	bool ___cantrack_33;
	// HandController GameManager1::hand
	HandController_t317030225 * ___hand_34;
	// System.Boolean GameManager1::politeplanet
	bool ___politeplanet_35;
	// Vuforia.VuforiaBehaviour GameManager1::ArCam
	VuforiaBehaviour_t359035403 * ___ArCam_36;
	// UnityEngine.GameObject GameManager1::videoManager
	GameObject_t1756533147 * ___videoManager_37;
	// Code GameManager1::codeobject
	Code_t686167511 * ___codeobject_38;
	// MediaPlayerCtrl GameManager1::player
	MediaPlayerCtrl_t1284484152 * ___player_39;
	// System.String GameManager1::tempfilename
	String_t* ___tempfilename_40;
	// UnityEngine.GameObject GameManager1::mainpanel
	GameObject_t1756533147 * ___mainpanel_41;
	// UnityEngine.GameObject GameManager1::Imagsets
	GameObject_t1756533147 * ___Imagsets_42;
	// System.String GameManager1::filepath
	String_t* ___filepath_43;
	// DataService GameManager1::ds
	DataService_t2602786891 * ___ds_44;
	// UnityEngine.GameObject GameManager1::Scorepanel
	GameObject_t1756533147 * ___Scorepanel_45;
	// UnityEngine.GameObject GameManager1::politeLoadingScreen
	GameObject_t1756533147 * ___politeLoadingScreen_46;
	// UnityEngine.GameObject GameManager1::PolitePlanet
	GameObject_t1756533147 * ___PolitePlanet_47;
	// System.Int32 GameManager1::i
	int32_t ___i_48;
	// UnityEngine.WWW GameManager1::www
	WWW_t2919945039 * ___www_49;

public:
	inline static int32_t get_offset_of_buyfreebool_2() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___buyfreebool_2)); }
	inline bool get_buyfreebool_2() const { return ___buyfreebool_2; }
	inline bool* get_address_of_buyfreebool_2() { return &___buyfreebool_2; }
	inline void set_buyfreebool_2(bool value)
	{
		___buyfreebool_2 = value;
	}

	inline static int32_t get_offset_of_freecode_toggle_3() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___freecode_toggle_3)); }
	inline Toggle_t3976754468 * get_freecode_toggle_3() const { return ___freecode_toggle_3; }
	inline Toggle_t3976754468 ** get_address_of_freecode_toggle_3() { return &___freecode_toggle_3; }
	inline void set_freecode_toggle_3(Toggle_t3976754468 * value)
	{
		___freecode_toggle_3 = value;
		Il2CppCodeGenWriteBarrier(&___freecode_toggle_3, value);
	}

	inline static int32_t get_offset_of_buythree_toggle_4() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___buythree_toggle_4)); }
	inline Toggle_t3976754468 * get_buythree_toggle_4() const { return ___buythree_toggle_4; }
	inline Toggle_t3976754468 ** get_address_of_buythree_toggle_4() { return &___buythree_toggle_4; }
	inline void set_buythree_toggle_4(Toggle_t3976754468 * value)
	{
		___buythree_toggle_4 = value;
		Il2CppCodeGenWriteBarrier(&___buythree_toggle_4, value);
	}

	inline static int32_t get_offset_of_notfound_5() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___notfound_5)); }
	inline GameObject_t1756533147 * get_notfound_5() const { return ___notfound_5; }
	inline GameObject_t1756533147 ** get_address_of_notfound_5() { return &___notfound_5; }
	inline void set_notfound_5(GameObject_t1756533147 * value)
	{
		___notfound_5 = value;
		Il2CppCodeGenWriteBarrier(&___notfound_5, value);
	}

	inline static int32_t get_offset_of_videoPrefab_6() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___videoPrefab_6)); }
	inline GameObject_t1756533147 * get_videoPrefab_6() const { return ___videoPrefab_6; }
	inline GameObject_t1756533147 ** get_address_of_videoPrefab_6() { return &___videoPrefab_6; }
	inline void set_videoPrefab_6(GameObject_t1756533147 * value)
	{
		___videoPrefab_6 = value;
		Il2CppCodeGenWriteBarrier(&___videoPrefab_6, value);
	}

	inline static int32_t get_offset_of_DisableMenu_7() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___DisableMenu_7)); }
	inline GameObjectU5BU5D_t3057952154* get_DisableMenu_7() const { return ___DisableMenu_7; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_DisableMenu_7() { return &___DisableMenu_7; }
	inline void set_DisableMenu_7(GameObjectU5BU5D_t3057952154* value)
	{
		___DisableMenu_7 = value;
		Il2CppCodeGenWriteBarrier(&___DisableMenu_7, value);
	}

	inline static int32_t get_offset_of_Dog_8() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___Dog_8)); }
	inline Animator_t69676727 * get_Dog_8() const { return ___Dog_8; }
	inline Animator_t69676727 ** get_address_of_Dog_8() { return &___Dog_8; }
	inline void set_Dog_8(Animator_t69676727 * value)
	{
		___Dog_8 = value;
		Il2CppCodeGenWriteBarrier(&___Dog_8, value);
	}

	inline static int32_t get_offset_of_unlockedImage_9() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___unlockedImage_9)); }
	inline Image_t2042527209 * get_unlockedImage_9() const { return ___unlockedImage_9; }
	inline Image_t2042527209 ** get_address_of_unlockedImage_9() { return &___unlockedImage_9; }
	inline void set_unlockedImage_9(Image_t2042527209 * value)
	{
		___unlockedImage_9 = value;
		Il2CppCodeGenWriteBarrier(&___unlockedImage_9, value);
	}

	inline static int32_t get_offset_of_roomobjects_10() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___roomobjects_10)); }
	inline ImageU5BU5D_t590162004* get_roomobjects_10() const { return ___roomobjects_10; }
	inline ImageU5BU5D_t590162004** get_address_of_roomobjects_10() { return &___roomobjects_10; }
	inline void set_roomobjects_10(ImageU5BU5D_t590162004* value)
	{
		___roomobjects_10 = value;
		Il2CppCodeGenWriteBarrier(&___roomobjects_10, value);
	}

	inline static int32_t get_offset_of_CongratsPanel_11() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___CongratsPanel_11)); }
	inline GameObject_t1756533147 * get_CongratsPanel_11() const { return ___CongratsPanel_11; }
	inline GameObject_t1756533147 ** get_address_of_CongratsPanel_11() { return &___CongratsPanel_11; }
	inline void set_CongratsPanel_11(GameObject_t1756533147 * value)
	{
		___CongratsPanel_11 = value;
		Il2CppCodeGenWriteBarrier(&___CongratsPanel_11, value);
	}

	inline static int32_t get_offset_of_starbar_12() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___starbar_12)); }
	inline Image_t2042527209 * get_starbar_12() const { return ___starbar_12; }
	inline Image_t2042527209 ** get_address_of_starbar_12() { return &___starbar_12; }
	inline void set_starbar_12(Image_t2042527209 * value)
	{
		___starbar_12 = value;
		Il2CppCodeGenWriteBarrier(&___starbar_12, value);
	}

	inline static int32_t get_offset_of_robjects_13() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___robjects_13)); }
	inline RoomObject_t2982870316 * get_robjects_13() const { return ___robjects_13; }
	inline RoomObject_t2982870316 ** get_address_of_robjects_13() { return &___robjects_13; }
	inline void set_robjects_13(RoomObject_t2982870316 * value)
	{
		___robjects_13 = value;
		Il2CppCodeGenWriteBarrier(&___robjects_13, value);
	}

	inline static int32_t get_offset_of_ArPanel_14() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___ArPanel_14)); }
	inline GameObject_t1756533147 * get_ArPanel_14() const { return ___ArPanel_14; }
	inline GameObject_t1756533147 ** get_address_of_ArPanel_14() { return &___ArPanel_14; }
	inline void set_ArPanel_14(GameObject_t1756533147 * value)
	{
		___ArPanel_14 = value;
		Il2CppCodeGenWriteBarrier(&___ArPanel_14, value);
	}

	inline static int32_t get_offset_of_StarBracket_15() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___StarBracket_15)); }
	inline GameObject_t1756533147 * get_StarBracket_15() const { return ___StarBracket_15; }
	inline GameObject_t1756533147 ** get_address_of_StarBracket_15() { return &___StarBracket_15; }
	inline void set_StarBracket_15(GameObject_t1756533147 * value)
	{
		___StarBracket_15 = value;
		Il2CppCodeGenWriteBarrier(&___StarBracket_15, value);
	}

	inline static int32_t get_offset_of_PlanetBracket_16() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___PlanetBracket_16)); }
	inline GameObject_t1756533147 * get_PlanetBracket_16() const { return ___PlanetBracket_16; }
	inline GameObject_t1756533147 ** get_address_of_PlanetBracket_16() { return &___PlanetBracket_16; }
	inline void set_PlanetBracket_16(GameObject_t1756533147 * value)
	{
		___PlanetBracket_16 = value;
		Il2CppCodeGenWriteBarrier(&___PlanetBracket_16, value);
	}

	inline static int32_t get_offset_of_ReplayPanel_17() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___ReplayPanel_17)); }
	inline GameObject_t1756533147 * get_ReplayPanel_17() const { return ___ReplayPanel_17; }
	inline GameObject_t1756533147 ** get_address_of_ReplayPanel_17() { return &___ReplayPanel_17; }
	inline void set_ReplayPanel_17(GameObject_t1756533147 * value)
	{
		___ReplayPanel_17 = value;
		Il2CppCodeGenWriteBarrier(&___ReplayPanel_17, value);
	}

	inline static int32_t get_offset_of_currentscore_18() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___currentscore_18)); }
	inline int32_t get_currentscore_18() const { return ___currentscore_18; }
	inline int32_t* get_address_of_currentscore_18() { return &___currentscore_18; }
	inline void set_currentscore_18(int32_t value)
	{
		___currentscore_18 = value;
	}

	inline static int32_t get_offset_of_PlanetEarnoints_19() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___PlanetEarnoints_19)); }
	inline GameObject_t1756533147 * get_PlanetEarnoints_19() const { return ___PlanetEarnoints_19; }
	inline GameObject_t1756533147 ** get_address_of_PlanetEarnoints_19() { return &___PlanetEarnoints_19; }
	inline void set_PlanetEarnoints_19(GameObject_t1756533147 * value)
	{
		___PlanetEarnoints_19 = value;
		Il2CppCodeGenWriteBarrier(&___PlanetEarnoints_19, value);
	}

	inline static int32_t get_offset_of_StarEarnPoints_20() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___StarEarnPoints_20)); }
	inline GameObject_t1756533147 * get_StarEarnPoints_20() const { return ___StarEarnPoints_20; }
	inline GameObject_t1756533147 ** get_address_of_StarEarnPoints_20() { return &___StarEarnPoints_20; }
	inline void set_StarEarnPoints_20(GameObject_t1756533147 * value)
	{
		___StarEarnPoints_20 = value;
		Il2CppCodeGenWriteBarrier(&___StarEarnPoints_20, value);
	}

	inline static int32_t get_offset_of_PlanetInScorePanel_21() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___PlanetInScorePanel_21)); }
	inline GameObject_t1756533147 * get_PlanetInScorePanel_21() const { return ___PlanetInScorePanel_21; }
	inline GameObject_t1756533147 ** get_address_of_PlanetInScorePanel_21() { return &___PlanetInScorePanel_21; }
	inline void set_PlanetInScorePanel_21(GameObject_t1756533147 * value)
	{
		___PlanetInScorePanel_21 = value;
		Il2CppCodeGenWriteBarrier(&___PlanetInScorePanel_21, value);
	}

	inline static int32_t get_offset_of_CoininScorePanel_22() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___CoininScorePanel_22)); }
	inline GameObject_t1756533147 * get_CoininScorePanel_22() const { return ___CoininScorePanel_22; }
	inline GameObject_t1756533147 ** get_address_of_CoininScorePanel_22() { return &___CoininScorePanel_22; }
	inline void set_CoininScorePanel_22(GameObject_t1756533147 * value)
	{
		___CoininScorePanel_22 = value;
		Il2CppCodeGenWriteBarrier(&___CoininScorePanel_22, value);
	}

	inline static int32_t get_offset_of_FreeCodeScreen_23() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___FreeCodeScreen_23)); }
	inline GameObject_t1756533147 * get_FreeCodeScreen_23() const { return ___FreeCodeScreen_23; }
	inline GameObject_t1756533147 ** get_address_of_FreeCodeScreen_23() { return &___FreeCodeScreen_23; }
	inline void set_FreeCodeScreen_23(GameObject_t1756533147 * value)
	{
		___FreeCodeScreen_23 = value;
		Il2CppCodeGenWriteBarrier(&___FreeCodeScreen_23, value);
	}

	inline static int32_t get_offset_of_ThreeCodeScreen_24() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___ThreeCodeScreen_24)); }
	inline GameObject_t1756533147 * get_ThreeCodeScreen_24() const { return ___ThreeCodeScreen_24; }
	inline GameObject_t1756533147 ** get_address_of_ThreeCodeScreen_24() { return &___ThreeCodeScreen_24; }
	inline void set_ThreeCodeScreen_24(GameObject_t1756533147 * value)
	{
		___ThreeCodeScreen_24 = value;
		Il2CppCodeGenWriteBarrier(&___ThreeCodeScreen_24, value);
	}

	inline static int32_t get_offset_of_ParentCorner_25() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___ParentCorner_25)); }
	inline GameObject_t1756533147 * get_ParentCorner_25() const { return ___ParentCorner_25; }
	inline GameObject_t1756533147 ** get_address_of_ParentCorner_25() { return &___ParentCorner_25; }
	inline void set_ParentCorner_25(GameObject_t1756533147 * value)
	{
		___ParentCorner_25 = value;
		Il2CppCodeGenWriteBarrier(&___ParentCorner_25, value);
	}

	inline static int32_t get_offset_of_SlideE_26() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___SlideE_26)); }
	inline GameObject_t1756533147 * get_SlideE_26() const { return ___SlideE_26; }
	inline GameObject_t1756533147 ** get_address_of_SlideE_26() { return &___SlideE_26; }
	inline void set_SlideE_26(GameObject_t1756533147 * value)
	{
		___SlideE_26 = value;
		Il2CppCodeGenWriteBarrier(&___SlideE_26, value);
	}

	inline static int32_t get_offset_of_SlideC_27() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___SlideC_27)); }
	inline GameObject_t1756533147 * get_SlideC_27() const { return ___SlideC_27; }
	inline GameObject_t1756533147 ** get_address_of_SlideC_27() { return &___SlideC_27; }
	inline void set_SlideC_27(GameObject_t1756533147 * value)
	{
		___SlideC_27 = value;
		Il2CppCodeGenWriteBarrier(&___SlideC_27, value);
	}

	inline static int32_t get_offset_of_CurrentScoretext_28() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___CurrentScoretext_28)); }
	inline Text_t356221433 * get_CurrentScoretext_28() const { return ___CurrentScoretext_28; }
	inline Text_t356221433 ** get_address_of_CurrentScoretext_28() { return &___CurrentScoretext_28; }
	inline void set_CurrentScoretext_28(Text_t356221433 * value)
	{
		___CurrentScoretext_28 = value;
		Il2CppCodeGenWriteBarrier(&___CurrentScoretext_28, value);
	}

	inline static int32_t get_offset_of_StarScoreText_29() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___StarScoreText_29)); }
	inline Text_t356221433 * get_StarScoreText_29() const { return ___StarScoreText_29; }
	inline Text_t356221433 ** get_address_of_StarScoreText_29() { return &___StarScoreText_29; }
	inline void set_StarScoreText_29(Text_t356221433 * value)
	{
		___StarScoreText_29 = value;
		Il2CppCodeGenWriteBarrier(&___StarScoreText_29, value);
	}

	inline static int32_t get_offset_of_PlanerScoreText_30() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___PlanerScoreText_30)); }
	inline Text_t356221433 * get_PlanerScoreText_30() const { return ___PlanerScoreText_30; }
	inline Text_t356221433 ** get_address_of_PlanerScoreText_30() { return &___PlanerScoreText_30; }
	inline void set_PlanerScoreText_30(Text_t356221433 * value)
	{
		___PlanerScoreText_30 = value;
		Il2CppCodeGenWriteBarrier(&___PlanerScoreText_30, value);
	}

	inline static int32_t get_offset_of_StarScore_31() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___StarScore_31)); }
	inline String_t* get_StarScore_31() const { return ___StarScore_31; }
	inline String_t** get_address_of_StarScore_31() { return &___StarScore_31; }
	inline void set_StarScore_31(String_t* value)
	{
		___StarScore_31 = value;
		Il2CppCodeGenWriteBarrier(&___StarScore_31, value);
	}

	inline static int32_t get_offset_of_PlanetScore_32() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___PlanetScore_32)); }
	inline String_t* get_PlanetScore_32() const { return ___PlanetScore_32; }
	inline String_t** get_address_of_PlanetScore_32() { return &___PlanetScore_32; }
	inline void set_PlanetScore_32(String_t* value)
	{
		___PlanetScore_32 = value;
		Il2CppCodeGenWriteBarrier(&___PlanetScore_32, value);
	}

	inline static int32_t get_offset_of_cantrack_33() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___cantrack_33)); }
	inline bool get_cantrack_33() const { return ___cantrack_33; }
	inline bool* get_address_of_cantrack_33() { return &___cantrack_33; }
	inline void set_cantrack_33(bool value)
	{
		___cantrack_33 = value;
	}

	inline static int32_t get_offset_of_hand_34() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___hand_34)); }
	inline HandController_t317030225 * get_hand_34() const { return ___hand_34; }
	inline HandController_t317030225 ** get_address_of_hand_34() { return &___hand_34; }
	inline void set_hand_34(HandController_t317030225 * value)
	{
		___hand_34 = value;
		Il2CppCodeGenWriteBarrier(&___hand_34, value);
	}

	inline static int32_t get_offset_of_politeplanet_35() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___politeplanet_35)); }
	inline bool get_politeplanet_35() const { return ___politeplanet_35; }
	inline bool* get_address_of_politeplanet_35() { return &___politeplanet_35; }
	inline void set_politeplanet_35(bool value)
	{
		___politeplanet_35 = value;
	}

	inline static int32_t get_offset_of_ArCam_36() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___ArCam_36)); }
	inline VuforiaBehaviour_t359035403 * get_ArCam_36() const { return ___ArCam_36; }
	inline VuforiaBehaviour_t359035403 ** get_address_of_ArCam_36() { return &___ArCam_36; }
	inline void set_ArCam_36(VuforiaBehaviour_t359035403 * value)
	{
		___ArCam_36 = value;
		Il2CppCodeGenWriteBarrier(&___ArCam_36, value);
	}

	inline static int32_t get_offset_of_videoManager_37() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___videoManager_37)); }
	inline GameObject_t1756533147 * get_videoManager_37() const { return ___videoManager_37; }
	inline GameObject_t1756533147 ** get_address_of_videoManager_37() { return &___videoManager_37; }
	inline void set_videoManager_37(GameObject_t1756533147 * value)
	{
		___videoManager_37 = value;
		Il2CppCodeGenWriteBarrier(&___videoManager_37, value);
	}

	inline static int32_t get_offset_of_codeobject_38() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___codeobject_38)); }
	inline Code_t686167511 * get_codeobject_38() const { return ___codeobject_38; }
	inline Code_t686167511 ** get_address_of_codeobject_38() { return &___codeobject_38; }
	inline void set_codeobject_38(Code_t686167511 * value)
	{
		___codeobject_38 = value;
		Il2CppCodeGenWriteBarrier(&___codeobject_38, value);
	}

	inline static int32_t get_offset_of_player_39() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___player_39)); }
	inline MediaPlayerCtrl_t1284484152 * get_player_39() const { return ___player_39; }
	inline MediaPlayerCtrl_t1284484152 ** get_address_of_player_39() { return &___player_39; }
	inline void set_player_39(MediaPlayerCtrl_t1284484152 * value)
	{
		___player_39 = value;
		Il2CppCodeGenWriteBarrier(&___player_39, value);
	}

	inline static int32_t get_offset_of_tempfilename_40() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___tempfilename_40)); }
	inline String_t* get_tempfilename_40() const { return ___tempfilename_40; }
	inline String_t** get_address_of_tempfilename_40() { return &___tempfilename_40; }
	inline void set_tempfilename_40(String_t* value)
	{
		___tempfilename_40 = value;
		Il2CppCodeGenWriteBarrier(&___tempfilename_40, value);
	}

	inline static int32_t get_offset_of_mainpanel_41() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___mainpanel_41)); }
	inline GameObject_t1756533147 * get_mainpanel_41() const { return ___mainpanel_41; }
	inline GameObject_t1756533147 ** get_address_of_mainpanel_41() { return &___mainpanel_41; }
	inline void set_mainpanel_41(GameObject_t1756533147 * value)
	{
		___mainpanel_41 = value;
		Il2CppCodeGenWriteBarrier(&___mainpanel_41, value);
	}

	inline static int32_t get_offset_of_Imagsets_42() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___Imagsets_42)); }
	inline GameObject_t1756533147 * get_Imagsets_42() const { return ___Imagsets_42; }
	inline GameObject_t1756533147 ** get_address_of_Imagsets_42() { return &___Imagsets_42; }
	inline void set_Imagsets_42(GameObject_t1756533147 * value)
	{
		___Imagsets_42 = value;
		Il2CppCodeGenWriteBarrier(&___Imagsets_42, value);
	}

	inline static int32_t get_offset_of_filepath_43() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___filepath_43)); }
	inline String_t* get_filepath_43() const { return ___filepath_43; }
	inline String_t** get_address_of_filepath_43() { return &___filepath_43; }
	inline void set_filepath_43(String_t* value)
	{
		___filepath_43 = value;
		Il2CppCodeGenWriteBarrier(&___filepath_43, value);
	}

	inline static int32_t get_offset_of_ds_44() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___ds_44)); }
	inline DataService_t2602786891 * get_ds_44() const { return ___ds_44; }
	inline DataService_t2602786891 ** get_address_of_ds_44() { return &___ds_44; }
	inline void set_ds_44(DataService_t2602786891 * value)
	{
		___ds_44 = value;
		Il2CppCodeGenWriteBarrier(&___ds_44, value);
	}

	inline static int32_t get_offset_of_Scorepanel_45() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___Scorepanel_45)); }
	inline GameObject_t1756533147 * get_Scorepanel_45() const { return ___Scorepanel_45; }
	inline GameObject_t1756533147 ** get_address_of_Scorepanel_45() { return &___Scorepanel_45; }
	inline void set_Scorepanel_45(GameObject_t1756533147 * value)
	{
		___Scorepanel_45 = value;
		Il2CppCodeGenWriteBarrier(&___Scorepanel_45, value);
	}

	inline static int32_t get_offset_of_politeLoadingScreen_46() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___politeLoadingScreen_46)); }
	inline GameObject_t1756533147 * get_politeLoadingScreen_46() const { return ___politeLoadingScreen_46; }
	inline GameObject_t1756533147 ** get_address_of_politeLoadingScreen_46() { return &___politeLoadingScreen_46; }
	inline void set_politeLoadingScreen_46(GameObject_t1756533147 * value)
	{
		___politeLoadingScreen_46 = value;
		Il2CppCodeGenWriteBarrier(&___politeLoadingScreen_46, value);
	}

	inline static int32_t get_offset_of_PolitePlanet_47() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___PolitePlanet_47)); }
	inline GameObject_t1756533147 * get_PolitePlanet_47() const { return ___PolitePlanet_47; }
	inline GameObject_t1756533147 ** get_address_of_PolitePlanet_47() { return &___PolitePlanet_47; }
	inline void set_PolitePlanet_47(GameObject_t1756533147 * value)
	{
		___PolitePlanet_47 = value;
		Il2CppCodeGenWriteBarrier(&___PolitePlanet_47, value);
	}

	inline static int32_t get_offset_of_i_48() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___i_48)); }
	inline int32_t get_i_48() const { return ___i_48; }
	inline int32_t* get_address_of_i_48() { return &___i_48; }
	inline void set_i_48(int32_t value)
	{
		___i_48 = value;
	}

	inline static int32_t get_offset_of_www_49() { return static_cast<int32_t>(offsetof(GameManager1_t1783026604, ___www_49)); }
	inline WWW_t2919945039 * get_www_49() const { return ___www_49; }
	inline WWW_t2919945039 ** get_address_of_www_49() { return &___www_49; }
	inline void set_www_49(WWW_t2919945039 * value)
	{
		___www_49 = value;
		Il2CppCodeGenWriteBarrier(&___www_49, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
