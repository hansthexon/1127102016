﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.ElementInit
struct ElementInit_t3898206436;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.Expression>
struct ReadOnlyCollection_1_t300650360;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Reflection.MethodInfo System.Linq.Expressions.ElementInit::get_AddMethod()
extern "C"  MethodInfo_t * ElementInit_get_AddMethod_m1104778660 (ElementInit_t3898206436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.Expression> System.Linq.Expressions.ElementInit::get_Arguments()
extern "C"  ReadOnlyCollection_1_t300650360 * ElementInit_get_Arguments_m3884615096 (ElementInit_t3898206436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Linq.Expressions.ElementInit::ToString()
extern "C"  String_t* ElementInit_ToString_m130256712 (ElementInit_t3898206436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
