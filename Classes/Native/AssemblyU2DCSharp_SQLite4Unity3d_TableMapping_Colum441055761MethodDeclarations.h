﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.TableMapping/Column
struct Column_t441055761;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.IndexedAttribute>
struct IEnumerable_1_t2977028236;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_PropertyInfo2253729065.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_CreateFlags3110178347.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Type1303803226.h"
#include "mscorlib_System_Nullable_1_gen334943763.h"
#include "mscorlib_System_Object2689449295.h"

// System.Void SQLite4Unity3d.TableMapping/Column::.ctor(System.Reflection.PropertyInfo,SQLite4Unity3d.CreateFlags)
extern "C"  void Column__ctor_m3234446420 (Column_t441055761 * __this, PropertyInfo_t * ___prop0, int32_t ___createFlags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.TableMapping/Column::get_Name()
extern "C"  String_t* Column_get_Name_m2031943999 (Column_t441055761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping/Column::set_Name(System.String)
extern "C"  void Column_set_Name_m1788151014 (Column_t441055761 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.TableMapping/Column::get_PropertyName()
extern "C"  String_t* Column_get_PropertyName_m2535629796 (Column_t441055761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type SQLite4Unity3d.TableMapping/Column::get_ColumnType()
extern "C"  Type_t * Column_get_ColumnType_m951700143 (Column_t441055761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping/Column::set_ColumnType(System.Type)
extern "C"  void Column_set_ColumnType_m1159958094 (Column_t441055761 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.TableMapping/Column::get_Collation()
extern "C"  String_t* Column_get_Collation_m1440679973 (Column_t441055761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping/Column::set_Collation(System.String)
extern "C"  void Column_set_Collation_m2207961974 (Column_t441055761 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.TableMapping/Column::get_IsAutoInc()
extern "C"  bool Column_get_IsAutoInc_m2230517524 (Column_t441055761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping/Column::set_IsAutoInc(System.Boolean)
extern "C"  void Column_set_IsAutoInc_m273618291 (Column_t441055761 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.TableMapping/Column::get_IsAutoGuid()
extern "C"  bool Column_get_IsAutoGuid_m4030391059 (Column_t441055761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping/Column::set_IsAutoGuid(System.Boolean)
extern "C"  void Column_set_IsAutoGuid_m99821716 (Column_t441055761 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.TableMapping/Column::get_IsPK()
extern "C"  bool Column_get_IsPK_m1773804144 (Column_t441055761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping/Column::set_IsPK(System.Boolean)
extern "C"  void Column_set_IsPK_m1346503455 (Column_t441055761 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.IndexedAttribute> SQLite4Unity3d.TableMapping/Column::get_Indices()
extern "C"  Il2CppObject* Column_get_Indices_m3589191385 (Column_t441055761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping/Column::set_Indices(System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.IndexedAttribute>)
extern "C"  void Column_set_Indices_m1724212480 (Column_t441055761 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.TableMapping/Column::get_IsNullable()
extern "C"  bool Column_get_IsNullable_m1177644932 (Column_t441055761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping/Column::set_IsNullable(System.Boolean)
extern "C"  void Column_set_IsNullable_m2739573865 (Column_t441055761 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> SQLite4Unity3d.TableMapping/Column::get_MaxStringLength()
extern "C"  Nullable_1_t334943763  Column_get_MaxStringLength_m1786513587 (Column_t441055761 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping/Column::set_MaxStringLength(System.Nullable`1<System.Int32>)
extern "C"  void Column_set_MaxStringLength_m4182727794 (Column_t441055761 * __this, Nullable_1_t334943763  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.TableMapping/Column::SetValue(System.Object,System.Object)
extern "C"  void Column_SetValue_m2549925353 (Column_t441055761 * __this, Il2CppObject * ___obj0, Il2CppObject * ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SQLite4Unity3d.TableMapping/Column::GetValue(System.Object)
extern "C"  Il2CppObject * Column_GetValue_m2237517460 (Column_t441055761 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
