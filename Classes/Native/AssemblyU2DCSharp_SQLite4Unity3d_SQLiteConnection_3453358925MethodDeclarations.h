﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey16
struct U3CInsertAllU3Ec__AnonStorey16_t3453358925;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey16::.ctor()
extern "C"  void U3CInsertAllU3Ec__AnonStorey16__ctor_m2976783344 (U3CInsertAllU3Ec__AnonStorey16_t3453358925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey16::<>m__7()
extern "C"  void U3CInsertAllU3Ec__AnonStorey16_U3CU3Em__7_m1331716582 (U3CInsertAllU3Ec__AnonStorey16_t3453358925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
