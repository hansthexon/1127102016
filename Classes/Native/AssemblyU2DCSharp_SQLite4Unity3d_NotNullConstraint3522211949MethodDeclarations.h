﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.NotNullConstraintViolationException
struct NotNullConstraintViolationException_t3522211949;
// System.String
struct String_t;
// SQLite4Unity3d.TableMapping
struct TableMapping_t3898710812;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.TableMapping/Column>
struct IEnumerable_1_t733182806;
// SQLite4Unity3d.SQLiteException
struct SQLiteException_t827126597;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_Result1450270481.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping3898710812.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteException827126597.h"

// System.Void SQLite4Unity3d.NotNullConstraintViolationException::.ctor(SQLite4Unity3d.SQLite3/Result,System.String)
extern "C"  void NotNullConstraintViolationException__ctor_m1367321575 (NotNullConstraintViolationException_t3522211949 * __this, int32_t ___r0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.NotNullConstraintViolationException::.ctor(SQLite4Unity3d.SQLite3/Result,System.String,SQLite4Unity3d.TableMapping,System.Object)
extern "C"  void NotNullConstraintViolationException__ctor_m4245592415 (NotNullConstraintViolationException_t3522211949 * __this, int32_t ___r0, String_t* ___message1, TableMapping_t3898710812 * ___mapping2, Il2CppObject * ___obj3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.TableMapping/Column> SQLite4Unity3d.NotNullConstraintViolationException::get_Columns()
extern "C"  Il2CppObject* NotNullConstraintViolationException_get_Columns_m1020407089 (NotNullConstraintViolationException_t3522211949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.NotNullConstraintViolationException::set_Columns(System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.TableMapping/Column>)
extern "C"  void NotNullConstraintViolationException_set_Columns_m771200206 (NotNullConstraintViolationException_t3522211949 * __this, Il2CppObject* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.NotNullConstraintViolationException SQLite4Unity3d.NotNullConstraintViolationException::New(SQLite4Unity3d.SQLite3/Result,System.String)
extern "C"  NotNullConstraintViolationException_t3522211949 * NotNullConstraintViolationException_New_m3039173513 (Il2CppObject * __this /* static, unused */, int32_t ___r0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.NotNullConstraintViolationException SQLite4Unity3d.NotNullConstraintViolationException::New(SQLite4Unity3d.SQLite3/Result,System.String,SQLite4Unity3d.TableMapping,System.Object)
extern "C"  NotNullConstraintViolationException_t3522211949 * NotNullConstraintViolationException_New_m3052794845 (Il2CppObject * __this /* static, unused */, int32_t ___r0, String_t* ___message1, TableMapping_t3898710812 * ___mapping2, Il2CppObject * ___obj3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SQLite4Unity3d.NotNullConstraintViolationException SQLite4Unity3d.NotNullConstraintViolationException::New(SQLite4Unity3d.SQLiteException,SQLite4Unity3d.TableMapping,System.Object)
extern "C"  NotNullConstraintViolationException_t3522211949 * NotNullConstraintViolationException_New_m3188086745 (Il2CppObject * __this /* static, unused */, SQLiteException_t827126597 * ___exception0, TableMapping_t3898710812 * ___mapping1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
