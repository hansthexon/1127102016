﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.QuickSort`1/<Sort>c__Iterator21<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct U3CSortU3Ec__Iterator21_t473315885;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IEnumerator_1_t2444651111;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"

// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor()
extern "C"  void U3CSortU3Ec__Iterator21__ctor_m1695042335_gshared (U3CSortU3Ec__Iterator21_t473315885 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21__ctor_m1695042335(__this, method) ((  void (*) (U3CSortU3Ec__Iterator21_t473315885 *, const MethodInfo*))U3CSortU3Ec__Iterator21__ctor_m1695042335_gshared)(__this, method)
// TElement System.Linq.QuickSort`1/<Sort>c__Iterator21<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.IEnumerator<TElement>.get_Current()
extern "C"  IndexedColumn_t674159988  U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m2583825052_gshared (U3CSortU3Ec__Iterator21_t473315885 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m2583825052(__this, method) ((  IndexedColumn_t674159988  (*) (U3CSortU3Ec__Iterator21_t473315885 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CTElementU3E_get_Current_m2583825052_gshared)(__this, method)
// System.Object System.Linq.QuickSort`1/<Sort>c__Iterator21<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m3365908642_gshared (U3CSortU3Ec__Iterator21_t473315885 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m3365908642(__this, method) ((  Il2CppObject * (*) (U3CSortU3Ec__Iterator21_t473315885 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m3365908642_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.QuickSort`1/<Sort>c__Iterator21<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m190871558_gshared (U3CSortU3Ec__Iterator21_t473315885 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m190871558(__this, method) ((  Il2CppObject * (*) (U3CSortU3Ec__Iterator21_t473315885 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m190871558_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TElement> System.Linq.QuickSort`1/<Sort>c__Iterator21<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.IEnumerable<TElement>.GetEnumerator()
extern "C"  Il2CppObject* U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m308640623_gshared (U3CSortU3Ec__Iterator21_t473315885 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m308640623(__this, method) ((  Il2CppObject* (*) (U3CSortU3Ec__Iterator21_t473315885 *, const MethodInfo*))U3CSortU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CTElementU3E_GetEnumerator_m308640623_gshared)(__this, method)
// System.Boolean System.Linq.QuickSort`1/<Sort>c__Iterator21<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::MoveNext()
extern "C"  bool U3CSortU3Ec__Iterator21_MoveNext_m2323785653_gshared (U3CSortU3Ec__Iterator21_t473315885 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_MoveNext_m2323785653(__this, method) ((  bool (*) (U3CSortU3Ec__Iterator21_t473315885 *, const MethodInfo*))U3CSortU3Ec__Iterator21_MoveNext_m2323785653_gshared)(__this, method)
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Dispose()
extern "C"  void U3CSortU3Ec__Iterator21_Dispose_m1170076402_gshared (U3CSortU3Ec__Iterator21_t473315885 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_Dispose_m1170076402(__this, method) ((  void (*) (U3CSortU3Ec__Iterator21_t473315885 *, const MethodInfo*))U3CSortU3Ec__Iterator21_Dispose_m1170076402_gshared)(__this, method)
// System.Void System.Linq.QuickSort`1/<Sort>c__Iterator21<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Reset()
extern "C"  void U3CSortU3Ec__Iterator21_Reset_m3654936232_gshared (U3CSortU3Ec__Iterator21_t473315885 * __this, const MethodInfo* method);
#define U3CSortU3Ec__Iterator21_Reset_m3654936232(__this, method) ((  void (*) (U3CSortU3Ec__Iterator21_t473315885 *, const MethodInfo*))U3CSortU3Ec__Iterator21_Reset_m3654936232_gshared)(__this, method)
