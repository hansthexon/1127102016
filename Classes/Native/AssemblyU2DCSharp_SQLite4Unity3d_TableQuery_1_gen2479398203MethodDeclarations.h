﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableQuery_1_gen4026329353MethodDeclarations.h"

// System.Void SQLite4Unity3d.TableQuery`1<Album>::.ctor(SQLite4Unity3d.SQLiteConnection,SQLite4Unity3d.TableMapping)
#define TableQuery_1__ctor_m2108375245(__this, ___conn0, ___table1, method) ((  void (*) (TableQuery_1_t2479398203 *, SQLiteConnection_t3529499386 *, TableMapping_t3898710812 *, const MethodInfo*))TableQuery_1__ctor_m2525682756_gshared)(__this, ___conn0, ___table1, method)
// System.Void SQLite4Unity3d.TableQuery`1<Album>::.ctor(SQLite4Unity3d.SQLiteConnection)
#define TableQuery_1__ctor_m1337690691(__this, ___conn0, method) ((  void (*) (TableQuery_1_t2479398203 *, SQLiteConnection_t3529499386 *, const MethodInfo*))TableQuery_1__ctor_m821176910_gshared)(__this, ___conn0, method)
// System.Collections.IEnumerator SQLite4Unity3d.TableQuery`1<Album>::System.Collections.IEnumerable.GetEnumerator()
#define TableQuery_1_System_Collections_IEnumerable_GetEnumerator_m3586260220(__this, method) ((  Il2CppObject * (*) (TableQuery_1_t2479398203 *, const MethodInfo*))TableQuery_1_System_Collections_IEnumerable_GetEnumerator_m2306269163_gshared)(__this, method)
// SQLite4Unity3d.SQLiteConnection SQLite4Unity3d.TableQuery`1<Album>::get_Connection()
#define TableQuery_1_get_Connection_m265757015(__this, method) ((  SQLiteConnection_t3529499386 * (*) (TableQuery_1_t2479398203 *, const MethodInfo*))TableQuery_1_get_Connection_m2018438234_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1<Album>::set_Connection(SQLite4Unity3d.SQLiteConnection)
#define TableQuery_1_set_Connection_m1993333718(__this, ___value0, method) ((  void (*) (TableQuery_1_t2479398203 *, SQLiteConnection_t3529499386 *, const MethodInfo*))TableQuery_1_set_Connection_m3046933261_gshared)(__this, ___value0, method)
// SQLite4Unity3d.TableMapping SQLite4Unity3d.TableQuery`1<Album>::get_Table()
#define TableQuery_1_get_Table_m1371601461(__this, method) ((  TableMapping_t3898710812 * (*) (TableQuery_1_t2479398203 *, const MethodInfo*))TableQuery_1_get_Table_m2172867230_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1<Album>::set_Table(SQLite4Unity3d.TableMapping)
#define TableQuery_1_set_Table_m560804714(__this, ___value0, method) ((  void (*) (TableQuery_1_t2479398203 *, TableMapping_t3898710812 *, const MethodInfo*))TableQuery_1_set_Table_m3580780791_gshared)(__this, ___value0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<Album>::Where(System.Linq.Expressions.Expression`1<System.Func`2<T,System.Boolean>>)
#define TableQuery_1_Where_m3076296753(__this, ___predExpr0, method) ((  TableQuery_1_t2479398203 * (*) (TableQuery_1_t2479398203 *, Expression_1_t1722355358 *, const MethodInfo*))TableQuery_1_Where_m4178518972_gshared)(__this, ___predExpr0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<Album>::Take(System.Int32)
#define TableQuery_1_Take_m4168625293(__this, ___n0, method) ((  TableQuery_1_t2479398203 * (*) (TableQuery_1_t2479398203 *, int32_t, const MethodInfo*))TableQuery_1_Take_m2215285898_gshared)(__this, ___n0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<Album>::Skip(System.Int32)
#define TableQuery_1_Skip_m484304255(__this, ___n0, method) ((  TableQuery_1_t2479398203 * (*) (TableQuery_1_t2479398203 *, int32_t, const MethodInfo*))TableQuery_1_Skip_m2335687758_gshared)(__this, ___n0, method)
// T SQLite4Unity3d.TableQuery`1<Album>::ElementAt(System.Int32)
#define TableQuery_1_ElementAt_m3381258494(__this, ___index0, method) ((  Album_t1142518145 * (*) (TableQuery_1_t2479398203 *, int32_t, const MethodInfo*))TableQuery_1_ElementAt_m4176579987_gshared)(__this, ___index0, method)
// SQLite4Unity3d.TableQuery`1<T> SQLite4Unity3d.TableQuery`1<Album>::Deferred()
#define TableQuery_1_Deferred_m1856423736(__this, method) ((  TableQuery_1_t2479398203 * (*) (TableQuery_1_t2479398203 *, const MethodInfo*))TableQuery_1_Deferred_m2927110549_gshared)(__this, method)
// System.Void SQLite4Unity3d.TableQuery`1<Album>::AddWhere(System.Linq.Expressions.Expression)
#define TableQuery_1_AddWhere_m2099069369(__this, ___pred0, method) ((  void (*) (TableQuery_1_t2479398203 *, Expression_t114864668 *, const MethodInfo*))TableQuery_1_AddWhere_m752477770_gshared)(__this, ___pred0, method)
// SQLite4Unity3d.SQLiteCommand SQLite4Unity3d.TableQuery`1<Album>::GenerateCommand(System.String)
#define TableQuery_1_GenerateCommand_m3678073295(__this, ___selectionList0, method) ((  SQLiteCommand_t2935685145 * (*) (TableQuery_1_t2479398203 *, String_t*, const MethodInfo*))TableQuery_1_GenerateCommand_m4193037006_gshared)(__this, ___selectionList0, method)
// SQLite4Unity3d.TableQuery`1/CompileResult<T> SQLite4Unity3d.TableQuery`1<Album>::CompileExpr(System.Linq.Expressions.Expression,System.Collections.Generic.List`1<System.Object>)
#define TableQuery_1_CompileExpr_m3299954882(__this, ___expr0, ___queryArgs1, method) ((  CompileResult_t2616859782 * (*) (TableQuery_1_t2479398203 *, Expression_t114864668 *, List_1_t2058570427 *, const MethodInfo*))TableQuery_1_CompileExpr_m3818611973_gshared)(__this, ___expr0, ___queryArgs1, method)
// System.Object SQLite4Unity3d.TableQuery`1<Album>::ConvertTo(System.Object,System.Type)
#define TableQuery_1_ConvertTo_m3885882567(__this /* static, unused */, ___obj0, ___t1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, Type_t *, const MethodInfo*))TableQuery_1_ConvertTo_m2602095610_gshared)(__this /* static, unused */, ___obj0, ___t1, method)
// System.String SQLite4Unity3d.TableQuery`1<Album>::CompileNullBinaryExpression(System.Linq.Expressions.BinaryExpression,SQLite4Unity3d.TableQuery`1/CompileResult<T>)
#define TableQuery_1_CompileNullBinaryExpression_m3240189280(__this, ___expression0, ___parameter1, method) ((  String_t* (*) (TableQuery_1_t2479398203 *, BinaryExpression_t2159924157 *, CompileResult_t2616859782 *, const MethodInfo*))TableQuery_1_CompileNullBinaryExpression_m4251067313_gshared)(__this, ___expression0, ___parameter1, method)
// System.String SQLite4Unity3d.TableQuery`1<Album>::GetSqlName(System.Linq.Expressions.Expression)
#define TableQuery_1_GetSqlName_m2905773927(__this, ___expr0, method) ((  String_t* (*) (TableQuery_1_t2479398203 *, Expression_t114864668 *, const MethodInfo*))TableQuery_1_GetSqlName_m1128366698_gshared)(__this, ___expr0, method)
// System.Int32 SQLite4Unity3d.TableQuery`1<Album>::Count()
#define TableQuery_1_Count_m3608348272(__this, method) ((  int32_t (*) (TableQuery_1_t2479398203 *, const MethodInfo*))TableQuery_1_Count_m3413050791_gshared)(__this, method)
// System.Int32 SQLite4Unity3d.TableQuery`1<Album>::Count(System.Linq.Expressions.Expression`1<System.Func`2<T,System.Boolean>>)
#define TableQuery_1_Count_m3920901649(__this, ___predExpr0, method) ((  int32_t (*) (TableQuery_1_t2479398203 *, Expression_1_t1722355358 *, const MethodInfo*))TableQuery_1_Count_m3405720028_gshared)(__this, ___predExpr0, method)
// System.Collections.Generic.IEnumerator`1<T> SQLite4Unity3d.TableQuery`1<Album>::GetEnumerator()
#define TableQuery_1_GetEnumerator_m2885254711(__this, method) ((  Il2CppObject* (*) (TableQuery_1_t2479398203 *, const MethodInfo*))TableQuery_1_GetEnumerator_m710809644_gshared)(__this, method)
// T SQLite4Unity3d.TableQuery`1<Album>::First()
#define TableQuery_1_First_m735636388(__this, method) ((  Album_t1142518145 * (*) (TableQuery_1_t2479398203 *, const MethodInfo*))TableQuery_1_First_m3018168943_gshared)(__this, method)
// T SQLite4Unity3d.TableQuery`1<Album>::FirstOrDefault()
#define TableQuery_1_FirstOrDefault_m1364973580(__this, method) ((  Album_t1142518145 * (*) (TableQuery_1_t2479398203 *, const MethodInfo*))TableQuery_1_FirstOrDefault_m3828705525_gshared)(__this, method)
// System.String SQLite4Unity3d.TableQuery`1<Album>::<GenerateCommand>m__11(SQLite4Unity3d.BaseTableQuery/Ordering)
#define TableQuery_1_U3CGenerateCommandU3Em__11_m4201541129(__this /* static, unused */, ___o0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, Ordering_t1038862794 *, const MethodInfo*))TableQuery_1_U3CGenerateCommandU3Em__11_m1866588696_gshared)(__this /* static, unused */, ___o0, method)
// System.String SQLite4Unity3d.TableQuery`1<Album>::<CompileExpr>m__12(SQLite4Unity3d.TableQuery`1/CompileResult<T>)
#define TableQuery_1_U3CCompileExprU3Em__12_m679944692(__this /* static, unused */, ___a0, method) ((  String_t* (*) (Il2CppObject * /* static, unused */, CompileResult_t2616859782 *, const MethodInfo*))TableQuery_1_U3CCompileExprU3Em__12_m1764005629_gshared)(__this /* static, unused */, ___a0, method)
