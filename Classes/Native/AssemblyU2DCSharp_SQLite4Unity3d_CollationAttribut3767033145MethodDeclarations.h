﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.CollationAttribute
struct CollationAttribute_t3767033145;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void SQLite4Unity3d.CollationAttribute::.ctor(System.String)
extern "C"  void CollationAttribute__ctor_m924007278 (CollationAttribute_t3767033145 * __this, String_t* ___collation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.CollationAttribute::get_Value()
extern "C"  String_t* CollationAttribute_get_Value_m731691885 (CollationAttribute_t3767033145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.CollationAttribute::set_Value(System.String)
extern "C"  void CollationAttribute_set_Value_m3725397696 (CollationAttribute_t3767033145 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
