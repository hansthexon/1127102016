﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Console
struct Console_t55256533;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_LogType1559732862.h"

// System.Void Console::.ctor()
extern "C"  void Console__ctor_m2646762720 (Console_t55256533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::.cctor()
extern "C"  void Console__cctor_m1926007647 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::Start()
extern "C"  void Console_Start_m3463118512 (Console_t55256533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::OnEnable()
extern "C"  void Console_OnEnable_m916480880 (Console_t55256533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::OnDisable()
extern "C"  void Console_OnDisable_m1517308857 (Console_t55256533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::Log(System.String,System.String,UnityEngine.LogType)
extern "C"  void Console_Log_m630287611 (Console_t55256533 * __this, String_t* ___logString0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::OnGUI()
extern "C"  void Console_OnGUI_m44307892 (Console_t55256533 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
