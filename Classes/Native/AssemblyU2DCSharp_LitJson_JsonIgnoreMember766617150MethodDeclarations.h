﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonIgnoreMember
struct JsonIgnoreMember_t766617150;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t2981295538;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t362681087;

#include "codegen/il2cpp-codegen.h"

// System.Void LitJson.JsonIgnoreMember::.ctor(System.String[])
extern "C"  void JsonIgnoreMember__ctor_m4034846130 (JsonIgnoreMember_t766617150 * __this, StringU5BU5D_t1642385972* ___members0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonIgnoreMember::.ctor(System.Collections.Generic.ICollection`1<System.String>)
extern "C"  void JsonIgnoreMember__ctor_m1769166493 (JsonIgnoreMember_t766617150 * __this, Il2CppObject* ___members0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.HashSet`1<System.String> LitJson.JsonIgnoreMember::get_Members()
extern "C"  HashSet_1_t362681087 * JsonIgnoreMember_get_Members_m3183235075 (JsonIgnoreMember_t766617150 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.JsonIgnoreMember::set_Members(System.Collections.Generic.HashSet`1<System.String>)
extern "C"  void JsonIgnoreMember_set_Members_m2160285324 (JsonIgnoreMember_t766617150 * __this, HashSet_1_t362681087 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
