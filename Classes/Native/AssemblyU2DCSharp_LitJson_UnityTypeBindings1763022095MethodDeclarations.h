﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Type
struct Type_t;
// LitJson.JsonWriter
struct JsonWriter_t1927598499;
// System.String
struct String_t;
// UnityEngine.RectOffset
struct RectOffset_t3387826427;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type1303803226.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1927598499.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Quaternion4030073918.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "UnityEngine_UnityEngine_RectOffset3387826427.h"

// System.Void LitJson.UnityTypeBindings::.cctor()
extern "C"  void UnityTypeBindings__cctor_m1604443740 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings::Register()
extern "C"  void UnityTypeBindings_Register_m4159923376 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings::<Register>m__35(System.Type,LitJson.JsonWriter)
extern "C"  void UnityTypeBindings_U3CRegisterU3Em__35_m2223178796 (Il2CppObject * __this /* static, unused */, Type_t * ___v0, JsonWriter_t1927598499 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type LitJson.UnityTypeBindings::<Register>m__36(System.String)
extern "C"  Type_t * UnityTypeBindings_U3CRegisterU3Em__36_m670881978 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings::<Register>m__37(UnityEngine.Vector2,LitJson.JsonWriter)
extern "C"  void UnityTypeBindings_U3CRegisterU3Em__37_m2402239199 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  ___v0, JsonWriter_t1927598499 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings::<Register>m__39(UnityEngine.Vector3,LitJson.JsonWriter)
extern "C"  void UnityTypeBindings_U3CRegisterU3Em__39_m3135347888 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, JsonWriter_t1927598499 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings::<Register>m__3B(UnityEngine.Vector4,LitJson.JsonWriter)
extern "C"  void UnityTypeBindings_U3CRegisterU3Em__3B_m680011476 (Il2CppObject * __this /* static, unused */, Vector4_t2243707581  ___v0, JsonWriter_t1927598499 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings::<Register>m__3C(UnityEngine.Quaternion,LitJson.JsonWriter)
extern "C"  void UnityTypeBindings_U3CRegisterU3Em__3C_m3552094868 (Il2CppObject * __this /* static, unused */, Quaternion_t4030073918  ___v0, JsonWriter_t1927598499 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings::<Register>m__3D(UnityEngine.Color,LitJson.JsonWriter)
extern "C"  void UnityTypeBindings_U3CRegisterU3Em__3D_m2367917276 (Il2CppObject * __this /* static, unused */, Color_t2020392075  ___v0, JsonWriter_t1927598499 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings::<Register>m__3E(UnityEngine.Color32,LitJson.JsonWriter)
extern "C"  void UnityTypeBindings_U3CRegisterU3Em__3E_m2091161038 (Il2CppObject * __this /* static, unused */, Color32_t874517518  ___v0, JsonWriter_t1927598499 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings::<Register>m__40(UnityEngine.Rect,LitJson.JsonWriter)
extern "C"  void UnityTypeBindings_U3CRegisterU3Em__40_m599969308 (Il2CppObject * __this /* static, unused */, Rect_t3681755626  ___v0, JsonWriter_t1927598499 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings::<Register>m__41(UnityEngine.RectOffset,LitJson.JsonWriter)
extern "C"  void UnityTypeBindings_U3CRegisterU3Em__41_m3522067582 (Il2CppObject * __this /* static, unused */, RectOffset_t3387826427 * ___v0, JsonWriter_t1927598499 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
