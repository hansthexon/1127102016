﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen3938509041MethodDeclarations.h"

// System.Void System.Action`2<UnityEngine.Vector2,LitJson.JsonWriter>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m2780419494(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t3176658245 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m497263298_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<UnityEngine.Vector2,LitJson.JsonWriter>::Invoke(T1,T2)
#define Action_2_Invoke_m1836733655(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3176658245 *, Vector2_t2243707579 , JsonWriter_t1927598499 *, const MethodInfo*))Action_2_Invoke_m961527555_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<UnityEngine.Vector2,LitJson.JsonWriter>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m1870529710(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t3176658245 *, Vector2_t2243707579 , JsonWriter_t1927598499 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m377899404_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<UnityEngine.Vector2,LitJson.JsonWriter>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1974816262(__this, ___result0, method) ((  void (*) (Action_2_t3176658245 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m3255379472_gshared)(__this, ___result0, method)
