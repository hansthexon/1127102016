﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3351159673.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22492407411.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2906363851_gshared (InternalEnumerator_1_t3351159673 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2906363851(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3351159673 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2906363851_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3640629387_gshared (InternalEnumerator_1_t3351159673 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3640629387(__this, method) ((  void (*) (InternalEnumerator_1_t3351159673 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3640629387_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2344787527_gshared (InternalEnumerator_1_t3351159673 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2344787527(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3351159673 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2344787527_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1817796636_gshared (InternalEnumerator_1_t3351159673 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1817796636(__this, method) ((  void (*) (InternalEnumerator_1_t3351159673 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1817796636_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3504749663_gshared (InternalEnumerator_1_t3351159673 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3504749663(__this, method) ((  bool (*) (InternalEnumerator_1_t3351159673 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3504749663_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>>::get_Current()
extern "C"  KeyValuePair_2_t2492407411  InternalEnumerator_1_get_Current_m3628801954_gshared (InternalEnumerator_1_t3351159673 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3628801954(__this, method) ((  KeyValuePair_2_t2492407411  (*) (InternalEnumerator_1_t3351159673 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3628801954_gshared)(__this, method)
