﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// System.IO.FileInfo
struct FileInfo_t3153503742;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SaveFileANdPlay
struct  SaveFileANdPlay_t1144534430  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text SaveFileANdPlay::scuk
	Text_t356221433 * ___scuk_2;
	// System.IO.FileInfo SaveFileANdPlay::t
	FileInfo_t3153503742 * ___t_3;

public:
	inline static int32_t get_offset_of_scuk_2() { return static_cast<int32_t>(offsetof(SaveFileANdPlay_t1144534430, ___scuk_2)); }
	inline Text_t356221433 * get_scuk_2() const { return ___scuk_2; }
	inline Text_t356221433 ** get_address_of_scuk_2() { return &___scuk_2; }
	inline void set_scuk_2(Text_t356221433 * value)
	{
		___scuk_2 = value;
		Il2CppCodeGenWriteBarrier(&___scuk_2, value);
	}

	inline static int32_t get_offset_of_t_3() { return static_cast<int32_t>(offsetof(SaveFileANdPlay_t1144534430, ___t_3)); }
	inline FileInfo_t3153503742 * get_t_3() const { return ___t_3; }
	inline FileInfo_t3153503742 ** get_address_of_t_3() { return &___t_3; }
	inline void set_t_3(FileInfo_t3153503742 * value)
	{
		___t_3 = value;
		Il2CppCodeGenWriteBarrier(&___t_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
