﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.UnityTypeBindings/<Register>c__AnonStorey1E
struct U3CRegisterU3Ec__AnonStorey1E_t484806451;
// LitJson.JsonWriter
struct JsonWriter_t1927598499;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "AssemblyU2DCSharp_LitJson_JsonWriter1927598499.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Bounds3033363703.h"

// System.Void LitJson.UnityTypeBindings/<Register>c__AnonStorey1E::.ctor()
extern "C"  void U3CRegisterU3Ec__AnonStorey1E__ctor_m1032773700 (U3CRegisterU3Ec__AnonStorey1E_t484806451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings/<Register>c__AnonStorey1E::<>m__38(UnityEngine.Vector2,LitJson.JsonWriter)
extern "C"  void U3CRegisterU3Ec__AnonStorey1E_U3CU3Em__38_m2318824356 (U3CRegisterU3Ec__AnonStorey1E_t484806451 * __this, Vector2_t2243707579  ___v0, JsonWriter_t1927598499 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings/<Register>c__AnonStorey1E::<>m__3A(UnityEngine.Vector3,LitJson.JsonWriter)
extern "C"  void U3CRegisterU3Ec__AnonStorey1E_U3CU3Em__3A_m1221592540 (U3CRegisterU3Ec__AnonStorey1E_t484806451 * __this, Vector3_t2243707580  ___v0, JsonWriter_t1927598499 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LitJson.UnityTypeBindings/<Register>c__AnonStorey1E::<>m__3F(UnityEngine.Bounds,LitJson.JsonWriter)
extern "C"  void U3CRegisterU3Ec__AnonStorey1E_U3CU3Em__3F_m3181630904 (U3CRegisterU3Ec__AnonStorey1E_t484806451 * __this, Bounds_t3033363703  ___v0, JsonWriter_t1927598499 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
