﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.InvalidRSAData
struct InvalidRSAData_t1674954323;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Purchasing.Security.InvalidRSAData::.ctor()
extern "C"  void InvalidRSAData__ctor_m3293493135 (InvalidRSAData_t1674954323 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
