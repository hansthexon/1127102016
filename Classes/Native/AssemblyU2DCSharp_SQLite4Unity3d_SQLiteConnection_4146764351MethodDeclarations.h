﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection/<Update>c__AnonStorey17
struct U3CUpdateU3Ec__AnonStorey17_t4146764351;
// SQLite4Unity3d.TableMapping/Column
struct Column_t441055761;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_Colum441055761.h"

// System.Void SQLite4Unity3d.SQLiteConnection/<Update>c__AnonStorey17::.ctor()
extern "C"  void U3CUpdateU3Ec__AnonStorey17__ctor_m1953246872 (U3CUpdateU3Ec__AnonStorey17_t4146764351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.SQLiteConnection/<Update>c__AnonStorey17::<>m__8(SQLite4Unity3d.TableMapping/Column)
extern "C"  bool U3CUpdateU3Ec__AnonStorey17_U3CU3Em__8_m1616423102 (U3CUpdateU3Ec__AnonStorey17_t4146764351 * __this, Column_t441055761 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SQLite4Unity3d.SQLiteConnection/<Update>c__AnonStorey17::<>m__9(SQLite4Unity3d.TableMapping/Column)
extern "C"  Il2CppObject * U3CUpdateU3Ec__AnonStorey17_U3CU3Em__9_m2453406878 (U3CUpdateU3Ec__AnonStorey17_t4146764351 * __this, Column_t441055761 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
