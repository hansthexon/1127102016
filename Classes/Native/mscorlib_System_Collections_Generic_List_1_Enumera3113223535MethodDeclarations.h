﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/ColumnInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1818511666(__this, ___l0, method) ((  void (*) (Enumerator_t3113223535 *, List_1_t3578493861 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/ColumnInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1490352312(__this, method) ((  void (*) (Enumerator_t3113223535 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/ColumnInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m95445854(__this, method) ((  Il2CppObject * (*) (Enumerator_t3113223535 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/ColumnInfo>::Dispose()
#define Enumerator_Dispose_m3743738761(__this, method) ((  void (*) (Enumerator_t3113223535 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/ColumnInfo>::VerifyState()
#define Enumerator_VerifyState_m3324534696(__this, method) ((  void (*) (Enumerator_t3113223535 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/ColumnInfo>::MoveNext()
#define Enumerator_MoveNext_m2990470295(__this, method) ((  bool (*) (Enumerator_t3113223535 *, const MethodInfo*))Enumerator_MoveNext_m4021114635_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<SQLite4Unity3d.SQLiteConnection/ColumnInfo>::get_Current()
#define Enumerator_get_Current_m2827493851(__this, method) ((  ColumnInfo_t4209372729 * (*) (Enumerator_t3113223535 *, const MethodInfo*))Enumerator_get_Current_m1222341175_gshared)(__this, method)
