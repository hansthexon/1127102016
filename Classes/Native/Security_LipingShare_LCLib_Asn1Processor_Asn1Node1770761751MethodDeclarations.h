﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t1770761751;
// System.String
struct String_t;
// System.IO.Stream
struct Stream_t3255436806;
// System.Byte[]
struct ByteU5BU5D_t3397334013;

#include "codegen/il2cpp-codegen.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_IO_Stream3255436806.h"

// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int64)
extern "C"  void Asn1Node__ctor_m2720905655 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___parentNode0, int64_t ___dataOffset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::.ctor()
extern "C"  void Asn1Node__ctor_m2386874033 (Asn1Node_t1770761751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::Init()
extern "C"  void Asn1Node_Init_m1357911443 (Asn1Node_t1770761751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetHexPrintingStr(LipingShare.LCLib.Asn1Processor.Asn1Node,System.String,System.String,System.Int32)
extern "C"  String_t* Asn1Node_GetHexPrintingStr_m134819188 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___startNode0, String_t* ___baseLine1, String_t* ___lStr2, int32_t ___lineLen3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::FormatLineString(System.String,System.Int32,System.Int32,System.String)
extern "C"  String_t* Asn1Node_FormatLineString_m3353585152 (Asn1Node_t1770761751 * __this, String_t* ___lStr0, int32_t ___indent1, int32_t ___lineLen2, String_t* ___msg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::FormatLineHexString(System.String,System.Int32,System.Int32,System.String)
extern "C"  String_t* Asn1Node_FormatLineHexString_m3369492261 (Asn1Node_t1770761751 * __this, String_t* ___lStr0, int32_t ___indent1, int32_t ___lineLen2, String_t* ___msg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte LipingShare.LCLib.Asn1Processor.Asn1Node::get_Tag()
extern "C"  uint8_t Asn1Node_get_Tag_m2005840874 (Asn1Node_t1770761751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte LipingShare.LCLib.Asn1Processor.Asn1Node::get_MaskedTag()
extern "C"  uint8_t Asn1Node_get_MaskedTag_m135294101 (Asn1Node_t1770761751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::LoadData(System.IO.Stream)
extern "C"  bool Asn1Node_LoadData_m1298383144 (Asn1Node_t1770761751 * __this, Stream_t3255436806 * ___xdata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::SaveData(System.IO.Stream)
extern "C"  bool Asn1Node_SaveData_m3078124111 (Asn1Node_t1770761751 * __this, Stream_t3255436806 * ___xdata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::ClearAll()
extern "C"  void Asn1Node_ClearAll_m2883531985 (Asn1Node_t1770761751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::AddChild(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void Asn1Node_AddChild_m134798510 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___xdata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::get_ChildNodeCount()
extern "C"  int64_t Asn1Node_get_ChildNodeCount_m1123088750 (Asn1Node_t1770761751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Node::GetChildNode(System.Int32)
extern "C"  Asn1Node_t1770761751 * Asn1Node_GetChildNode_m4133223467 (Asn1Node_t1770761751 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::get_TagName()
extern "C"  String_t* Asn1Node_get_TagName_m1549199302 (Asn1Node_t1770761751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LipingShare.LCLib.Asn1Processor.Asn1Node LipingShare.LCLib.Asn1Processor.Asn1Node::get_ParentNode()
extern "C"  Asn1Node_t1770761751 * Asn1Node_get_ParentNode_m4121820111 (Asn1Node_t1770761751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetText(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int32)
extern "C"  String_t* Asn1Node_GetText_m821606440 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___startNode0, int32_t ___lineLen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetDataStr(System.Boolean)
extern "C"  String_t* Asn1Node_GetDataStr_m2973067886 (Asn1Node_t1770761751 * __this, bool ___pureHexMode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::get_DataLength()
extern "C"  int64_t Asn1Node_get_DataLength_m4095104165 (Asn1Node_t1770761751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] LipingShare.LCLib.Asn1Processor.Asn1Node::get_Data()
extern "C"  ByteU5BU5D_t3397334013* Asn1Node_get_Data_m1676178324 (Asn1Node_t1770761751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::get_Deepness()
extern "C"  int64_t Asn1Node_get_Deepness_m684235038 (Asn1Node_t1770761751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::set_RequireRecalculatePar(System.Boolean)
extern "C"  void Asn1Node_set_RequireRecalculatePar_m3884225938 (Asn1Node_t1770761751 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::RecalculateTreePar()
extern "C"  void Asn1Node_RecalculateTreePar_m2615014223 (Asn1Node_t1770761751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 LipingShare.LCLib.Asn1Processor.Asn1Node::ResetBranchDataLength(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  int64_t Asn1Node_ResetBranchDataLength_m2407129041 (Il2CppObject * __this /* static, unused */, Asn1Node_t1770761751 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::ResetDataLengthFieldWidth(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void Asn1Node_ResetDataLengthFieldWidth_m9629946 (Il2CppObject * __this /* static, unused */, Asn1Node_t1770761751 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LipingShare.LCLib.Asn1Processor.Asn1Node::ResetChildNodePar(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int64)
extern "C"  void Asn1Node_ResetChildNodePar_m91912211 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___xNode0, int64_t ___subOffset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetListStr(LipingShare.LCLib.Asn1Processor.Asn1Node,System.Int32)
extern "C"  String_t* Asn1Node_GetListStr_m3438846266 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___startNode0, int32_t ___lineLen1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LipingShare.LCLib.Asn1Processor.Asn1Node::GetIndentStr(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  String_t* Asn1Node_GetIndentStr_m4071159475 (Asn1Node_t1770761751 * __this, Asn1Node_t1770761751 * ___startNode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::GeneralDecode(System.IO.Stream)
extern "C"  bool Asn1Node_GeneralDecode_m3857333286 (Asn1Node_t1770761751 * __this, Stream_t3255436806 * ___xdata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::ListDecode(System.IO.Stream)
extern "C"  bool Asn1Node_ListDecode_m89847718 (Asn1Node_t1770761751 * __this, Stream_t3255436806 * ___xdata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LipingShare.LCLib.Asn1Processor.Asn1Node::InternalLoadData(System.IO.Stream)
extern "C"  bool Asn1Node_InternalLoadData_m220543567 (Asn1Node_t1770761751 * __this, Stream_t3255436806 * ___xdata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
