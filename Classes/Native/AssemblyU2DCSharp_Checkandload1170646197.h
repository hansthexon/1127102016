﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// CodeTest[]
struct CodeTestU5BU5D_t2090683080;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>>
struct List_1_t1113123064;
// UnityEngine.UI.Image
struct Image_t2042527209;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1398341365;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// System.Collections.Generic.IEnumerable`1<Code>
struct IEnumerable_1_t978294556;
// Code[]
struct CodeU5BU5D_t1855765742;
// DataService
struct DataService_t2602786891;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Checkandload
struct  Checkandload_t1170646197  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject Checkandload::loadbutton
	GameObject_t1756533147 * ___loadbutton_2;
	// CodeTest[] Checkandload::vr
	CodeTestU5BU5D_t2090683080* ___vr_3;
	// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>> Checkandload::list
	List_1_t1113123064 * ___list_4;
	// System.Single Checkandload::perdownload
	float ___perdownload_5;
	// UnityEngine.UI.Image Checkandload::progressbar
	Image_t2042527209 * ___progressbar_6;
	// System.Collections.Generic.List`1<System.String> Checkandload::tobedownloaded
	List_1_t1398341365 * ___tobedownloaded_7;
	// System.Boolean Checkandload::wasthereadownlaod
	bool ___wasthereadownlaod_8;
	// System.Boolean Checkandload::canpass
	bool ___canpass_9;
	// System.Int32 Checkandload::i
	int32_t ___i_10;
	// UnityEngine.UI.Text Checkandload::updatetext
	Text_t356221433 * ___updatetext_11;
	// System.String Checkandload::savepath
	String_t* ___savepath_12;
	// UnityEngine.AsyncOperation Checkandload::asc
	AsyncOperation_t3814632279 * ___asc_13;
	// System.Collections.Generic.IEnumerable`1<Code> Checkandload::purchased
	Il2CppObject* ___purchased_14;
	// Code[] Checkandload::cr
	CodeU5BU5D_t1855765742* ___cr_15;
	// DataService Checkandload::ds
	DataService_t2602786891 * ___ds_16;

public:
	inline static int32_t get_offset_of_loadbutton_2() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___loadbutton_2)); }
	inline GameObject_t1756533147 * get_loadbutton_2() const { return ___loadbutton_2; }
	inline GameObject_t1756533147 ** get_address_of_loadbutton_2() { return &___loadbutton_2; }
	inline void set_loadbutton_2(GameObject_t1756533147 * value)
	{
		___loadbutton_2 = value;
		Il2CppCodeGenWriteBarrier(&___loadbutton_2, value);
	}

	inline static int32_t get_offset_of_vr_3() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___vr_3)); }
	inline CodeTestU5BU5D_t2090683080* get_vr_3() const { return ___vr_3; }
	inline CodeTestU5BU5D_t2090683080** get_address_of_vr_3() { return &___vr_3; }
	inline void set_vr_3(CodeTestU5BU5D_t2090683080* value)
	{
		___vr_3 = value;
		Il2CppCodeGenWriteBarrier(&___vr_3, value);
	}

	inline static int32_t get_offset_of_list_4() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___list_4)); }
	inline List_1_t1113123064 * get_list_4() const { return ___list_4; }
	inline List_1_t1113123064 ** get_address_of_list_4() { return &___list_4; }
	inline void set_list_4(List_1_t1113123064 * value)
	{
		___list_4 = value;
		Il2CppCodeGenWriteBarrier(&___list_4, value);
	}

	inline static int32_t get_offset_of_perdownload_5() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___perdownload_5)); }
	inline float get_perdownload_5() const { return ___perdownload_5; }
	inline float* get_address_of_perdownload_5() { return &___perdownload_5; }
	inline void set_perdownload_5(float value)
	{
		___perdownload_5 = value;
	}

	inline static int32_t get_offset_of_progressbar_6() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___progressbar_6)); }
	inline Image_t2042527209 * get_progressbar_6() const { return ___progressbar_6; }
	inline Image_t2042527209 ** get_address_of_progressbar_6() { return &___progressbar_6; }
	inline void set_progressbar_6(Image_t2042527209 * value)
	{
		___progressbar_6 = value;
		Il2CppCodeGenWriteBarrier(&___progressbar_6, value);
	}

	inline static int32_t get_offset_of_tobedownloaded_7() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___tobedownloaded_7)); }
	inline List_1_t1398341365 * get_tobedownloaded_7() const { return ___tobedownloaded_7; }
	inline List_1_t1398341365 ** get_address_of_tobedownloaded_7() { return &___tobedownloaded_7; }
	inline void set_tobedownloaded_7(List_1_t1398341365 * value)
	{
		___tobedownloaded_7 = value;
		Il2CppCodeGenWriteBarrier(&___tobedownloaded_7, value);
	}

	inline static int32_t get_offset_of_wasthereadownlaod_8() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___wasthereadownlaod_8)); }
	inline bool get_wasthereadownlaod_8() const { return ___wasthereadownlaod_8; }
	inline bool* get_address_of_wasthereadownlaod_8() { return &___wasthereadownlaod_8; }
	inline void set_wasthereadownlaod_8(bool value)
	{
		___wasthereadownlaod_8 = value;
	}

	inline static int32_t get_offset_of_canpass_9() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___canpass_9)); }
	inline bool get_canpass_9() const { return ___canpass_9; }
	inline bool* get_address_of_canpass_9() { return &___canpass_9; }
	inline void set_canpass_9(bool value)
	{
		___canpass_9 = value;
	}

	inline static int32_t get_offset_of_i_10() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___i_10)); }
	inline int32_t get_i_10() const { return ___i_10; }
	inline int32_t* get_address_of_i_10() { return &___i_10; }
	inline void set_i_10(int32_t value)
	{
		___i_10 = value;
	}

	inline static int32_t get_offset_of_updatetext_11() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___updatetext_11)); }
	inline Text_t356221433 * get_updatetext_11() const { return ___updatetext_11; }
	inline Text_t356221433 ** get_address_of_updatetext_11() { return &___updatetext_11; }
	inline void set_updatetext_11(Text_t356221433 * value)
	{
		___updatetext_11 = value;
		Il2CppCodeGenWriteBarrier(&___updatetext_11, value);
	}

	inline static int32_t get_offset_of_savepath_12() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___savepath_12)); }
	inline String_t* get_savepath_12() const { return ___savepath_12; }
	inline String_t** get_address_of_savepath_12() { return &___savepath_12; }
	inline void set_savepath_12(String_t* value)
	{
		___savepath_12 = value;
		Il2CppCodeGenWriteBarrier(&___savepath_12, value);
	}

	inline static int32_t get_offset_of_asc_13() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___asc_13)); }
	inline AsyncOperation_t3814632279 * get_asc_13() const { return ___asc_13; }
	inline AsyncOperation_t3814632279 ** get_address_of_asc_13() { return &___asc_13; }
	inline void set_asc_13(AsyncOperation_t3814632279 * value)
	{
		___asc_13 = value;
		Il2CppCodeGenWriteBarrier(&___asc_13, value);
	}

	inline static int32_t get_offset_of_purchased_14() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___purchased_14)); }
	inline Il2CppObject* get_purchased_14() const { return ___purchased_14; }
	inline Il2CppObject** get_address_of_purchased_14() { return &___purchased_14; }
	inline void set_purchased_14(Il2CppObject* value)
	{
		___purchased_14 = value;
		Il2CppCodeGenWriteBarrier(&___purchased_14, value);
	}

	inline static int32_t get_offset_of_cr_15() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___cr_15)); }
	inline CodeU5BU5D_t1855765742* get_cr_15() const { return ___cr_15; }
	inline CodeU5BU5D_t1855765742** get_address_of_cr_15() { return &___cr_15; }
	inline void set_cr_15(CodeU5BU5D_t1855765742* value)
	{
		___cr_15 = value;
		Il2CppCodeGenWriteBarrier(&___cr_15, value);
	}

	inline static int32_t get_offset_of_ds_16() { return static_cast<int32_t>(offsetof(Checkandload_t1170646197, ___ds_16)); }
	inline DataService_t2602786891 * get_ds_16() const { return ___ds_16; }
	inline DataService_t2602786891 ** get_address_of_ds_16() { return &___ds_16; }
	inline void set_ds_16(DataService_t2602786891 * value)
	{
		___ds_16 = value;
		Il2CppCodeGenWriteBarrier(&___ds_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
