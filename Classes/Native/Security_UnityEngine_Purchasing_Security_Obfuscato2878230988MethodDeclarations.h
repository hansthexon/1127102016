﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.Int32[]
struct Int32U5BU5D_t3030399641;

#include "codegen/il2cpp-codegen.h"

// System.Byte[] UnityEngine.Purchasing.Security.Obfuscator::DeObfuscate(System.Byte[],System.Int32[],System.Int32)
extern "C"  ByteU5BU5D_t3397334013* Obfuscator_DeObfuscate_m4253116256 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t3397334013* ___data0, Int32U5BU5D_t3030399641* ___order1, int32_t ___key2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
