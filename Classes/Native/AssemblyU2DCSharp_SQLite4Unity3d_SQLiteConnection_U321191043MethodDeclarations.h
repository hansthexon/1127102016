﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey14
struct U3CInsertAllU3Ec__AnonStorey14_t321191043;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey14::.ctor()
extern "C"  void U3CInsertAllU3Ec__AnonStorey14__ctor_m3454096038 (U3CInsertAllU3Ec__AnonStorey14_t321191043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteConnection/<InsertAll>c__AnonStorey14::<>m__5()
extern "C"  void U3CInsertAllU3Ec__AnonStorey14_U3CU3Em__5_m185491302 (U3CInsertAllU3Ec__AnonStorey14_t321191043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
