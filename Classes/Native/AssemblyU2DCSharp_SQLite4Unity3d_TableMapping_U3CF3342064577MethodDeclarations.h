﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.TableMapping/<FindColumn>c__AnonStorey1A
struct U3CFindColumnU3Ec__AnonStorey1A_t3342064577;
// SQLite4Unity3d.TableMapping/Column
struct Column_t441055761;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_Colum441055761.h"

// System.Void SQLite4Unity3d.TableMapping/<FindColumn>c__AnonStorey1A::.ctor()
extern "C"  void U3CFindColumnU3Ec__AnonStorey1A__ctor_m2968757840 (U3CFindColumnU3Ec__AnonStorey1A_t3342064577 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SQLite4Unity3d.TableMapping/<FindColumn>c__AnonStorey1A::<>m__E(SQLite4Unity3d.TableMapping/Column)
extern "C"  bool U3CFindColumnU3Ec__AnonStorey1A_U3CU3Em__E_m2787872565 (U3CFindColumnU3Ec__AnonStorey1A_t3342064577 * __this, Column_t441055761 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
