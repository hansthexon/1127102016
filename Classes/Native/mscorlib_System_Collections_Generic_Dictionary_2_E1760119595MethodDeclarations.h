﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>
struct Dictionary_2_t440094893;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1760119595.h"
#include "mscorlib_System_Collections_DictionaryEntry3048875398.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22492407411.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I848034765.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m811214818_gshared (Enumerator_t1760119595 * __this, Dictionary_2_t440094893 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m811214818(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1760119595 *, Dictionary_2_t440094893 *, const MethodInfo*))Enumerator__ctor_m811214818_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4203850179_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4203850179(__this, method) ((  Il2CppObject * (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4203850179_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m24795391_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m24795391(__this, method) ((  void (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m24795391_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t3048875398  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2251395880_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2251395880(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2251395880_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4171739561_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4171739561(__this, method) ((  Il2CppObject * (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4171739561_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2176216665_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2176216665(__this, method) ((  Il2CppObject * (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2176216665_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3944863691_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3944863691(__this, method) ((  bool (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_MoveNext_m3944863691_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Current()
extern "C"  KeyValuePair_2_t2492407411  Enumerator_get_Current_m505501491_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m505501491(__this, method) ((  KeyValuePair_2_t2492407411  (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_get_Current_m505501491_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m2900523578_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2900523578(__this, method) ((  Il2CppObject * (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_get_CurrentKey_m2900523578_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_CurrentValue()
extern "C"  IndexInfo_t848034765  Enumerator_get_CurrentValue_m2282140538_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m2282140538(__this, method) ((  IndexInfo_t848034765  (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_get_CurrentValue_m2282140538_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Reset()
extern "C"  void Enumerator_Reset_m2958379748_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2958379748(__this, method) ((  void (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_Reset_m2958379748_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2402519621_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2402519621(__this, method) ((  void (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_VerifyState_m2402519621_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m207879567_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m207879567(__this, method) ((  void (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_VerifyCurrent_m207879567_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,SQLite4Unity3d.SQLiteConnection/IndexInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m1427874974_gshared (Enumerator_t1760119595 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1427874974(__this, method) ((  void (*) (Enumerator_t1760119595 *, const MethodInfo*))Enumerator_Dispose_m1427874974_gshared)(__this, method)
