﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Purchasing.Security.RSAKey
struct RSAKey_t446464277;

#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Purchasing.Security.GooglePlayValidator
struct  GooglePlayValidator_t4061171767  : public Il2CppObject
{
public:
	// UnityEngine.Purchasing.Security.RSAKey UnityEngine.Purchasing.Security.GooglePlayValidator::key
	RSAKey_t446464277 * ___key_0;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(GooglePlayValidator_t4061171767, ___key_0)); }
	inline RSAKey_t446464277 * get_key_0() const { return ___key_0; }
	inline RSAKey_t446464277 ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RSAKey_t446464277 * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier(&___key_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
