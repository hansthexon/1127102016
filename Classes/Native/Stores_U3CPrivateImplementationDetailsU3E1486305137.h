﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"
#include "Stores_U3CPrivateImplementationDetailsU3E_U24Array1568637717.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1486305143  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-F59538F128558CDEE7C8F1BE48C08DBA78089463
	U24ArrayTypeU3D12_t1568637717  ___U24fieldU2DF59538F128558CDEE7C8F1BE48C08DBA78089463_0;

public:
	inline static int32_t get_offset_of_U24fieldU2DF59538F128558CDEE7C8F1BE48C08DBA78089463_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1486305143_StaticFields, ___U24fieldU2DF59538F128558CDEE7C8F1BE48C08DBA78089463_0)); }
	inline U24ArrayTypeU3D12_t1568637717  get_U24fieldU2DF59538F128558CDEE7C8F1BE48C08DBA78089463_0() const { return ___U24fieldU2DF59538F128558CDEE7C8F1BE48C08DBA78089463_0; }
	inline U24ArrayTypeU3D12_t1568637717 * get_address_of_U24fieldU2DF59538F128558CDEE7C8F1BE48C08DBA78089463_0() { return &___U24fieldU2DF59538F128558CDEE7C8F1BE48C08DBA78089463_0; }
	inline void set_U24fieldU2DF59538F128558CDEE7C8F1BE48C08DBA78089463_0(U24ArrayTypeU3D12_t1568637717  value)
	{
		___U24fieldU2DF59538F128558CDEE7C8F1BE48C08DBA78089463_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
