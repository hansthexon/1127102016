﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.TypeBinaryExpression
struct TypeBinaryExpression_t4211182725;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"

// System.Linq.Expressions.Expression System.Linq.Expressions.TypeBinaryExpression::get_Expression()
extern "C"  Expression_t114864668 * TypeBinaryExpression_get_Expression_m2856767424 (TypeBinaryExpression_t4211182725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Linq.Expressions.TypeBinaryExpression::get_TypeOperand()
extern "C"  Type_t * TypeBinaryExpression_get_TypeOperand_m580838540 (TypeBinaryExpression_t4211182725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
