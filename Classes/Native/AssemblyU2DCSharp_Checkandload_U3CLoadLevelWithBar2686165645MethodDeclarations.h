﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Checkandload/<LoadLevelWithBar>c__Iterator7
struct U3CLoadLevelWithBarU3Ec__Iterator7_t2686165645;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Checkandload/<LoadLevelWithBar>c__Iterator7::.ctor()
extern "C"  void U3CLoadLevelWithBarU3Ec__Iterator7__ctor_m2193303370 (U3CLoadLevelWithBarU3Ec__Iterator7_t2686165645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Checkandload/<LoadLevelWithBar>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadLevelWithBarU3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m645583400 (U3CLoadLevelWithBarU3Ec__Iterator7_t2686165645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Checkandload/<LoadLevelWithBar>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadLevelWithBarU3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m663664640 (U3CLoadLevelWithBarU3Ec__Iterator7_t2686165645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Checkandload/<LoadLevelWithBar>c__Iterator7::MoveNext()
extern "C"  bool U3CLoadLevelWithBarU3Ec__Iterator7_MoveNext_m1471043222 (U3CLoadLevelWithBarU3Ec__Iterator7_t2686165645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload/<LoadLevelWithBar>c__Iterator7::Dispose()
extern "C"  void U3CLoadLevelWithBarU3Ec__Iterator7_Dispose_m2515200487 (U3CLoadLevelWithBarU3Ec__Iterator7_t2686165645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Checkandload/<LoadLevelWithBar>c__Iterator7::Reset()
extern "C"  void U3CLoadLevelWithBarU3Ec__Iterator7_Reset_m265373525 (U3CLoadLevelWithBarU3Ec__Iterator7_t2686165645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
