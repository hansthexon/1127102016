﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// CodeTest
struct CodeTest_t2096217477;
// Code
struct Code_t686167511;
// Person
struct Person_t3241917763;
// SQLite4Unity3d.BaseTableQuery/Ordering
struct Ordering_t1038862794;
// SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>
struct CompileResult_t4163790932;
// SQLite4Unity3d.TableMapping/Column
struct Column_t441055761;
// SQLite4Unity3d.TableMapping
struct TableMapping_t3898710812;
// SQLite4Unity3d.SQLiteConnection/ColumnInfo
struct ColumnInfo_t4209372729;
// SQLite4Unity3d.IndexedAttribute
struct IndexedAttribute_t2684901191;
// SQLite4Unity3d.SQLiteCommand/Binding
struct Binding_t1831577297;
// LitJson.JsonData
struct JsonData_t269267574;
// LitJson.ExporterFunc
struct ExporterFunc_t408878057;
// LitJson.FactoryFunc
struct FactoryFunc_t2436388556;
// LitJson.ImporterFunc
struct ImporterFunc_t2977850894;
// LitJson.WriterContext
struct WriterContext_t4137194742;
// LitJson.Lexer/StateHandler
struct StateHandler_t387387051;
// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t2494532455;

#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_CodeTest2096217477.h"
#include "AssemblyU2DCSharp_Code686167511.h"
#include "AssemblyU2DCSharp_Person3241917763.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_BaseTableQuery_Or1038862794.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableQuery_1_Comp4163790932.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping_Colum441055761.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_TableMapping3898710812.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I848034765.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_4209372729.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_IndexedAttribute2684901191.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteCommand_Bin1831577297.h"
#include "AssemblyU2DCSharp_LitJson_JsonData269267574.h"
#include "AssemblyU2DCSharp_LitJson_PropertyMetadata3693826136.h"
#include "AssemblyU2DCSharp_LitJson_ArrayMetadata2008834462.h"
#include "AssemblyU2DCSharp_LitJson_ObjectMetadata3995922398.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc408878057.h"
#include "AssemblyU2DCSharp_LitJson_FactoryFunc2436388556.h"
#include "AssemblyU2DCSharp_LitJson_ImporterFunc2977850894.h"
#include "AssemblyU2DCSharp_LitJson_WriterContext4137194742.h"
#include "AssemblyU2DCSharp_LitJson_Lexer_StateHandler387387051.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455.h"

#pragma once
// CodeTest[]
struct CodeTestU5BU5D_t2090683080  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CodeTest_t2096217477 * m_Items[1];

public:
	inline CodeTest_t2096217477 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CodeTest_t2096217477 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CodeTest_t2096217477 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Code[]
struct CodeU5BU5D_t1855765742  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Code_t686167511 * m_Items[1];

public:
	inline Code_t686167511 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Code_t686167511 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Code_t686167511 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Person[]
struct PersonU5BU5D_t1129139794  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Person_t3241917763 * m_Items[1];

public:
	inline Person_t3241917763 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Person_t3241917763 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Person_t3241917763 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SQLite4Unity3d.BaseTableQuery/Ordering[]
struct OrderingU5BU5D_t1437299663  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Ordering_t1038862794 * m_Items[1];

public:
	inline Ordering_t1038862794 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Ordering_t1038862794 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Ordering_t1038862794 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SQLite4Unity3d.TableQuery`1/CompileResult<System.Object>[]
struct CompileResultU5BU5D_t3217057757  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) CompileResult_t4163790932 * m_Items[1];

public:
	inline CompileResult_t4163790932 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline CompileResult_t4163790932 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, CompileResult_t4163790932 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SQLite4Unity3d.TableMapping/Column[]
struct ColumnU5BU5D_t1579265676  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Column_t441055761 * m_Items[1];

public:
	inline Column_t441055761 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Column_t441055761 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Column_t441055761 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SQLite4Unity3d.TableMapping[]
struct TableMappingU5BU5D_t419391733  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TableMapping_t3898710812 * m_Items[1];

public:
	inline TableMapping_t3898710812 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TableMapping_t3898710812 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TableMapping_t3898710812 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SQLite4Unity3d.SQLiteConnection/IndexInfo[]
struct IndexInfoU5BU5D_t625615456  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IndexInfo_t848034765  m_Items[1];

public:
	inline IndexInfo_t848034765  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IndexInfo_t848034765 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IndexInfo_t848034765  value)
	{
		m_Items[index] = value;
	}
};
// SQLite4Unity3d.SQLiteConnection/IndexedColumn[]
struct IndexedColumnU5BU5D_t4219656765  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IndexedColumn_t674159988  m_Items[1];

public:
	inline IndexedColumn_t674159988  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IndexedColumn_t674159988 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IndexedColumn_t674159988  value)
	{
		m_Items[index] = value;
	}
};
// SQLite4Unity3d.SQLiteConnection/ColumnInfo[]
struct ColumnInfoU5BU5D_t52223172  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ColumnInfo_t4209372729 * m_Items[1];

public:
	inline ColumnInfo_t4209372729 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ColumnInfo_t4209372729 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ColumnInfo_t4209372729 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SQLite4Unity3d.IndexedAttribute[]
struct IndexedAttributeU5BU5D_t766744510  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IndexedAttribute_t2684901191 * m_Items[1];

public:
	inline IndexedAttribute_t2684901191 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IndexedAttribute_t2684901191 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IndexedAttribute_t2684901191 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// SQLite4Unity3d.SQLiteCommand/Binding[]
struct BindingU5BU5D_t3355571916  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Binding_t1831577297 * m_Items[1];

public:
	inline Binding_t1831577297 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Binding_t1831577297 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Binding_t1831577297 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.JsonData[]
struct JsonDataU5BU5D_t4077344499  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) JsonData_t269267574 * m_Items[1];

public:
	inline JsonData_t269267574 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline JsonData_t269267574 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, JsonData_t269267574 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.PropertyMetadata[]
struct PropertyMetadataU5BU5D_t3621088073  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PropertyMetadata_t3693826136  m_Items[1];

public:
	inline PropertyMetadata_t3693826136  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PropertyMetadata_t3693826136 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PropertyMetadata_t3693826136  value)
	{
		m_Items[index] = value;
	}
};
// LitJson.ArrayMetadata[]
struct ArrayMetadataU5BU5D_t1077509675  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ArrayMetadata_t2008834462  m_Items[1];

public:
	inline ArrayMetadata_t2008834462  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ArrayMetadata_t2008834462 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ArrayMetadata_t2008834462  value)
	{
		m_Items[index] = value;
	}
};
// LitJson.ObjectMetadata[]
struct ObjectMetadataU5BU5D_t3070734059  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ObjectMetadata_t3995922398  m_Items[1];

public:
	inline ObjectMetadata_t3995922398  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ObjectMetadata_t3995922398 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ObjectMetadata_t3995922398  value)
	{
		m_Items[index] = value;
	}
};
// LitJson.ExporterFunc[]
struct ExporterFuncU5BU5D_t1916072532  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ExporterFunc_t408878057 * m_Items[1];

public:
	inline ExporterFunc_t408878057 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ExporterFunc_t408878057 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ExporterFunc_t408878057 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.FactoryFunc[]
struct FactoryFuncU5BU5D_t4116142213  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FactoryFunc_t2436388556 * m_Items[1];

public:
	inline FactoryFunc_t2436388556 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FactoryFunc_t2436388556 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FactoryFunc_t2436388556 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.ImporterFunc[]
struct ImporterFuncU5BU5D_t90319355  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ImporterFunc_t2977850894 * m_Items[1];

public:
	inline ImporterFunc_t2977850894 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ImporterFunc_t2977850894 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ImporterFunc_t2977850894 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.WriterContext[]
struct WriterContextU5BU5D_t3808893555  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WriterContext_t4137194742 * m_Items[1];

public:
	inline WriterContext_t4137194742 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WriterContext_t4137194742 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WriterContext_t4137194742 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// LitJson.Lexer/StateHandler[]
struct StateHandlerU5BU5D_t1593819722  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) StateHandler_t387387051 * m_Items[1];

public:
	inline StateHandler_t387387051 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline StateHandler_t387387051 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, StateHandler_t387387051 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t2935582494  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WireframeBehaviour_t2494532455 * m_Items[1];

public:
	inline WireframeBehaviour_t2494532455 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline WireframeBehaviour_t2494532455 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, WireframeBehaviour_t2494532455 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
