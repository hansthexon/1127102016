﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.SQLiteCommand
struct SQLiteCommand_t2935685145;
// SQLite4Unity3d.SQLiteConnection
struct SQLiteConnection_t3529499386;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection3529499386.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLite3_ColType2341528904.h"
#include "mscorlib_System_Type1303803226.h"

// System.Void SQLite4Unity3d.SQLiteCommand::.ctor(SQLite4Unity3d.SQLiteConnection)
extern "C"  void SQLiteCommand__ctor_m4257028502 (SQLiteCommand_t2935685145 * __this, SQLiteConnection_t3529499386 * ___conn0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteCommand::.cctor()
extern "C"  void SQLiteCommand__cctor_m426856133 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLiteCommand::get_CommandText()
extern "C"  String_t* SQLiteCommand_get_CommandText_m3409045158 (SQLiteCommand_t2935685145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteCommand::set_CommandText(System.String)
extern "C"  void SQLiteCommand_set_CommandText_m2896812627 (SQLiteCommand_t2935685145 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.SQLiteCommand::ExecuteNonQuery()
extern "C"  int32_t SQLiteCommand_ExecuteNonQuery_m1704464760 (SQLiteCommand_t2935685145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteCommand::OnInstanceCreated(System.Object)
extern "C"  void SQLiteCommand_OnInstanceCreated_m260147906 (SQLiteCommand_t2935685145 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteCommand::Bind(System.String,System.Object)
extern "C"  void SQLiteCommand_Bind_m1961568607 (SQLiteCommand_t2935685145 * __this, String_t* ___name0, Il2CppObject * ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteCommand::Bind(System.Object)
extern "C"  void SQLiteCommand_Bind_m3574593673 (SQLiteCommand_t2935685145 * __this, Il2CppObject * ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SQLite4Unity3d.SQLiteCommand::ToString()
extern "C"  String_t* SQLiteCommand_ToString_m4258354333 (SQLiteCommand_t2935685145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr SQLite4Unity3d.SQLiteCommand::Prepare()
extern "C"  IntPtr_t SQLiteCommand_Prepare_m1554312292 (SQLiteCommand_t2935685145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteCommand::Finalize(System.IntPtr)
extern "C"  void SQLiteCommand_Finalize_m1966529826 (SQLiteCommand_t2935685145 * __this, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteCommand::BindAll(System.IntPtr)
extern "C"  void SQLiteCommand_BindAll_m1009981104 (SQLiteCommand_t2935685145 * __this, IntPtr_t ___stmt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.SQLiteCommand::BindParameter(System.IntPtr,System.Int32,System.Object,System.Boolean)
extern "C"  void SQLiteCommand_BindParameter_m2640782592 (Il2CppObject * __this /* static, unused */, IntPtr_t ___stmt0, int32_t ___index1, Il2CppObject * ___value2, bool ___storeDateTimeAsTicks3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object SQLite4Unity3d.SQLiteCommand::ReadCol(System.IntPtr,System.Int32,SQLite4Unity3d.SQLite3/ColType,System.Type)
extern "C"  Il2CppObject * SQLiteCommand_ReadCol_m1290746203 (SQLiteCommand_t2935685145 * __this, IntPtr_t ___stmt0, int32_t ___index1, int32_t ___type2, Type_t * ___clrType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
