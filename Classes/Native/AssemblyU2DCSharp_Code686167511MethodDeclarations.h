﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Code
struct Code_t686167511;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Code::.ctor()
extern "C"  void Code__ctor_m2985021040 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_CodeARimage()
extern "C"  String_t* Code_get_CodeARimage_m3452952331 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_CodeARimage(System.String)
extern "C"  void Code_set_CodeARimage_m1289088618 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_VideoFile()
extern "C"  String_t* Code_get_VideoFile_m2881642233 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_VideoFile(System.String)
extern "C"  void Code_set_VideoFile_m3475340960 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_Type()
extern "C"  String_t* Code_get_Type_m1375582958 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_Type(System.String)
extern "C"  void Code_set_Type_m1590234341 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_Type2()
extern "C"  String_t* Code_get_Type2_m111075714 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_Type2(System.String)
extern "C"  void Code_set_Type2_m203722147 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_PolitePlanet()
extern "C"  String_t* Code_get_PolitePlanet_m1684030339 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_PolitePlanet(System.String)
extern "C"  void Code_set_PolitePlanet_m2141438218 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_SelfVideoFile()
extern "C"  String_t* Code_get_SelfVideoFile_m3675906749 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_SelfVideoFile(System.String)
extern "C"  void Code_set_SelfVideoFile_m4279562158 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_Country()
extern "C"  String_t* Code_get_Country_m3974778262 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_Country(System.String)
extern "C"  void Code_set_Country_m1651326709 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_Purchased()
extern "C"  String_t* Code_get_Purchased_m1191772473 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_Purchased(System.String)
extern "C"  void Code_set_Purchased_m3233192608 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_Content()
extern "C"  String_t* Code_get_Content_m1294865951 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_Content(System.String)
extern "C"  void Code_set_Content_m1960219696 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Code::get_FirstScore()
extern "C"  int32_t Code_get_FirstScore_m2083072619 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_FirstScore(System.Int32)
extern "C"  void Code_set_FirstScore_m2786173910 (Code_t686167511 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Code::get_RescanScore()
extern "C"  int32_t Code_get_RescanScore_m1161558061 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_RescanScore(System.Int32)
extern "C"  void Code_set_RescanScore_m4128397936 (Code_t686167511 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Code::get_SelfFirstScore()
extern "C"  int32_t Code_get_SelfFirstScore_m1242725883 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_SelfFirstScore(System.Int32)
extern "C"  void Code_set_SelfFirstScore_m3028835264 (Code_t686167511 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Code::get_SelfRescanScore()
extern "C"  int32_t Code_get_SelfRescanScore_m3591771337 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_SelfRescanScore(System.Int32)
extern "C"  void Code_set_SelfRescanScore_m4123305542 (Code_t686167511 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Code::get_Version()
extern "C"  int32_t Code_get_Version_m3509325791 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_Version(System.Int32)
extern "C"  void Code_set_Version_m2234166874 (Code_t686167511 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_ForMonth()
extern "C"  String_t* Code_get_ForMonth_m332557661 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_ForMonth(System.String)
extern "C"  void Code_set_ForMonth_m2433585478 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_First()
extern "C"  String_t* Code_get_First_m567525038 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_First(System.String)
extern "C"  void Code_set_First_m1107183163 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_SelfFirst()
extern "C"  String_t* Code_get_SelfFirst_m3867599120 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_SelfFirst(System.String)
extern "C"  void Code_set_SelfFirst_m115556651 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_Location()
extern "C"  String_t* Code_get_Location_m3330705855 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_Location(System.String)
extern "C"  void Code_set_Location_m1003279798 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Code::get_PoliteFirst()
extern "C"  String_t* Code_get_PoliteFirst_m3092122503 (Code_t686167511 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Code::set_PoliteFirst(System.String)
extern "C"  void Code_set_PoliteFirst_m3810740632 (Code_t686167511 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
