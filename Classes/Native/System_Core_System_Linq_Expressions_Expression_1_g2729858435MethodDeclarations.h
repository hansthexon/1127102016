﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.Expression`1<System.Object>
struct Expression_1_t2729858435;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>
struct ReadOnlyCollection_1_t3201290647;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_Expressions_Expression114864668.h"

// System.Void System.Linq.Expressions.Expression`1<System.Object>::.ctor(System.Linq.Expressions.Expression,System.Collections.ObjectModel.ReadOnlyCollection`1<System.Linq.Expressions.ParameterExpression>)
extern "C"  void Expression_1__ctor_m2373241160_gshared (Expression_1_t2729858435 * __this, Expression_t114864668 * ___body0, ReadOnlyCollection_1_t3201290647 * ___parameters1, const MethodInfo* method);
#define Expression_1__ctor_m2373241160(__this, ___body0, ___parameters1, method) ((  void (*) (Expression_1_t2729858435 *, Expression_t114864668 *, ReadOnlyCollection_1_t3201290647 *, const MethodInfo*))Expression_1__ctor_m2373241160_gshared)(__this, ___body0, ___parameters1, method)
