﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LitJson.JsonMapper/<RegisterFactory>c__AnonStorey1D`1<System.Object>
struct U3CRegisterFactoryU3Ec__AnonStorey1D_1_t955442741;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void LitJson.JsonMapper/<RegisterFactory>c__AnonStorey1D`1<System.Object>::.ctor()
extern "C"  void U3CRegisterFactoryU3Ec__AnonStorey1D_1__ctor_m1595076882_gshared (U3CRegisterFactoryU3Ec__AnonStorey1D_1_t955442741 * __this, const MethodInfo* method);
#define U3CRegisterFactoryU3Ec__AnonStorey1D_1__ctor_m1595076882(__this, method) ((  void (*) (U3CRegisterFactoryU3Ec__AnonStorey1D_1_t955442741 *, const MethodInfo*))U3CRegisterFactoryU3Ec__AnonStorey1D_1__ctor_m1595076882_gshared)(__this, method)
// System.Object LitJson.JsonMapper/<RegisterFactory>c__AnonStorey1D`1<System.Object>::<>m__34()
extern "C"  Il2CppObject * U3CRegisterFactoryU3Ec__AnonStorey1D_1_U3CU3Em__34_m1776693373_gshared (U3CRegisterFactoryU3Ec__AnonStorey1D_1_t955442741 * __this, const MethodInfo* method);
#define U3CRegisterFactoryU3Ec__AnonStorey1D_1_U3CU3Em__34_m1776693373(__this, method) ((  Il2CppObject * (*) (U3CRegisterFactoryU3Ec__AnonStorey1D_1_t955442741 *, const MethodInfo*))U3CRegisterFactoryU3Ec__AnonStorey1D_1_U3CU3Em__34_m1776693373_gshared)(__this, method)
