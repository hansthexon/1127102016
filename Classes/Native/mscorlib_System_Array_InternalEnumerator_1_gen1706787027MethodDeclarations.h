﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1706787027.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I848034765.h"

// System.Void System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1550456612_gshared (InternalEnumerator_1_t1706787027 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1550456612(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t1706787027 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1550456612_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1804723080_gshared (InternalEnumerator_1_t1706787027 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1804723080(__this, method) ((  void (*) (InternalEnumerator_1_t1706787027 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1804723080_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21543446_gshared (InternalEnumerator_1_t1706787027 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21543446(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t1706787027 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m21543446_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m397456233_gshared (InternalEnumerator_1_t1706787027 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m397456233(__this, method) ((  void (*) (InternalEnumerator_1_t1706787027 *, const MethodInfo*))InternalEnumerator_1_Dispose_m397456233_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1246711028_gshared (InternalEnumerator_1_t1706787027 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1246711028(__this, method) ((  bool (*) (InternalEnumerator_1_t1706787027 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1246711028_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexInfo>::get_Current()
extern "C"  IndexInfo_t848034765  InternalEnumerator_1_get_Current_m204930273_gshared (InternalEnumerator_1_t1706787027 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m204930273(__this, method) ((  IndexInfo_t848034765  (*) (InternalEnumerator_1_t1706787027 *, const MethodInfo*))InternalEnumerator_1_get_Current_m204930273_gshared)(__this, method)
