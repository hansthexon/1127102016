﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.LinkedListNode`1<System.Int32>
struct LinkedListNode_1_t967983361;
// System.Collections.Generic.LinkedList`1<System.Int32>
struct LinkedList_1_t2376585677;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m3717885607_gshared (LinkedListNode_1_t967983361 * __this, LinkedList_1_t2376585677 * ___list0, int32_t ___value1, const MethodInfo* method);
#define LinkedListNode_1__ctor_m3717885607(__this, ___list0, ___value1, method) ((  void (*) (LinkedListNode_1_t967983361 *, LinkedList_1_t2376585677 *, int32_t, const MethodInfo*))LinkedListNode_1__ctor_m3717885607_gshared)(__this, ___list0, ___value1, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m1109227571_gshared (LinkedListNode_1_t967983361 * __this, LinkedList_1_t2376585677 * ___list0, int32_t ___value1, LinkedListNode_1_t967983361 * ___previousNode2, LinkedListNode_1_t967983361 * ___nextNode3, const MethodInfo* method);
#define LinkedListNode_1__ctor_m1109227571(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method) ((  void (*) (LinkedListNode_1_t967983361 *, LinkedList_1_t2376585677 *, int32_t, LinkedListNode_1_t967983361 *, LinkedListNode_1_t967983361 *, const MethodInfo*))LinkedListNode_1__ctor_m1109227571_gshared)(__this, ___list0, ___value1, ___previousNode2, ___nextNode3, method)
// System.Void System.Collections.Generic.LinkedListNode`1<System.Int32>::Detach()
extern "C"  void LinkedListNode_1_Detach_m3376839027_gshared (LinkedListNode_1_t967983361 * __this, const MethodInfo* method);
#define LinkedListNode_1_Detach_m3376839027(__this, method) ((  void (*) (LinkedListNode_1_t967983361 *, const MethodInfo*))LinkedListNode_1_Detach_m3376839027_gshared)(__this, method)
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_List()
extern "C"  LinkedList_1_t2376585677 * LinkedListNode_1_get_List_m934609283_gshared (LinkedListNode_1_t967983361 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_List_m934609283(__this, method) ((  LinkedList_1_t2376585677 * (*) (LinkedListNode_1_t967983361 *, const MethodInfo*))LinkedListNode_1_get_List_m934609283_gshared)(__this, method)
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Next()
extern "C"  LinkedListNode_1_t967983361 * LinkedListNode_1_get_Next_m1225954497_gshared (LinkedListNode_1_t967983361 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Next_m1225954497(__this, method) ((  LinkedListNode_1_t967983361 * (*) (LinkedListNode_1_t967983361 *, const MethodInfo*))LinkedListNode_1_get_Next_m1225954497_gshared)(__this, method)
// T System.Collections.Generic.LinkedListNode`1<System.Int32>::get_Value()
extern "C"  int32_t LinkedListNode_1_get_Value_m4037355866_gshared (LinkedListNode_1_t967983361 * __this, const MethodInfo* method);
#define LinkedListNode_1_get_Value_m4037355866(__this, method) ((  int32_t (*) (LinkedListNode_1_t967983361 *, const MethodInfo*))LinkedListNode_1_get_Value_m4037355866_gshared)(__this, method)
