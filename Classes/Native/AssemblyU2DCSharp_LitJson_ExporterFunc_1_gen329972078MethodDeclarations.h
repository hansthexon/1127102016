﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_LitJson_ExporterFunc_1_gen3926562242MethodDeclarations.h"

// System.Void LitJson.ExporterFunc`1<UnityEngine.RectOffset>::.ctor(System.Object,System.IntPtr)
#define ExporterFunc_1__ctor_m244348314(__this, ___object0, ___method1, method) ((  void (*) (ExporterFunc_1_t329972078 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ExporterFunc_1__ctor_m104989128_gshared)(__this, ___object0, ___method1, method)
// System.Void LitJson.ExporterFunc`1<UnityEngine.RectOffset>::Invoke(T,LitJson.JsonWriter)
#define ExporterFunc_1_Invoke_m859709522(__this, ___obj0, ___writer1, method) ((  void (*) (ExporterFunc_1_t329972078 *, RectOffset_t3387826427 *, JsonWriter_t1927598499 *, const MethodInfo*))ExporterFunc_1_Invoke_m1089798616_gshared)(__this, ___obj0, ___writer1, method)
// System.IAsyncResult LitJson.ExporterFunc`1<UnityEngine.RectOffset>::BeginInvoke(T,LitJson.JsonWriter,System.AsyncCallback,System.Object)
#define ExporterFunc_1_BeginInvoke_m866306379(__this, ___obj0, ___writer1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (ExporterFunc_1_t329972078 *, RectOffset_t3387826427 *, JsonWriter_t1927598499 *, AsyncCallback_t163412349 *, Il2CppObject *, const MethodInfo*))ExporterFunc_1_BeginInvoke_m729158479_gshared)(__this, ___obj0, ___writer1, ___callback2, ___object3, method)
// System.Void LitJson.ExporterFunc`1<UnityEngine.RectOffset>::EndInvoke(System.IAsyncResult)
#define ExporterFunc_1_EndInvoke_m3546683016(__this, ___result0, method) ((  void (*) (ExporterFunc_1_t329972078 *, Il2CppObject *, const MethodInfo*))ExporterFunc_1_EndInvoke_m2442068774_gshared)(__this, ___result0, method)
