﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataService
struct DataService_t2602786891;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Person>
struct IEnumerable_1_t3534044808;
// System.Collections.Generic.IEnumerable`1<Album>
struct IEnumerable_1_t1434645190;
// Person
struct Person_t3241917763;
// Code
struct Code_t686167511;
// RoomObject
struct RoomObject_t2982870316;
// Album
struct Album_t1142518145;
// System.Collections.Generic.IEnumerable`1<Code>
struct IEnumerable_1_t978294556;
// System.Collections.Generic.IEnumerable`1<RoomObject>
struct IEnumerable_1_t3274997361;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"
#include "AssemblyU2DCSharp_Code686167511.h"
#include "AssemblyU2DCSharp_RoomObject2982870316.h"

// System.Void DataService::.ctor(System.String)
extern "C"  void DataService__ctor_m1178386708 (DataService_t2602786891 * __this, String_t* ___DatabaseName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataService::CreateDB()
extern "C"  void DataService_CreateDB_m4222886094 (DataService_t2602786891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Person> DataService::GetPersons()
extern "C"  Il2CppObject* DataService_GetPersons_m2482766615 (DataService_t2602786891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Album> DataService::getALbum()
extern "C"  Il2CppObject* DataService_getALbum_m1648593864 (DataService_t2602786891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Person> DataService::GetPersonsNamedRoberto()
extern "C"  Il2CppObject* DataService_GetPersonsNamedRoberto_m3646699563 (DataService_t2602786891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Person DataService::GetJohnny()
extern "C"  Person_t3241917763 * DataService_GetJohnny_m428303022 (DataService_t2602786891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataService::UpdateFirst(Code)
extern "C"  void DataService_UpdateFirst_m3930421044 (DataService_t2602786891 * __this, Code_t686167511 * ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataService::UpdateRoomObjects(RoomObject)
extern "C"  void DataService_UpdateRoomObjects_m1526908968 (DataService_t2602786891 * __this, RoomObject_t2982870316 * ___robjects0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Album DataService::getartist()
extern "C"  Album_t1142518145 * DataService_getartist_m2530411173 (DataService_t2602786891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Code DataService::getVideo(System.String)
extern "C"  Code_t686167511 * DataService_getVideo_m2840148405 (DataService_t2602786891 * __this, String_t* ___code0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Code> DataService::GetPurchased()
extern "C"  Il2CppObject* DataService_GetPurchased_m2580769932 (DataService_t2602786891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Code> DataService::GetallCodes()
extern "C"  Il2CppObject* DataService_GetallCodes_m1402078414 (DataService_t2602786891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<RoomObject> DataService::GetUnclocked()
extern "C"  Il2CppObject* DataService_GetUnclocked_m1627237140 (DataService_t2602786891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RoomObject DataService::GetCurrentUnclocked()
extern "C"  RoomObject_t2982870316 * DataService_GetCurrentUnclocked_m2974009892 (DataService_t2602786891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Person DataService::CreatePerson()
extern "C"  Person_t3241917763 * DataService_CreatePerson_m1871168869 (DataService_t2602786891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
