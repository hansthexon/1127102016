﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// UnityEngine.Purchasing.Security.SignerInfo
struct SignerInfo_t4122348804;
// UnityEngine.Purchasing.Security.X509Cert
struct X509Cert_t481809278;
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt
struct AppleInAppPurchaseReceipt_t3271698749;
// UnityEngine.Purchasing.Security.IPurchaseReceipt
struct IPurchaseReceipt_t2402701844;

#include "mscorlib_System_Array3829468939.h"
#include "Security_UnityEngine_Purchasing_Security_SignerInf4122348804.h"
#include "Security_UnityEngine_Purchasing_Security_X509Cert481809278.h"
#include "Security_UnityEngine_Purchasing_Security_AppleInAp3271698749.h"

#pragma once
// UnityEngine.Purchasing.Security.SignerInfo[]
struct SignerInfoU5BU5D_t2363599469  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) SignerInfo_t4122348804 * m_Items[1];

public:
	inline SignerInfo_t4122348804 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline SignerInfo_t4122348804 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, SignerInfo_t4122348804 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Purchasing.Security.X509Cert[]
struct X509CertU5BU5D_t908306635  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) X509Cert_t481809278 * m_Items[1];

public:
	inline X509Cert_t481809278 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline X509Cert_t481809278 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, X509Cert_t481809278 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Purchasing.Security.AppleInAppPurchaseReceipt[]
struct AppleInAppPurchaseReceiptU5BU5D_t579068208  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) AppleInAppPurchaseReceipt_t3271698749 * m_Items[1];

public:
	inline AppleInAppPurchaseReceipt_t3271698749 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline AppleInAppPurchaseReceipt_t3271698749 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, AppleInAppPurchaseReceipt_t3271698749 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Purchasing.Security.IPurchaseReceipt[]
struct IPurchaseReceiptU5BU5D_t988864285  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
