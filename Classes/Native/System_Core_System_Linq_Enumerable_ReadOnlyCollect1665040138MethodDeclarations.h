﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/ReadOnlyCollectionOf`1<System.Object>::.cctor()
extern "C"  void ReadOnlyCollectionOf_1__cctor_m107455798_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ReadOnlyCollectionOf_1__cctor_m107455798(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ReadOnlyCollectionOf_1__cctor_m107455798_gshared)(__this /* static, unused */, method)
