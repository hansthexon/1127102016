﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataService/<getVideo>c__AnonStorey12
struct U3CgetVideoU3Ec__AnonStorey12_t1692944193;

#include "codegen/il2cpp-codegen.h"

// System.Void DataService/<getVideo>c__AnonStorey12::.ctor()
extern "C"  void U3CgetVideoU3Ec__AnonStorey12__ctor_m2583375890 (U3CgetVideoU3Ec__AnonStorey12_t1692944193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
