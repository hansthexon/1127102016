﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestScript
struct TestScript_t905662927;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void TestScript::.ctor()
extern "C"  void TestScript__ctor_m1586702086 (TestScript_t905662927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestScript::Start()
extern "C"  void TestScript_Start_m3348685306 (TestScript_t905662927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestScript::Update()
extern "C"  void TestScript_Update_m4230496631 (TestScript_t905662927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator TestScript::Test()
extern "C"  Il2CppObject * TestScript_Test_m2739197092 (TestScript_t905662927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
