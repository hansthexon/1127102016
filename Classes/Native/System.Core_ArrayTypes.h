﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Linq.Expressions.ParameterExpression
struct ParameterExpression_t3015504955;
// System.Linq.Expressions.Expression
struct Expression_t114864668;
// System.Linq.Expressions.MemberBinding
struct MemberBinding_t3487798541;
// System.Linq.Expressions.ElementInit
struct ElementInit_t3898206436;
// System.Action
struct Action_t3226471752;

#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Linq_Expressions_ParameterExpre3015504955.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li865133271.h"
#include "System_Core_System_Linq_Expressions_Expression114864668.h"
#include "System_Core_System_Linq_Expressions_MemberBinding3487798541.h"
#include "System_Core_System_Linq_Expressions_ElementInit3898206436.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li118159244.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3674339243.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li116960033.h"
#include "System_Core_System_Action3226471752.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L3738892376.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L2866035586.h"
#include "System_Core_System_Collections_Generic_HashSet_1_L1908629703.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li863261303.h"
#include "System_Core_System_Collections_Generic_HashSet_1_Li204904209.h"

#pragma once
// System.Linq.Expressions.ParameterExpression[]
struct ParameterExpressionU5BU5D_t1255163002  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ParameterExpression_t3015504955 * m_Items[1];

public:
	inline ParameterExpression_t3015504955 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ParameterExpression_t3015504955 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ParameterExpression_t3015504955 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.HashSet`1/Link<System.Object>[]
struct LinkU5BU5D_t3597933550  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t865133271  m_Items[1];

public:
	inline Link_t865133271  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t865133271 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t865133271  value)
	{
		m_Items[index] = value;
	}
};
// System.Linq.Expressions.Expression[]
struct ExpressionU5BU5D_t4218295797  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Expression_t114864668 * m_Items[1];

public:
	inline Expression_t114864668 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Expression_t114864668 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Expression_t114864668 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Linq.Expressions.MemberBinding[]
struct MemberBindingU5BU5D_t3602168864  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) MemberBinding_t3487798541 * m_Items[1];

public:
	inline MemberBinding_t3487798541 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MemberBinding_t3487798541 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MemberBinding_t3487798541 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Linq.Expressions.ElementInit[]
struct ElementInitU5BU5D_t150110989  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ElementInit_t3898206436 * m_Items[1];

public:
	inline ElementInit_t3898206436 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline ElementInit_t3898206436 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, ElementInit_t3898206436 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.HashSet`1/Link<UnityEngine.Purchasing.ProductDefinition>[]
struct LinkU5BU5D_t96492741  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t118159244  m_Items[1];

public:
	inline Link_t118159244  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t118159244 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t118159244  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<UnityEngine.Purchasing.Product>[]
struct LinkU5BU5D_t711398730  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t3674339243  m_Items[1];

public:
	inline Link_t3674339243  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t3674339243 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t3674339243  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<UnityEngine.UI.IClippable>[]
struct LinkU5BU5D_t222250812  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t116960033  m_Items[1];

public:
	inline Link_t116960033  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t116960033 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t116960033  value)
	{
		m_Items[index] = value;
	}
};
// System.Action[]
struct ActionU5BU5D_t87223449  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Action_t3226471752 * m_Items[1];

public:
	inline Action_t3226471752 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Action_t3226471752 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Action_t3226471752 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Collections.Generic.HashSet`1/Link<UnityEngine.MeshRenderer>[]
struct LinkU5BU5D_t1550459721  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t3738892376  m_Items[1];

public:
	inline Link_t3738892376  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t3738892376 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t3738892376  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<Vuforia.VideoBackgroundAbstractBehaviour>[]
struct LinkU5BU5D_t3324718135  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t2866035586  m_Items[1];

public:
	inline Link_t2866035586  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t2866035586 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t2866035586  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<Vuforia.BackgroundPlaneAbstractBehaviour>[]
struct LinkU5BU5D_t1001895998  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t1908629703  m_Items[1];

public:
	inline Link_t1908629703  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t1908629703 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t1908629703  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<Vuforia.HideExcessAreaAbstractBehaviour>[]
struct LinkU5BU5D_t753392078  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t863261303  m_Items[1];

public:
	inline Link_t863261303  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t863261303 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t863261303  value)
	{
		m_Items[index] = value;
	}
};
// System.Collections.Generic.HashSet`1/Link<System.String>[]
struct LinkU5BU5D_t1625685388  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t204904209  m_Items[1];

public:
	inline Link_t204904209  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Link_t204904209 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Link_t204904209  value)
	{
		m_Items[index] = value;
	}
};
