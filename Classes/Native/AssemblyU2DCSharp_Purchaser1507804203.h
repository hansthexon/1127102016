﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t92554892;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t2460996543;
// UnityEngine.WWW
struct WWW_t2919945039;
// UnityEngine.Purchasing.Security.CrossPlatformValidator
struct CrossPlatformValidator_t305796431;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.UI.Text[]
struct TextU5BU5D_t4216439300;
// System.Action`1<System.Boolean>
struct Action_1_t3627374100;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t3986656710;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Purchaser
struct  Purchaser_t1507804203  : public MonoBehaviour_t1158329972
{
public:
	// System.String Purchaser::_day
	String_t* ____day_2;
	// System.String Purchaser::_year
	String_t* ____year_3;
	// System.String Purchaser::_month
	String_t* ____month_4;
	// System.String Purchaser::_cname
	String_t* ____cname_5;
	// System.String Purchaser::_pname
	String_t* ____pname_6;
	// System.String Purchaser::_emailaddress
	String_t* ____emailaddress_7;
	// System.String Purchaser::_add
	String_t* ____add_8;
	// System.Boolean Purchaser::canupload
	bool ___canupload_9;
	// System.String Purchaser::expDate
	String_t* ___expDate_27;
	// System.String Purchaser::PackName
	String_t* ___PackName_28;
	// System.String Purchaser::packdeatils
	String_t* ___packdeatils_29;
	// UnityEngine.WWW Purchaser::www
	WWW_t2919945039 * ___www_30;
	// UnityEngine.Purchasing.Security.CrossPlatformValidator Purchaser::validator
	CrossPlatformValidator_t305796431 * ___validator_31;
	// System.String Purchaser::setpackname
	String_t* ___setpackname_32;
	// UnityEngine.UI.Text Purchaser::child
	Text_t356221433 * ___child_33;
	// UnityEngine.UI.Text Purchaser::pname
	Text_t356221433 * ___pname_34;
	// UnityEngine.UI.Text Purchaser::email
	Text_t356221433 * ___email_35;
	// UnityEngine.UI.Text Purchaser::add
	Text_t356221433 * ___add_36;
	// UnityEngine.UI.Text Purchaser::day
	Text_t356221433 * ___day_37;
	// UnityEngine.UI.Text Purchaser::month
	Text_t356221433 * ___month_38;
	// UnityEngine.UI.Text Purchaser::year
	Text_t356221433 * ___year_39;
	// UnityEngine.UI.Text Purchaser::scode
	Text_t356221433 * ___scode_40;
	// System.String Purchaser::Platforms
	String_t* ___Platforms_41;
	// System.String Purchaser::schooolscode
	String_t* ___schooolscode_42;
	// UnityEngine.UI.Text[] Purchaser::txtstobecheck
	TextU5BU5D_t4216439300* ___txtstobecheck_43;

public:
	inline static int32_t get_offset_of__day_2() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ____day_2)); }
	inline String_t* get__day_2() const { return ____day_2; }
	inline String_t** get_address_of__day_2() { return &____day_2; }
	inline void set__day_2(String_t* value)
	{
		____day_2 = value;
		Il2CppCodeGenWriteBarrier(&____day_2, value);
	}

	inline static int32_t get_offset_of__year_3() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ____year_3)); }
	inline String_t* get__year_3() const { return ____year_3; }
	inline String_t** get_address_of__year_3() { return &____year_3; }
	inline void set__year_3(String_t* value)
	{
		____year_3 = value;
		Il2CppCodeGenWriteBarrier(&____year_3, value);
	}

	inline static int32_t get_offset_of__month_4() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ____month_4)); }
	inline String_t* get__month_4() const { return ____month_4; }
	inline String_t** get_address_of__month_4() { return &____month_4; }
	inline void set__month_4(String_t* value)
	{
		____month_4 = value;
		Il2CppCodeGenWriteBarrier(&____month_4, value);
	}

	inline static int32_t get_offset_of__cname_5() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ____cname_5)); }
	inline String_t* get__cname_5() const { return ____cname_5; }
	inline String_t** get_address_of__cname_5() { return &____cname_5; }
	inline void set__cname_5(String_t* value)
	{
		____cname_5 = value;
		Il2CppCodeGenWriteBarrier(&____cname_5, value);
	}

	inline static int32_t get_offset_of__pname_6() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ____pname_6)); }
	inline String_t* get__pname_6() const { return ____pname_6; }
	inline String_t** get_address_of__pname_6() { return &____pname_6; }
	inline void set__pname_6(String_t* value)
	{
		____pname_6 = value;
		Il2CppCodeGenWriteBarrier(&____pname_6, value);
	}

	inline static int32_t get_offset_of__emailaddress_7() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ____emailaddress_7)); }
	inline String_t* get__emailaddress_7() const { return ____emailaddress_7; }
	inline String_t** get_address_of__emailaddress_7() { return &____emailaddress_7; }
	inline void set__emailaddress_7(String_t* value)
	{
		____emailaddress_7 = value;
		Il2CppCodeGenWriteBarrier(&____emailaddress_7, value);
	}

	inline static int32_t get_offset_of__add_8() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ____add_8)); }
	inline String_t* get__add_8() const { return ____add_8; }
	inline String_t** get_address_of__add_8() { return &____add_8; }
	inline void set__add_8(String_t* value)
	{
		____add_8 = value;
		Il2CppCodeGenWriteBarrier(&____add_8, value);
	}

	inline static int32_t get_offset_of_canupload_9() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___canupload_9)); }
	inline bool get_canupload_9() const { return ___canupload_9; }
	inline bool* get_address_of_canupload_9() { return &___canupload_9; }
	inline void set_canupload_9(bool value)
	{
		___canupload_9 = value;
	}

	inline static int32_t get_offset_of_expDate_27() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___expDate_27)); }
	inline String_t* get_expDate_27() const { return ___expDate_27; }
	inline String_t** get_address_of_expDate_27() { return &___expDate_27; }
	inline void set_expDate_27(String_t* value)
	{
		___expDate_27 = value;
		Il2CppCodeGenWriteBarrier(&___expDate_27, value);
	}

	inline static int32_t get_offset_of_PackName_28() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___PackName_28)); }
	inline String_t* get_PackName_28() const { return ___PackName_28; }
	inline String_t** get_address_of_PackName_28() { return &___PackName_28; }
	inline void set_PackName_28(String_t* value)
	{
		___PackName_28 = value;
		Il2CppCodeGenWriteBarrier(&___PackName_28, value);
	}

	inline static int32_t get_offset_of_packdeatils_29() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___packdeatils_29)); }
	inline String_t* get_packdeatils_29() const { return ___packdeatils_29; }
	inline String_t** get_address_of_packdeatils_29() { return &___packdeatils_29; }
	inline void set_packdeatils_29(String_t* value)
	{
		___packdeatils_29 = value;
		Il2CppCodeGenWriteBarrier(&___packdeatils_29, value);
	}

	inline static int32_t get_offset_of_www_30() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___www_30)); }
	inline WWW_t2919945039 * get_www_30() const { return ___www_30; }
	inline WWW_t2919945039 ** get_address_of_www_30() { return &___www_30; }
	inline void set_www_30(WWW_t2919945039 * value)
	{
		___www_30 = value;
		Il2CppCodeGenWriteBarrier(&___www_30, value);
	}

	inline static int32_t get_offset_of_validator_31() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___validator_31)); }
	inline CrossPlatformValidator_t305796431 * get_validator_31() const { return ___validator_31; }
	inline CrossPlatformValidator_t305796431 ** get_address_of_validator_31() { return &___validator_31; }
	inline void set_validator_31(CrossPlatformValidator_t305796431 * value)
	{
		___validator_31 = value;
		Il2CppCodeGenWriteBarrier(&___validator_31, value);
	}

	inline static int32_t get_offset_of_setpackname_32() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___setpackname_32)); }
	inline String_t* get_setpackname_32() const { return ___setpackname_32; }
	inline String_t** get_address_of_setpackname_32() { return &___setpackname_32; }
	inline void set_setpackname_32(String_t* value)
	{
		___setpackname_32 = value;
		Il2CppCodeGenWriteBarrier(&___setpackname_32, value);
	}

	inline static int32_t get_offset_of_child_33() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___child_33)); }
	inline Text_t356221433 * get_child_33() const { return ___child_33; }
	inline Text_t356221433 ** get_address_of_child_33() { return &___child_33; }
	inline void set_child_33(Text_t356221433 * value)
	{
		___child_33 = value;
		Il2CppCodeGenWriteBarrier(&___child_33, value);
	}

	inline static int32_t get_offset_of_pname_34() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___pname_34)); }
	inline Text_t356221433 * get_pname_34() const { return ___pname_34; }
	inline Text_t356221433 ** get_address_of_pname_34() { return &___pname_34; }
	inline void set_pname_34(Text_t356221433 * value)
	{
		___pname_34 = value;
		Il2CppCodeGenWriteBarrier(&___pname_34, value);
	}

	inline static int32_t get_offset_of_email_35() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___email_35)); }
	inline Text_t356221433 * get_email_35() const { return ___email_35; }
	inline Text_t356221433 ** get_address_of_email_35() { return &___email_35; }
	inline void set_email_35(Text_t356221433 * value)
	{
		___email_35 = value;
		Il2CppCodeGenWriteBarrier(&___email_35, value);
	}

	inline static int32_t get_offset_of_add_36() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___add_36)); }
	inline Text_t356221433 * get_add_36() const { return ___add_36; }
	inline Text_t356221433 ** get_address_of_add_36() { return &___add_36; }
	inline void set_add_36(Text_t356221433 * value)
	{
		___add_36 = value;
		Il2CppCodeGenWriteBarrier(&___add_36, value);
	}

	inline static int32_t get_offset_of_day_37() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___day_37)); }
	inline Text_t356221433 * get_day_37() const { return ___day_37; }
	inline Text_t356221433 ** get_address_of_day_37() { return &___day_37; }
	inline void set_day_37(Text_t356221433 * value)
	{
		___day_37 = value;
		Il2CppCodeGenWriteBarrier(&___day_37, value);
	}

	inline static int32_t get_offset_of_month_38() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___month_38)); }
	inline Text_t356221433 * get_month_38() const { return ___month_38; }
	inline Text_t356221433 ** get_address_of_month_38() { return &___month_38; }
	inline void set_month_38(Text_t356221433 * value)
	{
		___month_38 = value;
		Il2CppCodeGenWriteBarrier(&___month_38, value);
	}

	inline static int32_t get_offset_of_year_39() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___year_39)); }
	inline Text_t356221433 * get_year_39() const { return ___year_39; }
	inline Text_t356221433 ** get_address_of_year_39() { return &___year_39; }
	inline void set_year_39(Text_t356221433 * value)
	{
		___year_39 = value;
		Il2CppCodeGenWriteBarrier(&___year_39, value);
	}

	inline static int32_t get_offset_of_scode_40() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___scode_40)); }
	inline Text_t356221433 * get_scode_40() const { return ___scode_40; }
	inline Text_t356221433 ** get_address_of_scode_40() { return &___scode_40; }
	inline void set_scode_40(Text_t356221433 * value)
	{
		___scode_40 = value;
		Il2CppCodeGenWriteBarrier(&___scode_40, value);
	}

	inline static int32_t get_offset_of_Platforms_41() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___Platforms_41)); }
	inline String_t* get_Platforms_41() const { return ___Platforms_41; }
	inline String_t** get_address_of_Platforms_41() { return &___Platforms_41; }
	inline void set_Platforms_41(String_t* value)
	{
		___Platforms_41 = value;
		Il2CppCodeGenWriteBarrier(&___Platforms_41, value);
	}

	inline static int32_t get_offset_of_schooolscode_42() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___schooolscode_42)); }
	inline String_t* get_schooolscode_42() const { return ___schooolscode_42; }
	inline String_t** get_address_of_schooolscode_42() { return &___schooolscode_42; }
	inline void set_schooolscode_42(String_t* value)
	{
		___schooolscode_42 = value;
		Il2CppCodeGenWriteBarrier(&___schooolscode_42, value);
	}

	inline static int32_t get_offset_of_txtstobecheck_43() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203, ___txtstobecheck_43)); }
	inline TextU5BU5D_t4216439300* get_txtstobecheck_43() const { return ___txtstobecheck_43; }
	inline TextU5BU5D_t4216439300** get_address_of_txtstobecheck_43() { return &___txtstobecheck_43; }
	inline void set_txtstobecheck_43(TextU5BU5D_t4216439300* value)
	{
		___txtstobecheck_43 = value;
		Il2CppCodeGenWriteBarrier(&___txtstobecheck_43, value);
	}
};

struct Purchaser_t1507804203_StaticFields
{
public:
	// UnityEngine.Purchasing.IStoreController Purchaser::m_StoreController
	Il2CppObject * ___m_StoreController_10;
	// UnityEngine.Purchasing.IExtensionProvider Purchaser::m_StoreExtensionProvider
	Il2CppObject * ___m_StoreExtensionProvider_11;
	// System.String Purchaser::kProductIDConsumable
	String_t* ___kProductIDConsumable_12;
	// System.String Purchaser::kProductIDNonConsumable
	String_t* ___kProductIDNonConsumable_13;
	// System.String Purchaser::kProductIDSubscription
	String_t* ___kProductIDSubscription_14;
	// System.String Purchaser::kProductIDSubscription24
	String_t* ___kProductIDSubscription24_15;
	// System.String Purchaser::kProductIDSubscription36
	String_t* ___kProductIDSubscription36_16;
	// System.String Purchaser::kProductNameAppleConsumable
	String_t* ___kProductNameAppleConsumable_17;
	// System.String Purchaser::kProductNameAppleNonConsumable
	String_t* ___kProductNameAppleNonConsumable_18;
	// System.String Purchaser::kProductNameAppleSubscription
	String_t* ___kProductNameAppleSubscription_19;
	// System.String Purchaser::kProductNameAppleSubscription24
	String_t* ___kProductNameAppleSubscription24_20;
	// System.String Purchaser::kProductNameAppleSubscription36
	String_t* ___kProductNameAppleSubscription36_21;
	// System.String Purchaser::kProductNameGooglePlayConsumable
	String_t* ___kProductNameGooglePlayConsumable_22;
	// System.String Purchaser::kProductNameGooglePlayNonConsumable
	String_t* ___kProductNameGooglePlayNonConsumable_23;
	// System.String Purchaser::kProductNameGooglePlaySubscription
	String_t* ___kProductNameGooglePlaySubscription_24;
	// System.String Purchaser::kProductNameGooglePlaySubscription24
	String_t* ___kProductNameGooglePlaySubscription24_25;
	// System.String Purchaser::kProductNameGooglePlaySubscription36
	String_t* ___kProductNameGooglePlaySubscription36_26;
	// System.Action`1<System.Boolean> Purchaser::<>f__am$cache2A
	Action_1_t3627374100 * ___U3CU3Ef__amU24cache2A_44;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Purchaser::<>f__switch$map0
	Dictionary_2_t3986656710 * ___U3CU3Ef__switchU24map0_45;

public:
	inline static int32_t get_offset_of_m_StoreController_10() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___m_StoreController_10)); }
	inline Il2CppObject * get_m_StoreController_10() const { return ___m_StoreController_10; }
	inline Il2CppObject ** get_address_of_m_StoreController_10() { return &___m_StoreController_10; }
	inline void set_m_StoreController_10(Il2CppObject * value)
	{
		___m_StoreController_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_StoreController_10, value);
	}

	inline static int32_t get_offset_of_m_StoreExtensionProvider_11() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___m_StoreExtensionProvider_11)); }
	inline Il2CppObject * get_m_StoreExtensionProvider_11() const { return ___m_StoreExtensionProvider_11; }
	inline Il2CppObject ** get_address_of_m_StoreExtensionProvider_11() { return &___m_StoreExtensionProvider_11; }
	inline void set_m_StoreExtensionProvider_11(Il2CppObject * value)
	{
		___m_StoreExtensionProvider_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_StoreExtensionProvider_11, value);
	}

	inline static int32_t get_offset_of_kProductIDConsumable_12() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductIDConsumable_12)); }
	inline String_t* get_kProductIDConsumable_12() const { return ___kProductIDConsumable_12; }
	inline String_t** get_address_of_kProductIDConsumable_12() { return &___kProductIDConsumable_12; }
	inline void set_kProductIDConsumable_12(String_t* value)
	{
		___kProductIDConsumable_12 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDConsumable_12, value);
	}

	inline static int32_t get_offset_of_kProductIDNonConsumable_13() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductIDNonConsumable_13)); }
	inline String_t* get_kProductIDNonConsumable_13() const { return ___kProductIDNonConsumable_13; }
	inline String_t** get_address_of_kProductIDNonConsumable_13() { return &___kProductIDNonConsumable_13; }
	inline void set_kProductIDNonConsumable_13(String_t* value)
	{
		___kProductIDNonConsumable_13 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDNonConsumable_13, value);
	}

	inline static int32_t get_offset_of_kProductIDSubscription_14() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductIDSubscription_14)); }
	inline String_t* get_kProductIDSubscription_14() const { return ___kProductIDSubscription_14; }
	inline String_t** get_address_of_kProductIDSubscription_14() { return &___kProductIDSubscription_14; }
	inline void set_kProductIDSubscription_14(String_t* value)
	{
		___kProductIDSubscription_14 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDSubscription_14, value);
	}

	inline static int32_t get_offset_of_kProductIDSubscription24_15() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductIDSubscription24_15)); }
	inline String_t* get_kProductIDSubscription24_15() const { return ___kProductIDSubscription24_15; }
	inline String_t** get_address_of_kProductIDSubscription24_15() { return &___kProductIDSubscription24_15; }
	inline void set_kProductIDSubscription24_15(String_t* value)
	{
		___kProductIDSubscription24_15 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDSubscription24_15, value);
	}

	inline static int32_t get_offset_of_kProductIDSubscription36_16() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductIDSubscription36_16)); }
	inline String_t* get_kProductIDSubscription36_16() const { return ___kProductIDSubscription36_16; }
	inline String_t** get_address_of_kProductIDSubscription36_16() { return &___kProductIDSubscription36_16; }
	inline void set_kProductIDSubscription36_16(String_t* value)
	{
		___kProductIDSubscription36_16 = value;
		Il2CppCodeGenWriteBarrier(&___kProductIDSubscription36_16, value);
	}

	inline static int32_t get_offset_of_kProductNameAppleConsumable_17() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameAppleConsumable_17)); }
	inline String_t* get_kProductNameAppleConsumable_17() const { return ___kProductNameAppleConsumable_17; }
	inline String_t** get_address_of_kProductNameAppleConsumable_17() { return &___kProductNameAppleConsumable_17; }
	inline void set_kProductNameAppleConsumable_17(String_t* value)
	{
		___kProductNameAppleConsumable_17 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameAppleConsumable_17, value);
	}

	inline static int32_t get_offset_of_kProductNameAppleNonConsumable_18() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameAppleNonConsumable_18)); }
	inline String_t* get_kProductNameAppleNonConsumable_18() const { return ___kProductNameAppleNonConsumable_18; }
	inline String_t** get_address_of_kProductNameAppleNonConsumable_18() { return &___kProductNameAppleNonConsumable_18; }
	inline void set_kProductNameAppleNonConsumable_18(String_t* value)
	{
		___kProductNameAppleNonConsumable_18 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameAppleNonConsumable_18, value);
	}

	inline static int32_t get_offset_of_kProductNameAppleSubscription_19() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameAppleSubscription_19)); }
	inline String_t* get_kProductNameAppleSubscription_19() const { return ___kProductNameAppleSubscription_19; }
	inline String_t** get_address_of_kProductNameAppleSubscription_19() { return &___kProductNameAppleSubscription_19; }
	inline void set_kProductNameAppleSubscription_19(String_t* value)
	{
		___kProductNameAppleSubscription_19 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameAppleSubscription_19, value);
	}

	inline static int32_t get_offset_of_kProductNameAppleSubscription24_20() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameAppleSubscription24_20)); }
	inline String_t* get_kProductNameAppleSubscription24_20() const { return ___kProductNameAppleSubscription24_20; }
	inline String_t** get_address_of_kProductNameAppleSubscription24_20() { return &___kProductNameAppleSubscription24_20; }
	inline void set_kProductNameAppleSubscription24_20(String_t* value)
	{
		___kProductNameAppleSubscription24_20 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameAppleSubscription24_20, value);
	}

	inline static int32_t get_offset_of_kProductNameAppleSubscription36_21() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameAppleSubscription36_21)); }
	inline String_t* get_kProductNameAppleSubscription36_21() const { return ___kProductNameAppleSubscription36_21; }
	inline String_t** get_address_of_kProductNameAppleSubscription36_21() { return &___kProductNameAppleSubscription36_21; }
	inline void set_kProductNameAppleSubscription36_21(String_t* value)
	{
		___kProductNameAppleSubscription36_21 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameAppleSubscription36_21, value);
	}

	inline static int32_t get_offset_of_kProductNameGooglePlayConsumable_22() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameGooglePlayConsumable_22)); }
	inline String_t* get_kProductNameGooglePlayConsumable_22() const { return ___kProductNameGooglePlayConsumable_22; }
	inline String_t** get_address_of_kProductNameGooglePlayConsumable_22() { return &___kProductNameGooglePlayConsumable_22; }
	inline void set_kProductNameGooglePlayConsumable_22(String_t* value)
	{
		___kProductNameGooglePlayConsumable_22 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameGooglePlayConsumable_22, value);
	}

	inline static int32_t get_offset_of_kProductNameGooglePlayNonConsumable_23() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameGooglePlayNonConsumable_23)); }
	inline String_t* get_kProductNameGooglePlayNonConsumable_23() const { return ___kProductNameGooglePlayNonConsumable_23; }
	inline String_t** get_address_of_kProductNameGooglePlayNonConsumable_23() { return &___kProductNameGooglePlayNonConsumable_23; }
	inline void set_kProductNameGooglePlayNonConsumable_23(String_t* value)
	{
		___kProductNameGooglePlayNonConsumable_23 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameGooglePlayNonConsumable_23, value);
	}

	inline static int32_t get_offset_of_kProductNameGooglePlaySubscription_24() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameGooglePlaySubscription_24)); }
	inline String_t* get_kProductNameGooglePlaySubscription_24() const { return ___kProductNameGooglePlaySubscription_24; }
	inline String_t** get_address_of_kProductNameGooglePlaySubscription_24() { return &___kProductNameGooglePlaySubscription_24; }
	inline void set_kProductNameGooglePlaySubscription_24(String_t* value)
	{
		___kProductNameGooglePlaySubscription_24 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameGooglePlaySubscription_24, value);
	}

	inline static int32_t get_offset_of_kProductNameGooglePlaySubscription24_25() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameGooglePlaySubscription24_25)); }
	inline String_t* get_kProductNameGooglePlaySubscription24_25() const { return ___kProductNameGooglePlaySubscription24_25; }
	inline String_t** get_address_of_kProductNameGooglePlaySubscription24_25() { return &___kProductNameGooglePlaySubscription24_25; }
	inline void set_kProductNameGooglePlaySubscription24_25(String_t* value)
	{
		___kProductNameGooglePlaySubscription24_25 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameGooglePlaySubscription24_25, value);
	}

	inline static int32_t get_offset_of_kProductNameGooglePlaySubscription36_26() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___kProductNameGooglePlaySubscription36_26)); }
	inline String_t* get_kProductNameGooglePlaySubscription36_26() const { return ___kProductNameGooglePlaySubscription36_26; }
	inline String_t** get_address_of_kProductNameGooglePlaySubscription36_26() { return &___kProductNameGooglePlaySubscription36_26; }
	inline void set_kProductNameGooglePlaySubscription36_26(String_t* value)
	{
		___kProductNameGooglePlaySubscription36_26 = value;
		Il2CppCodeGenWriteBarrier(&___kProductNameGooglePlaySubscription36_26, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2A_44() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___U3CU3Ef__amU24cache2A_44)); }
	inline Action_1_t3627374100 * get_U3CU3Ef__amU24cache2A_44() const { return ___U3CU3Ef__amU24cache2A_44; }
	inline Action_1_t3627374100 ** get_address_of_U3CU3Ef__amU24cache2A_44() { return &___U3CU3Ef__amU24cache2A_44; }
	inline void set_U3CU3Ef__amU24cache2A_44(Action_1_t3627374100 * value)
	{
		___U3CU3Ef__amU24cache2A_44 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2A_44, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_45() { return static_cast<int32_t>(offsetof(Purchaser_t1507804203_StaticFields, ___U3CU3Ef__switchU24map0_45)); }
	inline Dictionary_2_t3986656710 * get_U3CU3Ef__switchU24map0_45() const { return ___U3CU3Ef__switchU24map0_45; }
	inline Dictionary_2_t3986656710 ** get_address_of_U3CU3Ef__switchU24map0_45() { return &___U3CU3Ef__switchU24map0_45; }
	inline void set_U3CU3Ef__switchU24map0_45(Dictionary_2_t3986656710 * value)
	{
		___U3CU3Ef__switchU24map0_45 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_45, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
