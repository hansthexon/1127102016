﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Expressions.MemberAssignment
struct MemberAssignment_t3987033531;
// System.Linq.Expressions.Expression
struct Expression_t114864668;

#include "codegen/il2cpp-codegen.h"

// System.Linq.Expressions.Expression System.Linq.Expressions.MemberAssignment::get_Expression()
extern "C"  Expression_t114864668 * MemberAssignment_get_Expression_m2780742900 (MemberAssignment_t3987033531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
