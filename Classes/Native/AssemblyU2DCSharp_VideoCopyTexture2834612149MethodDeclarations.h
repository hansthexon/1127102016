﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VideoCopyTexture
struct VideoCopyTexture_t2834612149;

#include "codegen/il2cpp-codegen.h"

// System.Void VideoCopyTexture::.ctor()
extern "C"  void VideoCopyTexture__ctor_m623981352 (VideoCopyTexture_t2834612149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoCopyTexture::Start()
extern "C"  void VideoCopyTexture_Start_m3552642144 (VideoCopyTexture_t2834612149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VideoCopyTexture::Update()
extern "C"  void VideoCopyTexture_Update_m2396481633 (VideoCopyTexture_t2834612149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
