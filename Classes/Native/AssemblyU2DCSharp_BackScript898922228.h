﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Purchaser
struct Purchaser_t1507804203;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BackScript
struct  BackScript_t898922228  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean BackScript::canopenparent
	bool ___canopenparent_2;
	// UnityEngine.UI.Text BackScript::day
	Text_t356221433 * ___day_3;
	// UnityEngine.UI.Text BackScript::month
	Text_t356221433 * ___month_4;
	// UnityEngine.UI.Text BackScript::year
	Text_t356221433 * ___year_5;
	// UnityEngine.UI.Text BackScript::child
	Text_t356221433 * ___child_6;
	// UnityEngine.UI.Text BackScript::pname
	Text_t356221433 * ___pname_7;
	// UnityEngine.UI.Text BackScript::addr
	Text_t356221433 * ___addr_8;
	// UnityEngine.UI.Text BackScript::emailaddress
	Text_t356221433 * ___emailaddress_9;
	// UnityEngine.GameObject BackScript::ParentCirner
	GameObject_t1756533147 * ___ParentCirner_10;
	// UnityEngine.GameObject BackScript::Slidec
	GameObject_t1756533147 * ___Slidec_11;
	// UnityEngine.GameObject BackScript::SldieE
	GameObject_t1756533147 * ___SldieE_12;
	// UnityEngine.GameObject BackScript::threemenu
	GameObject_t1756533147 * ___threemenu_13;
	// UnityEngine.GameObject BackScript::membershipdetails
	GameObject_t1756533147 * ___membershipdetails_14;
	// UnityEngine.GameObject BackScript::purch
	GameObject_t1756533147 * ___purch_15;
	// UnityEngine.GameObject BackScript::togg
	GameObject_t1756533147 * ___togg_16;
	// Purchaser BackScript::pur
	Purchaser_t1507804203 * ___pur_17;

public:
	inline static int32_t get_offset_of_canopenparent_2() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___canopenparent_2)); }
	inline bool get_canopenparent_2() const { return ___canopenparent_2; }
	inline bool* get_address_of_canopenparent_2() { return &___canopenparent_2; }
	inline void set_canopenparent_2(bool value)
	{
		___canopenparent_2 = value;
	}

	inline static int32_t get_offset_of_day_3() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___day_3)); }
	inline Text_t356221433 * get_day_3() const { return ___day_3; }
	inline Text_t356221433 ** get_address_of_day_3() { return &___day_3; }
	inline void set_day_3(Text_t356221433 * value)
	{
		___day_3 = value;
		Il2CppCodeGenWriteBarrier(&___day_3, value);
	}

	inline static int32_t get_offset_of_month_4() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___month_4)); }
	inline Text_t356221433 * get_month_4() const { return ___month_4; }
	inline Text_t356221433 ** get_address_of_month_4() { return &___month_4; }
	inline void set_month_4(Text_t356221433 * value)
	{
		___month_4 = value;
		Il2CppCodeGenWriteBarrier(&___month_4, value);
	}

	inline static int32_t get_offset_of_year_5() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___year_5)); }
	inline Text_t356221433 * get_year_5() const { return ___year_5; }
	inline Text_t356221433 ** get_address_of_year_5() { return &___year_5; }
	inline void set_year_5(Text_t356221433 * value)
	{
		___year_5 = value;
		Il2CppCodeGenWriteBarrier(&___year_5, value);
	}

	inline static int32_t get_offset_of_child_6() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___child_6)); }
	inline Text_t356221433 * get_child_6() const { return ___child_6; }
	inline Text_t356221433 ** get_address_of_child_6() { return &___child_6; }
	inline void set_child_6(Text_t356221433 * value)
	{
		___child_6 = value;
		Il2CppCodeGenWriteBarrier(&___child_6, value);
	}

	inline static int32_t get_offset_of_pname_7() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___pname_7)); }
	inline Text_t356221433 * get_pname_7() const { return ___pname_7; }
	inline Text_t356221433 ** get_address_of_pname_7() { return &___pname_7; }
	inline void set_pname_7(Text_t356221433 * value)
	{
		___pname_7 = value;
		Il2CppCodeGenWriteBarrier(&___pname_7, value);
	}

	inline static int32_t get_offset_of_addr_8() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___addr_8)); }
	inline Text_t356221433 * get_addr_8() const { return ___addr_8; }
	inline Text_t356221433 ** get_address_of_addr_8() { return &___addr_8; }
	inline void set_addr_8(Text_t356221433 * value)
	{
		___addr_8 = value;
		Il2CppCodeGenWriteBarrier(&___addr_8, value);
	}

	inline static int32_t get_offset_of_emailaddress_9() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___emailaddress_9)); }
	inline Text_t356221433 * get_emailaddress_9() const { return ___emailaddress_9; }
	inline Text_t356221433 ** get_address_of_emailaddress_9() { return &___emailaddress_9; }
	inline void set_emailaddress_9(Text_t356221433 * value)
	{
		___emailaddress_9 = value;
		Il2CppCodeGenWriteBarrier(&___emailaddress_9, value);
	}

	inline static int32_t get_offset_of_ParentCirner_10() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___ParentCirner_10)); }
	inline GameObject_t1756533147 * get_ParentCirner_10() const { return ___ParentCirner_10; }
	inline GameObject_t1756533147 ** get_address_of_ParentCirner_10() { return &___ParentCirner_10; }
	inline void set_ParentCirner_10(GameObject_t1756533147 * value)
	{
		___ParentCirner_10 = value;
		Il2CppCodeGenWriteBarrier(&___ParentCirner_10, value);
	}

	inline static int32_t get_offset_of_Slidec_11() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___Slidec_11)); }
	inline GameObject_t1756533147 * get_Slidec_11() const { return ___Slidec_11; }
	inline GameObject_t1756533147 ** get_address_of_Slidec_11() { return &___Slidec_11; }
	inline void set_Slidec_11(GameObject_t1756533147 * value)
	{
		___Slidec_11 = value;
		Il2CppCodeGenWriteBarrier(&___Slidec_11, value);
	}

	inline static int32_t get_offset_of_SldieE_12() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___SldieE_12)); }
	inline GameObject_t1756533147 * get_SldieE_12() const { return ___SldieE_12; }
	inline GameObject_t1756533147 ** get_address_of_SldieE_12() { return &___SldieE_12; }
	inline void set_SldieE_12(GameObject_t1756533147 * value)
	{
		___SldieE_12 = value;
		Il2CppCodeGenWriteBarrier(&___SldieE_12, value);
	}

	inline static int32_t get_offset_of_threemenu_13() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___threemenu_13)); }
	inline GameObject_t1756533147 * get_threemenu_13() const { return ___threemenu_13; }
	inline GameObject_t1756533147 ** get_address_of_threemenu_13() { return &___threemenu_13; }
	inline void set_threemenu_13(GameObject_t1756533147 * value)
	{
		___threemenu_13 = value;
		Il2CppCodeGenWriteBarrier(&___threemenu_13, value);
	}

	inline static int32_t get_offset_of_membershipdetails_14() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___membershipdetails_14)); }
	inline GameObject_t1756533147 * get_membershipdetails_14() const { return ___membershipdetails_14; }
	inline GameObject_t1756533147 ** get_address_of_membershipdetails_14() { return &___membershipdetails_14; }
	inline void set_membershipdetails_14(GameObject_t1756533147 * value)
	{
		___membershipdetails_14 = value;
		Il2CppCodeGenWriteBarrier(&___membershipdetails_14, value);
	}

	inline static int32_t get_offset_of_purch_15() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___purch_15)); }
	inline GameObject_t1756533147 * get_purch_15() const { return ___purch_15; }
	inline GameObject_t1756533147 ** get_address_of_purch_15() { return &___purch_15; }
	inline void set_purch_15(GameObject_t1756533147 * value)
	{
		___purch_15 = value;
		Il2CppCodeGenWriteBarrier(&___purch_15, value);
	}

	inline static int32_t get_offset_of_togg_16() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___togg_16)); }
	inline GameObject_t1756533147 * get_togg_16() const { return ___togg_16; }
	inline GameObject_t1756533147 ** get_address_of_togg_16() { return &___togg_16; }
	inline void set_togg_16(GameObject_t1756533147 * value)
	{
		___togg_16 = value;
		Il2CppCodeGenWriteBarrier(&___togg_16, value);
	}

	inline static int32_t get_offset_of_pur_17() { return static_cast<int32_t>(offsetof(BackScript_t898922228, ___pur_17)); }
	inline Purchaser_t1507804203 * get_pur_17() const { return ___pur_17; }
	inline Purchaser_t1507804203 ** get_address_of_pur_17() { return &___pur_17; }
	inline void set_pur_17(Purchaser_t1507804203 * value)
	{
		___pur_17 = value;
		Il2CppCodeGenWriteBarrier(&___pur_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
