﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.SurfaceAbstractBehaviour
struct SurfaceAbstractBehaviour_t2669615494;
// Vuforia.Surface
struct Surface_t2221641095;
// UnityEngine.MeshFilter
struct MeshFilter_t3026937449;
// UnityEngine.MeshCollider
struct MeshCollider_t2718867283;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MeshFilter3026937449.h"
#include "UnityEngine_UnityEngine_MeshCollider2718867283.h"

// Vuforia.Surface Vuforia.SurfaceAbstractBehaviour::get_Surface()
extern "C"  Il2CppObject * SurfaceAbstractBehaviour_get_Surface_m2886945944 (SurfaceAbstractBehaviour_t2669615494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::InternalUnregisterTrackable()
extern "C"  void SurfaceAbstractBehaviour_InternalUnregisterTrackable_m500321914 (SurfaceAbstractBehaviour_t2669615494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.InitializeSurface(Vuforia.Surface)
extern "C"  void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_InitializeSurface_m3267410962 (SurfaceAbstractBehaviour_t2669615494 * __this, Il2CppObject * ___surface0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.SetMeshFilterToUpdate(UnityEngine.MeshFilter)
extern "C"  void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshFilterToUpdate_m2480972345 (SurfaceAbstractBehaviour_t2669615494 * __this, MeshFilter_t3026937449 * ___meshFilterToUpdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshFilter Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.get_MeshFilterToUpdate()
extern "C"  MeshFilter_t3026937449 * SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshFilterToUpdate_m1203218325 (SurfaceAbstractBehaviour_t2669615494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.SetMeshColliderToUpdate(UnityEngine.MeshCollider)
extern "C"  void SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_SetMeshColliderToUpdate_m1490185749 (SurfaceAbstractBehaviour_t2669615494 * __this, MeshCollider_t2718867283 * ___meshColliderToUpdate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshCollider Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorSurfaceBehaviour.get_MeshColliderToUpdate()
extern "C"  MeshCollider_t2718867283 * SurfaceAbstractBehaviour_Vuforia_IEditorSurfaceBehaviour_get_MeshColliderToUpdate_m2186157457 (SurfaceAbstractBehaviour_t2669615494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::.ctor()
extern "C"  void SurfaceAbstractBehaviour__ctor_m4051989004 (SurfaceAbstractBehaviour_t2669615494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_enabled()
extern "C"  bool SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_enabled_m3909875400 (SurfaceAbstractBehaviour_t2669615494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.set_enabled(System.Boolean)
extern "C"  void SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_set_enabled_m3123293125 (SurfaceAbstractBehaviour_t2669615494 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_transform()
extern "C"  Transform_t3275118058 * SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_transform_m3843180515 (SurfaceAbstractBehaviour_t2669615494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Vuforia.SurfaceAbstractBehaviour::Vuforia.IEditorTrackableBehaviour.get_gameObject()
extern "C"  GameObject_t1756533147 * SurfaceAbstractBehaviour_Vuforia_IEditorTrackableBehaviour_get_gameObject_m3247009779 (SurfaceAbstractBehaviour_t2669615494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
