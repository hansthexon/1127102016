﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct List_1_t43281120;
// System.Collections.Generic.IEnumerable`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IEnumerable_1_t966287033;
// System.Collections.Generic.IEnumerator`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct IEnumerator_1_t2444651111;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct ICollection_1_t1626235293;
// System.Collections.ObjectModel.ReadOnlyCollection`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct ReadOnlyCollection_1_t859945680;
// SQLite4Unity3d.SQLiteConnection/IndexedColumn[]
struct IndexedColumnU5BU5D_t4219656765;
// System.Predicate`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct Predicate_1_t3412097399;
// System.Comparison`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>
struct Comparison_1_t1935898839;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_SQLite4Unity3d_SQLiteConnection_I674159988.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3872978090.h"

// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor()
extern "C"  void List_1__ctor_m2177611879_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1__ctor_m2177611879(__this, method) ((  void (*) (List_1_t43281120 *, const MethodInfo*))List_1__ctor_m2177611879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3989671355_gshared (List_1_t43281120 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3989671355(__this, ___collection0, method) ((  void (*) (List_1_t43281120 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3989671355_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m4204710169_gshared (List_1_t43281120 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m4204710169(__this, ___capacity0, method) ((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))List_1__ctor_m4204710169_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::.cctor()
extern "C"  void List_1__cctor_m1995328883_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1995328883(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1995328883_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2560244226_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2560244226(__this, method) ((  Il2CppObject* (*) (List_1_t43281120 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2560244226_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m266102924_gshared (List_1_t43281120 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m266102924(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t43281120 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m266102924_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2834930177_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2834930177(__this, method) ((  Il2CppObject * (*) (List_1_t43281120 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m2834930177_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1439241948_gshared (List_1_t43281120 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1439241948(__this, ___item0, method) ((  int32_t (*) (List_1_t43281120 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1439241948_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m3729349938_gshared (List_1_t43281120 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m3729349938(__this, ___item0, method) ((  bool (*) (List_1_t43281120 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m3729349938_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m3723295914_gshared (List_1_t43281120 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m3723295914(__this, ___item0, method) ((  int32_t (*) (List_1_t43281120 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3723295914_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m2594309881_gshared (List_1_t43281120 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m2594309881(__this, ___index0, ___item1, method) ((  void (*) (List_1_t43281120 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2594309881_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3381705393_gshared (List_1_t43281120 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3381705393(__this, ___item0, method) ((  void (*) (List_1_t43281120 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3381705393_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3480933197_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3480933197(__this, method) ((  bool (*) (List_1_t43281120 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3480933197_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1768514220_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1768514220(__this, method) ((  bool (*) (List_1_t43281120 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1768514220_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1464552284_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1464552284(__this, method) ((  Il2CppObject * (*) (List_1_t43281120 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1464552284_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m1070203109_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m1070203109(__this, method) ((  bool (*) (List_1_t43281120 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m1070203109_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m544361872_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m544361872(__this, method) ((  bool (*) (List_1_t43281120 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m544361872_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2588552949_gshared (List_1_t43281120 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2588552949(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t43281120 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2588552949_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m742815526_gshared (List_1_t43281120 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m742815526(__this, ___index0, ___value1, method) ((  void (*) (List_1_t43281120 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m742815526_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Add(T)
extern "C"  void List_1_Add_m1039228243_gshared (List_1_t43281120 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method);
#define List_1_Add_m1039228243(__this, ___item0, method) ((  void (*) (List_1_t43281120 *, IndexedColumn_t674159988 , const MethodInfo*))List_1_Add_m1039228243_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m529225910_gshared (List_1_t43281120 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m529225910(__this, ___newCount0, method) ((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m529225910_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m4021244222_gshared (List_1_t43281120 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m4021244222(__this, ___collection0, method) ((  void (*) (List_1_t43281120 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m4021244222_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m543446830_gshared (List_1_t43281120 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m543446830(__this, ___enumerable0, method) ((  void (*) (List_1_t43281120 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m543446830_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m173034401_gshared (List_1_t43281120 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m173034401(__this, ___collection0, method) ((  void (*) (List_1_t43281120 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m173034401_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t859945680 * List_1_AsReadOnly_m1881111912_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1881111912(__this, method) ((  ReadOnlyCollection_1_t859945680 * (*) (List_1_t43281120 *, const MethodInfo*))List_1_AsReadOnly_m1881111912_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Clear()
extern "C"  void List_1_Clear_m2591881969_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_Clear_m2591881969(__this, method) ((  void (*) (List_1_t43281120 *, const MethodInfo*))List_1_Clear_m2591881969_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Contains(T)
extern "C"  bool List_1_Contains_m645061595_gshared (List_1_t43281120 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method);
#define List_1_Contains_m645061595(__this, ___item0, method) ((  bool (*) (List_1_t43281120 *, IndexedColumn_t674159988 , const MethodInfo*))List_1_Contains_m645061595_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CopyTo(T[])
extern "C"  void List_1_CopyTo_m38513116_gshared (List_1_t43281120 * __this, IndexedColumnU5BU5D_t4219656765* ___array0, const MethodInfo* method);
#define List_1_CopyTo_m38513116(__this, ___array0, method) ((  void (*) (List_1_t43281120 *, IndexedColumnU5BU5D_t4219656765*, const MethodInfo*))List_1_CopyTo_m38513116_gshared)(__this, ___array0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2904850141_gshared (List_1_t43281120 * __this, IndexedColumnU5BU5D_t4219656765* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2904850141(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t43281120 *, IndexedColumnU5BU5D_t4219656765*, int32_t, const MethodInfo*))List_1_CopyTo_m2904850141_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Find(System.Predicate`1<T>)
extern "C"  IndexedColumn_t674159988  List_1_Find_m1987600831_gshared (List_1_t43281120 * __this, Predicate_1_t3412097399 * ___match0, const MethodInfo* method);
#define List_1_Find_m1987600831(__this, ___match0, method) ((  IndexedColumn_t674159988  (*) (List_1_t43281120 *, Predicate_1_t3412097399 *, const MethodInfo*))List_1_Find_m1987600831_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m688679932_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3412097399 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m688679932(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3412097399 *, const MethodInfo*))List_1_CheckMatch_m688679932_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3335490459_gshared (List_1_t43281120 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3412097399 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3335490459(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t43281120 *, int32_t, int32_t, Predicate_1_t3412097399 *, const MethodInfo*))List_1_GetIndex_m3335490459_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::GetEnumerator()
extern "C"  Enumerator_t3872978090  List_1_GetEnumerator_m3398976172_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3398976172(__this, method) ((  Enumerator_t3872978090  (*) (List_1_t43281120 *, const MethodInfo*))List_1_GetEnumerator_m3398976172_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3435406113_gshared (List_1_t43281120 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3435406113(__this, ___item0, method) ((  int32_t (*) (List_1_t43281120 *, IndexedColumn_t674159988 , const MethodInfo*))List_1_IndexOf_m3435406113_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m3451272920_gshared (List_1_t43281120 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m3451272920(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t43281120 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3451272920_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m398277233_gshared (List_1_t43281120 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m398277233(__this, ___index0, method) ((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))List_1_CheckIndex_m398277233_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m448615326_gshared (List_1_t43281120 * __this, int32_t ___index0, IndexedColumn_t674159988  ___item1, const MethodInfo* method);
#define List_1_Insert_m448615326(__this, ___index0, ___item1, method) ((  void (*) (List_1_t43281120 *, int32_t, IndexedColumn_t674159988 , const MethodInfo*))List_1_Insert_m448615326_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2966359231_gshared (List_1_t43281120 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2966359231(__this, ___collection0, method) ((  void (*) (List_1_t43281120 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2966359231_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Remove(T)
extern "C"  bool List_1_Remove_m2030530920_gshared (List_1_t43281120 * __this, IndexedColumn_t674159988  ___item0, const MethodInfo* method);
#define List_1_Remove_m2030530920(__this, ___item0, method) ((  bool (*) (List_1_t43281120 *, IndexedColumn_t674159988 , const MethodInfo*))List_1_Remove_m2030530920_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m37964820_gshared (List_1_t43281120 * __this, Predicate_1_t3412097399 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m37964820(__this, ___match0, method) ((  int32_t (*) (List_1_t43281120 *, Predicate_1_t3412097399 *, const MethodInfo*))List_1_RemoveAll_m37964820_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m4120205290_gshared (List_1_t43281120 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m4120205290(__this, ___index0, method) ((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))List_1_RemoveAt_m4120205290_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Reverse()
extern "C"  void List_1_Reverse_m2520075922_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_Reverse_m2520075922(__this, method) ((  void (*) (List_1_t43281120 *, const MethodInfo*))List_1_Reverse_m2520075922_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Sort()
extern "C"  void List_1_Sort_m1662150106_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_Sort_m1662150106(__this, method) ((  void (*) (List_1_t43281120 *, const MethodInfo*))List_1_Sort_m1662150106_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m1527224475_gshared (List_1_t43281120 * __this, Comparison_1_t1935898839 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m1527224475(__this, ___comparison0, method) ((  void (*) (List_1_t43281120 *, Comparison_1_t1935898839 *, const MethodInfo*))List_1_Sort_m1527224475_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::ToArray()
extern "C"  IndexedColumnU5BU5D_t4219656765* List_1_ToArray_m1726840235_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_ToArray_m1726840235(__this, method) ((  IndexedColumnU5BU5D_t4219656765* (*) (List_1_t43281120 *, const MethodInfo*))List_1_ToArray_m1726840235_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2532032509_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2532032509(__this, method) ((  void (*) (List_1_t43281120 *, const MethodInfo*))List_1_TrimExcess_m2532032509_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2338338523_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2338338523(__this, method) ((  int32_t (*) (List_1_t43281120 *, const MethodInfo*))List_1_get_Capacity_m2338338523_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3871896830_gshared (List_1_t43281120 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3871896830(__this, ___value0, method) ((  void (*) (List_1_t43281120 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3871896830_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Count()
extern "C"  int32_t List_1_get_Count_m1991288812_gshared (List_1_t43281120 * __this, const MethodInfo* method);
#define List_1_get_Count_m1991288812(__this, method) ((  int32_t (*) (List_1_t43281120 *, const MethodInfo*))List_1_get_Count_m1991288812_gshared)(__this, method)
// T System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::get_Item(System.Int32)
extern "C"  IndexedColumn_t674159988  List_1_get_Item_m537033892_gshared (List_1_t43281120 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m537033892(__this, ___index0, method) ((  IndexedColumn_t674159988  (*) (List_1_t43281120 *, int32_t, const MethodInfo*))List_1_get_Item_m537033892_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<SQLite4Unity3d.SQLiteConnection/IndexedColumn>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m666324973_gshared (List_1_t43281120 * __this, int32_t ___index0, IndexedColumn_t674159988  ___value1, const MethodInfo* method);
#define List_1_set_Item_m666324973(__this, ___index0, ___value1, method) ((  void (*) (List_1_t43281120 *, int32_t, IndexedColumn_t674159988 , const MethodInfo*))List_1_set_Item_m666324973_gshared)(__this, ___index0, ___value1, method)
