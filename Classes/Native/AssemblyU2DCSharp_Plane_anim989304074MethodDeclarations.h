﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Plane_anim
struct Plane_anim_t989304074;

#include "codegen/il2cpp-codegen.h"

// System.Void Plane_anim::.ctor()
extern "C"  void Plane_anim__ctor_m2251425589 (Plane_anim_t989304074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plane_anim::Start()
extern "C"  void Plane_anim_Start_m2529749877 (Plane_anim_t989304074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plane_anim::Update()
extern "C"  void Plane_anim_Update_m2651298220 (Plane_anim_t989304074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Plane_anim::planeAnimate()
extern "C"  void Plane_anim_planeAnimate_m2483925036 (Plane_anim_t989304074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
