﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.IgnoreAttribute
struct IgnoreAttribute_t2062332784;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.IgnoreAttribute::.ctor()
extern "C"  void IgnoreAttribute__ctor_m3534419511 (IgnoreAttribute_t2062332784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
