﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TestScript/<Test>c__Iterator11
struct U3CTestU3Ec__Iterator11_t1507409797;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void TestScript/<Test>c__Iterator11::.ctor()
extern "C"  void U3CTestU3Ec__Iterator11__ctor_m1511787248 (U3CTestU3Ec__Iterator11_t1507409797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TestScript/<Test>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CTestU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4162040056 (U3CTestU3Ec__Iterator11_t1507409797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TestScript/<Test>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CTestU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m3635843248 (U3CTestU3Ec__Iterator11_t1507409797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TestScript/<Test>c__Iterator11::MoveNext()
extern "C"  bool U3CTestU3Ec__Iterator11_MoveNext_m3499820636 (U3CTestU3Ec__Iterator11_t1507409797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestScript/<Test>c__Iterator11::Dispose()
extern "C"  void U3CTestU3Ec__Iterator11_Dispose_m3888324455 (U3CTestU3Ec__Iterator11_t1507409797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TestScript/<Test>c__Iterator11::Reset()
extern "C"  void U3CTestU3Ec__Iterator11_Reset_m3197721633 (U3CTestU3Ec__Iterator11_t1507409797 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
