﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Purchasing.Security.X509Cert
struct X509Cert_t481809278;
// LipingShare.LCLib.Asn1Processor.Asn1Node
struct Asn1Node_t1770761751;
// System.Byte[]
struct ByteU5BU5D_t3397334013;
// System.String
struct String_t;
// UnityEngine.Purchasing.Security.RSAKey
struct RSAKey_t446464277;
// UnityEngine.Purchasing.Security.DistinguishedName
struct DistinguishedName_t1881593989;

#include "codegen/il2cpp-codegen.h"
#include "Security_LipingShare_LCLib_Asn1Processor_Asn1Node1770761751.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_DateTime693205669.h"
#include "Security_UnityEngine_Purchasing_Security_RSAKey446464277.h"
#include "Security_UnityEngine_Purchasing_Security_Distingui1881593989.h"
#include "Security_UnityEngine_Purchasing_Security_X509Cert481809278.h"

// System.Void UnityEngine.Purchasing.Security.X509Cert::.ctor(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void X509Cert__ctor_m2063768312 (X509Cert_t481809278 * __this, Asn1Node_t1770761751 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.X509Cert::.ctor(System.Byte[])
extern "C"  void X509Cert__ctor_m3040169225 (X509Cert_t481809278 * __this, ByteU5BU5D_t3397334013* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Purchasing.Security.X509Cert::get_SerialNumber()
extern "C"  String_t* X509Cert_get_SerialNumber_m3398499893 (X509Cert_t481809278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_SerialNumber(System.String)
extern "C"  void X509Cert_set_SerialNumber_m3397980220 (X509Cert_t481809278 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.Purchasing.Security.X509Cert::get_ValidAfter()
extern "C"  DateTime_t693205669  X509Cert_get_ValidAfter_m1286975886 (X509Cert_t481809278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_ValidAfter(System.DateTime)
extern "C"  void X509Cert_set_ValidAfter_m451723427 (X509Cert_t481809278 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.Purchasing.Security.X509Cert::get_ValidBefore()
extern "C"  DateTime_t693205669  X509Cert_get_ValidBefore_m641622829 (X509Cert_t481809278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_ValidBefore(System.DateTime)
extern "C"  void X509Cert_set_ValidBefore_m2131887164 (X509Cert_t481809278 * __this, DateTime_t693205669  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.RSAKey UnityEngine.Purchasing.Security.X509Cert::get_PubKey()
extern "C"  RSAKey_t446464277 * X509Cert_get_PubKey_m3319710642 (X509Cert_t481809278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_PubKey(UnityEngine.Purchasing.Security.RSAKey)
extern "C"  void X509Cert_set_PubKey_m2350741731 (X509Cert_t481809278 * __this, RSAKey_t446464277 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_SelfSigned(System.Boolean)
extern "C"  void X509Cert_set_SelfSigned_m2342926440 (X509Cert_t481809278 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.DistinguishedName UnityEngine.Purchasing.Security.X509Cert::get_Subject()
extern "C"  DistinguishedName_t1881593989 * X509Cert_get_Subject_m640225680 (X509Cert_t481809278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_Subject(UnityEngine.Purchasing.Security.DistinguishedName)
extern "C"  void X509Cert_set_Subject_m3399824887 (X509Cert_t481809278 * __this, DistinguishedName_t1881593989 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Purchasing.Security.DistinguishedName UnityEngine.Purchasing.Security.X509Cert::get_Issuer()
extern "C"  DistinguishedName_t1881593989 * X509Cert_get_Issuer_m3153344599 (X509Cert_t481809278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_Issuer(UnityEngine.Purchasing.Security.DistinguishedName)
extern "C"  void X509Cert_set_Issuer_m1817635190 (X509Cert_t481809278 * __this, DistinguishedName_t1881593989 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LipingShare.LCLib.Asn1Processor.Asn1Node UnityEngine.Purchasing.Security.X509Cert::get_Signature()
extern "C"  Asn1Node_t1770761751 * X509Cert_get_Signature_m3266380160 (X509Cert_t481809278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.X509Cert::set_Signature(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void X509Cert_set_Signature_m1044505155 (X509Cert_t481809278 * __this, Asn1Node_t1770761751 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.Security.X509Cert::CheckCertTime(System.DateTime)
extern "C"  bool X509Cert_CheckCertTime_m317868091 (X509Cert_t481809278 * __this, DateTime_t693205669  ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Purchasing.Security.X509Cert::CheckSignature(UnityEngine.Purchasing.Security.X509Cert)
extern "C"  bool X509Cert_CheckSignature_m4098105751 (X509Cert_t481809278 * __this, X509Cert_t481809278 * ___signer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Purchasing.Security.X509Cert::ParseNode(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  void X509Cert_ParseNode_m3118026731 (X509Cert_t481809278 * __this, Asn1Node_t1770761751 * ___root0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime UnityEngine.Purchasing.Security.X509Cert::ParseTime(LipingShare.LCLib.Asn1Processor.Asn1Node)
extern "C"  DateTime_t693205669  X509Cert_ParseTime_m1888822247 (X509Cert_t481809278 * __this, Asn1Node_t1770761751 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
