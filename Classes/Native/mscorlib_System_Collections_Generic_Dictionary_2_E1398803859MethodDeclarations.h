﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3601534125MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2406222793(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1398803859 *, Dictionary_2_t78779157 *, const MethodInfo*))Enumerator__ctor_m3742107451_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2608981564(__this, method) ((  Il2CppObject * (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m229223308_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3723326980(__this, method) ((  void (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3225937576_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1637393947(__this, method) ((  DictionaryEntry_t3048875398  (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m221119093_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3450363022(__this, method) ((  Il2CppObject * (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m467957770_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1861922304(__this, method) ((  Il2CppObject * (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2325383168_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::MoveNext()
#define Enumerator_MoveNext_m3329841288(__this, method) ((  bool (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_MoveNext_m3349738440_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::get_Current()
#define Enumerator_get_Current_m1185103244(__this, method) ((  KeyValuePair_2_t2131091675  (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_get_Current_m25299632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m3877259405(__this, method) ((  Type_t * (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_get_CurrentKey_m3839846791_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m4138014637(__this, method) ((  FactoryFunc_t2436388556 * (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_get_CurrentValue_m402763047_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::Reset()
#define Enumerator_Reset_m3445284367(__this, method) ((  void (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_Reset_m3129803197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::VerifyState()
#define Enumerator_VerifyState_m538313040(__this, method) ((  void (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_VerifyState_m262343092_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m3061724436(__this, method) ((  void (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_VerifyCurrent_m1702320752_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Type,LitJson.FactoryFunc>::Dispose()
#define Enumerator_Dispose_m4023389113(__this, method) ((  void (*) (Enumerator_t1398803859 *, const MethodInfo*))Enumerator_Dispose_m1905011127_gshared)(__this, method)
