﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Vuforia.VuforiaManagerImpl/<>c__DisplayClass3
struct U3CU3Ec__DisplayClass3_t2337328204;

#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl1947527974.h"

// System.Void Vuforia.VuforiaManagerImpl/<>c__DisplayClass3::.ctor()
extern "C"  void U3CU3Ec__DisplayClass3__ctor_m3524984730 (U3CU3Ec__DisplayClass3_t2337328204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Vuforia.VuforiaManagerImpl/<>c__DisplayClass3::<UpdateTrackers>b__1(Vuforia.VuforiaManagerImpl/TrackableResultData)
extern "C"  bool U3CU3Ec__DisplayClass3_U3CUpdateTrackersU3Eb__1_m2298443389 (U3CU3Ec__DisplayClass3_t2337328204 * __this, TrackableResultData_t1947527974  ___tr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
