﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SQLite4Unity3d.MaxLengthAttribute
struct MaxLengthAttribute_t3545664484;

#include "codegen/il2cpp-codegen.h"

// System.Void SQLite4Unity3d.MaxLengthAttribute::.ctor(System.Int32)
extern "C"  void MaxLengthAttribute__ctor_m1460950052 (MaxLengthAttribute_t3545664484 * __this, int32_t ___length0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SQLite4Unity3d.MaxLengthAttribute::get_Value()
extern "C"  int32_t MaxLengthAttribute_get_Value_m445734071 (MaxLengthAttribute_t3545664484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SQLite4Unity3d.MaxLengthAttribute::set_Value(System.Int32)
extern "C"  void MaxLengthAttribute_set_Value_m2744863698 (MaxLengthAttribute_t3545664484 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
